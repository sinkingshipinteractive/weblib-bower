/*! weblib-ss 0.0.1 */
// Declare the main Sinking Ship namespace object if not already declared
if ( typeof ( window.ss ) === String ( undefined ) ) {
	window.ss = new Object(); // jshint ignore:line
}// jshint ignore:line


/* global require, ss */



( function() {
    "use strict";

    /**
    * @param {String} type - The type of the event
    * @param {Object} data - Any object containing extra information related to the event
    * @param {Object} subclassReference [optional] - Reference to a subclass object that is
    *       invoking this as its base class constructor
    */
    function BaseEvent ( type, data, subclassReference ) {

        // private functions
        var _this = subclassReference || this;
        var _type;

        function _construct ( type, data ) {
            _type = type;

            _this.data = data;

            return _this;
        }

        /*
        * Get the type of this event
        * @return:String - String representing the type of this event
        */
        _this.getType = function() {
            return _type;
        };

        _this.toString = function() {
            return _type;
        };

        return _construct ( type, data, subclassReference);
    }

    ss.BaseEvent = BaseEvent;

} () );

/* global require, ss */




( function() {
    "use strict";

    /**
    *   Provides a base for classes that can have listeners registered and can dispatch events
    */
    function AbstractEventDispatcher ( reference ) {

        var _this = reference || this;
        var _listenerMap;

        function _construct () {
            _listenerMap = {};

            _this._listenerMap = _listenerMap;

            return _this;
        }

        _this.indexOfListener = function( listener, context, listenerSet ) {

            for( var i = 0; i < listenerSet.length; i++ ) {
                if ( listener === listenerSet [ i ].listener && context === listenerSet [ i ].context ) {
                    return i;
                }
            }

            return -1;
        };

        _this.addEventListener = function( event, listener, context ) {

            if ( !_this.hasEventListener ( event.toUpperCase (), listener, context ) ) {
                try {
                    var listenerSet = _listenerMap [ event.toUpperCase () ];
                    listenerSet.push ( { listener : listener, context : context } );
                } catch ( error ) {
                    _listenerMap [ event.toUpperCase () ] = [ { listener : listener, context : context } ];
                }
            }
        };

        _this.removeEventListener = function( event, listener, context ) {
            var listenerSet = _listenerMap [ event.toUpperCase () ];
            if ( listenerSet ) {
                var indexOf = _this.indexOfListener ( listener, context, listenerSet );
                if ( indexOf > -1 ) {
                    listenerSet.splice ( indexOf, 1 );
                }
            }
        };

        _this.hasEventListener = function( event, listener, context ) {

                // check if the
            var listenerSet = _listenerMap [ event.toUpperCase () ];
            if ( listenerSet ) {
                if ( _this.indexOfListener ( listener, context, listenerSet ) > -1 ) {
                    return true;
                }
            }

            return false;
        };

        _this.dispatchEvent = function( event, data ) {
            var listenerSet = _listenerMap [ String ( event ).toUpperCase () ];

            if ( listenerSet ) {

                // If this event already has at least one target set, append this object to the end of the target list
                if ( event.target instanceof Array ) {
                    event.target.push ( _this );
                } else {
                    event.target = [ _this ];
                }

                for( var i = 0; i < listenerSet.length; i++ ) {

                    // maintains backwards compatibility
                    if ( typeof ( event ) == "string" ) {
                        listenerSet [ i ].listener.apply ( listenerSet [ i ].context, [ _this, data ] );
                    } else {
                       // try {
                             listenerSet [ i ].listener.apply ( listenerSet [ i ].context, [ event ] );
                       // } catch ( e ) {
                       //     console.error ( e );
                        //    console.log ( String ( event ).toUpperCase () )
                       // }

                    }

                }
            }
        };

        _this.release = function() {

            for( var property in _listenerMap ) {
                delete _listenerMap [ property ];
    		}
        };

        return _construct ();
    }

    window.AbstractEventDispatcher = AbstractEventDispatcher; // jshint ignore:line
    ss.AbstractEventDispatcher = AbstractEventDispatcher;

} () );

/* global require, ss */




( function() {
	"use strict";

	/**
	*	Class AbstractObject
	*	Provides a base for all classes which includes:
	*		Event Dispatching
	*		Child / Parent relationships
	*		Component registration and removal
	*/
	function AbstractObject ( reference ) {

		var _this = ss.AbstractEventDispatcher ( reference || this ) ;
		var _components;
		var _children;
		var _id;

		function _construct  () {
			_children = [];
			_components = [];

			return _this;
		}

		/* _this.parent = function () {
			return _parent;
		}; */

		_this.id = function( id ) {
			return ( ss.isEmpty ( id ) ) ? id : _id = id;
		};

		_this.addChild = function( gameObject ) {

				// first remove the child from the previous parent ( if one is available )
			var gameObjectParent = gameObject.parent ();
			if ( !ss.isEmpty ( gameObjectParent ) ) {
				gameObjectParent.removeChild ( gameObject );
			}

				// just add the child to the
			_children.push ( gameObject );

				// return "this" for daisy chaining
			return _this;
		};

		_this.removeChild = function( gameObject ) {
				// if it's already a child take it out and push it to the end
			var indexOf = _children.indexOf ( gameObject );
			if ( indexOf > -1 ) {
				_children.splice ( indexOf, 1 );
			}

				// return "this" for daisy chaining
			return _this;
		};

		_this.getChildById = function( id ) {

				// loop through the children and see if any has
			for( var i = 0; i < _children; i++ ) {
				var child = _children [ i ];
				if ( child.id () == id ) {
					return child;
				}

			}

			return undefined;
	 	};

	 	_this.addComponent = function( component ) {

	 			// some error handling. Can't add multiple component of the same instance. LOL! then just add it
	 		_this.removeComponent ( component );
	 		_components.push ( component );

	 			// return "this" for daisy chaining
	 		return _this;
	 	};

	 	_this.removeComponent = function( component ) {
	 		var indexOf = _components.indexOf ( component );
	 		if ( indexOf > -1 ) {
				_children.splice ( indexOf, 1 );
			}

	 			// return "this" for daisy chaining
	 		return _this;
	 	};

	 	_this.getComponentById = function( id ) {
	 		for( var i = 0; i < _components.length; i++ ) {
	 			var child = _components [ i ];
	 			if ( child.id () == id ) {
	 				return _components [ i ];
	 			}

	 		}

	 		return undefined;
	 	};

	 	_this.edit = function( $container ) {
	 		for( var i = 0; i < _components.length; i++ ) {
	 			_components [ i ].edit ( $container );
	 		}

	 	};

	 	_this.begin = function() {
	 		for( var i = 0; i < _components.length; i++ ) {
	 			_components [ i ].begin ();
	 		}
	 	};

	 	_this.update = function() {
	 		for( var i = 0; i < _components.length; i++ ) {
	 			_components [ i ].update ( _children );
	 		}

	 	};

	 	_this.end = function() {
	 		for( var i = 0; i < _components.length; i++ ) {
	 			_components [ i ].end ();
	 		}

	 	};

		return _construct ();
	}

	ss.AbstractObject = AbstractObject;

} ());

/* global require, ss */



( function() {
    "use strict";

    /*
    * Check if a particular value is undefined
    * @return:[Boolean] - True if the value is undefined, false if it is defined
    */
    function isUndefined ( value ) {
        return ( typeof ( value ) === String ( undefined ) );
    }

    ss.isUndefined = isUndefined;

    /*
    * Check if a particular value is defined
    * @return:[Boolean] - True if the value has been defined, false if it is undefined
    */
    function isDefined ( value ) {
        return ( typeof ( value ) !== String ( undefined ) );
    }

    ss.isDefined = isDefined;

    function isEmpty ( value ) {
        return ( typeof ( value ) === String ( undefined ) || value === null || value === false );
    }

    ss.isEmpty = isEmpty;

    function setDefault ( value, defaultVal ) {
        return ( isUndefined ( value ) ) ? defaultVal : value;
    }

    ss.setDefault = setDefault;

    function isNumber ( value ) {
        return !isNaN ( parseFloat ( value ) ) && isFinite ( value );
    }

    ss.isNumber = isNumber;

    ss.getRange = function ( start, end ) {
        var t = Math.random ();
        return end * t + start * ( 1 - t );
    };

} () );

/* global require, ss, console */




(function() {
	"use strict";

	/*
	* Static Class SystemInfo
	*/

	ss.SystemInfo = {};

	ss.SystemInfo.isIOS = undefined;
	ss.SystemInfo.isIPhone = undefined;
	ss.SystemInfo.isIPod = undefined;
	ss.SystemInfo.isIPad = undefined;
	ss.SystemInfo.isNexus = undefined;
	ss.SystemInfo.isNabi = undefined;

	ss.SystemInfo.isIE = undefined;
	ss.SystemInfo.isSafari = undefined;
	ss.SystemInfo.isChrome = undefined;
	ss.SystemInfo.isMaxthon = undefined;
	ss.SystemInfo.isFirefox = undefined;
	ss.SystemInfo.isSilk = undefined;

	/*
	* Check if this program is being run on a mobile browser
	* @return:[Boolean] - True if this program is being run on a mobile browser, false otherwise
	*/
	ss.SystemInfo.isMobile = function() {
		if ( navigator.userAgent.match(/Android/i) ||
				navigator.userAgent.match(/webOS/i) || // jshint ignore:line
				navigator.userAgent.match(/iPhone/i) || // jshint ignore:line
		 		navigator.userAgent.match(/iPad/i) || // jshint ignore:line
		 		navigator.userAgent.match(/iPod/i) ||  // jshint ignore:line
			 	navigator.userAgent.match(/BlackBerry/i) || // jshint ignore:line
			 	navigator.userAgent.match(/Windows Phone/i) || // jshint ignore:line
			 	navigator.userAgent.match(/Mobile/i) || // jshint ignore:line
			 	navigator.userAgent.match(/Silk/i) || // jshint ignore:line
				navigator.userAgent.match(/NABI/i)) { // jshint ignore:line
			return true;
		}else {
		    return false;
		}
	};

	/*
	* Check if this program is being run on a browser on iOS
	* @return:[Boolean] - True if this program is being run on an iOS browser, false otherwise
	*/
	/*
	ss.SystemInfo.isIOS = function(){
		return navigator.userAgent.match(/iPhone/i)
		 	|| navigator.userAgent.match(/iPad/i)
		 	|| navigator.userAgent.match(/iPod/i);
	}*/

	/*
	* Get the version of iOS the game is running on
	* @return:[Integer] -
	*/
	ss.SystemInfo.getIOSVersion = function() {
		// Return -1 if not running on IOS
		if (!ss.SystemInfo.isIOS) {
			return -1;
		}

		var ua = window.navigator.userAgent;
		var osIndex = ua.indexOf("OS ");

		if (osIndex >= 0) {
			return parseInt(ua.substring(osIndex + 3, osIndex + 4));
		}

		console.warn("SystemInfo.getIOSVersion: Couldn't detect IOS version!");
		return 0;
	};

	/*
	* Get the version of IE the game is running on
	* @return:[Integer] - If on IE, the version of IE the game is running on.
	*					- If not on IE, returns -1.
	*/
	ss.SystemInfo.getIEVersion = function() {
		// Return -1 if not running on IE
		if (!ss.SystemInfo.isIE) {
			return -1;
		}

		var ua = window.navigator.userAgent;
		var msie = ua.indexOf("MSIE ");
		var trident = ua.indexOf("Trident/");

		// Check for IE 10 or older and return version number
		if (msie >= 0) {
			return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
		}

		// Check for IE 11 or newer and return version number
		if (trident >= 0) {
			var rv = ua.indexOf("rv");
			return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
		}

		console.warn("SystemInfo.getIEVersion: Couldn't detect IE version!");
		return 0;
	};

	/*
	* Get the version of Safari the game is running on
	* @return:[Integer] - If on Safari, the version of Safari the game is running on.
	*					- If not on Safari, returns -1.
	*/
	ss.SystemInfo.getSafariVersion = function() {
		var versionIndex;
		var versionStart;

		// return -1 if not on Safari
		if (!ss.SystemInfo.isSafari) {
			return -1;
		}

		// Look for the start of the version tag
		versionIndex = parseInt(window.navigator.userAgent.indexOf("Version/"));
		versionStart = versionIndex + 8;

		// ss.DebugUtil.showMessage("Version Start: " + versionStart, "VStart");

		// TESTING
		// ss.DebugUtil.showMessage("Version Index: " + window.navigator.userAgent.indexOf("Version/"), "VIndex");
		// ss.DebugUtil.showMessage("Version Index Add: " + Number(versionIndex + 8), "VIndex2");
		// ss.DebugUtil.showMessage("Version text: " + window.navigator.userAgent.substring(versionStart, versionStart + 1), "VText");

		if (versionIndex < 0) {
			console.warn("SystemInfo: Unable to detect Safari version!");
			return 0;
		}

		return parseInt(window.navigator.userAgent.substring(versionStart, versionStart + 1));
	};

	/*
	* Check if this program is being run on any version of Internet Explorer
	* @return:
	*/
	// ss.SystemInfo.isInternetExplorer = function(){

	// 	// if(navigator.appName == 'Microsoft Internet Explorer'){
	// 	// 	return true;
	// 	// }else{
	// 	// 	return false;
	// 	// }
	// }

	/*
	* Initialize the system info object
	*/
	ss.SystemInfo._init = function() {

		var ua = window.navigator.userAgent;

		this.isIOS = false;
		this.isIPhone = false;
		this.isIPod = false;
		this.isIPad = false;
		this.isNexus = false;
		this.isIE = false;
		this.isSafari = false;
		this.isChrome = false;
		this.isFirefox = false;
		this.isMaxthon = false;
		this.isSilk = false;

		// Check for (cr)Apple devices
		if (ua.match(/iPhone/i)) {
			this.isIPhone = true;
		}else if (ua.match(/iPad/i)) {
			this.isIPad = true;
		}else if (ua.match(/iPod/i)) {
			this.isIPod = true;
		}

		// Check for any IOS device
		if (this.isIPhone || this.isIPad || this.isIPod) {
			this.isIOS = true;
		}
		// Check for a Nexus device
		if (ua.match(/Nexus/i)) {
			this.isNexus = true;
		}

		// Check for Internet Explorer
		if (ua.indexOf("MSIE ") >= 0 || ua.indexOf("Trident") >= 0) {
			this.isIE = true;
		// Check for Silk
		}else if (ua.match(/Silk/i)) {
			this.isSilk = true;
		// Check for Maxthon
		}else if (ua.match(/NABI/i)) {
			this.isMaxthon = true;
			// Check for Chrome
		}else if (ua.match(/Chrome/i)) {
			this.isChrome = true;
		// Check for Firefox
		}else if (ua.match(/Firefox/i)) {
			this.isFirefox = true;
		// Check for Safari
		}else if (ua.match(/Safari/i)) {
			this.isSafari = true;
		}else {
			console.warn("SystemInfo: Couldn't detect browser type!");
		}
	};

	// Run the initialization code immediately
	ss.SystemInfo._init();

} ());

/* global require, ss */


(function() {

	/*
	* Class MathConst
	* 	Static class that provides access to standard math constants
	*/

	/*
	* Get the singleton reference to this class
	*/
	ss.MathConst = ss.MathConst || new function MathConst() {}; // jshint ignore:line

	// Multiplier to convert from degrees to radians
	ss.MathConst.DEG_TO_RAD = Math.PI / 180.0;
	// Multiplier to convert from radians to degrees
	ss.MathConst.RAD_TO_DEG = 180.0 / Math.PI;

	// Variants of PI
	ss.MathConst.TWO_PI = Math.PI * 2.0;
	ss.MathConst.HALF_PI = Math.PI / 2.0;

	// Tau - A more reasonable value to use than PI?  The debate continues....
	ss.MathConst.TAU = ss.MathConst.TWO_PI;

} ());

/* global require, ss */



( function() {
	"use strict";

	/**
	 * Object for handling 2d vectors, and their operations.
	 * @param {Number} x - the x component of this vector.
	 * @param {Number} y - the y component of this vector.
	 * @constructor
	 */
	ss.Vector2 = function(x, y) {

		// The x component of the vector.
		this.x = x;

		// The y component of the vector.
		this.y = y;

	};

	ss.Vector2.prototype.constructor = ss.Vector2;

	/**
	* Get the magnitude of this vector.
	* @return - the magnitude of the vector.
	*/
	ss.Vector2.prototype.magnitude = function() {
		return Math.sqrt(this.sqrMagnitude());
	};

	/**
	* Get the squared magnitude of this vector.
	* @return the squared magnitude of this vector.
	**/
	ss.Vector2.prototype.sqrMagnitude = function() {
		return this.x * this.x + this.y * this.y;
	};

	/**
	 * Normalize this vector.
	 */
	ss.Vector2.prototype.normalize = function() {
		if (this.magnitude() === 0) {
			return this;
		}

		this.setAll(this.x / this.magnitude(), this.y / this.magnitude());
		return this;
	};


	/**
	 * Get a normalized version of the vector.
	 * @return {ss.Vector2} the normalized vector.
	 */
	ss.Vector2.prototype.normalized = function() {
		if (this.magnitude() === 0) {
			return this;
		}

		var norm = new ss.Vector2(this.x / this.magnitude(), this.y / this.magnitude());

		return norm;
	};

	/**
	 * Get a normalized version of the vector, and store it in an out vector.
	 * @param  {ss.Vector2} out - The output vector.
	 * @return {ss.Vector2}     - The output vector.
	 */
	ss.Vector2.prototype.normalizedOut = function (out) {
		if (this.magnitude() === 0) {
			out.setAll(this.x, this.y);
		}

		out.setAll(this.x / this.magnitude(), this.y / this.magnitude());

		return out;
	};

	/**
	* Set the x and y coordinates in one handy function
	* @param {Number} x - The x coordinate
	* @param {Number} y - The y coordinate
	*/
	ss.Vector2.prototype.setAll = function(x, y) {
		this.x = x;
		this.y = y;
	};

	/**
	* Dot Product.
	* @param {ss.Vector2} other - the other vector to dot this one with.
	*/
	ss.Vector2.prototype.dot = function(other) {
		return (this.x * other.x + this.y * other.y);
	};

	/**
	* Add this vector to another vector.
	* @param {ss.Vector2} other - the other vector in the addition.
	*/
	ss.Vector2.prototype.add = function(other) {
		return new ss.Vector2(this.x + other.x, this.y + other.y);
	};

	/**
	* Add this vector to another vector, and save this in the vector itself.
	* @param {ss.Vector2} other - the other vector in the addition.
	*/
	ss.Vector2.prototype.addSelf = function(other) {
		this.x = this.x + other.x;
		this.y = this.y + other.y;

		return this;
	};

	/**
	* Add this vector to another vector, and save the result in the out parameter.
	* @param {ss.Vector2} other - the other vector in the addition.
	* @param {ss.Vector2} out - the out vector.
	*/
	ss.Vector2.prototype.addOut = function(other, out) {
		out.x = this.x + other.x;
		out.y = this.y + other.y;
	};

	/**
	 * Get the angle between this and another vector using dot product
	 * @param {ss.Vector2} other - the other vector.
	 * @param {Boolean} isDegree - if the result should be in degree or radians
	 * @return - the angle to the other vector.
	 */
	ss.Vector2.prototype.angleBetween = function(other, isDegree) {
		if ( (this.x === 0 && this.y === 0) || (other.x === 0 && other.y === 0)) {
			throw "Tried to get angle between a zero vector and another vector.";
		}

		if (isDegree) {
			return Math.acos(this.dot(other) / this.magnitude() * other.magnitude()) * ss.MathConst.RAD_TO_DEG;
		}

		return Math.acos(this.dot(other) / this.magnitude() * other.magnitude());
	};

	/**
	 * Get the angle between this and another vector using atan2
	 * @param {ss.Vector2} other - the other vector.
	 * @param {Boolean} isDegree - if the result should be in degree or radians
	 * @return - the angle to the other vector.
	 */
	ss.Vector2.prototype.angleBetween2 = function(other, isDegree) {
		if ((this.x === 0 && this.y === 0) || (other.x === 0 && other.y === 0)) {
			throw "Tried to get angle between a zero vector and another vector.";
		}
		if (isDegree) {
			return (this.angleRad() - other.angleRad()) * ss.MathConst.RAD_TO_DEG;
		}
		return this.angleRad() - other.angleRad();
	};

	/**
	* Subtract another vector from this vector.
	* @param {ss.Vector2}  other - the vector to subtract.
	**/
	ss.Vector2.prototype.subtract = function(other) {
		return new ss.Vector2(this.x - other.x, this.y - other.y);
	};

	/**
	* Subtract another vector from this vector and store the result in yourself.
	* @param {ss.Vector2} other - the vector to subtract from yourself.
	**/
	ss.Vector2.prototype.subtractSelf = function(other) {
		this.x = this.x - other.x;
		this.y = this.y - other.y;
		return this;
	};

	/**
	* Subtract another vector from this vector and store the result in out.
	* @param {ss.Vector2} other - the other vector to subtract.
	* @param {ss.Vector2} out - the output vector.
	**/
	ss.Vector2.prototype.subtractOut = function(other, out) {
		out.x = this.x - other.x;
		out.y = this.y - other.y;
	};

	/**
	* Multiply this vector by a scalar.
	**/
	ss.Vector2.prototype.mult = function(scalar) {
		return new ss.Vector2(this.x * scalar, this.y * scalar);
	};

	/**
	* Multiply this vector by a scalar, and store the result in yourself.
	* @param {Number} scalar - the scalar to multiply by.
	**/
	ss.Vector2.prototype.multSelf = function(scalar) {
		this.x = this.x * scalar;
		this.y = this.y * scalar;
		return this;
	};

	/*
	* Multiply this vector by a scalar and store the result in the out parameter.
	* @param {Number} scalar - the scalar to multiply.
	* @param {ss.Vector2} out - the vector to output to.
	*/
	ss.Vector2.prototype.multOut = function(scalar, out) {
		out.x = this.x * scalar;
		out.y = this.y * scalar;
	};

	/**
	* Multiply this vector by a scalar.
	* @param {Number} scalar - the scalar to multiply.
	**/
	ss.Vector2.prototype.scale = function(scalar) {
		return this.mult(scalar);
	};


	/**
	* Multiply this vector by a scalar, and store the result in yourself.
	* * @param {Number} scalar - the scalar to multiply.
	**/
	ss.Vector2.prototype.scaleSelf = function(scalar) {
		return this.multSelf(scalar);
	};

	/**
	* Multiply this vector by a scalar, and store the result in yourself.
	* @param {Number} scalar - the scalar to multiply.
	* @param {ss.Vector2} out - the vector to output to.
	**/
	ss.Vector2.prototype.scaleOut = function(scalar, out) {
		return this.multOut(scalar, out);
	};

	/**
	 * Get the distance from this vector to another vector.
	 * @param {ss.Vector2}other - the other vector.
	 * @return - the distance to the other vector.
	 */
	ss.Vector2.prototype.distance = function(other) {
		return Math.sqrt(Math.pow(this.x - other.x, 2) + Math.pow(this.y - other.y, 2));
	};

	/**
	 * Get the distance from this vector to another vector squared.
	 * @param {ss.Vector2} other - the other vector.
	 * @return - the distance to the other vector squared.
	 */
	ss.Vector2.prototype.distanceSquared = function(other ) {
		return Math.pow(this.x - other.x, 2) + Math.pow(this.y - other.y, 2);
	};

	/**
	* Get the negative of this vector. returns a vector of the same
	* magnitude as this vector, but in the opposite direction.
	**/
	ss.Vector2.prototype.negative = function() {
		return new ss.Vector2(-this.x, -this.y);
	};

	/**
	* negate this vector. sets this vector a vector of the same
	* magnitude as this vector, but in the opposite direction.
	**/
	ss.Vector2.prototype.negativeSelf = function() {
		this.x = -this.x;
		this.y = -this.y;
		return this;
	};

	/**
	 * Negate this vector, and put the result in the out parameter.
	 * @param  {ss.Vector2} out where to put the result of the calculation.
	 */
	ss.Vector2.prototype.negativeOut = function(out) {
		out.x = -this.x;
		out.y = -this.y;
	};

	/*
	* Get the angle (in radians) this vector forms with respect to the X-axis
	* @return:Number - The angle (in radians) this vector forms with respect to the X-axis
	*				   This will be between -PI and PI radians
	*/
	ss.Vector2.prototype.angleRad = function() {
		return Math.atan2(this.y, this.x);
	};

	/*
	* Get the angle (in degrees) this vector forms with respect to the X-axis
	* @return:Number - The angle (in degrees) this vector forms with respect to the X-axis
	*				   This will be between -180 and 180 degrees
	*/
	ss.Vector2.prototype.angleDeg = function() {
		return this.angleRad() * ss.MathConst.RAD_TO_DEG;
	};

	/*
	* Shift this vector by a certain amount
	* @param shiftX:Number - Amount to shift in X
	* @param shiftY:Number - Amount to shift in Y
	* @return:ss.Vector2 - The vector instance this was called on, updated accordingly
	*/
	ss.Vector2.prototype.shift = function(shiftX, shiftY) {
		this.x += shiftX;
		this.y += shiftY;

		return this;
	};

	/*
	* Rotate this vector around (0, 0) by a specified amount in the positive
	* @param angle:Number - The angle in degrees to rotate by
	* @return:ss.Vector2 - A new Vector2 which is the rotated version of the original
	*/
	ss.Vector2.prototype.rotate = function(angle) {
		angle = angle * ss.MathConst.DEG_TO_RAD;
		return new ss.Vector2(this.x * Math.cos(angle) - this.y * Math.sin(angle),
			this.x * Math.sin(angle) + this.y * Math.cos(angle));
	};

	/*
	* Rotate this vector instance around (0, 0) by a specified amount
	* @param angle:Number - The angle in degrees to rotate by
	* @return:ss.Vector2 - The rotated instance of the Vector2 this was called on
	*/
	ss.Vector2.prototype.rotateSelf = function(angle) {
		angle = angle * ss.MathConst.DEG_TO_RAD;

		this.setAll(this.x * Math.cos(angle) - this.y * Math.sin(angle), this.x * Math.sin(angle) + this.y * Math.cos(angle));

		return this;
	};

	/**
	 * Rotate the vector around (0,0) by a specified amount and store the result in out.
	 * @param  {[type]} angle [description]
	 * @param  {[type]} out   [description]
	 */
	ss.Vector2.prototype.rotateOut = function(angle, out) {
		angle = angle * ss.MathConst.DEG_TO_RAD;
		out.setAll(this.x * Math.cos(angle) - this.y * Math.sin(angle), this.x * Math.sin(angle) + this.y * Math.cos(angle));
	};

	/**
	* Checks equality of two vectors with an optional tolerance.
	* @param {ss.Vector2} other: the other vector to check.
	* @param {Number} tolerance: (optional) A tolerance for what to consider equal. This is to work around floating point error.
	* @return: true if the vectors are equal.
	**/
	ss.Vector2.prototype.equals = function(other, tolerance) {
		if (tolerance) {
			return (Math.abs(this.x - other.x) <= tolerance && Math.abs(this.y - other.y) <= tolerance);
		} else {
			return (other.x === this.x && other.y === this.y);
		}
	};

	/**
	 * Copy this vector out to another one.
	 * @param  {ss.Vector2} out the vector to copy to.
	 */
	ss.Vector2.prototype.copyOut = function(out) {
		out.x = this.x;
		out.y = this.y;
	};

	/**
	 * Clone this vector, and return a new vector containing the clone.
	 * @return {ss.Vector2} a clone of this vector.
	 */
	ss.Vector2.prototype.clone = function() {
		return new ss.Vector2(this.x, this.y);
	};

	/*
	* Converts this object to a string
	* @return: A string representing the vector's current values
	*/
	ss.Vector2.prototype.toString = function() {
		return "(x=" + this.x + ", y=" + this.y + ")";
	};

	/**
	* Comparison function for x coordinate of the vectors.
	* @param  {ss.Vector2} v1 - The 1st Vector in the comparison.
	* @param  {ss.Vector2} v2 - The 2nd Vector in the comparison.
	* @return {Number}    - 0 if v1 should be sorted the same as v2, > 0 if v1 is greater than v2, < 0 is v1 is less than v2.
	*/
	ss.Vector2.compareX = function(v1, v2) {
		return v1.x - v2.x;
	};

	/**
	 * Comparison function for y coordinate of the vectors.
	 * @param  {ss.Vector2} v1 - The 1st Vector in the comparison.
	 * @param  {ss.Vector2} v2 - The 2nd Vector in the comparison.
	 * @return {Number}    - 0 if v1 should be sorted the same as v2, > 0 if v1 is greater than v2, < 0 is v1 is less than v2.
	 */
	ss.Vector2.compareY = function(v1, v2) {
		return v1.y - v2.y;
	};

	/**
	 * Comparison function used to obtain a lexicographic ordering of a set of Vectors.
	 * @param  {ss.Vector2} v1 - The 1st Vector in the comparison.
	 * @param  {ss.Vector2} v2 - The 2nd Vector in the comparison.
	 * @return {Number}    - 0 if v1 should be sorted the same as v2, > 0 if v1 is greater than v2, < 0 is v1 is less than v2.
	 */
	ss.Vector2.compareLexicographic = function(v1, v2) {
		if (ss.Vector2.compareX(v1, v2) !== 0) {
			return ss.Vector2.compareX(v1, v2);
		} else {
			return ss.Vector2.compareY(v1, v2);
		}
	};
} ());

/* global require, ss */




(function() {
	"use strict";

	/*
	* Class MathUtils
	*	Singleton class with functions for performing various general math operations
	*/

	/*
	* Get the singleton reference to this class
	*/
	ss.MathUtils = ss.MathUtils || new function MathUtils() {}; // jshint ignore:line

	/*
	* Get the sign of a given number
	* @param num:Number - The number to get the sign for
	* @return:Integer - 1 if num >= 0, -1 if num < 0
	* 	This differs from standard Math.sign which returns 0 if num = 0
	*/
	ss.MathUtils.getSignNonZero = function(num) {
		return num < 0 ? -1 : 1;
	};

	/*
	* Get the sign of a given number
	* @param num:Number - The number to get the sign for
	* @return:Integer - 1 if num > 0, 0 if num == 0, -1 if num < 0
	* 	This differs from standard Math.sign which returns 0 if num = 0
	*/
	ss.MathUtils.getSign = function(num) {
		if (num === 0) {
			return 0;
		}else {
			return num > 0 ? 1 : -1;
		}
	};

	/*
	* Clamp a value between a minimum and maximum value
	* @param value:Number - Value to be clamped
	* @param min:Number - Minimum allowed value
	* @param max:Number - Maximum allowed value
	*/
	ss.MathUtils.clamp = function(value, min, max) {
		if (value < min) {
			return min;
		}else if (value > max) {
			return max;
		}else {
			return value;
		}
	};

	/**
	 * Clamp a value in the zero to 1 range.
	 * @param  {Number} value 		The value to clamp.
	 * @return {Number}       		The value clamped to the 0-1 range.
	 */
	ss.MathUtils.clamp01 = function(value) {
		return ss.MathUtils.clamp(value, 0, 1);
	};

	/*
	* Linear interpolation between to points.
	* @param p1: [Vector2] the first point in the interpolation.
	* @param p2: [Vector2] the second point in the interpolation.
	* @param t: [Number] a vale from 0 to 1 for where along the line to interpolate.
	*			NOTE: this will extrapolate beyond these values.
	*/
	ss.MathUtils.lerp = function(p1, p2, t) {
		var out = new ss.Vector2(0, 0);
		p2.subtractOut(p1, out);
		out.multSelf(t);
		p1.addOut(out, out);
		return out;
	};

	ss.MathUtils.lerp1D = function(p1, p2, t) {
		t = ss.MathUtils.clamp01(t);
		var p = p2 - p1;
		p *= t;
		return p1 + p;
	};

	/*
	* Linear interpolation between to points, using an out vector for the result.
	* @param p1: [Vector2] the first point in the interpolation.
	* @param p2: [Vector2] the second point in the interpolation.
	* @param t: [Number] a vale from 0 to 1 for where along the line to interpolate.
	*			NOTE: this will extrapolate beyond these values.
	*/
	ss.MathUtils.lerpOut = function(p1, p2, t, out) {
		p2.subtractOut(p1, out);
		out.multSelf(t);
		p1.addOut(out, out);
	};

	/*
	* Get a random number betwen two numbers.
	* @param min: [Number] the min value.
	* @param max: [Number] the max value.
	*/
	ss.MathUtils.randomRange = function(min, max) {
		return (Math.random() * (max - min)) + min;
	};

	/**
	 * Uniform Random
	 * @param  {Number} radius - The radius of the circle to randomize.
	 * @return {ss.Vector2}    - A uniformly distributed point in the
	 */
	ss.MathUtils.uniformRandomInCircle = function(radius) {
		var a = Math.random();
		var b = Math.random();

		if (b < a) {
			var c = b;
			b = a;
			a = c;
		}

		return new ss.Vector2(b * radius * Math.cos(2 * Math.PI * a / b), b * radius * Math.sin(2 * Math.PI * a / b));
	};

	/**
	 * Get the minimum distance between two angles in degrees.
	 * @param  {Number} angleA - The first angle (in degrees).
	 * @param  {Number} angleB - The second angle (in degrees).
	 * @return {Number}        - The minimum distance between the two angles.
	 */
	ss.MathUtils.distanceBetweenAnglesInDegrees = function(angleA, angleB) {
		var a = angleA - angleB;
		if (a > 180) {
			a -= 360;
		}
		if (a <= -180) {
			a += 360;
		}

		return a;
	};

	/*
	* Get a random number between two numbers, but weigh it based on the last result.
	* This prevents two extreme values from occurring in a row.
	* @param min: the min number.
	* @param max: the max number.
	* @param last: the last result.
	*/
	ss.MathUtils.weightedRandomInt = function(min, max, lastResult) {
		if (ss.isEmpty(lastResult)) {
			lastResult = (max + min) / 2;
		}

		if (lastResult < (max + min) / 2) {
			return Math.floor(ss.MathUtils.randomRange(min + ((max + min) / 2 - lastResult), max));
		}

		if (lastResult > (max + min) / 2) {
			return Math.floor(ss.MathUtils.randomRange(min , max - (lastResult - ((max + min) / 2))));
		}

	};

	ss.MathUtils.getRandomNumberWithDistribution = function(interpolationFunction, flip) {
		var randomVal = Math.random();
		var filterVal = Math.random();

		while(filterVal > interpolationFunction(randomVal)) {
			randomVal = Math.random();
			filterVal = Math.random();
		}

		if (flip) {
			return 1 - randomVal;
		} else {
			return randomVal;
		}
	};

}());

/* global require, ss */



(function() {
	"use strict";
	/**
	* Class representing a circle.
	**/
	function Circle (point, radius) {

		/**
		* Declare the center point of the circle
		**/
		this.x = point.x;
		this.y = point.y;

		/**
		* Declare the radius of the circle
		**/
		this.radius = radius;
	}

	Circle.prototype.constructor = Circle;

	/**
	* Find if there is a collision between self and the other circle
	* Return true if collision exist
	**/
	Circle.prototype.detectCollision = function( other ) {

		return ((this.x - other.x) * (this.x - other.x) +
			(this.y - other.y) * (this.y - other.y)	< (this.radius + other.radius) * (this.radius + other.radius));
	};

	/**
	 * Project the circle onto a given axis (this is used to check if the circle is colliding with various other geometric primitives).
	 * @param  {ss.Vector} axis - A normalized Vector representing the axis we're colliding with.
	 * @return {Array}      	- An 2 element array [min, max] containing the min and max values of the projection.
	 */
	Circle.prototype.projectOntoAxis = function(axis) {
		var centerVector = new ss.Vector2(this.x, this.y);
		var axisNorm = axis.normalized();

		var minVector = centerVector.subtract(axisNorm.mult(this.radius));
		var maxVector = centerVector.add(axisNorm.mult(this.radius));

		var minProjection =  minVector.dot(axis);
		var maxProjection = maxVector.dot(axis);

		return [ minProjection, maxProjection ];
	};

	ss.Circle = Circle;

} ());

/* global require, ss*/





(function() {
	"use strict";
	/*
	* Class GeomUtils
	*	Singleton class with functions for performing various geometric operations
	*/

	/*
	* Get the singleton reference to this class
	*/
	ss.GeomUtils = ss.GeomUtils || new function GeomUtils() { }; // jshint ignore:line

	/*
	* Get the points of intersection between a line and a circle of a given radius
	* @param line:Line2D - The 2D line segment defining the line to check for intersection
	* @param circleRadius:Number - Radius of the cirlce
	* @param circleCenter:Vector2 (optional) - Center point of the circle.  If not provided, a center
	*		of (0, 0) will be used by default
	* @return:Array[Line2D] - An array consisting of 0, 1, or 2 points of intersection
	*/
	ss.GeomUtils.getLineCircleIntersection = function(line, circleRadius, circleCenter) {
		var dx, dy, dr, det, disc;

		// If neccessary, shift the points on the line so the circle is centered on the origin
		if (circleCenter) {
			line.shift(-circleCenter.x, -circleCenter.y);
		}

		// Change in x and y between the two points
		dx = line.p2.x - line.p1.x;
		dy = line.p2.y - line.p1.y;
		dr = Math.sqrt(dx * dx + dy * dy);

		// Determinant
		det = line.p1.x * line.p2.y - line.p2.x * line.p1.y;

		// Discriminant
		disc = circleRadius * circleRadius * dr * dr - det * det;

		var intersectPoints = [];

		var numeratorX;
		var numeratorY;
		var intPoint;

		// If discriminant is less than zero, we will have imaginary intersection points
		// We return an empty list since there are no real intersections
		if (disc < 0) {
			return intersectPoints;
		}

		numeratorX = ss.MathUtils.getSignNonZero(dy) * dx * Math.sqrt(disc);
		numeratorY = Math.abs(dy) * Math.sqrt(disc);

		// Calculate and add the first intersection point
		intPoint = new ss.Vector2(0, 0);
		intPoint.x = (det * dy + numeratorX) / (dr * dr);
		intPoint.y = (-det * dx + numeratorY) / (dr * dr);

		// Shift this intersection point back if we shifted for an off-center circle
		if (circleCenter) {
			intPoint.x += circleCenter.x;
			intPoint.y += circleCenter.y;
		}
		intersectPoints.push(intPoint);

		// If the descriminant is nearly 0, this line is tangent to the circle and so there's only one intersection point
		var equalityTolerance = 0.00001;
		if (Math.abs(disc) <= equalityTolerance) {
			return intersectPoints;
		}

		// Calculate and add the second intersection point
		intPoint = new ss.Vector2(0, 0);
		intPoint.x = (det * dy - numeratorX) / (dr * dr);
		intPoint.y = (-det * dx - numeratorY) / (dr * dr);

			// Shift this intersection point back if we shifted for an off-center circle
		if (circleCenter) {
			intPoint.x += circleCenter.x;
			intPoint.y += circleCenter.y;
		}
		intersectPoints.push(intPoint);

		return intersectPoints;
	};

	/*
	* Get the angle (in degrees) of the line going from p1 to p2
	*/
	// GeomUtils.getAngleBetweenPoints = function(p1, p2){

	// }

}());

/* global require, ss */





(function() {
	"use strict";

	/**
	* Class representing a 2d line segment.
	* Defined by two endpoints, p1 and p2.
	**/
	function Line2D (p1, p2) {
		/*global Vector2 */

		// The first endpoint of the line.
		this.p1 = p1;

		// The second endpoint of the line.
		this.p2 = p2;
	}

	Line2D.prototype.constructor = Line2D;

	/**
	* Get a normalized vector which is tangent to this line.
	* It will point in the direction from p1 -> p2
	**/
	Line2D.prototype.getTangent = function() {
		var tangent = this.p2.subtract(this.p1);
		if (tangent.magnitude() === 0) {
			throw "Trying to get a vector in the direction of a zero length line segment.";
		}
		tangent.normalize();
		return tangent;
	};


	/**
	* Get a normalized vector which is tangent to this line, and store it in the out parameter.
	* It will point in the direction from p1 -> p2
	* @param {ss.Vector2} out: the out vector to store the tangent in.
	**/
	Line2D.prototype.getTangentOut = function(out) {
		this.p2.subtractOut(this.p1, out);
		if (out.magnitude() === 0) {
			throw "Trying to get a vector in the direction of a zero length line segment.";
		}
		out.normalize();
	};

	/**
	 * Get the length of the line.
	 * @return {Number} - The length of the line.
	 */
	Line2D.prototype.getLength = function() {
		var tan = this.p2.subtract(this.p1);
		return tan.magnitude();
	};

	/**
	* Gets a normal to the line. To get the other normal to the line,
	* take the results of this and make them negative.
	**/
	Line2D.prototype.getNormal = function() {
		var tangent = this.getTangent();
		var t = tangent.x;
		tangent.x = tangent.y;
		tangent.y = -t;

		return tangent;
	};

	/**
	* Gets a normal to the line and stores it an an out parameter.
	* To get the other normal to the line, take the results of
	* this and make them negative.
	* @param {ss.Vector2} out - the output vector.
	**/
	Line2D.prototype.getNormalOut = function(out) {
		this.getTangentOut(out);
		var t = out.x;
		out.x = out.y;
		out.y = -t;
	};

	/**
	* Gets the midpoint of the line segment.
	**/
	Line2D.prototype.getMidpoint = function() {
		var out = this.p1.add(this.p2);
		out.multSelf(0.5);

		return out;
	};

	/**
	* Gets the midpoint of the line segment, and stores the result in
	* an output parameter.
	* @param {ss.Vector2} out - the output vector.
	**/
	Line2D.prototype.getMidpointOut = function(out) {
		this.p1.addOut(this.p2, out);
		out.multSelf(0.5);
	};
	/**
	* Gets the slope of the line segment.
	**/
	Line2D.prototype.getSlope = function() {
		if (Math.abs(this.p2.x - this.p1.x) === 0) {
			return NaN;
		}
		return (this.p2.y - this.p1.y) / (this.p2.x - this.p1.x);
	};

	/**
	* Checks whether a given point is within the AABB defined by this line.
	* @param {Vector2} point : the point to check.
	* @returns: true if the point is within the AABB defined by the line,
	* false otherwise.
	**/
	Line2D.prototype.withinLineBounds = function(point) {
		if (point.x < Math.min(this.p1.x, this.p2.x) || point.x > Math.max(this.p1.x, this.p2.x)) {
			return false;
		}
		if (point.y < Math.min(this.p1.y, this.p2.y) || point.y > Math.max(this.p1.y, this.p2.y)) {
			return false;
		}
		return true;
	};

	/*
	* Shift this entire line segment by a certain amount
	* @param shiftX:Number - Amount to shift along X-axis
	* @param shiftY:Number - Amount to shift along Y-axis
	*/
	Line2D.prototype.shift = function(shiftX, shiftY) {
		// Shift start point
		this.p1.shift(shiftX, shiftY);

		// Shift end point
		this.p2.shift(shiftX, shiftY);

		return this;
	};

	/**
	* Try to get the intersection point between two line segments.
	* @param {Line2D} other: The other line to intersect this line with.
	* @returns: false if the lines don't intersect, or the intersection point if they do.
	**/
	Line2D.prototype.tryGetIntersection = function(other) {
		var a1, b1, c1, a2, b2, c2, det, intersectionPoint;

		// determine the coeficients A, B , C for this line, if the line is expressed as Ax+By=C
		a1 = this.p2.y - this.p1.y;
		b1 = this.p1.x - this.p2.x;
		c1 = a1 * this.p1.x + b1 * this.p1.y;

		// determine the coeficients A, B , C for the other line, if the line is expressed as Ax+By=C
		a2 = other.p2.y - other.p1.y;
		b2 = other.p1.x - other.p2.x;
		c2 = a2 * other.p1.x + b2 * other.p1.y;

		det = a1 * b2 - a2 * b1;

		if ( det === 0 ) {
			// lines are parallel, we need to do a bit more here.

			// if they don't line up, return false
			if (a1 !== a2 || b1 !== b2 || c1 !== c2) {
				return false;
			}

			// if they do, check if there's an overlap.
			// TODO: Deal with the possible overlaps in this situation. Currently, false is returned in all parallel cases,
			// but we should return the midpoint of the overlapping area.
			return false;

		}  else {
			// if these were lines, as opposed to line segments, they'd intersect here:
			intersectionPoint = new ss.Vector2((b2 * c1 - b1 * c2) / det, (a1 * c2 - a2 * c1) / det);

			// If we're in both segments, return the intersection point, or return false if we're not.
			if (this.withinLineBounds(intersectionPoint) && other.withinLineBounds(intersectionPoint)) {
				return intersectionPoint;
			} else {
				return false;
			}
		}
	};

	/*
	* Get the point on this line segment closest to the provided point
	* @param point:Vector2 - The point
	* @param clamp:Boolean - Set to true if the point should be restricted to this line segment,
	*	false if co-linear points beyond the start and end points are acceptable
	* @return:Vector2 - The point on this line segment closest to the provided point
	*/
	Line2D.prototype.getClosestPointOnLine = function(point, clamp) {
		var out = this.p2.subtract(this.p1);
		if (this.p1.equals(this.p2)) {
			return new ss.Vector2(this.p1.x, this.p1.y);
		}
		if (clamp) {
			out.multSelf(ss.MathUtils.clamp(((point.x - this.p1.x) * (this.p2.x - this.p1.x) +
				(point.y - this.p1.y) * (this.p2.y - this.p1.y)) /
			((this.p2.x - this.p1.x) * (this.p2.x - this.p1.x) +
				(this.p2.y - this.p1.y) * (this.p2.y - this.p1.y)), 0, 1));
		} else {
			out.multSelf(((point.x - this.p1.x) * (this.p2.x - this.p1.x) +
				(point.y - this.p1.y) * (this.p2.y - this.p1.y)) /
			((this.p2.x - this.p1.x) * (this.p2.x - this.p1.x) +
				(this.p2.y - this.p1.y) * (this.p2.y - this.p1.y)));
		}
		this.p1.addOut(out, out);

		return out;
	};

	/*
	* Get the point on this line segment closest to the provided point, and store the result in an out vector.
	* @param point:Vector2 - The point
	* @param clamp:Boolean - Set to true if the point should be restricted to this line segment,
	*	false if co-linear points beyond the start and end points are acceptable
	* @param out:Vector2 - the output vector.
	* @return:Vector2 - The point on this line segment closest to the provided point
	*/
	Line2D.prototype.getClosestPointOnLineOut = function(point, clamp, out) {
		this.p2.subtractOut(this.p1, out);
		if (this.p1.equals(this.p2)) {
			out.x = this.p1.x;
			out.y = this.p1.y;
			return out;
		}
		if (clamp) {
			out.multSelf(ss.MathUtils.clamp(((point.x - this.p1.x) * (this.p2.x - this.p1.x) +
				(point.y - this.p1.y) * (this.p2.y - this.p1.y)) /
			((this.p2.x - this.p1.x) * (this.p2.x - this.p1.x) +
				(this.p2.y - this.p1.y) * (this.p2.y - this.p1.y)), 0, 1));
		} else {
			out.multSelf(((point.x - this.p1.x) * (this.p2.x - this.p1.x) +
				(point.y - this.p1.y) * (this.p2.y - this.p1.y)) /
			((this.p2.x - this.p1.x) * (this.p2.x - this.p1.x) +
				(this.p2.y - this.p1.y) * (this.p2.y - this.p1.y)));
		}
		this.p1.addOut(out, out);

		return out;
	};

	/*
	* Rotate this entire line around a particular point
	* @param rotateAngle:Number - The angle to rotate around the origin point
	* @param rotateOrigin:Vector2 (Optional) - Origin point to rotate around
	*	If none is provided, the line will be rotated around the origin
	* @return:Line2D - This Line2D instance in its updated form
	*/
	Line2D.prototype.rotate = function(rotateAngle, rotateOrigin) {
		// console.log("Line Rotate: Start: " + this);

		// If an origin was provided, offset the points to be with respect to it
		if (rotateOrigin) {
			this.p1.shift(-rotateOrigin.x, -rotateOrigin.y);
			this.p2.shift(-rotateOrigin.x, -rotateOrigin.y);
		}

		// Rotate the points around the origin
		this.p1.rotateSelf(rotateAngle);
		this.p2.rotateSelf(rotateAngle);

		// If we shifted points at the beginning, shift them back
		if (rotateOrigin) {
			this.p1.shift(rotateOrigin.x, rotateOrigin.y);
			this.p2.shift(rotateOrigin.x, rotateOrigin.y);
		}

		return this;
	};

	/**
	 * Translate this line by a given amount.
	 * @param  {Number} amount - The amount to translate by.
	 * @return {ss.Line2D}        - this object (useful for chaining).
	 */
	Line2D.prototype.translate = function(amount) {
		this.p1.shift(amount);
		this.p2.shift(amount);
		return this;
	};

	/*
	* Create a deep copy of this line
	* @return:Line2D - A deep copy of this line
	*/
	Line2D.prototype.clone = function() {
		return new Line2D(this.p1.clone(), this.p2.clone());
	};

	/*
	* Get a string representation of this Line2D
	* @return:String - A string representation of this Line2D
	*/
	Line2D.prototype.toString = function() {
		return this.p1.toString() + " - " + this.p2.toString();
	};

	/*
	* Set both points of this line at the same time
	* @param x1:Number - X coordinate of the first point
	* @param y1:Number - Y coordinate of the first point
	* @param x2:Number - X coordinate of the second point
	* @param y2:Number - Y coordinate of the second point
	*/
	Line2D.prototype.setAll = function(x1, y1, x2, y2) {
		this.p1.x = x1;
		this.p1.y = y1;
		this.p2.x = x2;
		this.p2.y = y2;
	};

	/*
	* Check if two line segments are equal.
	*/
	Line2D.prototype.equals = function(other, tolerance) {
		return this.p1.equals(other.p1, tolerance) &&  this.p2.equals(other.p2, tolerance);
	};

	/*
	* Get the angle of this line (in radians) with respect to the positive x axis.
	* @return:[Number] - The angle of this line segment in radians, between -PI and PI
	*/
	Line2D.prototype.getAngle = function() {
		return Math.atan2(this.p2.y - this.p1.y, this.p2.x - this.p1.x);
	};

	/**
	 * Draw this line to a graphics object.
	 * @param  {String} colour      			- The colour of the line.
	 * @param  {createjs.Graphics} graphics    	- The graphics object to draw
	 * @param  {Number} strokeWidth 			- The width of stroke to use when drawing.
	 */
	Line2D.prototype.drawToGraphics = function(colour, graphics, strokeWidth) {

		if (ss.isEmpty(strokeWidth)) {
			strokeWidth = 1;
		}

		graphics.setStrokeStyle(strokeWidth);
		graphics.beginStroke(colour);

		graphics.moveTo(this.p1.x, this.p1.y);
		graphics.lineTo(this.p2.x, this.p2.y);
	};

	ss.Line2D = Line2D;

} ());

/* global require, ss */




(function() {
	"use strict";

	/**
	 * Class representing an axis aligned rectangle.
	 * @param {Number} x - The x coordinate of the top left point in the rectangle.
	 * @param {Number} y - The y xoordinate of the top left point in the rectangle.
	 * @param {Number} w - The width of the rectangle.
	 * @param {Number} h - The height of the rectangle.
	 */
	ss.Rectangle = function(x, y, w, h) {
		this.x = x;
		this.y = y;
		this.width = w;
		this.height = h;
	};

	ss.Rectangle.prototype.constructor = ss.Rectangle;

	/**
	 * Get the maximum x coordinate of the rectangle.
	 * @return {Number} The maximum X coordinate.
	 */
	ss.Rectangle.prototype.getMaxX = function() {
		return Math.max(this.x, (this.x + this.width));
	};

	/*
	* Get the maximum y coordinate of the rectangle.
	* @return {Number} The maximum y coordinate of the rectangle.
	*/
	ss.Rectangle.prototype.getMaxY = function() {
		return Math.max(this.y, (this.y + this.height));
	};

	/*
	* Get the minimum x coordinate of the rectangle.
	* @return {Number} The minimum y coordinate of the rectangle.
	*/
	ss.Rectangle.prototype.getMinX = function() {
		return Math.min(this.x, (this.x + this.width));
	};

	/*
	* Get the minimum y coordinate of the rectangle.
	* @return {Number} the minimum y coordinate of the rectangle.
	*/
	ss.Rectangle.prototype.getMinY = function() {
		return Math.min(this.y, (this.y + this.height));
	};

	/*
	* Get the width of the rectangle.
	* @return {Number} the width of the rectangle.
	*/
	ss.Rectangle.prototype.getWidth = function() {
		return this.width;
	};

	/*
	* Get the Height of the rectangle.
	* @return {Number} the width of the rectangle.
	*/
	ss.Rectangle.prototype.getHeight = function() {
		return this.height;
	};

	/**
	 * Get the center of the rectangle.
	 * @return {ss.Vector2} - The center of the rectangle
	 */
	ss.Rectangle.prototype.getCenter = function() {
		var centerX = (this.getMinX() + this.getMaxX()) / 2;
		var centerY = (this.getMinY() + this.getMaxY()) / 2;
		return new ss.Vector2(centerX, centerY);
	};

	ss.Rectangle.prototype.translate = function(amount) {
		this.x += amount.x;
		this.y += amount.y;
	};

	/*
	* Perform a hit test on the rectangle, to see if a point is within it.
	* @param point {Vector2}: the point to test against the rectangle.
	* @returns {Boolean}: true if the point is in the rectangle, false otherwise.
	*/
	ss.Rectangle.prototype.hitTest = function(point) {

		if(typeof point !== 'object' || (!("x" in point) ||  !("y" in point))) {
			console.error("Rectangle hit test requires a Vector2 as its parameter");
			return false;
		}

		if (this.getMinX() > point.x) {
			return false;
		}
		if (this.getMaxX() < point.x) {
			return false;
		}
		if (this.getMinY() > point.y) {
			return false;
		}
		if (this.getMaxY() < point.y) {
			return false;
		}
		// we passed al the test, we're inside the rectangle!
		return true;
	};


	/**
	 * Check if this rectangle intersects another rectangle
	 * @param  {ss.Rectangle}  other - The other rectangle.
	 * @return {Boolean}       - True if the rectangles are intersecting, false otherwise.
	 */
	ss.Rectangle.prototype.isIntersectingRectangle = function(other) {

		// check if there's an overlap in the horizontal span.
		if (this.getMinX() < other.getMinX()) {
			if (other.getMinX() - this.getMaxX() > 0) {
				return false;
			}
		} else {
			if (this.getMinX() - other.getMaxX() > 0 ) {
				return false;
			}
		}

		// check if there's an overlap in the vertical span.
		if (this.getMinY() < other.getMinY()) {
			if (other.getMinY() - this.getMaxY() > 0) {
				return false;
			}
		} else {
			if (this.getMinY() - other.getMaxY() > 0 ) {
				return false;
			}
		}

		// if both, we have a hit.
		return true;
	};

	ss.Rectangle.prototype.debugDraw = function(shape, outlineColour, fillColour) {
		if (ss.isEmpty(shape)) {
			return;
		}

		if (ss.isEmpty(outlineColour)) {
			outlineColour = "#FFFFFF";
		}

		if (ss.isEmpty(fillColour)) {
			fillColour = "rgba(255,255,255,0.25)";
		}

		shape.graphics.clear();

		// draw the outline.
		shape.graphics.setStrokeStyle(1);
		shape.graphics.beginStroke(outlineColour);
		shape.graphics.beginFill(fillColour);
		shape.graphics.moveTo(this.x, this.y);
		shape.graphics.lineTo(this.x + this.width, this.y);
		shape.graphics.lineTo(this.x + this.width, this.y + this.height);
		shape.graphics.lineTo(this.x, this.y + this.height);
		shape.graphics.lineTo(this.x, this.y);
	};

} ());

/*global require, ss */






(function() {
	"use strict";

	/**
	* Class representing a polygon.
	**/
	function Polygon (points) {

		/**
		* An array containing the points that surround this polygon.
		**/
		this.points = points;

		this.convexHull = undefined;

		this._i = 0;

		this.aabb = new ss.Rectangle(0, 0, 0, 0);
		this.aabbValid = false;

	}

	/**
	 * Helper function to build a polygon from an array of 2d arrays representing each point (as opposed to an arry of vectors).
	 * @param  {Array} pointArray - The input array.
	 * @return {[type]}           - The polygon built from those points.
	 */
	Polygon.fromPointArray = function(pointArray) {
		var vectorList = [];
		for(var i = 0; i < pointArray.length; i++) {
			var newVector = new ss.Vector2(pointArray[i][0], pointArray[i][1]);
			vectorList.push(newVector);
		}
		return new Polygon(vectorList);
	};

	Polygon.prototype.constructor = Polygon;

	/**
	 * Whether or not a given edge intersects a ray cast to the left from a given point.
	 * @param  {ss.Vector2}  point     The point to test.
	 * @param  {ss.Vector2}  edgeVert1 The 1st vertex of the edge you're testing.
	 * @param  {ss.Vector2}  edgeVert2 The 2nd vertex of the edge you're testing.
	 * @return {Boolean}           	True if the ray intersects the edge, false otherwise.
	 */
	Polygon.prototype._isRayToLeftOfPointIntersectingEdge = function(point, edgeVert1, edgeVert2) {
		// special case if line is vertical.
		if ((edgeVert2.x - edgeVert1.x) === 0) {
			// check that the line is left of the point.
			if (point.x >= edgeVert1.x) {
				// we intersected the vertical line, so return whether we're within bounds of the line segment.
			 	return ((edgeVert1.y <= point.y && edgeVert2.y >= point.y) || (edgeVert1.y >= point.y && edgeVert2.y <= point.y));
			} else {
				return false;
			}
		}

		// find the slope, if the line isn't vertical.
		var m = (edgeVert2.y - edgeVert1.y) / (edgeVert2.x - edgeVert1.x);
		// find the y-intercept
		var b = -m * edgeVert1.x + edgeVert1.y;

		// determine where x would hit.
		var x = (point.y - b) / m;
		var y = m * x + b;

		if ((edgeVert1.y <= y && edgeVert2.y >= y) || (edgeVert1.y >= y && edgeVert2.y <= y)) {
			// check that we're on the line.

				if (point.x >= x) {
					// left of point.
					return true;
				}
		}

		return false;
	};

	/**
	 * Checks whether a given point is in this polygon.
	 * @param  {ss.Vector2}  point - The point to test.
	 * @return {Boolean}       True if the point is in the polygon, false otherwise.
	 */
	Polygon.prototype.isPointInPolygon = function(point) {
		var hits = 0;

		// loop through the lines in the polygon.
		for(this._i = 0; this._i < this.points.length - 1; this._i++) {
			if (this._isRayToLeftOfPointIntersectingEdge(point, this.points[this._i], this.points[this._i + 1])) {
				hits += 1;
			}
		}

		// check the last line segment
		if (this._isRayToLeftOfPointIntersectingEdge(point, this.points[this.points.length - 1], this.points[0])) {
			hits += 1;
		}

		// return true if an odd number of hits, false if even.
		return (hits % 2 !== 0);
	};


	/**
	 * Returns another polygon which represents the convex hull of this polygon.
	 * Adapted from: https://github.com/mgomes/ConvexHull/blob/master/convex_hull.js
	 * @return {ss.Polygon} - The convex hull of this polygon.
	 */
	Polygon.prototype.getConvexHull = function() {
		// since this is fairly expensive, cache the operation.
		if (this.convexHull !== undefined) {
			return this.convexHull;
		}

		/**
		* Checks if a point (p2) is left of an infinite line defined by the points p0 & p1.
		* @param  {ss.Vector2}  p0 - The 1st point on the infinite line.
	 	* @param  {ss.Vector2}  p1 - The 2nd point on the infinite line.
	 	* @param  {ss.Vector2}  p2 - The point to check against the line.
	 	* @return {float}    - <0 if the point is left of the line, >0 if the point is right of the line, 0 if the point is on the line.
	 	*/
		var _isPointLeftOfLine = function(p0, p1, p2) {
			return (p1.x - p0.x) * (p2.y - p0.y) - (p2.x - p0.x) * (p1.y - p0.y);
		};

		// sort the points in lexicographic order.
		this.sortPointsLexicographic();

		// the index of the start of the top hull.
		var bottomIndex = 0;

		// the index of the top of the hull stack.
		var topIndex = -1;
		var i;

		// the index of the minimum & maxmimum point who share the minimum x value.
		var minmin = 0;
		var minmax;

		var hull = [];

		var n = this.points.length;

		var xmin = this.points[0].x;
		for(i = 1; i < n; i++) {
			if (this.points[i].x != xmin) {
				break;
			}
		}

		minmax = i - 1;
		if (minmax == n - 1) { // all x-coordinates are the same..
			hull[++topIndex] = this.points[minmin];
			if (this.points[minmax].x != this.points[minmin].x) {
				hull[++topIndex] = this.points[minmax];
			}
			hull[++topIndex] = this.points[minmin];

			this.convexHull = new ss.Polygon(hull);
			return this.convexHull;
		}

		// Get the indices of points with max x-coord and min|max y-coordinate.
		var maxmin = n - 1;
		var maxmax = n - 1;
		var xmax = this.points[n - 1].x;
		for(i = n - 2; i >= 0; i--) {
			if (this.points[i].x != xmax) {
				break;
			}
		}
		maxmin = i + 1;

		// compute the lower hull on the stack hull.
		hull[++topIndex] = this.points[minmin]; // push minmin point onto stack.
		i = minmax;
		while(++i <= maxmin) {
			// the lower line joins P[minmin] with P[maxmin]
			if (_isPointLeftOfLine(this.points[minmin], this.points[maxmin], this.points[i]) >= 0 && i < maxmin) {
				continue; // ignore P[i] above or on the lower line.
			}

			while(topIndex > 0) { // there are at least 2 points on the stack.
				// test if P[i] is left of the line at the stack top.
				if (_isPointLeftOfLine(hull[topIndex - 1], hull[topIndex], this.points[i]) > 0) {
					break;
				} else {
					topIndex--; // pop top point off stack.
				}
			}

			hull[++topIndex] = this.points[i];

		}

		// Next, compute the upper hull on the stack H above the bottom hull
		if (maxmax != maxmin) { // if distinct xmax points
			hull[++topIndex] = this.points[maxmax];
		}

		bottomIndex = topIndex; // the bottom point of the upper hull stack.
		i = maxmin;
		while(--i >= minmax) {
			// the upper line joins P[maxmax] with P[minmax].
			if (_isPointLeftOfLine(this.points[maxmax], this.points[minmax], this.points[i]) >= 0 && i > minmax) {
				continue; // ignore P[i] below or on the upper line.
			}

			while(topIndex > bottomIndex) { // at least 2 points on the upper stack.
				// test if P[i] is left of the line at the stack top.
				if (_isPointLeftOfLine(hull[topIndex - 1], hull[topIndex], this.points[i]) > 0) {
					break; // P[i] is a new hull vertex.
				} else {
					topIndex--;
				}
			}

			/*if (this.points[i].x == hull[0].x && this.points[i].y == hull[0].y) {
				this.convexHull = new ss.Polygon(hull);
				return this.convexHull;
	        }*/

			hull[++topIndex] = this.points[i];
		}

		if (minmax != minmin) {
			hull[++topIndex] = this.points[minmin]; // push joining endpoint onto the stack.
		}

		this.convexHull = new ss.Polygon(hull);
		return this.convexHull;
	};

	/**
	 * Project the polygon onto a given axis (vector2) to recieve a min and max.
	 * @param  {ss.Vector2} axis - The axis to project onto.
	 * @return {array}      - Array containing [min,max] , the projection onto the axis.
	 */
	Polygon.prototype.projectOntoAxis = function(axis) {
		var dotProduct = this.points[0].dot(axis);
		var min = dotProduct;
		var max = dotProduct;

		for(var i = 0; i < this.points.length; i++) {
			dotProduct = this.points[i].dot(axis);
			if (dotProduct < min) {
				min = dotProduct;
			} else if (dotProduct > max) {
				max = dotProduct;
			}
		}

		return [ min, max ];
	};

	/**
	 * Get an axis aligned bounding box for this polygon.
	 * @return {ss.Rectangle} - The AABB for the polygon.
	 */
	Polygon.prototype.getAABB = function() {

		// if we already have a valid cached AABB, return that instead of doing the calculation.
		if (this.aabbValid) {
			return this.aabb;
		}

		// if we don't have any points in the polygon, just return an empty rectangle centered at the origin.
		if (ss.isEmpty(this.points) || this.points.length < 1) {
			return new ss.Rectangle(0, 0, 0, 0);
		}

		// track the edges of the AABB.
		var minX, minY, maxX, maxY;

		// to start, set the edges of the AABB to the first point in the polygon.
		minX = this.points[0].x;
		maxX = this.points[0].x;
		minY = this.points[0].y;
		maxY = this.points[0].y;

		// loop through all the points, to push the edges to the proper positions.
		for(var i = 0; i < this.points.length; i++) {
			if (this.points[i].x < minX) {
				// push the minimum X;
				minX = this.points[i].x;
			}

			if (this.points[i].x > maxX) {
				// push the maximum X.
				maxX = this.points[i].x;
			}

			if (this.points[i].y < minY) {
				// push the minimum Y.
				minY = this.points[i].y;
			}

			if (this.points[i].y > maxY) {
				// push the maximum Y.
				maxY = this.points[i].y;
			}
		}

		// set the values on the AABB to what we calculated above.
		this.aabb.x = minX;
		this.aabb.y = minY;
		this.aabb.width = maxX - minX;
		this.aabb.height = maxY - minY;
		this.aabbValid = true;

		return this.aabb;
	};

	/**
	 * Translate this polygon.
	 * @param  {ss.Vector2} amount - The amount to translate it by.
	 * @return {}        [description]
	 */
	Polygon.prototype.translate = function(amount) {
		var i;
		// translate points.
		for(i = 0; i < this.points.length; i++) {
			this.points[i].addSelf(amount);
		}
		// the edges && hull reference the points themselves, so we don't need to do anything to them here.

		// update the aabb.
		this.aabb.translate(amount);
	};

	/**
	 * Rotate this polygon.
	 * @param  {Number} amount - The amount to rotate, in degrees.
	 * @param  {ss.Vector2} around - The point to rotate around.
	 */
	Polygon.prototype.rotate = function(amount, around) {
		if (around === undefined) {
			around = this.getCenter();
		}

		var angle = amount * ss.MathConst.DEG_TO_RAD;
		var cosA = Math.cos(angle);
		var sinA = Math.sin(angle);

		// used for tracking of the AABB (used as a coarse check for collision detection.)
		var minX = Number.MAX_VALUE;
		var minY = Number.MAX_VALUE;
		var maxX = -Number.MAX_VALUE;
		var maxY = -Number.MAX_VALUE;

		for(var i = 0; i < this.points.length; i++) {
			this.points[i].subtractSelf(around);

			// use the precalculated cos and sin values to rotate each point
			// (we could call rotateSelf on the vector, but that would result in the values being re-computed for each point,
			//  and this way should be faster).
			this.points[i].setAll(this.points[i].x * cosA - this.points[i].y * sinA, this.points[i].x * sinA + this.points[i].y * cosA);

			this.points[i].addSelf(around);

			// START updating the AABB.
			if (this.points[i].x < minX) {
				minX = this.points[i].x;
			}

			if (this.points[i].x > maxX) {
				maxX = this.points[i].x;
			}

			if (this.points[i].y < minY) {
				minY = this.points[i].y;
			}

			if (this.points[i].y > maxY) {
				maxY = this.points[i].y;
			}
			// END updating the AABB.
		}

		// the edges && hull reference the points themselves, so we don't need to do anything here.

		// set aabb values to the new results.
		this.aabbValid = true;
		this.aabb.x = minX;
		this.aabb.y = minY;
		this.aabb.width = maxX - minX;
		this.aabb.height = maxY - minY;
	};

	/**
	 * Scale this polygon.
	 * @param  {Number} amount - The amount to scale by, as a factor of the scale of the polygon.
	 * @param  {ss.Vector2} around - The point on the polygon to scale around.
	 */
	Polygon.prototype.scale = function(amount, around) {
		if (around === undefined) {
			around = this.getCenter();
		}

		// used for tracking of the AABB (used as a coarse check for collision detection.)
		var minX = Number.MAX_VALUE;
		var minY = Number.MAX_VALUE;
		var maxX = -Number.MAX_VALUE;
		var maxY = -Number.MAX_VALUE;

		for(var i = 0; i < this.points.length; i++) {
			this.points[i].subtractSelf(around);
			this.points[i].scaleSelf(amount);
			this.points[i].addSelf(around);

			// START updating the AABB.
			if (this.points[i].x < minX) {
				minX = this.points[i].x;
			}

			if (this.points[i].x > maxX) {
				maxX = this.points[i].x;
			}

			if (this.points[i].y < minY) {
				minY = this.points[i].y;
			}

			if (this.points[i].y > maxY) {
				maxY = this.points[i].y;
			}
			// END updating the AABB.
		}

		// for now, just invalidate the aabb, so it'll be recalculated, we may want to speed this up later.
		this.aabbValid = true;
		this.aabb.x = minX;
		this.aabb.y = minY;
		this.aabb.width = maxX - minX;
		this.aabb.height = maxY - minY;
	};

	/**
	 * Get the edges of this polygon.
	 * @return {Array{Line2D}} - An array of line2D objects representing the edges.
	 */
	Polygon.prototype.getEdges = function() {
		// cache the results.
		if (this.edges !== undefined) {
			return this.edges;
		}

		var edges = [];
		for(var i = 0 ;i < this.points.length - 1; i++) {
			edges.push (new ss.Line2D (
					this.points[i],
					this.points[i + 1]
				)
			);
		}
		edges.push (new ss.Line2D (
				this.points[this.points.length - 1],
				this.points[0]
			)
		);
		this.edges = edges;
		return this.edges;
	};

	/**
	 * Check if this polygon's convex hull is colliding with a given circle.
	 * NOTE: the convex hull will be calculated if not available for the polygon prior to performing the calculation,
	 * So this may be slower 1st frame than subsequent frames.
	 * @param  {ss.Circle} circle - The circle to check against.
	 * @return {Boolean}          - True if the objects are colliding, false otherwise.
	 */
	Polygon.prototype.checkCircleCollision = function(circle) {
		var hull = this.getConvexHull();

		var edges = hull.getEdges();

		var polyProjection;
		var circleProjection;
		var axis;
		var edge;

		for(var edgeIndex = 0; edgeIndex < edges.length; edgeIndex++) {
			edge = edges[edgeIndex];
			if (edge.getLength() > 0) {
				// get an axis perpendicular to each edge.
				axis = edge.getNormal();

				polyProjection = hull.projectOntoAxis(axis);
				circleProjection = circle.projectOntoAxis(axis);

				if (this._intervalDistance(polyProjection, circleProjection) > 0) {
					return false;
				}
			}
		}

		var centerAxis = new ss.Vector2(circle.x, circle.y).subtract(hull.getCenter());

		polyProjection = hull.projectOntoAxis(centerAxis);
		circleProjection = circle.projectOntoAxis(centerAxis);

		if (this._intervalDistance(polyProjection, circleProjection) > 0) {
			return false;
		}

		return true;
	};

	/**
	 * Check if this polygon's convex hull is colliding with the other polygon's convex hull.
	 * NOTE: the convex hulls will be calculated if not available for both polygons prior to performing the calculation,
	 * So this may be slower 1st frame than subsequent frames.
	 * @param  {ss.Polygon} otherPolygon - The other polygon.
	 * @return {Boolean}              - True if we have a collision, false otherwise.
	 */
	Polygon.prototype.checkPolygonCollision = function(otherPolygon) {

		// first do a coarse check using the AABB of each polygon, if that fails, we don't need to do the more expensive check.
		if (!this.getAABB().isIntersectingRectangle(otherPolygon.getAABB())) {
			// NOTE: this will actually be slower if we expect the polygons to collide,
			// but in most cases, we don't expect them to collide, so this is a good optimization to make.
			// for most game-dev scenarios.
			return false;
		}

		// Get the convex hull of polygon A. (this polygon)
		var polygonA = this.getConvexHull();
		// Get the convex hull of polygon B. (the other polygon)
		var polygonB = otherPolygon.getConvexHull();

		// get edges from the two convex hulls.
		var edgesA = polygonA.getEdges();
		var edgesB = polygonB.getEdges();

		// make a list of all the edges (makes looping through them a bit cleaner and easier,
		// and it's rare to have enough edges for this to be a big performance problem).
		// if this becomes an issue later, we can tweak the loop to loop through the two edge lists seperately.
		var allEdges = edgesA.concat(edgesB);

		// put the variable declarations up here, so we're being explicit about their scoping.
		var edge;
		var axis;
		var aProjection;
		var bProjection;

		// loop through all the edges.
		for(var edgeIndex = 0; edgeIndex < allEdges.length; edgeIndex++) {
			edge = allEdges[edgeIndex];

			if (edge.getLength() > 0) {
				// get an axis perpendicular to each edge.
				axis = edge.getNormal();

				// project the two polygons onto the axis.
				aProjection = polygonA.projectOntoAxis(axis);
				bProjection = polygonB.projectOntoAxis(axis);

				// check if the two projections overlap, and if they don't we can
				// short-circuit right here, because we know the two polygons aren't colliding.
				if (this._intervalDistance(aProjection, bProjection) > 0) {
					return false;
				}
			}
		}
		return true;
	};

	/**
	 * Gets the distance between intervalA [min, max] and intervalB [min, max].
	 * @param  {Array} intervalA - The 1st interval in the test.
	 * @param  {Array} intervalB - The 2nd interval in the test.
	 * @return {Number}          - The signed distance between the 2 given projections. The distance will be negative if the intervals overlap.
	 */
	Polygon.prototype._intervalDistance = function(intervalA, intervalB) {
		if (intervalA[0] < intervalB[0]) {
			return intervalB[0] - intervalA[1];
		} else {
			return intervalA[0] - intervalB[1];
		}
	};



	/**
	* Gets the "center" of the polygon by averaging the positions of all the points that make it up.
	**/
	Polygon.prototype.getCenter = function() {

		if (ss.isEmpty(this.centerPos)) {
			this.centerPos = new ss.Vector2(0, 0);
		} else {
			this.centerPos.x = 0;
			this.centerPos.y = 0;
		}

		for(this._i = 0; this._i < this.points.length; this._i++)
		{
			this.centerPos.addSelf(this.points[this._i]);
		}
		this.centerPos.multSelf(1.0 / this.points.length);
		return this.centerPos;
	};

	/**
	* Gets the "center" of the polygon by averaging the positions of all the points that make it up.
	**/
	Polygon.prototype.getCenterOut = function(out) {

		out.x = 0;
		out.y = 0;

		for(this._i = 0; this._i < this.points.length; this._i++)
		{
			out.addSelf(this.points[this._i]);
		}
		out.multSelf(1.0 / this.points.length);
		return out;
	};



	/**
	* Sort the points in this polygon in clockwise order around their center.
	**/
	Polygon.prototype.sortPointsClockwise = function() {

		// if we already have this sort order cached, just use it.
		if (!ss.isEmpty(this.clockwiseOrdering)) {
			this.points = this.clockwiseOrdering;
			return;
		}

		var center = this.getCenter();

		function compare (a, b) {
			var angleA, angleB;

			angleA = Math.atan2(center.y - a.y, center.x - a.x);
			angleB = Math.atan2(center.y - b.y, center.x - b.x);

			return angleB - angleA;
		}

		this.points.sort(compare);

		this.clockwiseOrdering = this.points.slice(0);

	};

	/**
	 * Sort the points in this polygon by basic lexicographic (y, then x) ordering.
	 * (this is used as a 1st step in calculating the convex hull of the polygon.)
	 */
	Polygon.prototype.sortPointsLexicographic = function() {
		// if we already have this sort order cached, just use it.
		if (!ss.isEmpty(this.lexicographicOrdering)) {
			this.points = this.lexicographicOrdering;
			return;
		}

		this.points.sort(ss.Vector2.compareLexicographic);

		this.lexicographicOrdering = this.points.slice(0);
	};

	/**
	* Draws this polygon to a createjs graphics object.
	**/
	Polygon.prototype.drawToGraphics = function(color, graphics, clear) {
	//	this.sortPointsClockwise();
	
		clear = clear === undefined ? true : clear;
		if(!clear)
		{
			graphics.clear();
		}
		
		graphics.beginFill(color);

		graphics.moveTo(this.points[0].x, this.points[0].y);
		for( var i = 0; i < this.points.length; i++ ) {
			graphics.lineTo(this.points[i].x, this.points[i].y);
		}
		graphics.lineTo(this.points[0].x, this.points[0].y);
		graphics.endFill();
	};

	/**
	 * Release this polygon, used to clear it fully from memory;.
	 */
	Polygon.prototype.release = function() {
		for(this._i = 0; this._i < this.points.length; this._i++) {
			this.points[this._i] = undefined;
		}
		this.aabb = undefined;
		this.aabbValid = undefined;
		this.points = undefined;
		this._i = undefined;
		this.edges = undefined;
		this.convexHull = undefined;
		this.centerPos = undefined;
		this.lexicographicOrdering = undefined;
		this.clockwiseOrdering = undefined;
	};

	ss.Polygon = Polygon;
} ());

/* global require, ss*/






(function() {
	"use strict";

	ss.Quad = function(p1, p2, p3, p4) {
		this.p1 = ss.isUndefined(p1) ? new ss.Vector2(0, 0) : p1;
		this.p2 = ss.isUndefined(p2) ? new ss.Vector2(0, 0) : p2;
		this.p3 = ss.isUndefined(p3) ? new ss.Vector2(0, 0) : p3;
		this.p4 = ss.isUndefined(p4) ? new ss.Vector2(0, 0) : p4;

		return this;
	};

	ss.Quad.prototype.constructor = ss.Quad;

	/*
	* Gets the center of mass of this quad
	* @return:Vector2 - A point indicating the center of mass of this quad
	*/
	ss.Quad.prototype.getCenter = function() {
		return new ss.Vector2( (this.p1.x + this.p2.x + this.p3.x + this.p4.x) / 4, (this.p1.y + this.p2.y + this.p3.y + this.p4.y) / 4);
	};

	/*
	* Get an axis aligned bounding box for this quad
	* @return:[Rectangle] - A rectangle specifying an axis-aligned bounding box for this quad
	*/
	ss.Quad.prototype.getBounds = function() {
		var minX = Math.min(this.p1.x, this.p2.x, this.p3.x, this.p4.x);
		var minY = Math.min(this.p1.y, this.p2.y, this.p3.y, this.p4.y);

	 	return new ss.Rectangle(minX, minY, Math.max(this.p1.x, this.p2.x, this.p3.x, this.p4.x) - minX, Math.max(this.p1.y, this.p2.y, this.p3.y, this.p4.y) - minY);
	};

}());

/* global require, ss*/


( function() {
	"use strict";

	/*
	* Class TransformData
	*	Holds data describing a transformation for an object
	*/

	/*
	* Create a new TransformData object
	*/
	function TransformData (locX, locY, scaleX, scaleY, rotation, skewX, skewY, regX, regY) {
		// Set provided values or default values for all properties
		this.locX = typeof locX !== undefined ? locX : 0;
		this.locY = typeof locY !== undefined ? locY : 0;
		this.scaleX = typeof scaleX !== undefined ? scaleX : 1;
		this.scaleY = typeof scaleY !== undefined ? scaleY : 1;
		this.rotation = typeof rotation !== undefined ? rotation : 0;
		this.skewX = typeof skewX !== undefined ? skewX : 0;
		this.skewY = typeof skewY !== undefined ? skewY : 0;
		this.regX = typeof regX !== undefined ? regX : 0;
		this.regY = typeof regY !== undefined ? regY : 0;

		// TODO: Function to set all values at once

	}

	TransformData.prototype.constructor = TransformData;

	ss.TransformData = TransformData;

} ());

/* global require, ss */




(function() {
	"use strict";

	/*
	* Class representing a bezier curve, defined by a set of control points.
	* @param points: An array of ss.Vector2s representing the control points of the Bezier curve, in order.
	*/
	function Bezier (points) {

		// variable for iterating in loops for this class.
		this._i = 0 ;

		// the list of points.
		this._points = points;
		this._pointsCopy = [];

		for(this._i = 0; this._i < points.length; this._i++) {
			this._pointsCopy.push(this._points[this._i].clone());
		}

		// temporary out vector used in calculations.
		this._tempOut = new ss.Vector2(0, 0);
	}

	Bezier.prototype.constructor = Bezier;

	/*
	* Get the list of points in the curve.
	*/
	Bezier.prototype.getPoints = function() {

		return this._points;
	};

	/*
	* Set the list of points in the curve.
	*/
	Bezier.prototype.setPoints = function(points) {

		this._points = points;
		this._pointsCopy = [];
		for(this._i = 0; this._i < this._points.length; this._i++) {
			this._pointsCopy.push(this._points[this._i].clone());
		}
	};

	/**
	 * Make a copy of the points.
	 * @param  {Array} points - The points to copy.
	 * @param  {Array} out   - Where to put them
	 */
	Bezier.prototype.copyPoints = function(points, out) {

		for(this._i = 0; this._i < points.length; this._i++) {
			points[this._i].copyOut(out[this._i]);
		}
	};

	/*
	* Set the point at a given index in the points list.
	*/
	Bezier.prototype.setPointAtIndex = function(point, index) {

		this._points[index] = point;
	};

	/**
	 * Get the approximate tangent to the curve at a given location.
	 * NOTE: this tangent will point in the direction of the curve from 0->1.
	 * @param  {Number} t     	A value in the 0-1 range for how far along the curve to evaluate.
	 * @param  {Number} range 	The distance away from the given point to sample the points for the tangent.
	 * @return {ss.Vector2}   	A normalized Vector2 which is the approximate tangent to the curve at the given location.
	 */
	Bezier.prototype.getApproximateTangent = function(t, range) {
		if (typeof range == "undefined") {
			range = 0.000005;
		}

		var t1 = t - range;
		var t2 = t + range;

		var point1 = this.evaluate(t1).clone();
		var point2 = this.evaluate(t2).clone();

		var tan = point2.subtract(point1);
		tan.normalize();

		return tan;
	};

	/**
	 * Get the approximate length of a part of, or the entire bezier curve.
	 * @param  {Number} start    (optional)   	Number in the 0 to 1 range, for the start of the segment of the curve to measure.
	 * @param  {Number} end    (optional)    	Number in the 0 to 1 range, for the end of the segment of the curve to measure.
	 * @param  {Number} resolution (optional)   The distance along the curve to do each linera interpolation step when determining the measurement.
	 * @return {Number}            				The length of the given part of the curve.
	 */
	Bezier.prototype.getApproximateLength = function(start, end, resolution) {
		if (typeof start == "undefined") {
			start = 0;
		}

		if (typeof end == "undefined") {
			end = 1;
		}

		if (typeof resolution == "undefined") {
			resolution = 0.005;
		}

		var totalDistance = 0;

		for(var i = start; i <= end; i += resolution) {
			var startT = ss.MathUtils.clamp01(i);
			var nextT = ss.MathUtils.clamp01(i + resolution);

			var point1 = this.evaluate(startT).clone();
			var point2 = this.evaluate(nextT).clone();

			totalDistance += point2.subtract(point1).magnitude();
		}

		return totalDistance;
	};

	/*
	* Evaluate the curve at a point.
	*/
	Bezier.prototype.evaluate = function(t) {

		this.copyPoints(this._points, this._pointsCopy);
		return this._evaluateRecursive(this._pointsCopy, t, this._pointsCopy.length);
	};

	/*
	* Recursive function to evaluate the curve.
	*/
	Bezier.prototype._evaluateRecursive = function(points, t, length) {

		if (length === 0) {
			return new ss.Vector2(0, 0);
		}

		if (length === 1) {
			return points[0];
		}

		if (length === 2) {
			// do a liner interpolation.
			ss.MathUtils.lerpOut(points[0], points[1], t, this._tempOut);
			return this._tempOut;
		}

		if (length === 3) {
			// use the formula for a quadratic beizer curve.
			this._tempOut.x = (points[0].x * (1.0 - t) * (1.0 - t))	+ (points[1].x * 2.0 * (1.0 - t) * t)	+ (points[2].x * t * t);

			this._tempOut.y = (points[0].y * (1.0 - t) * (1.0 - t)) + (points[1].y * 2.0 * (1.0 - t) * t)	+ (points[2].y * t * t);

			return this._tempOut;
		}

		if (length === 4) {

			// use the formula for a cubic beizer curve.
			this._tempOut.x = (points[0].x * (1.0 - t) * (1.0 - t) * (1.0 - t)) +
				(points[1].x * 3.0 * (1.0 - t) * (1.0 - t) * t) +
				(points[2].x * 3.0 * (1.0 - t) * t * t)	+ (points[3].x * t * t * t);

			this._tempOut.y = (points[0].y * (1.0 - t) * (1.0 - t) * (1.0 - t)) +
				(points[1].y * 3.0 * (1.0 - t) * (1.0 - t) * t) +
				(points[2].y * 3.0 * (1.0 - t) * t * t)	+ (points[3].y * t * t * t);

			return this._tempOut;
		}

		// Since were at a higher degree than cubic, time to recurse!
		for(this._i = 0; this._i < length - 1; this._i++) {
			ss.MathUtils.lerpOut(points[this._i], points[this._i + 1], t, this._tempOut);
			this._tempOut.copyOut(points[this._i]);
		}

		return this._evaluateRecursive(points, t, length - 1);

	};

	Bezier.prototype.release = function() {
		this._points = undefined;
		this._pointsCopy = undefined;
		this._i = undefined;
		this._tempOut = undefined;
	};

	ss.Bezier = Bezier;

}());

/* global require, ss */




(function() {
	"use strict";

	BezierPath.prototype = {};
	BezierPath.prototype.constructor = BezierPath;

	/**
	 * Class representing a list of connected bezier curves, loaded from a point array.
	 */
	function BezierPath ( pointArray ) {
		// keep a locally scoped copy of this.
		var _this = this;

		// The reolution (in percent along the path) to use to calculate approximate tangents and lengths.
		var _resolution = 0.01;

		// The sub-curves along the path.
		var _curves = [];

		// The lengths of each of the sub-curves along the path.
		var _lengths = [];

		// The lengths of the segments, normalized across the whole path.
		var _normalizedLengths = [];

		// The approximate total length of the curve.
		var _totalApproximateLength;

		// Temp iterator variable, used by loops within the class.
		// NOTE: this is stored to avoid costing extra memory to evaluate the curves.
		var _i;

		// Temp variable used in calculations of how far along the path we are.
		var _tRelative;

		// Temp variable used in calculations of how far along the path we are.
		var _segmentIndex;

		// Whether or not the path is flipped.
		var _isFlipped = false;

		/**
		 * Set the isFlipped variable.
		 * @param {Boolean} isFlipped - Whether or not the path is flipped.
		 */
		_this.setFlipped = function(isFlipped) {
			_isFlipped = isFlipped;
		};

		/**
		 * Create a path from a point array.
		 * @param  {Array} pointArray  - The array of points to use to create the curve.
		 */
		_this.createFromPointArray = function(pointArray) {
			for(_i = 0; _i < pointArray.length; _i++) {
				var newCurvePoints = [];
				for(var j = 0; j < pointArray[_i].length; j++) {
					newCurvePoints.push(new ss.Vector2(pointArray[_i][j][0], pointArray[_i][j][1]));
				}
				var newCurve = new ss.Bezier(newCurvePoints);
				_curves.push(newCurve);
				_lengths.push(newCurve.getApproximateLength(0, 1, _resolution));
			}

			_totalApproximateLength = 0;
			for(_i = 0; _i < _lengths.length; _i++) {
				_totalApproximateLength += _lengths[_i];
			}

			for(_i = 0; _i < _lengths.length; _i++) {
				_normalizedLengths.push(_lengths[_i] / _totalApproximateLength);
			}
		};

		/**
		 * Evaluate the curve at a given percent along it.
		 * @param  {Number} t 	  - How far along the curve you are, as a value from zero to 1.
		 * @return {ss.Vector2}   - The point on the curve at the given t value.
		 */
		_this.evaluate = function(t) {
			// determine the segment we're in.
			if (_isFlipped) {
				t = 1 - t;
			}
			_segmentIndex = _this.getSegmentIndex(t);
			return _curves[_segmentIndex].evaluate(_tRelative / _normalizedLengths[_i]);
		};

		/**
		 * Get the index of the segment at a given point along the curve.
		 * @param  {Number} t - How far along the curve you are.
		 * @return {Number}   - The index in the segments array pointing to the segment at the given index.
		 */
		_this.getSegmentIndex = function(t) {
			_tRelative = t;
			_segmentIndex = 0;
			for(_i = 0; _i < _normalizedLengths.length; _i++) {
				if (_tRelative <= _normalizedLengths[_i]) {
					_segmentIndex = _i;
					break;
				} else {
					_tRelative -= _normalizedLengths[_i];
				}
			}
			return _segmentIndex;
		};

		/**
		 * Get the approximate tangent to the curve at a given time t.
		 * @param  {Number} t - How far along the curve, as a value from 0-1.
		 * @return {ss.Vector2}   - The approximate tangent along the curve.
		 */
		_this.getApproximateTangent = function(t) {
			_tRelative = t;
			_segmentIndex = 0;
			for(_i = 0; _i < _normalizedLengths.length; _i++) {
				if (_tRelative < _normalizedLengths[_i]) {
					_segmentIndex = _i;
					break;
				} else {
					_tRelative -= _normalizedLengths[_i];
				}
			}
			return _curves[_segmentIndex].getApproximateTangent(_tRelative / _normalizedLengths[_i]);
		};

		/**
		 * Draw this curve to a graphics object.
		 * @param  {Color} colour       		   - The colour to draw the curve.
		 * @param  {createjs.Graphics} graphics    - The graphics object to draw to.
		 * @param  {Number} strokeWidth (optional) - The width of stroke to use to draw the curve.
		 */
		_this.drawToGraphics = function(colour, graphics, strokeWidth) {

			if (ss.isEmpty(strokeWidth)) {
				strokeWidth = 1;
			}

			graphics.setStrokeStyle(strokeWidth);
			graphics.beginStroke(colour);

			for(var i = 0; i < _curves.length; i++) {
				var points = _curves[i].getPoints();
				if (points.length == 4) {
					graphics.moveTo(points[0].x, points[0].y);
					graphics.bezierCurveTo (points[1].x, points[1].y, points[2].x, points[2].y, points[3].x, points[3].y);
				} else {
					graphics.moveTo(points[0].x, points[0].y);
					graphics.lineTo(points[1].x, points[1].y);
				}
			}

			graphics.endStroke();
		};

		/*
		* Get the approximate length of the curve.
		* @return The approximate length of the curve.
		**/
		_this.getApproximateLength = function() {
			return _totalApproximateLength;
		};

		/**
		 * Clear out the memory used by this curve.
		 */
		_this.release = function() {
			_tRelative = undefined;
			_segmentIndex = undefined;

			for(_i = 0; _i < _curves.length; _i++) {
				_curves[_i].release();
				_curves[_i] = undefined;
				_lengths[_i] = undefined;
				_normalizedLengths[_i] = undefined;
			}

			_i = undefined;
			_curves = undefined;
			_lengths = undefined;
			_normalizedLengths = undefined;
			_resolution = undefined;
			_totalApproximateLength = undefined;
		};

		if ( !ss.isEmpty ( pointArray ) ) {
			_this.createFromPointArray ( pointArray );
		}
		
	}

	ss.BezierPath = BezierPath;
}());

/* global require, ss */



( function() {
	"use strict";

	ss.Interpolation = ss.Interpolation || new function() { // jshint ignore:line

		/*
		* Linear interpolation.
		* @param x: input from 0 to 1.
		* @return: a linear interpolation from 0 to 1.
		*/
		this.linear = function(x) {
			return x;
		};

		/*
		* SmoothStep interpolation.
		* @param x: input from 0 to 1.
		* @return: a smoothstep interpolation from 0 to 1.
		*/
		this.smoothStep = function(x) {
			return x * x * (3 - 2 * x);
		};

		/*
		* SmoothStep interpolation with 2 iterations.
		* @param x: input from 0 to 1.
		* @return: a smoothstep interpolation from 0 to 1 with 2 iterations.
		*/
		this.smoothStepX2 = function(x) {
			x = x * x * (3 - 2 * x);
			return x * x * (3 - 2 * x);
		};

		/*
		* SmoothStep interpolation with 3 iterations.
		* @param x: input from 0 to 1.
		* @return: a smoothstep interpolation from 0 to 1 with 3 iterations.
		*/
		this.smoothStepX3 = function(x) {
			x = x * x * (3 - 2 * x);
			x = x * x * (3 - 2 * x);
			return x * x * (3 - 2 * x);
		};

		/*
		* Square interpolation.
		* @param x input from 0 to 1.
		* @return: the number squared.
		*/
		this.squared = function(x) {
			return x * x;
		};

		/*
		* Inverse square interpolation.
		* @return: the inverse square of the number.
		*/
		this.inverseSquared = function(x) {
			return 1 - (1 - x) * (1 - x);
		};

		/*
		* Sin wave interpolation.
		* @return: interpolation along the portion of a sin wave that rises from a trough to a crest.
		*/
		this.sin = function(x) {
			return 0.5 - Math.cos(-x * Math.PI) * 0.5;
		};

		/**
		 * Always return 1.
		* @return: 1
		 */
		this.one = function(x) { // jshint ignore:line
			return 1;
		};

		/**
		 * Always return 0;
		 * @return: 0
		 */
		this.zero = function(x) {// jshint ignore:line
	 		return 0;
	 	};

		/*
		* Bounce twice interpolation
		* @return: two bounces after drop
		*/
		this.easeBounceSmall = function(t) {
			var cut1, cut2, bounceHeight1, bounceHeight2, newT, fasterT, cosT, bounceT;

			cut1 = 0.6;
			cut2 = 0.85;
			bounceHeight1 = 0.05;
			bounceHeight2 = 0.02;

			if ( t < cut1 ) {

				// first movement - drop down
				// scale t to move all the way to end position, return 0 to 1
				newT = t * (1 / cut1);

				// speed up
				fasterT = newT * newT;
				return fasterT;

			} else if ( t < cut2 ) {

				// second movement - bounce
				// scale t from between cut1 and cut2 to 0 to 1
				newT = (t - cut1) * (1 / (cut2 - cut1));

				// get 0 to 1 to 0 based on new T
				cosT = Math.abs(Math.cos((newT * 2 - 1) * Math.PI / 2));

				// modify to the first bounce height magnitude
				bounceT = cosT * bounceHeight1;

				// return in respect to 1
				return 1 - bounceT;

			} else {

				// last movement - bounce
				// scale t from after cut2 to 0 to 1
				newT = (t - cut2) * (1 / (1 - cut2));

				// get 0 to 1 to 0 based on new T
				cosT = Math.abs(Math.cos((newT * 2 - 1) * Math.PI / 2));

				// modify to the first bounce height magnitude
				bounceT = cosT * bounceHeight2;

				// return in respect to 1
				return 1 - bounceT;
			}
		};


		/*
		* Evaluate an interpolation for a range.
		* @param min: the min value of the range.
		* @param max: tha max value of the range.
		* @param t: how far along the curve we are (from 0 to 1)
		* @param: type the function to use for the interpolation.
		*/
		this.evaluate = function(min, max, t, type) {
			// cap t in the range of zero to 1.
			if (t > 1) {
				t = 1;
			} else if (t < 0) {
				t = 0;
			}

			// return the result.
			return min + ((max - min) * type(t));
		};
	}; // jshint ignore:line

} ());

/* global require, ss */


(function() {

/**
 * Loop between time 0 to 1, return value 0 to 0.
 */

ss.Loopolation = ss.Loopolation || new function() { // jshint ignore:line
	"use strict";

	// create a locally scoped copy of this.
	var _this = this;


	/*
	* Sin interpolation.
	* @param t input from 0 to 1.
	* @return: 0 to 1 to -1 to 0.
	*/
	_this.easeSine = function(t) {
		return Math.sin((t * 4 - 2) * (Math.PI / 2));
	};

	return _this;
}; // jshint ignore:line

} ());

/* global console, require, ss  */



ss.Distribution = ss.Distribution || new function() { // jshint ignore:line

	"use strict";

	var _this = this;

	// Maximum number of tries to find a before
	var _MAX_TRIES = 100;

	var _range;
	var _testInput;
	var _filterValue;

	/*
	* Select a random vaule from a given distribution function
	* @param distFunc:[Function] - Function defining the distribution to select from
	* @param minInput:[Number] - Minimum possible value that can be selected from the distribution
	* @param maxInput:[Number] - Maximum possible value that can be selected from the distribution
	* @return:[Number] - A value between minInput and maxInput randomly selected based on the
	*						value of the function at each point
	*	NOTE: In general, function values should be between 0 and 1 in the provided range,
	*		and the function shouldn't often evaluate to values close to 0 (low probability)
	*/
	_this.selectFromDistribution = function(distFunc, minInput, maxInput) {
		_range = maxInput - minInput;

		// Choose random input values and filter them until we find a value one
		for(var i = 0; i < _MAX_TRIES; i++) {
			_testInput = minInput + Math.random() * _range;
			_filterValue = Math.random();
			if (_filterValue <= distFunc(_testInput)) {
				return _testInput;
			}
		}

		// Show a warning if no valid value could be found
		console.warn("Good Sir, you have provided a finicky function for your distribution and I couldn't find a valid value!");
		console.log(">>Handy Tip: A nice function produces values between 0 and 1 in requested range, and isn't often close to 0.");
	};

	return _this;
};// jshint ignore:line
