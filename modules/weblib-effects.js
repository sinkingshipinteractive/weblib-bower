/*! weblib-ss 0.0.1 */
/* global require, ss */



( function() {

	"use strict";

	var Effects = Effects || {};

	// Create a namespaced instance of this object
	ss.Effects = Effects;

	// [Element] - The DOM Element on which fullscreen will be called by default
	Effects._defaultFullScreenElem = document.body;

	/*
	* Enter fullscreen mode on a given element.
	* @param element (optional) -> the element on the page to make fullscreen.
	*/
	ss.Effects.enterFullScreen = function(element) {

		// use document as the default.
		if (ss.isEmpty(element)) {
			// element = document.body;
			element = ss.Effects._defaultFullScreenElem;
		}

		if (element.webkitRequestFullScreen) {
			element.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
		}else if (element.mozRequestFullScreen) {
			element.mozRequestFullScreen();
		}else if (element.requestFullScreen) {
			element.requestFullScreen();
		}else if (element.msRequestFullscreen) {
			if (document.msFullscreenElement) {
				document.msExitFullscreen();
			}else {
				element.msRequestFullscreen();
			}
		}

		ss.Effects.isFullScreen = true;
	};

	/*
	* Exit Fullscreen mode.
	*/
	ss.Effects.exitFullScreen = function() {
		if (document.webkitExitFullscreen) {
			document.webkitExitFullscreen();
		} else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if (document.exitFullScreen) {
			document.exitFullScreen();
		} else if (document.msExitFullscreen) {
			document.msExitFullscreen();
		}

		ss.Effects.isFullScreen = false;
	};

	/*
	* Toggle fullscreen mode on a given element.
	* @param element (optional) -> the element on the page to make fullscreen.
	*/
	ss.Effects.toggleFullScreen = function(element) {
		if (ss.isEmpty(element)) {
			// element = document.body;
			element = ss.Effects._defaultFullScreenElem;
		}

		if (ss.Effects.isFullScreen) {
			ss.Effects.exitFullScreen ();
		} else {
			ss.Effects.enterFullScreen(element);
		}

	};

	/*
	* Set the element from which any fullscreen requests should be called
	* @param element:[Element] - The DOM Element on which fullscreen will be called by default
	*/
	ss.Effects.setDefaultFullScreenElement = function(element) {
		ss.Effects._defaultFullScreenElem = element;
	};

} ());

/* global require, createjs, ss */




(function() {
	"use strict";

	LightningEffect.prototype = new createjs.Container();
	LightningEffect.prototype.constructor = LightningEffect;

	/**
	 * Class to generate lightning effects.
	 * @param {ss.Vector2} startPoint    - The start point of the lightning bolt.
	 * @param {ss.Vector2} endPoint      - The end point of the lightning bolt.
	 * @param {Number} shockWidth    	 - The width of the shock itself.
	 * @param {Number} minLineLength 	 - When a line ni the bolt reaches this length it will not be split further.
	 */
	function LightningEffect (startPoint, endPoint, shockWidth, minLineLength) {
		// call the parent container constructor.
		createjs.Container.call(this);

		// keep a locally scoped copy of this.
		var _this = this;

		// save the start point.
		_this.startPoint = startPoint;

		// save the end point.
		_this.endPoint = endPoint;

		// save the width of the shock.
		_this.shockWidth = shockWidth;

		// save the minimum line length.
		_this.minLineLength = minLineLength;

		// The points on the bolt.
		var _points;

		/**
		 * Start this instance.
		 */
		_this.start = function() {
			 _points = _this.getPoints();
		};

		/**
		 * Update this instance (generate new points for the lightning bolt.)
		 */
		_this.update = function() {
			 _points = _this.getPoints();
		};

		/**
		 * Get new points for the lightning bolt.
		 * @return {Array} - The new set of points.
		 */
		_this.getPoints = function() {

			var points = [ _this.startPoint, _this.endPoint ];

			var done = false;

			while(!done) {
				var newPoints = [];
				done = true;

				for(var i = 0; i < points.length - 1; i++) {
					newPoints.push(points[i]);

					var midPoint = _getMidpointPreturbed(points[i],  points[i + 1] );

					if (!ss.isEmpty(midPoint)) {
						done = false;
						newPoints.push(midPoint);
					}

				}

				newPoints.push(points[points.length - 1]);

				points = newPoints.slice(0);
			}

			return points;

		};

		/**
		 * Get a preturbed version of the midpoint between to line segments in the bolt.
		 * @param  {ss.Vector2} startPoint - The start point in the segment whose midpoint is being preturbed.
		 * @param  {ss.Vector2} endPoint   - The end point in the segment whose midpoint is being preturbed.
		 * @return {ss.Vector2}            - The preturbed midpoint.
		 */
		var _getMidpointPreturbed = function(startPoint, endPoint) {
			var dist = startPoint.distance(endPoint);

			if (dist < _this.minLineLength) {
				return undefined;
			}

			var midPoint = endPoint.subtract(startPoint);
			midPoint.multSelf(0.5);
			midPoint.addSelf(startPoint);

			// preturb the midPoint.
			midPoint.addSelf(ss.MathUtils.uniformRandomInCircle(dist * shockWidth));

			return midPoint;
		};

		/**
		 * Draw the lightning bolt to a graphics object.
		 * @param  {Color} color        		- The colour to use.
		 * @param  {createjs.Graphics} graphics - The graphics object to draw to.
		 * @param  {Number} strokeWidth 		- The width of the stroke to draw.
		 */
		function _drawToGraphics (color, graphics, strokeWidth) {
			graphics.setStrokeStyle(strokeWidth);
			graphics.beginStroke(color);

			graphics.moveTo(_points[0].x, _points[0].y);

			for(var i = 1; i < _points.length; i++) {
				graphics.lineTo(_points[i].x, _points[i].y);
			}
		}
		/**
		 * Draw the lightning bolt to a graphics object.
		 * @param  {Color} color        		- The colour to use.
		 * @param  {createjs.Graphics} graphics - The graphics object to draw to.
		 * @param  {Number} strokeWidth 		- The width of the stroke to draw.
		 * @param  {Number} glowIterations 		- How many segments the glow will have. (lower is faster, higher, will have a smoother gradient).
		 * @param  {Number} glowThickness		- How thick (as a factor of the stroke width) each glow segment will be.
		 */
		_this.drawToGraphics = function(color, graphics, strokeWidth, glowIterations, glowThickness, glowColour) {
			if (ss.isEmpty(glowIterations)) {
				glowIterations = 1;
			}

			if (ss.isEmpty(strokeWidth)) {
				strokeWidth = 1;
			}

			if (ss.isEmpty(glowThickness)) {
				glowThickness = 1;
			}

			graphics.clear();

			for(var i = 0; i < glowIterations; i++) {
				_drawToGraphics(createjs.Graphics.getRGB(glowColour, 0.5 / glowIterations), graphics, strokeWidth * i * glowThickness);
			}

			_drawToGraphics(createjs.Graphics.getRGB(color, 1), graphics, strokeWidth);
		};

		/**
		 * Release the memory used by this instance.
		 */
		_this.release = function() {
			_this = undefined;
		};
	}

	ss.LightningEffect = LightningEffect;

}());


/* global require, ss*/



( function() {
	"use strict";

	// Singleton effects class to hold various effects.
	ss.Effects = ss.Effects || {};

	// Time between each shake cycle.
	ss.Effects.BETWEEN_SHAKES_TIME = 0.01;

	// make a command queue to cancel old shakes optionally.
	ss.Effects.shakeQueue = new ss.CommandQueue(null, true);

	/*
	* Shake a createjs Display Object.
	* @param displayObject: The object to shake.
	* @param duration (optional): how long to shake (in seconds, defaults to 0.25).
	* @param amount (optional) : the max diameter of the shake, in pixels. The object will shake 1/2 this value away from it's starting  point. Defaults to 50.
	* @param interrupt: true to stop any currently running shakes before starting this one.
	*/
	ss.Effects.shakeDisplayObject = function(displayObject, duration, amount, interrupt) {
		// check if the duration is set, and if not, use the default.
		if (typeof duration !== "number") {
			duration = 0.25;
		}

		// check if the amount is set, and if not, use the default.
		if (typeof amount !== "number") {
			amount = 50;
		}

		// If interrupt is set, cancel any previously running shakes.
		if (interrupt === true) {
			ss.Effects.shakeQueue.cancel ();
		}

		// get the original position of the object.
		var originalX = displayObject.x;
		var originalY = displayObject.y;

		// create a command set for the shake, tweening to a new random position every 0.05 seconds.
		var commandSet = new ss.CommandSet();
		for(var i = 0; i < duration - ss.Effects.BETWEEN_SHAKES_TIME; i += ss.Effects.BETWEEN_SHAKES_TIME) {
			commandSet.tweenTo(displayObject, ss.Effects.BETWEEN_SHAKES_TIME, {
					"x" : originalX + Math.random() * amount - (amount / 2),
					"y" : originalY + Math.random() * amount - (amount / 2)
				});
		}
		commandSet.tweenTo(displayObject, ss.Effects.BETWEEN_SHAKES_TIME, { "x" : originalX, "y" : originalY });

		// execute the shake.
		ss.Effects.shakeQueue.queue ( commandSet );
	};

	/*
	* Shake a DOM Object.
	* @param displayObject: The object to shake.
	* @param duration (optional): how long to shake (in seconds, defaults to 0.25).
	* @param amount (optional) : the max diameter of the shake, in pixels. The object will shake 1/2 this value away from it's starting  point. Defaults to 50.
	* @param interrupt: true to stop any currently running shakes before starting this one.
	*/
	ss.Effects.shakeDomObject = function(domObject, duration, amount, interrupt) {
		// check if the duration is set, and if not, use the default.
		if (isNaN(duration)) {
			duration = 0.25;
		}

		// check if the amount is set, and if not, use the default.
		if (isNaN(amount)) {
			amount = 50;
		}

		// If interrupt is set, cancel any previously running shakes.
		if (interrupt === true) {
			ss.Effects.shakeQueue.cancel ();
		}

		// get the original position of the object.
		var originalX = domObject.style.left;
		var originalY = domObject.style.top;

		// create a command set for the shake, tweening to a new random position every 0.05 seconds.
		var commandSet = new ss.CommandSet();
		for(var i = 0; i < duration - ss.Effects.BETWEEN_SHAKES_TIME; i += ss.Effects.BETWEEN_SHAKES_TIME) {
			commandSet.tweenTo(domObject, ss.Effects.BETWEEN_SHAKES_TIME, {
					"left" : originalX + Math.random() * amount - (amount / 2),
					"top"  : originalY + Math.random() * amount - (amount / 2)
				});
		}
		commandSet.tweenTo(domObject, ss.Effects.BETWEEN_SHAKES_TIME, { "left" : originalX, "top" : originalY });

		// execute the shake.
		ss.Effects.shakeQueue.queue ( commandSet );
	};

} ());

/* global require, ss*/




( function() {
	"use strict";

	/**
	* Class that aids in drawing shadows for arbitrary convex polygons.
	*
	* NOTE: The assumption is that the polygon being used as a shape is convex.
	* The vertices of the polygon should be defined in clockwise order.
	* If either the polygon is concave, or the vertices are in counter-clockwise order,
	* The shadow will not display correctly.
	*
	* @param {ss.Polygon} shape: The polygon whose shadow we'll be drawing.
	* @param {Number} distance: how far the shadow should extend from the points in the polygon.
	**/
	function ShapeShadow (shape, distance) {
		// keep a locally scoped copy of this.
		var _this = this;

		/* The shape that this shadow is being applied to. */
		_this.shape = shape;

		/* The distance from the points in the shape that the shadow will extend. */
		_this.distance = distance;

		/**
		* Gets the shape of the base of the shadow, given a light position.
		* This projects the starting shape, by extending each vertex in the direction
		* of the light position, by the distance of the shadow.
		* @param {ss.Vector2} lightPosition: The position of the light that is casting the shadow.
		**/
		_this.getShadowShape = function(lightPosition)
		{
			var shadowVerts, i, currentVert, direction, newVert;

			// create a list to hold the vertices of the shadow shape.
			shadowVerts = [];

			for( i = 0; i < _this.shape.points.length; i++ ) {

				currentVert = _this.shape.points[i];

				// determine the direction to extend the vertex.
				direction = currentVert.subtract(lightPosition);
				direction.normalize();

				// determine the new location of the vertex, by extending it by the distance,
				// in the correct direction.
				newVert = currentVert.add(direction.mult(_this.distance));
				shadowVerts.push(newVert);
			}

			// create a polygon from the new vertices.
			_this.shadowShape = new ss.Polygon(shadowVerts);
			return _this.shadowShape;
		};

		/**
		* Checks whether a given line is facing away from the light source.
		* @param {ss.Line2D} lineToCheck: The line whose facing we are checking.
		* @param {ss.Vector2} lightPosition: The position of the light we are checking against.
		**/
		_this.isLineAwayFromLight = function(lineToCheck, lightPosition)
		{
			var center, normal, lightDirection;

			center = lineToCheck.getMidpoint();
			normal = lineToCheck.getNormal();

			lightDirection = lightPosition.subtract(center);

			return lightDirection.dot(normal) >= 0;
		};

		/**
		* Culls points in a polygon that are either facing towards or away from the light source.
		* @param {Array} points: The points that define the polygon. NOTE: The points should be sorted in
		* clockwise order. If they are in counter-clockwise order, this will cull the opposite
		* sides of the polygon.
		* @param {ss.Vector2} lightPosition: Where the light being used in the culling is positioned.
		* @param {Boolean} cullTowardsLight: True to cull the sides of the polygon that are facing the light,
		* 	false to cull the sides of the polygin that are facing away from the light.
		**/
		_this.cullPoints = function(points, lightPosition, cullTowardsLight)
		{
			var potentialIndicesToCull, i, lineToCheck, lineAway, returnList;

			// Keep a count of how many edges linked to each vertex are facing the direction we want to cull.
			// We only want to cull the a point if both edges it's connected two are facing the culling direction.
			potentialIndicesToCull = new Array(points.length);
			for(i = 0; i < points.length; i++) {
				potentialIndicesToCull[i] = 0;
			}

			// loop through all the points, checking the line connecting each point to the next point
			// and check which direction they're facing.
			for(i = 0; i < points.length - 1; i++)
			{
				lineToCheck = new ss.Line2D(points[i], points[i + 1]);

				lineAway = _this.isLineAwayFromLight(lineToCheck, lightPosition);

				if ((cullTowardsLight && !lineAway) || (!cullTowardsLight && lineAway))
				{
					// If an edge is facing the right direction,
					// increment the count of each point on it.
					potentialIndicesToCull[i] += 1;
					potentialIndicesToCull[i + 1] += 1;
				}
			}

			// Check the line connecting the last point in the polygon to the first.
			// The loop above checks all other points, but we need to make sure to include this one too.
			lineToCheck = new ss.Line2D(points[points.length - 1], points[0]);
			lineAway = _this.isLineAwayFromLight(lineToCheck, lightPosition);

			if ((cullTowardsLight && !lineAway) || (!cullTowardsLight && lineAway))
			{
				// If the line connecting the last point to the first is facing the culling direction,
				// increment the count on it's vertices.
				potentialIndicesToCull[points.length - 1] += 1;
				potentialIndicesToCull[0] += 1;
			}

			// set up a list to contain the remaining vertices.
			returnList = [];

			for(i = 0; i < potentialIndicesToCull.length; i++)
			{
				// include any point in the list whose count is less than 2.
				if (potentialIndicesToCull[i] <= 1)
				{
					returnList.push(points[i]);
				}
			}

			return returnList;
		};

		/**
		* Gets the shadow volume for the shape for a given light position.
		* @param {ss.Vector2} lightPosition: The position of the light that's casting this shadow volume.
		**/
		_this.getShadowVolume = function(lightPosition) {

			var shadowShape, culledShadowPoints, culledTilePoints, newShapePoints, culledPolygon;

			// Get the projected shape for the base of the shadow volume.
			shadowShape = _this.getShadowShape(lightPosition);

			// cull the points in the base of the volume that are facing towards the light.
			culledShadowPoints = _this.cullPoints(shadowShape.points, lightPosition, false);

			// cull the points in the shape itself that are facing away from the light.
			culledTilePoints = _this.cullPoints(_this.shape.points, lightPosition, true);

			// join the remaining points in the shape itself, and the base of the shadow volume.
			newShapePoints = culledTilePoints.concat(culledShadowPoints);

			// create a new polygon and sort it's vertices.
			culledPolygon = new ss.Polygon(newShapePoints);
			culledPolygon.sortPointsClockwise(newShapePoints);

			return culledPolygon;

		};

		/**
		* Draws the shadow volume to a createjs graphics object.
		* @param {ss.Vector2} lightPosition: The position of the light source that's casting this shadow.
		* @param {createjs.Graphics} graphics: The createjs graphics object that the shadow is being drawn to.
		**/
		_this.drawShadowToGraphics = function(lightPosition, graphics) {
			var shadowVolume, objectCenter, direction, points, gradientEnd, i;

			// calculate the shape of the shadow volume.
			shadowVolume = _this.getShadowVolume(lightPosition);

			// start the gradient at the center of the shape that's casting the shadow.
			objectCenter = _this.shape.getCenter();

			// determine the gradient direction.
			direction = objectCenter.subtract(lightPosition);
			direction.normalize();

			points = shadowVolume.points;

			// determine where the gradient should end.
			gradientEnd = objectCenter.add(direction.mult(_this.distance * 0.75));

			// draw the shadow to the graphcis object.
			graphics.clear();
			graphics.beginLinearGradientFill([ "rgba(0,0,0,0.075)", "rgba(0,0,0,0)" ], [ 0.1, 1 ],
											objectCenter.x, objectCenter.y,  gradientEnd.x ,  gradientEnd.y);
			graphics.moveTo(points[0].x, points[0].y);
			for(i = 0; i < points.length; i++ ) {
				graphics.lineTo(points[i].x, points[i].y);
			}
			graphics.lineTo(points[0].x, points[0].y);
			graphics.endFill();
		};
	}

	ss.ShapeShadow = ShapeShadow;

} ());

/* global require, createjs, ss, $M, $V */





(function() {
	"use strict";

	SquashAndStretchBitmap.prototype = new createjs.Container();
	SquashAndStretchBitmap.prototype.constructor = SquashAndStretchBitmap;

	/**
	 * Squashes and stretches a bitmap as it moves.
	 */
	function SquashAndStretchBitmap () {
		// call the parent container constructor.
		createjs.Container.call(this);

		// keep a locally scoped copy of this.
		var _this = this;

		var _imageWidth;
		var _imageHeight;
		var _image;

		var _basePoints;

		var _lastPosition;
		var _currentPosition;
		var _positionDelta;

		var _xAxis;
		var _yAxis;

		var _stretchAmount;

		var _stretchedPoints;

		/**
		 * Get the transform matrix from the 3 points.
		 * @param  {[type]} fromPoints [description]
		 * @param  {[type]} toPoints   [description]
		 * @return {[type]}            [description]
		 */
		function _getTransformMatrixFrom3Points (fromPoints, toPoints) {
		    var m = $M([ [ fromPoints[0][0], fromPoints[0][1], 1 ],
		      [ fromPoints[0][0], fromPoints[1][1], 1 ],
		      [ fromPoints[2][0], fromPoints[0][1], 1 ] ]);

		    var mInv = m.inverse();

		    var xVector = $V([ toPoints[0][0], toPoints[1][0], toPoints[2][0] ]);
		    var yVector = $V([ toPoints[0][1], toPoints[1][1], toPoints[2][1] ]);

		    var r1 = mInv.x(xVector);
		    var r2 = mInv.x(yVector);

		    return new createjs.Matrix2D(r1.e(1), r2.e(1), r1.e(2), r2.e(2), r1.e(3), r2.e(3));
		}

		function _stretchPoints (originalPoints, xAxis, yAxis, xStretch, yStretch) {
			var newPoints = originalPoints.slice(0);

			for(var i = 0; i < newPoints.length; i++) {
				var curPoint = new ss.Vector2(newPoints[i][0], newPoints[i][1]);
				var xVal = curPoint.dot(xAxis);
				var yVal = curPoint.dot(yAxis);
				var newXVal = xVal * xStretch;
				var newYVal = yVal * yStretch;
				var newPoint = xAxis.mult(newXVal).add(yAxis.mult(newYVal));
				newPoints[i] = [ newPoint.x, newPoint.y ];
			}

			return newPoints;
		}

		/**
		 * Start this instance.
		 */
		_this.start = function(bitmap, width, height, stretchAmount, regX, regY) {
			if (ss.isEmpty(regX)) {
				regX = width / 2;
			}

			if (ss.isEmpty(regY)) {
				regY = height / 2;
			}

			if (ss.isEmpty(stretchAmount)) {
				stretchAmount = 0.05;
			}

			_stretchAmount = stretchAmount;

			_image = bitmap;
			_image.regX = regX;
			_image.regY = regY;
			_image.x = 0;
			_image.y = 0;

			_this.addChild(_image);

			_imageWidth = width;
			_imageHeight = height;

			_basePoints = [ [ 0, 0 ], [ _imageWidth, 0 ], [ _imageWidth, _imageHeight ], [ 0, _imageHeight ] ];
			_stretchedPoints = _basePoints.slice();

			_lastPosition = new ss.Vector2(this.x, this.y);
			_currentPosition = new ss.Vector2(this.x, this.y);
			_positionDelta = new ss.Vector2(0, 0);

			_xAxis = new ss.Vector2(0, 0);
			_yAxis = new ss.Vector2(0, 0);
		};

		_this.update = function(delta) {
			_currentPosition.setAll(this.x, this.y);
			_currentPosition.subtractOut(_lastPosition, _positionDelta);

			_positionDelta.normalizedOut(_xAxis);
			_yAxis.setAll(-_xAxis.y, _xAxis.x);

			var strength = _positionDelta.magnitude() / delta;

			var factor = 1 + (strength * _stretchAmount);

			var xStretch = factor;
			var yStretch = 1 / factor;

			if (strength !== 0) {
				_stretchedPoints = _stretchPoints(_basePoints, _xAxis, _yAxis, xStretch, yStretch);
			} else {
				_stretchedPoints = _basePoints.slice(0);
			}

			var transformMatrix = _getTransformMatrixFrom3Points ( [ _basePoints[0], _basePoints[3], _basePoints[1] ], [ _stretchedPoints[0], _stretchedPoints[3], _stretchedPoints[1] ]);

			transformMatrix.decompose(_image);

			_lastPosition.setAll(_currentPosition.x, _currentPosition.y);
		};


		/**
		 * Release the memory used by this instance.
		 */
		_this.release = function() {

	 		_imageWidth = undefined;
			_imageHeight = undefined;

			_this.removeChild(_image);
			_image = undefined;

			_basePoints = undefined;

			_lastPosition = undefined;
			_currentPosition = undefined;
			_positionDelta = undefined;

			_xAxis = undefined;
			_yAxis = undefined;

			_stretchAmount = undefined;

			_stretchedPoints = undefined;

			_this = undefined;
		};
	}

	ss.SquashAndStretchBitmap = SquashAndStretchBitmap;

}());

