/*! weblib-ss 0.0.1 */
/* global ss, require */



(function() {
	"use strict";

	Camera.prototype = {};
	Camera.prototype.constructor = Camera;

	/**
	 * Class representing a camera,
	 * typically used in conjuntion with the
	 * CameraLayer and CameraLayerManager classes.
	 * @param {Number} x      - The x coordinate of the camera.
	 * @param {Number} y      - The y coordinate of the camera.
	 * @param {Number} width  - The width of the camera.
	 * @param {Number} height - The height of the camera.
	 */
	function Camera (x, y, width, height) {
		// {ss.Camera} Keep a locally scoped copy of this.
		var _this = this;

		// {Number} The x-coordinate of the camera.
		_this.x = x;

		// {Number} The y-coordinate of the camera.
		_this.y = y;

		// {Number} The width of the camera.
		_this.width = width;

		// {Number} The height of the camera.
		_this.height = height;

		// {Number} The rotation of the camera.
		_this.rotation = 0;

		// {Number} The scale of the camera.
		_this.scale = 1;

		/**
		 * Release the memory used by this object.
		 */
		_this.release = function() {
			_this.x = undefined;
			_this.y = undefined;
			_this.width = undefined;
			_this.height = undefined;

			_this = undefined;
		};

	}

	// put the camera class into it's namespace.
	ss.Camera = Camera;

}());

/* global require, createjs, ss */





(function() {
	"use strict";

	CameraLayer.prototype = new createjs.Container();
	CameraLayer.prototype.constructor = CameraLayer;

	/**
	 * Class representing a camera layer for the camera system.
	 * @param {Number} parallaxRate - The parallax rate for this layer, defaults to 1 if not included.
	 */
	function CameraLayer (parallaxRate) {
		// call the parent container constructor.
		createjs.Container.call(this);

		// keep a locally scoped copy of this.
		var _this = this;

		// {ss.Rectangle} - The boudns of this layer.
		_this.bounds = undefined;

		//
		_this.hasBounds = false;

		// default the parallax rate to 1 if not specified.
		if (ss.isEmpty(parallaxRate)) {
			parallaxRate = 1;
		}

		_this.parallaxRate = parallaxRate;

		/**
		 * Set the bounds of this layer, in pixels.
		 * @param {ss.Rectangle} bounds - The layer's bounds.
		 */
		_this.setBounds = function(bounds) {
			if (!(bounds instanceof ss.Rectangle)) {
				throw ("Camera layer bounds should be a rectangle.");
			}

			_this.hasBounds = true;
			_this.bounds = bounds;

		};

		/**
		 * Release the memory used by this instance.
		 */
		_this.release = function() {
			_this.parallaxRate = undefined;
			_this.hasBounds = undefined;
			this.bounds = undefined;
			_this = undefined;
		};

	}

	ss.CameraLayer = CameraLayer;

}());


/* global createjs, require, ss */





(function() {
	"use strict";

	CameraLayerManager.prototype = {};
	CameraLayerManager.prototype.constructor = CameraLayerManager;

	/**
	 * Manager for a camera and camera layers.
	 * @param {ss.Camera} camera 				- The camera that's looking at the layers.
	 * @param {Array[{ss.CameraLayer}]} layers 	- The array of the layers that the camera is looking at.
	 */
	function CameraLayerManager (camera, layers) {
		// keep a locally scoped copy of this.
		var _this = this;

		// {ss.Camera} - The camera that's looking at the layers.
		var _camera;

		// {Array[{ss.CameraLayer}]} - The layers that the camera is looking at.
		var _layers;

		// {createjs.Container} - The container that the camera's layers will be put into.
		var _container;

		// {Boolean} - Whether or not the layers have been added to the container.
		var _hasAddedLayers;

		/**
		 * Enforce the bounds on a given layer.
		 * @param  {ss.Camera} camera 		- The Camera whose position will be locked to the bounds of the layer.
		 * @param  {ss.CameraLayer} layer   - The layer which we'll be locking the camera to.
		 */
		function _enforceBounds (camera, layer) {

			// TODO: Make this handle camera scaling, currently the bounds only work correctly on an unscaled (scale: 1) camera.
			var maxWidth = ((layer.bounds.width - _camera.width) / layer.parallaxRate) + _camera.width;
			var maxHeight = ((layer.bounds.height - _camera.height) / layer.parallaxRate) + _camera.height;

			if ( _camera.x + (_camera.width / 2) > maxWidth) {
				camera.x = maxWidth - (_camera.width / 2);
			}
			if (_camera.x < _camera.width / 2) {
				_camera.x = _camera.width / 2;
			}

			if (_camera.y + (_camera.height / 2) > maxHeight) {
				_camera.y = maxHeight - (_camera.height / 2);
			}

			if (_camera.y < _camera.height / 2) {
				_camera.y = _camera.height / 2;
			}
		}

		/**
		 * Update this instance.
		 * @param  {Number} delta - How much time has passed since the last frame (in seconds).
		 */
		_this.update = function(delta) { // jshint ignore:line
			if(ss.isEmpty(_layers)) {
				return;
			}
			var i, layer;

			// loop though the layers and enforce their bounds.
			for( i = 0 ; i < _layers.length; i++) {
				layer = _layers[i];
				if (layer.hasBounds) {
					// enforce bounds.
					_enforceBounds(camera, layer);
				}
			}

			// Get the center of the camera.
			var cameraCenterX = _camera.x - (_camera.width / 2);
			var cameraCenterY = _camera.y - (_camera.height / 2);

			for(i = 0 ; i < _layers.length; i++) {
				layer = _layers[i];

				// set the registration point of the layer.
				layer.regX = (cameraCenterX * layer.parallaxRate) +  (_camera.width / 2);
				layer.regY = (cameraCenterY * layer.parallaxRate) +  (_camera.height / 2);

				// position the layer.
				layer.x = (_camera.width / 2);
				layer.y = (_camera.height / 2);

				// scale tha layer.
				layer.scaleX = 1 / (_camera.scale);
				layer.scaleY = 1 / (_camera.scale);

				// rotate the layer.
				layer.rotation = -_camera.rotation;
			}

		};

		/**
		 * Add the layers to a container, in order.
		 * @param {createjs.Container} container - The container to add the layers to.
		 */
		_this.addLayersToContainer = function(container) {
			_hasAddedLayers = true;
			if (!(container instanceof createjs.Container)) {
				// If something was passed in that wasn't a layer, yell at whoever did it.
				throw( "container must be a createjs.Container");
			}

			_layers = _layers.sort(_layerSortMethod);

			for(var i = 0; i < _layers.length; i++) {
				container.addChild(_layers[i]);
			}

			_container = container;
		};

		/**
		 * Remove all the layers from the given container.
		 * @param  {createjs.Container} container - The container to remove the layers from.
		 */
		_this.removeLayersFromContainer = function(container) {
			if (!(container instanceof createjs.Container)) {
				// If something was passed in that wasn't a layer, yell at whoever did it.
				throw( "container must be a createjs.Container");
			}

			for(var i = 0; i < _layers.length; i++) {
				container.removeChild(_layers[i]);
			}
		};

		/**
		 * Adds a mask to the container,m asking all layers
		 * @param {createjs.Shape} mask - The shape of the mask to be used
		 */
		_this.setMask = function(mask) {
			_container.mask = mask;
		};

		/**
		 * Comparison function used to sort the camera layers.
		 * @param  {ss.CameraLayer} layerA - The first layer in the comparison.
		 * @param  {ss.CameraLayer} layerB - The second layer in the comparison.
		 * @return {Number}        			- < 0 if layerA < layerB, > 0 if layerA > layerB, 0 if layerA = layerB
		 */
		var _layerSortMethod = function(layerA, layerB) {
			return layerA.parallaxRate - layerB.parallaxRate;
		};

		/**
		 * Construct this CameraLayerManager.
		 * @param  {ss.Camera} camera 	   - The camera object.
		 * @param  {ss.CameraLayer} layers - The camera layer object.
		 */
		_this.construct = function(camera, layers) {
			_hasAddedLayers = false;

			if (ss.isEmpty(layers)) {
				layers = [];
			}

			_camera = camera;
			_layers = layers;
			return _this;
		};

		/**
		 * Release the memory used by this instance.
		 */
		_this.release = function() {
			if (_hasAddedLayers) {
				_this.removeLayersFromContainer(_container);
			}
			_camera = undefined;
			_layers = undefined;
			_container = undefined;
		 	_hasAddedLayers = undefined;
			_this = undefined;
		};

		return _this.construct(camera, layers);

	}

	// put this class into it's namespace.
	ss.CameraLayerManager = CameraLayerManager;

} ());

/* global require, ss */




(function() {
	"use strict";

	LerpCameraToTarget.prototype = {};
	LerpCameraToTarget.prototype.constructor = LerpCameraToTarget;

	/**
	 * Class used to lerp a camera towards a given target.
	 * NOTE: the update method of this class should be called before the update method of The CameraLayerManager, in order
	 * to ensure that the bounds constraints are run after the camera is moved, as opposed to before, which could result in
	 * the camera going out of bounds.
	 * @param {ss.Camera} camera     							- The camera we're working with.
	 * @param {ss.Vector2|createjs.DisplayObject} target        - The target to aim the camera at.
	 * @param {Number} lerpFactor 								- The factor to lerp by (0-1), lower numbers will make the camera move smoother,
	 *                                    						 but lag further behind the target,
	 *                                    						 while higher numbers will track the target faster, but be less smooth.
	 */
	function LerpCameraToTarget (camera, target, lerpFactor) {

		// {ss.LerpCameraToTarget} keep a locally scoped copy of this.
		var _this = this;

		// {ss.Camera} the camera which is tracking the target.
		var _camera = camera;

		// {ss.Vector2|createjs.DisplayObject} The target being tracked.
		var _target = target;

		// {Nubmer} The lerping factor (0-1)
		var _lerpFactor = lerpFactor;

		// Set up a reasonable initial value for the lerp factor.
		if (ss.isEmpty(lerpFactor)) {
			lerpFactor = 0.005;
		}

		/**
		 * Update this camera tracker.
		 * NOTE: the update method of this class should be called before the update method of The CameraLayerManager, in order
		 * to ensure that the bounds constraints are run after the camera is moved, as opposed to before, which could result in
	 	 * the camera going out of bounds.
		 * @param  {Number} delta - How much time has passed since the last frame, in seconds.
		 */
		_this.update = function(delta) {
			if(ss.isEmpty(_camera) || ss.isEmpty(_target)) {
				return;
			}
			_camera.x = ss.MathUtils.lerp1D(_camera.x, _target.x, _lerpFactor * delta);
			_camera.y = ss.MathUtils.lerp1D(_camera.y, _target.y, _lerpFactor * delta);

		};

		_this.isAtTarget = function (minDelta) {
			if(ss.isUndefined(minDelta)) {
				minDelta = 10;
			}
			var targetPosition = new ss.Vector2(_target.x, _target.y);
			var cameraPosition = new ss.Vector2(_camera.x, _camera.y);
			if(targetPosition.subtract(cameraPosition).magnitude() < minDelta) {
				return true;
			} else {
				return false;
			}
		};

		_this.setLerpFactor = function(newFactor) {
			_lerpFactor = newFactor;
		};

		_this.getLerpFactor = function() {
			return _lerpFactor;
		};

		/**
		 * Set the target of this tracker.
		 * @param {ss.Vector2|createjs.DisplayObject} target - The new target to track.
		 */
		_this.setTarget = function(target) {
			_target = target;
		};

		/**
		 * Release the memory used by this instance.
		 */
		_this.release = function( ) {
			_lerpFactor = undefined;
			_camera = undefined;
			_target = undefined;
			_this = undefined;
		};

	}

	// put this class into it's namespace.
	ss.LerpCameraToTarget = LerpCameraToTarget;

}());

/* global require, ss */


(function() {
	"use strict";

	LockCameraToTarget.prototype = {};
	LockCameraToTarget.prototype.constructor = LockCameraToTarget;

	/**
	 * Lock camera to a given target.
	 * NOTE: the update method of this class should be called before the update method of The CameraLayerManager, in order
	 * to ensure that the bounds constraints are run after the camera is moved, as opposed to before, which could result in
	 * the camera going out of bounds.
	 * @param {ss.Camera} camera - The camera which is tracking the target.
	 * @param {ss.Vector2|createjs.DisplayObject} target - The target that the camera is tracking.
	 */
	function LockCameraToTarget (camera, target) {

		//  {ss.LockCameraToTarget} keep a locally scoped copy of this.
		var _this = this;

		// {ss.Camera} The camera which is tracking the target.
		var _camera = camera;

		// {ss.Vector2|createjs.DisplayObject} The target that the camera is tracking.
		var _target = target;

		/**
		 * Update this instance.
		 * NOTE: the update method of this class should be called before the update method of The CameraLayerManager, in order
		 * to ensure that the bounds constraints are run after the camera is moved, as opposed to before, which could result in
	 	 * the camera going out of bounds.
		 * @param  {Number} delta - The time passed since the last frame.
		 */
		_this.update = function(delta) { // jshint ignore:line
			_camera.x = _target.x;
			_camera.y = _target.y;

		};

		/**
		 * Set the target for this camera tracker.
		 * @param {ss.Vector2|createjs.DisplayObject} target - The camera target.
		 */
		_this.setTarget = function(target) {
			_target = target;
		};

		/**
		 * Release the memory used by this instance.
		 */
		_this.release = function( ) {
			_camera = undefined;
			_target = undefined;
			_this = undefined;
		};

	}

	ss.LockCameraToTarget = LockCameraToTarget;

}());
