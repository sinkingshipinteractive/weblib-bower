/*! weblib-ss 0.0.1 */
/* global console, require, createjs, ss */




(function() {
	"use strict";
	/*
	* Audio Element Sound Manager.
	* Provides limited audio support for devices which are not compatible with soundJS.
	* Uses a single audio element to play sounds.
	* There are two main restrictions when using this manager.
	* 1) In order to work on iOS / older android phones, all audio must be triggered by user input,
	*   so the manager must be instantiated within a user input event.
	* 2) Only 1 channel will be available when playing audio using this manager.
	*
	* Dispatches the following events:
	*	"loaded" -> when an audio sprite has finished loading and is ready to play.
	*	"complete" (audioId) -> When an audio finishes playing, or when one cycle of a looping audio track finishes.
	*/
	ss.AudioElementSoundManager = ss.AudioElementSoundManager || new function() {// jshint ignore:line

		// create a locally scoped copy of this, extending AbstractEventDispatcher.
		var _this = AbstractEventDispatcher ( this ); // jshint ignore:line

		// How far from the correct time a sound must start in order to be considered an error
		var _TIME_TOLERANCE = 0.5;

		// sprite map definition within the audio sprite.
		var _spriteMap;

		// the audio element that will play the sounds.
		var _audioElement;

		// [Array<Object>] - List of audio queued to play while the sound is being loaded, along with additional properties
		// Object format: {"soundId", "priority", "loop"}
		var _queuedAudio;

		// [Boolean] - Whether or not the audio sprite has loaded.
		var _loaded;
		// [Boolean] - Whether the sound is completely ready to play
		var _ready;

		// the soundId of the currently playing sound.
		var _currentSoundId;

		// [Number] - The starting position of the current sound (in seconds)
		var _currentSoundStart;
		// [Number] - The ending position of the current sound (in seconds)
		var _currentSoundEnd;

		var _currentSoundLoop;

		// The priority of the currently playing sound.
		var _currentPriority;

		// the current volume.
		var _volume;

		// [Boolean] - Whether this sound manager is currently paused
		var _isPaused;

		/*
		* Constructor, builds the manager.
		*/
		function _construct () {
			_loaded = false;
			_ready = false;
			_volume = 1;
			_queuedAudio = [];
			_isPaused = false;

			return _this;
		}

		/*
		* Listener for when loading is finished.
		* plays any queued audio, and dispatches the "loaded" event.
		*/
		_this.handleLoad = function() {
			_audioElement.removeEventListener("canplaythrough", _this.handleLoad);

			// flag loaded as true.
			_loaded = true;

			// Stop all sound just in case some browser start playing audio by default
			_this.stopAllSounds();

			// Check if the sound is all ready to play now
			_checkSoundReady();

			// Inform others that the sound has been loaded
			_this.dispatchEvent(new ss.BaseEvent("loaded"));
		};

		/*
		* Loads an audio sprite.
		* @param audioDefinitionJSON: an object containing an audio definition, as output by the audiosprite tool here: https://github.com/tonistiigi/audiosprite
		* See weblib/examples/sound/SoundTest.js for a sample output in the correct format.
		*/
		_this.loadSoundSprite = function(audioDefinitionJson) {

			// pull the data we need out of the JSON.
			_spriteMap = audioDefinitionJson.spritemap;
			var audioResources = audioDefinitionJson.resources;

			// determine the main file, and build a list of all available extensions.
			var i;
			var extensionList = [];

			// create the audio element to play this sound sprite.
			_audioElement = document.getElementById("audio");
			if (!_audioElement) {
				_audioElement = document.createElement("audio");
			}
			_audioElement.volume = 1;

			_audioElement.id = "combined";
			_audioElement.controls = true;
			_audioElement.preload = "auto";

			// var mainFile = audioResources[0];
			// console.log("looping through resources.");
			for(i = 0; i < audioResources.length; i++) {

				var res = audioResources[i];

				// extract the file extension.
				var extension = res.substring(res.length - 3, res.length);

				extensionList.push(extension);

				// if we have an m4a, set that as the main file, since it's supported on all mobile platforms.
							// set the sources on the audio element.
				var type;
				if (extension ===  "mp3" || extension == "m4a") {
					type = "audio/mpeg";
				} else {
					type = "audio/" + extension;
				}

				// console.log("audio type: " + type);
				var source = document.createElement("source");
				source.type = type;

				source.src = res;
				_audioElement.appendChild(source);
			}

			_audioElement.addEventListener("canplaythrough", _this.handleLoad);
			_audioElement.addEventListener("timeupdate", _this.onTimeUpdate);
			_audioElement.load();

			// need to play this immediately so it's tied to the mouse event, then we can pause it immediately afterwards.
			_audioElement.play();
			_audioElement.pause();
		};

		/*
		* Check if the sound is completely ready to play
		*/
		function _checkSoundReady () {
			// If no duration is set, wait until it is before continuing
			if (isNaN(_audioElement.duration) || (_audioElement.duration <= 0) || (_audioElement.readyState < 4)) {
				createjs.Tween.get(_this).wait(100).call(_this.handleLoad);
			// If duration is set, set the sound as ready
			} else {
				_handleSoundReady();
			}
		}

		/*
		* Handle the sound being completely ready to play
		*/
		function _handleSoundReady () {
			_ready = true;
			_tryPlayNextQueuedSound();
		}

		/*
		* Helper function that handles completion of audio loading
		*/
		// function _completeLoad(){

		// 	_tryPlayNextQueuedSound();
		// }

		/*
		* Play the next queued sound, if any
		* @return:[Boolean] - True if a queued sound started playing, false if there wasn't any
		*/
		function _tryPlayNextQueuedSound () {
			var queuedObj;

			if (_queuedAudio.length > 0) {
				queuedObj = _queuedAudio.shift();
				_this.playSound(queuedObj.soundId, queuedObj.priority, queuedObj.loop);
				return true;
			}

			return false;
		}

		// TESTING
		// function _handleLoadProgress(e){
		// 	ss.DebugUtil.showMessage("Audio Load Progress: " + e, "AudioLoadProgress");
		// 	ss.DebugUtil.showMessage("Audio Load Duration: " + _audioElement.duration, "AudioLoadDuration");
		// }
		// END TESTING

		// TESTING
		// function _handleLoadStart(e){
		// 	//console.log("AESoundMan: Load Start");
		// }

		// function _handleCanPlay(e){
		// 	//console.log("AESoundMan: Can Play");
		// }


		/*
		* Return the audio text that came with the audio clip.
		* @param soundId: The id of the sound from the sound sprite. NOTE: using a sound that isn't in the sprite will result in an error.
		* @return - the text specified in the json file for given audio, otherwise return undefined.
		*/
		_this.getText = function(soundId) {

			// get the text field of the audio sprite data
			if (_isValid(soundId)) {
				var audioText = _spriteMap[soundId].text;
				return audioText;
			}
			return undefined;
		};


		/*
		* Return the audio length of the audio clip.
		* @param soundId: The id of the sound from the sound sprite. NOTE: using a sound that isn't in the sprite will result in an error.
		* @return - the length in second of the audio clip.
		*/
		_this.getDuration = function(soundId) {

			// extract the times for the audio file.
			if (_isValid(soundId)) {
				var start = _spriteMap[soundId].start;
				var end = _spriteMap[soundId].end;
				var length = end - start;
				return length;
			}
			return 0;
		};

		/*
		* Play a sound.
		* @param soundId: The id of the sound from the sound sprite. NOTE: using a sound that isn't in the sprite will result in an error.
		* @param priority (optional): a SoundPriority specifying the priority of the sound we are starting.
		* @param loop: True to loop the sound, false otherwise.
		*/
		_this.playSound = function(soundId, priority, loop) {

			priority = !ss.isEmpty(priority) ? priority : ss.SoundPriority.LOW;
			loop = !ss.isEmpty(loop) ? loop : false;

			// this sound manager doesn't support multi channel sounds, so bail.
			if (priority == ss.SoundPriority.MULTI_CHANNEL_ONLY) {
				return ;
			}

			// if there's a high priority sound playing, and we aren't high priority, bail.
			if (_currentSoundId !== null && _currentPriority == ss.SoundPriority.HIGH && priority != ss.SoundPriority.HIGH) {
				return;
			}

			// if we don't have any sounds loaded, bail.
			if (ss.isEmpty(_spriteMap)) {
				console.warn ("AudioElementSoundManager: Trying to play a sound without setting an audiosprite.");
				return;
			}

			// if there's no sound by that name, bail.
			if (ss.isEmpty(_spriteMap[soundId])) {
				return;
			}

			// if we're on iOS and our volume is less than 1/2, bail.
			var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
			if (iOS && _volume < 0.5) {
				return;
			}

			// pause the audio, just in case.
			_audioElement.pause();

			// Stop the sound immediately if the sound isn't ready yet
			if (!_ready) {
				// If the sound is high priority, queue it for later
				if (priority == ss.SoundPriority.HIGH) {
					_queuedAudio.push({ "soundId" : soundId, "priority" : priority, "loop" :loop });
					return;
				}
				_currentSoundEnd = 0;
			}

			// set the currently playing sound, and the current priority.
			_currentSoundId = soundId;
			_currentPriority = priority;
			_currentSoundLoop = loop;

			// determine where in the sound sprite to play.
			var start = _spriteMap[soundId].start;
			var end = _spriteMap[soundId].end;
			// var length = end - start;

			_currentSoundStart = start;
			_currentSoundEnd = end;

			// TESTING
			// ss.DebugUtil.showMessage("Play Audio: ID = " + soundId + " Duration = " + _audioElement.duration + ", Start = " + start + ", End = " + end, "SoundPlay" + soundId);


			// play the sound, and setup the tween to stop the sound.
			_audioElement.currentTime = start;

			// Start playing the sound right away if not paused
			if (!_isPaused) {
				_audioElement.play();
			}
		};

		_this.onTimeUpdate = function(e) { // jshint ignore:line
			var finSound;

			// Don't perform updates if paused
			if (_isPaused) {
				return;
			}

			// TESTING
			ss.DebugUtil.showMessage("CurSound: " + _currentSoundId + ", Sound: CurTime: " + _audioElement.currentTime + ", EndTime: " + _currentSoundEnd, "SoundCheck");

			if (_currentSoundId !== null) {
				// Check if the sound is erroniously playing from prior to its start time, and try to correct it
				if (_audioElement.currentTime < _currentSoundStart - _TIME_TOLERANCE) {
					// TESTING
					// console.log("AudioElementSoundManager: Sound started from incorrect time! Start = " + _currentSoundStart + ", Actual = " + _audioElement.currentTime);

					_audioElement.currentTime = _currentSoundStart;
				}

				// Check if the sound has completed playing
				if (_audioElement.currentTime >= _currentSoundEnd) {
				_audioElement.pause();
				_audioElement.currentTime = 0;
					if (!_currentSoundLoop) {
						finSound = _currentSoundId;
						_currentSoundId = null;

						// Start any additional queued sounds
						_tryPlayNextQueuedSound();

						_this.dispatchEvent(new ss.BaseEvent("complete", finSound));
					} else {
						_this.playSound(_currentSoundId, _currentPriority, _currentSoundLoop);
					}
				}
			}
		};

		/*
		* Pause a particular sound currently being played
		* @param soundId:[String] - Id of the sound to pause
		*/
		_this.pauseSound = function(soundId) {
			if (!ss.isEmpty(_currentSoundId) && _currentSoundId == soundId) {
				_audioElement.pause();
			}
		};

		/*
		* Resume a particular sound currently being played
		* @param soundId:[String] - Id of the sound to resume
		*/
		_this.resumeSound = function(soundId) {
			if (!ss.isEmpty(_currentSoundId) && _currentSoundId == soundId && !_isPaused) {
				_audioElement.play();
			}
		};

		/*
		* Pause all currently playing sounds
		*/
		_this.pauseAllSounds = function() {
			// Do nothing if already paused
			if (_isPaused) {
				return;
			}

			if (!ss.isEmpty(_currentSoundId)) {
				_audioElement.pause();
			}

			_isPaused = true;
		};

		/*
		* Resume all currently playing sounds
		*/
		_this.resumeAllSounds = function() {
			// Do nothing if not currently paused
			if (!_isPaused) {
				return;
			}

			if (!ss.isEmpty(_currentSoundId)) {
				_audioElement.play();
			}

			_isPaused = false;
		};

		/*
		* Stops all instances of sounds playing with the given soundId.
		* @param soundId: The identifier of the sound instance(s) to stop.
		*/
		_this.stopSound = function(soundId) {
			var finSound;

			if (_currentSoundId == soundId) {
				_audioElement.pause();
				finSound = _currentSoundId;
				_currentSoundId = null;
				_this.dispatchEvent(new ss.BaseEvent("complete", finSound));

			}
		};

		/*
		* Stop all sounds that are currently playing.
		*/
		_this.stopAllSounds = function() {
			var finSound;
			finSound = _currentSoundId;

			if (!ss.isEmpty(_audioElement)) {
				_audioElement.pause();
			}

			_currentSoundId = null;

			_this.dispatchEvent(new ss.BaseEvent("complete", finSound));
		};

		/*
		* Sets the global volume for the sound manager.
		* @param vol: the volume to use, ranging from (0-1) meaning (silence-full volume)
		* NOTE: iOS does not respect the volume property on _audioElements,
		*  -on iOS a volume less than 0.5 will play no sounds.
		*  -on iOS a volume greater than or equal to 0.5 will play soudns at full volme.
		*/
		_this.setVolume = function(vol) {
			if (ss.isEmpty(_audioElement)) {
				return;
			}
			_audioElement.volume = vol;

			_volume = vol;

			var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );

			if (iOS && vol < 0.5)  {
				_this.stopAllSounds();
			}
		};

		/*
		* Gets the current volume.
		* @return - the current volume of the sound manager.
		*/
		_this.getVolume = function() {
			return _volume;
		};

		/*
		* Gets a list of currently playing sounds.
		* @return - a list of the soundIds that are currently playing.
		* NOTE: If a sound has multiple playing instances, it will be listed multiple times.
		*/
		_this.getPlayingSounds = function() {
			return [ _currentSoundId ];
		};

		/*
		* Returns whether there are any instances of a given SoundId playing.
		*/
		_this.isPlaying = function(soundId) {
			return (_currentSoundId == soundId);
		};

		/*
		 * Check if the audio id is valid
		 * @param soundId: The id of the sound from the sound sprite. NOTE: using a sound that isn't in the sprite will result in an error.
		 * @return - true if the audio id is valid and exist such data, false otherwise
		 */
		function _isValid (soundId) {

			// if we haven't set a sprite map, bail.
			if (ss.isEmpty(_spriteMap)) {
				return false;
			}

			// if there's no sound by that name, bail.
			if (ss.isEmpty(_spriteMap[soundId])) {
				return false;
			}

			return true;
		}

		return _construct ();
	}; // jshint ignore:line

} ());

/* global require, createjs, ss, console */



( function() {
	"use strict";

	/*
	* CreateJS Sound Manager.
	* Provides access to soundJS, and adds audio priority.
	* Dispatches the following events:
	*	"loaded" -> when an audio sprite has finished loading.
	* 	"complete"  (audioId) -> When an audio file finishes playing, or when one cycle of a looping audio track finishes.
	*/
	ss.CreateJSSoundManager = ss.CreateJSSoundManager || new function() { // jshint ignore:line

		// locally scoped copy of this, extend AbstractEventDispatcher.
		var _this = ss.AbstractEventDispatcher ( this );

		// sprite map definition within the audio sprite.
		var _spriteMap;

		// Queued audio file (if play was called before audio was loaded.)
		var _queuedAudio;

		// Whether or not the audio sprite has been loaded.
		var _loaded;

		// [Array<Object>] List of objects containing data for currently playing sounds.
		var _currentSounds = [];

		// Current volume.
		var _volume;

		// [Boolean] - Whether the sound manager is currently paused or not
		var _isPaused;

		/*
		* Constructor. Volume defaults to 1.
		*/
		function _construct () {
			_volume = 1;
			_loaded = false;
			_isPaused = false;

			return _this;
		}

		/*
		 * Check if the audio id is valid
		 * @param soundId: The id of the sound from the sound sprite. NOTE: using a sound that isn't in the sprite will result in an error.
		 * @return - true if the audio id is valid and exist such data, false otherwise
		 */
		function _isValid (soundId) {

			// if we haven't set a sprite map, bail.
			if (ss.isEmpty(_spriteMap)) {
				return false;
			}

			// if we haven't loaded yet, queue the playback.
			if (!_loaded) {
				_queuedAudio = soundId;
				return false;
			}

			// if there's no sound by that name, bail.
			if (ss.isEmpty(_spriteMap[soundId])) {
				return false;
			}

			return true;
		}

		_this.isValid = _isValid;

		/*
		* Loads an audio sprite.
		* @param audioDefinitionJSON: an object containing an audio definition, as output by the audiosprite tool here: https://github.com/tonistiigi/audiosprite
		* See weblib/examples/sound/SoundTest.js for a sample output in the correct format.
		*/
		_this.loadSoundSprite = function(audioDefinitionJson) {
			createjs.Tween.removeAllTweens();
			createjs.Sound.removeAllSounds();
			createjs.Sound.removeAllEventListeners();
			_currentSounds = [];

			// pull the data we need out of the JSON.
			_spriteMap = audioDefinitionJson.spritemap;
			var audioResources = audioDefinitionJson.resources;

			// determine the main file, and build a list of all available extensions.
			var i;
			var extensionList = [];

			// use the 1st file as a default, unless we find one in our preferred format.
			var mainFile = audioResources[0];

			for(i = 0; i < audioResources.length; i++) {
				var res = audioResources[i];

				// extract the file extension.
				var extension = res.substring(res.length - 3, res.length);
				if (extension !== "ogg") {
					// if it's not an ogg, ad dit to the extension list.
					extensionList.push(extension);
				}

				// if we find an ogg version of the file, use that one, as it has the
				// best compatibility across desktop browsers. see: https://developer.mozilla.org/en-US/docs/Web/HTML/Supported_media_formats for details on browser support.
				if (extension === "ogg") {
					mainFile = res;
				}
			}

			// set the list of alternate extensions.
			createjs.Sound.alternateExtensions = extensionList;

			// wait for load.
			createjs.Sound.addEventListener("fileload", createjs.proxy(_this.handleLoad, _this));

			// register the audio sprite.
			createjs.Sound.registerSound(mainFile, "mainAudio");
		};

		/*
		* Listener for when load is complete.
		*/
		_this.handleLoad = function(event) { // jshint ignore:line
			// set the loaded flag.
			_loaded = true;

			// Stop all sound just in case some browser start playing audio by default
			//_this.stopAllSounds();

			// If we have a queued piece of audio, play it.
			if (!ss.isEmpty(_queuedAudio)) {
				_this.playSound(_queuedAudio);
			}

			document.addEventListener("touchstart", _playEmpty);

			// dispatch the loaded event.
			_this.dispatchEvent(new ss.BaseEvent("loaded"));

		};

		function _playEmpty() {
			document.removeEventListener("touchstart", _playEmpty);
			createjs.WebAudioPlugin.playEmptySound();
		}

		/*
		* Return the audio text that came with the audio clip.
		* @param soundId: The id of the sound from the sound sprite. NOTE: using a sound that isn't in the sprite will result in an error.
		* @return - the text specified in the json file for given audio, otherwise return undefined.
		*/
		_this.getText = function(soundId) {

			// get the text field of the audio sprite data
			if (_isValid(soundId)) {
				var audioText = _spriteMap[soundId].text;
				return audioText;
			}
			return undefined;
		};

		/*
		* Return the audio length of the audio clip.
		* @param soundId: The id of the sound from the sound sprite. NOTE: using a sound that isn't in the sprite will result in an error.
		* @return - the length in second of the audio clip.
		*/
		_this.getDuration = function(soundId) {

			// extract the times for the audio file.
			if (_isValid(soundId)) {
				var start = _spriteMap[soundId].start;
				var end = _spriteMap[soundId].end;
				var length = end - start;
				return length;
			}
			return 0;
		};

		/*
		* Play a sound.
		* @param soundId: The id of the sound from the sound sprite. NOTE: using a sound that isn't in the sprite will result in an error.
		* @param priority (optional): a SoundPriority specifying the priority of the sound we are starting.
		* @param loop: True to loop the sound, false otherwise.
		*/
		_this.playSound = function(soundId, priority, loop) {

			console.log("PLAYING: " + soundId);
			// default priority to low.
			if (ss.isEmpty(priority))
			{
				priority = ss.SoundPriority.LOW;
			}

			// default loop to false.
			if (ss.isEmpty(loop)) {
				loop = false;
			}

			// if we haven't set a sprite map, bail.
			if (ss.isEmpty(_spriteMap)) {
				return;
			}

			// if we haven't loaded yet, queue the playback.
			if (!_loaded) {
				_queuedAudio = soundId;
				return;
			}

			// if there's no sound by that name, bail.
			if (ss.isEmpty(_spriteMap[soundId])) {
				console.warn("Good sir, your request to play sound '" + soundId + "' is completely unreasonable, for it does not exist!");
				return;
			}

			// extract the times for the audio file.
			var start = _spriteMap[soundId].start;
			var end = _spriteMap[soundId].end;
			var length = end - start;

			// TESTING
			// ss.DebugUtil.showMessage("Play Audio: ID = " + soundId + ", Start = " + start + ", End = " + end, "SoundPlay" + soundId);
			// END TESTING

			// console.log("START: " + start + " END: " + end + " LENGTH: " + length);
			// create a new instance for playback.
			var instance = createjs.Sound.createInstance("mainAudio");

			// set the interrupt on the audio playback based on the priority.
			var interrupt;
			if (priority == ss.SoundPriority.HIGH) {
				// set high priority stuff to interrupt, and anything else to not.
				interrupt = createjs.Sound.INTERRUPT_ANY;
				// console.log("PLAYING: " + soundId + " WITH INTERRUPT_ANY");
			} else {
				interrupt = createjs.Sound.INTERRUPT_NONE;
				// //console.log("PLAYING: " + soundId + " WITH INTERRUPT_NONE");
			}

			// play the clip.
			instance.play({ offset : start * 1000, interrupt : interrupt, volume : _volume });

			// create a holder for the instance and the sound ID (to be referenced if we need to stop a sound.)
			var soundObj = { soundId : soundId, instance : instance, priority : priority };
			_currentSounds.push (soundObj);

			if (!loop) {
				// if we aren't looping, stop the audio when the length is up.
				createjs.Tween.get(instance).wait(length * 1000).call(function() {
					var remIndex;

					instance.stop();
					_this.dispatchEvent(new ss.BaseEvent("complete", soundObj.soundId));

					// Remove the sound from the current sounds list
					remIndex = _currentSounds.indexOf(soundObj);
					if (remIndex < 0) {
						console.log("!! WARNING !! Couldn't find sound " + soundObj.soundId + " in the current sounds list while attempting to stop it!");
					} else {
						_currentSounds.splice(remIndex, 1);
					}
				});
			} else {
				// if we are looping, restart the audio when the length is up.
				createjs.Tween.get(instance).wait(length * 1000).call(function() {
					var remIndex;

					instance.stop();
					_this.dispatchEvent(new ss.BaseEvent("complete", soundObj.soundId));

					// Remove the sound from the current sounds list
					remIndex = _currentSounds.indexOf(soundObj);
					if (remIndex < 0) {
						console.log("!! WARNING !! Couldn't find sound " + soundObj.soundId + " in the current sounds list while attempting to stop it!");
					} else {
						_currentSounds.splice(remIndex, 1);
					}

					_this.playSound(soundObj.soundId, priority, loop);
				});
			}
		};

		/*
		* Pause a particular sound currently being played
		* @param soundId:[String] - Id of the sound to pause
		*/
		_this.pauseSound = function(soundId) {
			// Look for this sound in the list of current sounds and pause it
			for(var i = 0; i < _currentSounds.length; i++) {
				if (_currentSounds[i].soundId == soundId) {
					_currentSounds[i].instance.pause();
				}
			}
		};

		/*
		* Resume a particular sound currently being played
		* @param soundId:[String] - Id of the sound to resume
		*/
		_this.resumeSound = function(soundId) {
			// Look for this sound in the list of current sounds and resume it
			for(var i = 0; i < _currentSounds.length; i++) {
				if (_currentSounds[i].soundId == soundId) {
					_currentSounds[i].instance.resume();
				}
			}
		};

		/*
		* Pause all currently playing sounds
		*/
		_this.pauseAllSounds = function() {
			// Do nothing if already paused
			if (_isPaused) {
				return;
			}

			// Loop through all current sounds and pause them
			for(var i = 0; i < _currentSounds.length; i++) {
				_currentSounds[i].instance.paused = true;
			}

			_isPaused = true;
		};

		/*
		* Resume all currently playing sounds
		*/
		_this.resumeAllSounds = function() {
			// Do nothing if not currently paused
			if (!_isPaused) {
				return;
			}

			// Loop through all current sounds and resume them
			for(var i = 0; i < _currentSounds.length; i++) {
				_currentSounds[i].instance.paused = false;
			}

			_isPaused = false;
		};

		/*
		* Stops all instances of sounds playing with the given soundId.
		* @param soundId: The identifier of the sound instance(s) to stop.
		*/
		_this.stopSound = function(soundId) {
			// keep a list of which sounds need to be removed from the currently playing list.
			var toRemove = [];
			// var removeIndices = [];
			var snd;
			var i;
			var remIndex;

			// Look through all sounds for any with a matching ID
			for(i = 0; i < _currentSounds.length; i++) {
				snd = _currentSounds[i];

				if (snd.soundId == soundId) {

					// if the sound matches, stop it and flag it to be removed from the currently playing list.
					if (!ss.isEmpty(snd.instance)) {
						snd.instance.stop();

						// stop any tweens waiting on the end of the sound.
						createjs.Tween.removeTweens(snd.instance);
					}
					_this.dispatchEvent(new ss.BaseEvent("complete", snd.soundId));
					toRemove.push(snd);
				}
			}

			// remove the stopped instances from the currently playing list.
			for(i = 0; i < toRemove.length; i++) {

				remIndex = _currentSounds.indexOf(toRemove[i]);
				if (remIndex >= 0) {
					_currentSounds.splice(remIndex, 1);
				}
				// _currentSounds.pop(toRemove[i]);

			}
		};

		/*
		* Stop all sounds that are currently playing.
		* @param priority:SoundPriority (Optional) - If provided, only sounds with this priority will be stopped
		*/
		_this.stopAllSounds = function( priority ) {

			var snd;
			// keep a list of which sounds need to be removed from the currently playing list.
			var toRemove = [];
			var remIndex;

			for(var i = 0; i < _currentSounds.length; i++) {
				snd = _currentSounds[i];

				if (ss.isEmpty(priority) || snd.priority === priority)
	 			{
					snd.instance.stop();
					toRemove.push(snd);

					// stop any tweens that are waiting on the end of the sound.
					createjs.Tween.removeTweens(snd.instance);
					_this.dispatchEvent(new ss.BaseEvent("complete", snd.soundId));
				}
			}

			// remove the stopped instances from the currently playing list.
			for(i = 0; i < toRemove.length; i++) {
				remIndex = _currentSounds.indexOf(toRemove[i]);
				if (remIndex >= 0) {
					_currentSounds.splice(remIndex, 1);
				}
				// _currentSounds.pop(toRemove[i]);
			}
		};

		/*
		* Sets the global volume for the sound manager.
		* @param vol: the volume to use, ranging from (0-1) meaning (silence-full volume)
		*/
		_this.setVolume = function(vol) {
			var snd;

			_volume = vol;
			for(var i = 0; i < _currentSounds.length; i++) {
				snd = _currentSounds[i];

				// set the volume on any currently playing instances.
				snd.instance.volume = _volume;
			}
		};

		/*
		* Gets the current volume.
		* @return - the current volume of the sound manager.
		*/
		_this.getVolume = function() {
			return _volume;
		};

		/*
		* Gets a list of currently playing sounds.
		* @return - a list of the soundIds that are currently playing.
		* NOTE: If a sound has multiple playing instances, it will be listed multiple times.
		*/
		_this.getPlayingSounds = function() {
			var soundList = [];
			var snd;

			for(var i = 0; i < _currentSounds.length; i++) {
				snd = _currentSounds[i];
				soundList.push(snd.soundId);
			}
			return soundList;
		};

		/*
		* Returns whether there are any instances of a given SoundId playing.
		*/
		_this.isPlaying = function(soundId) {
			var snd;

			for(var i = 0; i < _currentSounds.length; i++) {
				snd = _currentSounds[i];
				if (snd.soundId == soundId) {
					return true;
				}
			}
			return false;
		};

		_this.isPlayingAnything = function() {
			return _currentSounds.length > 0;
		};

		_this.release = function(){
			// locally scoped copy of this, extend AbstractEventDispatcher.
			_spriteMap = undefined;
			_queuedAudio = undefined;
			_loaded = undefined;
			_currentSounds = undefined;
			_volume = undefined;
			_isPau = undefined;
		};

		return _construct ();
	}; // jshint ignore:line

} ());

/* global require, createjs, ss, console*/







( function() {
	"use strict";

	/*
	* Sound Manager.
	* Delegates it's work out to either a CreateJSSoundManager or an AudioElementSoundManager, depending on support.
	*/
	ss.SoundManager = ss.SoundManager || new function() { // jshint ignore:line

		// locally scoped copy of this. Will be set to the appropriate sound manager in the constructor.
		var _this;

		function _construct () {

			if (ss.isEmpty(_this)) {

				// Always use AudioElementSoundManager on Silk
				if (ss.SystemInfo.isSilk) {
					_this = ss.AudioElementSoundManager;

				// Always use AudioElementSoundManager on IE and possibily Samsung Galaxy Tab 4
				// } else if(ss.SystemInfo.isIE) ||  (navigator.userAgent.indexOf("SAMSUNG SM-T330NU") > 0)) {
				} else if (ss.SystemInfo.isIE) {
					// TESTING
					console.log("ss.SoundManager - [IE]");

					_this = ss.AudioElementSoundManager;
				// If soundJS is supported, use the createjs sound manager.
				} else if (createjs.Sound.initializeDefaultPlugins()) {
					// TESTING
					console.log("ss.SoundManager - using CreateJSSoundManager.");

					_this = ss.CreateJSSoundManager;
					_this.name = "SM__"+Math.round(Math.random()*10000/Math.random());
				// if soundJS is not supported, use the audio element sound manager.
				} else {
					// TESTING
					console.log("ss.SoundManager - using AudioElementSoundManager.");

					_this = ss.AudioElementSoundManager;
				}
			} else {
				console.log("SM not empty, already exists.");
			}
			console.log("SM CONSTRUCTED, is "+_this);
			return _this;
		}

		return _construct();
	}; // jshint ignore:line

	// Event dispatched when all sounds are loaded for a sound manager
	ss.SoundManager.LOADED = "loaded";
	// Event dispatched when a sound has completed playing
	ss.SoundManager.SOUND_COMPLETE = "complete";


	/*
	* Sound Priority,
	* specifies when sounds should interrupt other sounds, and when they should play.
	*/
	ss.SoundPriority = {
		HIGH               : 0, // Will play a sound no matter what is currently playing.
		LOW                : 1,	// Plays a sound if there are channels available. If there aren't channels available, play if the current sound isn't high priority, otherwise don't.
		MULTI_CHANNEL_ONLY : 2 // Only plays sounds on devices with support for multiple audio channels.
	};

} ());

/* global require, ss, createjs, console */



(function () {
    "use strict";

    /**
     * 'static' singleton Class CreateJSCaptionManager
     *
     * Displays one caption at a time 
     * - You set the Container in which captions are displayed
     * - You define styles which can be used on the fly to customize each caption
     * 
     */
    ss.CreateJSCaptionManager = ss.CreateJSCaptionManager || new function() { // jshint ignore:line

        var _this = ss.AbstractEventDispatcher ( this );
        
        var _caption, _captionContainer, _boxX, _boxY, _boxW, _boxH, _boxStyle, _boxCol, _textCol, _textFont;

        var _styleParams = {
                "roundRect": {func: _drawRoundRectBox}
        };

        function _construct(){
            
            _boxStyle = "roundRect";

            return _this;
        }

        /**
         * Custom design your captions any way you want!  
         * Give each character in your game a uniquely looking caption!
         * 
         * Provide a name for your style, a function that will style a provided caption DisplayObject, and any
         * additional parameters (e.g. image references) your function may need to run.
         * 
         * To display a caption in your style, call ss.CreateJSCaptionManager.showCaption("the caption text", "yourStyle");
         * Your function will be called on to draw the caption.
         * Your function will receive two parameters:
         * 
         * 1 - A createJS DisplayObject ('cap') containing references to its two children:
         *      cap.back, a createJS Shape, and 
         *      cap.textField, a createJS Textfield
         *
         * 2 - A string with the text to be displayed in the caption. 
         *
         * 3 - The parameters you provided.
         *
         * The rest (positioning, sizing, colours, design, etc.) is all up to you.
         * -------------------------------------------------------------------------------------------------
         * @param {string}      styleName   -- an identifier for your custom style
         * @param {function}    styleFunc   -- a function to draw a caption box in your style
         * @param {object}      styleParams -- any additional parameters your function needs
         * @return {void}
         */
        _this.defineNewStyle = function ( styleName, styleFunc, styleParams ){
            if( typeof styleFunc == "function" ){

                if(!ss.isEmpty(_styleParams[styleName])){
                    console.warn("! Overwriting existing caption style named "+styleName);
                }  
                _styleParams[ styleName ] = {func:styleFunc, params:styleParams};  

            } else {
                var exceptionText = "The provided style does not include a valid function.";
                throw exceptionText;
            }
        };

        /**
         * Alternately to defining a whole style, just set a bunch of default style values and 
         * call ss.CreateJSCaptionManager.showCaption("the caption text") to use the default draw function. 
         * ---------------------------------------------------------------------------------------------------
         * @param  {Number} x           x position of caption on screen
         * @param  {Number} y           y position of caption on screen
         * @param  {Number} w           width of caption box
         * @param  {Number} h           height of caption box
         * @param  {String} capStyle    Name of a defined style function to use in drawing the caption
         * @param  {String} textCol     CSS color name or hex value for the text
         * @param  {String} textFont    CSS font style, e.g. "24px gill_sans-boldregular"
         * @param  {String} bgCol      CSS color name or hex value for the background box of the caption
         * @return {void}
         */
        _this.formatCaptions = function( x, y, w, h, capStyle, textCol, textFont, bgCol ){
            _boxX = x || _boxX;
            _boxY = y || _boxY;
            _boxW = w || _boxW;
            _boxH = h || _boxH;
            _boxStyle = capStyle || _boxStyle;
            _textCol = textCol || _textCol;
            _textFont = textFont || _textFont;
            _boxCol = bgCol || _boxCol;
        };

        /**
         * Display captions inside the indicated container object.
         * @param  {createJS Container} container 
         * @return {void}
         */
        _this.setContainer = function( container ){
            _captionContainer = container;
        };

        /**
         * For cleanup
         * @return {void}
         */
        _this.clearReferences = function(){
            _captionContainer = null;
            _caption = null;
        };

        
        /**
         * Create a caption to display the given text in the given style.
         * -------------------------------------------------------------------------------------------------------
         * @param  {String} newText  -- What the caption should read
         * @param  {String} capStyle -- name of a style function to use to draw the caption
         * @return {void}
         */
        _this.captionSpeechBox = function( newText, capStyle ){

            var margin = ( _boxW + _boxH )/20;
            var bs = (capStyle === undefined) ? _boxStyle : capStyle;

            // Clear out the _caption to be redrawn or create it if it doesn't yet exist.
            if ( !ss.isEmpty( _caption ) && !ss.isEmpty( _caption.back ) && !ss.isEmpty( _caption.textField ) ){
                _caption.back.graphics.clear();
            } else {
                _caption = new createjs.Container();
                _caption.back = new createjs.Shape();
                _caption.textField = new createjs.Text("",_textFont,_textCol);
                _caption.addChild( _caption.back);
                _caption.addChild( _caption.textField );
            }

            _caption.margin = margin;
            _caption.x = _boxX;
            _caption.y = _boxY;
            _caption.textField.font = _textFont;
            _caption.textField.color = _textCol;
            if (!ss.isEmpty(_caption.textField.text)) {
                _caption.textField.text = newText;
            }

            // Render the display box in the chosen style.
            _styleParams[ bs ].func( _caption, newText, _styleParams[ bs ].params );

            _caption.back.setBounds(
                ( ss.isEmpty( _caption ) ? 0 : _caption.x, ( ss.isEmpty( _caption )) ? 0 : _caption.y ), _boxW, _boxH
            );

            return _caption;
        };

        function _drawRoundRectBox( cap, txt ){
            try {
                cap.back.graphics.beginFill(_boxCol).drawRoundRect(0,0,_boxW,_boxH,_caption.margin*0.75);
                cap.textField.lineWidth = _boxW - cap.margin;
                cap.textField.x = cap.textField.y = cap.margin/2;
                if (!ss.isEmpty(_caption.textField.text)) {
                    cap.textField.text = txt;
                }
            } catch (err){
                console.warn("Unable to draw caption box:  "+err.message);
            }
        }

        /**
         * Make the display object appear to pulse (increase scale to 105% and then back)
         * @param  {Container} displayObj 
         * @param  {Number}    scaleMultiplier  OPTIONAL Make the button this much larger than its starting scale mid-pulse.
         * @param  {function} followFunc        OPTIONAL Call this function after pulsing
         * @return {void}            
         */
        _this.pulse = function( displayObj, scaleMultiplier, followFunc ){

            var BASE_SCALEX = displayObj.scaleX;
            var BASE_SCALEY = displayObj.scaleY;
            var startX = displayObj.x;
            var startY = displayObj.y;
            var startW = displayObj.getBounds().width;
            var startH = displayObj.getBounds().height;

            var POS_ADJ = ((scaleMultiplier || 1.05) - 1)/2;
            var isBitmapOrContainer = ((displayObj instanceof createjs.Bitmap) || (displayObj instanceof createjs.Container));
            var isCentredText = (displayObj instanceof createjs.Text && displayObj.textAlign=="center");

            var targetX = ((isBitmapOrContainer && displayObj.regX !== 0) || isCentredText) ? startX : startX-startW*POS_ADJ;
            var targetY = ((isBitmapOrContainer && displayObj.regY !== 0) || isCentredText) ? startY : startY-startH*POS_ADJ;
           
            var PULSE_SCALE = (scaleMultiplier || 1.05)*BASE_SCALEX;
            if(ss.isEmpty(followFunc)){
                followFunc = function(){};  
            } 
            createjs.Tween.get(displayObj).to({scaleX:PULSE_SCALE,scaleY:PULSE_SCALE,x:targetX,y:targetY},200).wait(100).to({scaleX:BASE_SCALEX,scaleY:BASE_SCALEY,x:startX,y:startY},200).wait(500).call(followFunc);
        };

        _this.showCaption = function( txt, capStyle, showEffect ){
            var withEffect = ss.isEmpty(showEffect) ? false : showEffect;
            _caption = _this.captionSpeechBox( txt, capStyle );
            _captionContainer.addChild( _caption );
            if(withEffect){
                _this.pulse(_captionContainer);
            }
        };

        _this.showCaptionForTime = function(txt, capStyle, showEffect, time) {
            _this.showCaption(txt, capStyle, showEffect);
            createjs.Tween.get(_captionContainer).wait(time).call(function() {_this.hideCaption(showEffect);});
        };

        _this.hideCaption = function( showEffect ){
            var withEffect = ss.isEmpty(showEffect) ? false : showEffect;

            var hideFunc = function(){
                try{
                    _captionContainer.removeChild( _caption );
                    createjs.Tween.removeTweens(_captionContainer);
                } catch (err){
                    console.warn(err.message);
                }
            };

            if(withEffect){
                _this.pulse(_captionContainer, 1.2, hideFunc);
            } else {
                hideFunc();
            }
            _this.dispatchEvent(new ss.BaseEvent(ss.CreateJSCaptionManager.HID_CAPTION_EVENT));
        };

        _this.release = function(){
            _caption.removeAllChildren();
            _caption = undefined; 
            _captionContainer = undefined; 
            _boxX = undefined; 
            _boxY = undefined; 
            _boxW = undefined; 
            _boxH = undefined; 
            _boxStyle = undefined; 
            _boxCol = undefined; 
            _textCol = undefined; 
            _textFont = undefined;
            _styleParams = undefined;
        };

        _this.release = function(){
            _caption.removeAllChildren();
            _caption = undefined; 
            _captionContainer = undefined; 
            _boxX = undefined; 
            _boxY = undefined; 
            _boxW = undefined; 
            _boxH = undefined; 
            _boxStyle = undefined; 
            _boxCol = undefined; 
            _textCol = undefined; 
            _textFont = undefined;
            _styleParams = undefined;
        };

        return _construct();
    }();
    ss.CreateJSCaptionManager.HID_CAPTION_EVENT = "HID_CAPTION_EVENT";
/*
    $("document").ready ( function  () {
        ss.CreateJSCaptionManager =  new CreateJSCaptionManager ();
    } );
  */  
} ());
/* global require, ss, createjs, console, soundsSpriteJSON */








( function () {
    "use strict";

    /**
     * 'static' singleton Class CreateJSVoiceOvers
     */
    

    ss.CreateJSVoiceOvers = ss.CreateJSVoiceOvers || new function() {  // jshint ignore:line

        var _this = ss.AbstractEventDispatcher ( this );
        
        var _captionManager;
        var _tweener = {};

        // JSONs
        var _captionsData = {};
        var _soundData = {};
        var _captionsContainer;
        var _baseURL;

        var _currentCaption = "";

        var _soundEnabled = false;

        function _construct(){

            return _this;
        }

        /**
         * Initialize your VoiceOver Manager with a sound sprite, a list of captions, a place to display
         * them, and a path to your assets!
         * ------------------------------------------------------------------------------------------------------
         * @param  {JSON} sounds_sprite 
         * @param  {JSON} captions -- An object with a caption text for each sound_id to match the sounds_sprite.
         * 
         * @param  {createjs.Container} in which the captions will be displayed.
         * @return {void} Dispatches a READY event when all assets are loaded.
         */
        _this.init = function( sounds_sprite, captions, captionsContainer, baseURL ){          
            _captionManager = ss.CreateJSCaptionManager;

            _soundData = sounds_sprite;
            _captionsData = captions;
            _captionsContainer = captionsContainer;
            _baseURL = baseURL;
            _loadAudio();
        };

        _this.clearReferences = function(){
            _captionsContainer = null;
            _captionManager.clearReferences();
        };

        _this.getSoundManager = function(){
            if(!_soundEnabled){
                console.warn("Returning Sound Manager, but sounds NOT ready to use yet.");
            }
            return ss.SoundManager;
            
            
        };

        function _loadAudio () {
            if(!ss.isEmpty(_soundData) && !ss.isEmpty(_soundData.spritemap)) {
                for (var firstSoundName in _soundData.spritemap){
                    
                    if(!ss.SoundManager.isValid(firstSoundName)){
            
                        console.dir(ss.SoundManager);

                        ss.SoundManager.addEventListener(ss.SoundManager.LOADED, _handleSoundLoaded);   
                        ss.SoundManager.addEventListener(ss.SoundManager.SOUND_COMPLETE, _handleSoundComplete); 

                        // Add in the namespace provided by the page that has loaded this game
                        // in order to correctly access the location of the audio files.
                        for(var i=0; i<soundsSpriteJSON.resources.length; i++){
                            soundsSpriteJSON.resources[i] = _baseURL + soundsSpriteJSON.resources[i];
                        }
                        ss.SoundManager.setVolume(1);
                        ss.SoundManager.loadSoundSprite(soundsSpriteJSON);
            
                    } else {                       
                        _announceReady();
                    }

                    break;
                } 
            }
        }

        function _handleSoundLoaded(){
            //console.log("_hSL");
            _announceReady();
        }

        function _handleSoundComplete( event ){
            if (event.data == _currentCaption){
                _captionManager.hideCaption();
                _currentCaption = "";
            }
        }

        function _announceReady(){
            _soundEnabled = true;
            _captionManager.setContainer(_captionsContainer);
            //console.log("audio all loaded, dispatch READY, code: ["+ss.CreateJSVoiceOvers.READY+"].");
            _this.dispatchEvent(new ss.BaseEvent(ss.CreateJSVoiceOvers.READY));
            
        }

        /**
         * ===================================================================================================
         * !! FULL INSTRUCTIONS FOR USE HERE --->    weblib/sound/CreateJSVoiceOvers  -> .defineNewStyle    !!
         * ---------------------------------------------------------------------------------------------------
         * @param {string}      styleName   -- an identifier for your custom style
         * @param {function}    styleFunc   -- a function to draw a caption box in your style,
         *                                     taking parameters (theCaptionObject, textToDisplay, yourParams)
         * @param {object}      styleParams -- any additional parameters your function needs
         */
        _this.defineCaptionStyle = function( styleName, styleFunc, styleParams ){
            _captionManager.defineNewStyle( styleName, styleFunc, styleParams );
        };

        /**
         * Alternately to defining a whole style, just set a bunch of default style values and 
         * call ss.CreateJSCaptionManager.showCaption("the caption text") to use the default draw function. 
         * ---------------------------------------------------------------------------------------------------
         * @param  {Number} x           x position of caption on screen
         * @param  {Number} y           y position of caption on screen
         * @param  {Number} w           width of caption box
         * @param  {Number} h           height of caption box
         * @param  {String} capStyle    Name of a defined style function to use in drawing the caption
         * @param  {String} textCol     CSS color name or hex value for the text
         * @param  {String} textFont    CSS font style, e.g. "24px gill_sans-boldregular"
         * @param  {String} bgCol      CSS color name or hex value for the background box of the caption
         * @return {void}
         */
        _this.formatCaptions = function( x, y, w, h, capStyle, textCol, textFont, bgCol ){
            _captionManager.formatCaptions( x, y, w, h, capStyle, textCol, textFont, bgCol );

        };

        _this.playAfter = function( sound_id, ms, captionStyle ){
            createjs.Tween.get( _tweener ).wait( ms ).call( _this.play, [ sound_id, captionStyle ] );
        };

        _this.play = function( sound_id, captionStyle ){
            if(_this.hasValidSoundId(sound_id)){
                ss.SoundManager.playSound( sound_id );

                if(_this.hasValidCaptionId(sound_id)){
                    _currentCaption = sound_id;
                    _captionManager.showCaption( _captionsData[ sound_id ], captionStyle );
                }
            } 
        };

        _this.stopAllVOs = function(){
            ss.SoundManager.stopAllSounds();
        };

        _this.hasValidSoundId = function( sound_id ){
            return ss.SoundManager.isValid( sound_id );
        };

        _this.hasValidCaptionId = function( sound_id ){
            // if we haven't set a captions JSON, bail.
            if (ss.isEmpty(_captionsData)) {
                return false;
            }
            // if there's no caption by that name, bail.
            if (ss.isEmpty(_captionsData[ sound_id])) {
                return false;
            }

            return true;
        };

         _this.release = function() {
            
            _this.stopAllVOs();
            _captionManager.hideCaption();
            _this.clearReferences();

            _captionManager = undefined;
            ss.SoundManager = undefined;
            _tweener = {};
            _captionsData = {};
            _soundData = {};
            _captionsContainer = undefined;
            _baseURL = undefined;
            _currentCaption = "";
            _soundEnabled = false;
        };

        return _construct();
    }();

    ss.CreateJSVoiceOvers.READY = "voiceovers_ready";

}());