/*! weblib-ss 0.0.1 */
/* global createjs, ss */

(function() {
	"use strict";

	IsometricTile.prototype = new createjs.Container();
	IsometricTile.prototype.constructor = IsometricTile;

	/**
	 * Class for a tile in an isometric grid system.
	 * @param {Number} width 			     - The width of the tile (in pixels).
	 * @param {Number} height 				 - The height of the tile (in pixels).
	 * @param {Number} depth    			 - The depth of the tile (in pixels).
	 * @param {createjs.DisplayObject} image - The image to use for the tile.
	 *                                        NOTE: the dimensions of the image, must match the supplied dimensions
	 *                                        or you will have unexpected results.
	 *                         ^     _
	 *                        / \     |
	 *                       /   \    |
	 *                      |\   /|  height
	 *                      | \ / |   |
	 *                 _    \  v  /  _|
	 *                |      \ | /
	 *               depth    \|/
	 *                |_       v
	 *
	 *                     |_width_|
	 */
	function IsometricTile (width, height, depth, image) {
		// call the parent container constructor.
		createjs.Container.call(this);

		// keep a locally scoped copy of this.
		var _this = this;

		// {Number} The width of the tile in pixels.
		var _width;

		// {Number} The height of the tile in pixels.
		var _height;

		// {Number} The "depth" of the tile in pixels.
		var _depth;

		// {createjs.Container} The container that holds the images.
		var _imageContainers = [];

		// {createjs.DisplayObject} The tile images (optional)
		var _images = [];

		// {createjs.Shape} The overlay shape drawn on top of the image.
		var _overlayShape;

		// {Number} The depth offset of this tile (in pixels)
		var _depthOffset;

		// {createjs.Container} The container on top of the tile, that objects can be put into to place them on this tile.
		var _tileContainers = [];

		// {Object} The game specific parameters for this tile.
		var _gameParameters = {};

		var _alpha;

		/**
		 * Start this instance.
		 * @param  {Number} width  					- The width of the tile (in pixels).
		 * @param  {Number} height 					- The height of the tile (in pixels).
		 * @param  {NumbeR} depth  					- The depth of the tile (in pixels).
		 * @param  {createjs.DisplayObject} image 	- The image to display for the tile.
		 * @return {ss.IsometricTile}        		- The tile itself.
		 */
		_this.start = function(width, height, depth, image) {

			_width = width;
			_height = height;
			_depth = depth;
			_depthOffset = 0;
			_alpha = 1;

			_imageContainers.push(new createjs.Container());
			_this.addChild(_imageContainers[0]);

			if (!ss.isEmpty(image)) {
				_images[0] = image;
				_images[0].regY = height / 2;
				_imageContainers[0].addChild(_images[0]);
			}

			_overlayShape = new createjs.Shape();
			_this.addChild(_overlayShape);

			_tileContainers.push(new createjs.Container());
			_this.addChild(_tileContainers[0]);

			return _this;
		};

		/**
		 * Get the width of the tile (in pixels). (see ASCII diagram at the top of the file for more info.)
		 * @return {Number} - The width of the tile (in pixels).
		 */
		_this.getWidth = function() {
			return _width;
		};

		/**
		 * Get the height of the tile (in pixels). (see ASCII diagram at the top of the file for more info.)
		 * @return {Number} - The height of the tile (in pixels).
		 */
		_this.getHeight = function() {
			return _height;
		};

		/**
		 * Get the depth of the tile (in pixels). (see ASCII diagram at the top of the file for more info.)
		 * @return {Number} - The depth of the tile (in pixels).
		 */
		_this.getDepth = function() {
			return _depth;
		};

		/**
		 * Get the depth offset (height above the default) of the tile (in pixels).
		 * @return {Number} - The depth offset.
		 */
		_this.getDepthOffset = function() {
			return _depthOffset;
		};

		/**
		 * Set the depth offset for this tile.
		 * @param {Number} depthOffset - The new depth offset.
		 */
		_this.setDepthOffset = function(depthOffset) {
			var i;

			_depthOffset = depthOffset;

			for(i = 0; i < _tileContainers.length; i++) {
				_tileContainers[i].y = _depthOffset;
			}
			for(i = 0; i < _images.length; i++) {
				var image = _images[i];
				if (!ss.isEmpty(image)) {
					image.y = _depthOffset;
				}
			}

			_overlayShape.y = _depthOffset;
		};

		_this.clearAllImagesExceptBottom = function() {
			for(var i = 1; i < _images.length; i++) {
				var image = _images[i];
				image.visible = false;
			}
		};

		_this.showAllImages = function() {
			for(var i = 1; i < _images.length; i++) {
				var image = _images[i];
				image.visible = true;
			}
		};

		/**
		 * Set the tile image.
		 * @param {createjs.DisplayObject} image - The image to draw for the tile.
		 */
		_this.setImage = function(image, layer) {
			if (ss.isEmpty(layer)) {
				layer = 0;
			}

			if (!ss.isEmpty(_images[layer])) {
				_imageContainers[layer].removeChild(_images[layer]);
			}

			if (ss.isEmpty(_imageContainers[layer])) {
				_imageContainers[layer] = new createjs.Container();
				_this.addChild(_imageContainers[layer]);
			}

			_images[layer] = image;
			// _images[layer].alpha = 0.5;
			var regY;
			if (!ss.isEmpty(image.getBounds())) {
				regY = image.getBounds().height - (_depth + (_height / 2));
			} else {
				regY = _height / 2;
			}
			_images[layer].regY = regY;
			_imageContainers[layer].addChild(_images[layer]);
		};

		_this.getImage = function(layer) {
			console.dir(_images);
			console.dir(_images[layer]);
			return _images[layer];
		};

		_this.setTopImage = function(image) {
			_this.setImage(image, _images.length - 1);
		};

		/**
		 * Push a new image onto the layer stack.
		 * @param  {createjs.DisplayObject} image - The image to add.
		 */
		_this.pushImage = function(image) {
			var nextLayer = _images.length;
			_this.setImage(image, nextLayer);

			_tileContainers[nextLayer] = (new createjs.Container());
			_this.addChild(_tileContainers[nextLayer]);

			_this.setDepthOffset(_depthOffset);

		};

		/**
		 * Set the game specific parameters for this tile.
		 * (NOTE: this is a generic object, as the game will set the properties on it based
		 * on the needs of the specific game).
		 * @param {Object} gameParameters - The game specific parameters for this tile.
		 */
		_this.setGameParameters = function(gameParameters) {
			_gameParameters = gameParameters;
		};

		/**
		 * Get the game specific parameters for this tile.
		 * @return {Object} - The game specific parameters for this tile.
		 */
		_this.getGameParameters = function() {
			return _gameParameters;
		};

		/**
		 * Get the container.
		 * @return {createjs.Container} - The container.
		 */
		_this.getContainer = function(layer) {
			if (ss.isUndefined(layer)) {
				layer = _tileContainers.length - 1;
			}
			return _tileContainers[layer];
 		};

 		/**
 		 * Set the shape alpha.
 		 * @param {Number} alpha - The alpha to set the shape to.
 		 */
 		_this.setShapeAlpha = function(alpha) {
 			_overlayShape.alpha = alpha;
 			_alpha = alpha;
 		};

		/**
		 * Draw the overlay shape.
		 * @param  {string} topColour     - The top colour.
		 * @param  {string} leftColour    - The left colour.
		 * @param  {string} rightColour   - The right colour.
		 * @param  {string} outlineColour - The outline colour.
		 * @param  {Number} strokeWidth   - The stroke width.
		 * @param  {Number} alpha         - The alpha.
		 */
		_this.drawShape = function(topColour, leftColour, rightColour, outlineColour, strokeWidth, alpha) {
			if (ss.isEmpty(topColour)) {
				topColour = "#FFFFFF";
			}

			if (ss.isEmpty(leftColour)) {
				leftColour = "#999999";
			}

			if (ss.isEmpty(rightColour)) {
				rightColour = "#CCCCCC";
			}

			if (ss.isEmpty(outlineColour)) {
				outlineColour = "#000000";
			}

			if (ss.isEmpty(strokeWidth)) {
				strokeWidth = 1;
			}

			if (ss.isEmpty(alpha)) {
				alpha = _alpha;
			}

			_overlayShape.alpha = alpha;
			var g = _overlayShape.graphics;

			g.clear();

			// draw the top.
			g.setStrokeStyle(strokeWidth);
			g.beginStroke(outlineColour);
			g.beginFill(topColour);
			g.moveTo(0, 0);
			g.lineTo(_width / 2, -_height / 2);
			g.lineTo(_width, 0);
			g.lineTo(_width / 2,  _height / 2);
			g.lineTo(0, 0);

			// draw the left.
			g.setStrokeStyle(strokeWidth);
			g.beginStroke(outlineColour);
			g.beginFill(leftColour);
			g.moveTo(0, 0);
			g.lineTo(0, _depth );
			g.lineTo(_width / 2, _height / 2 + _depth);
			g.lineTo(_width / 2, _height / 2);
			g.lineTo(0, 0);

			// draw the right.
			g.setStrokeStyle(strokeWidth);
			g.beginStroke(outlineColour);
			g.beginFill(rightColour);
			g.moveTo(_width / 2, _height / 2 );
			g.lineTo(_width, 0);
			g.lineTo(_width, depth);
			g.lineTo(_width / 2, _height / 2 + _depth);
			g.lineTo(_width / 2, _height / 2);
		};

		/**
		 * Release the memory used by this instance.
		 */
		_this.release = function() {
			var i;

			_width = undefined;
			_height = undefined;
			_depth = undefined;

			for(i = 0; i < _images.length; i++) {
				if (!ss.isEmpty(_images[i])) {
					_imageContainers[i].removeChild(_images[i]);
					_images[i] = undefined;
				}
			}
			_images = undefined;

			for(i = 0; i < _imageContainers.length; i++) {
				_this.removeChild(_imageContainers[i]);
				_imageContainers[i] = undefined;
			}
			_imageContainers = undefined;

			_depthOffset = undefined;

			for(i = 0; i < _tileContainers.length; i++) {
				_tileContainers[i].removeAllChildren();
				_this.removeChild(_tileContainers[i]);
				_tileContainers[i] = undefined;
			}
			_tileContainers = undefined;

			_this.removeChild(_overlayShape);
			_overlayShape = undefined;

			_gameParameters = undefined;

			_this = undefined;
		};

		// call the start method.
		return _this.start (width, height, depth, image);
	}

	// put this class into it's namespace.
	ss.IsometricTile = IsometricTile;

}());


/* global ss */

(function() {
	"use strict";

	IsometricTileset.prototype = {};
	IsometricTileset.prototype.constructor = IsometricTileset;

	/**
	 * Tileset for isometric engine.
	 */
	function IsometricTileset () {
		// {ss.IsometricTileset} keep a locally scoped copy of this
		var _this = this;

		// {Array} the list of tiles in this set.
		var _tiles;

		/**
		 * Construct this instance.
		 * @return {ss.IsometricTileset} - The tileset.
		 */
		_this.construct = function() {
			_tiles = [];
			return _this;
		};

		/**
		 * Remove a tile with a given tileNumber from the tileset, if it exists.
		 * @param  {Number} tileNumber - The tile number to remove.
		 */
		_this.removeTile = function(tileNumber) {
			for( var i = _tiles.length - 1; i >= 0; i--) {
				if (_tiles[i].tileNumber === tileNumber) {
					_tiles.splice(i, 1);
				}
			}
		};

		/**
		 * Check whether or not this tileset contains a tile with a given number.
		 * @param  {Number}  tileNumber - The tile number to check for.
		 * @return {Boolean}            - True if there's a number with the given tile, false otherwise.
		 */
		_this.hasTile = function(tileNumber) {
			for(var i = 0; i < _tiles.length; i++) {
				if (_tiles[i].tileNumber === tileNumber) {
					return true;
				}
			}
			return false;
		};

		/**
		 * Get the tile data for the tile with the given number.
		 * @param  {Number} tileNumber - The tile number to get.
		 * @return {Object}            - The tile data for the number in the form:
		 *                               {
		 *                               	"tileNumber"	 : {Number},
		 *                               	"displayObject"  : {createjs.DisplayObject},
		 *                               	"gameParameters" : {Object}
		 *                               }
		 */
		_this.getTile = function(tileNumber) {
			for(var i = 0; i < _tiles.length; i++) {
				if (_tiles[i].tileNumber === tileNumber) {
					return _tiles[i];
				}
			}
			return undefined;
		};

		/**
		 * Get the game specific parameters that were set for a given tile.
		 * @param  {Number} tileNumber - The tile number for the tile whose parameters to get.
		 * @return {Object}            - The game specific parameters for the tile.
		 */
		_this.getGameParametersForTile = function(tileNumber) {
			if (!ss.isEmpty(_this.getTile(tileNumber) && !ss.isEmpty(_this.getTile(tileNumber).gameParameters))) {
				return _this.getTile(tileNumber).gameParameters;
			} else {
				return {};
			}
		};

		/**
		 * Get the display object for a given tile.
		 * @param  {Number} tileNumber 		- The tile number for the tile whose display object to get.
		 * @return {createjs.DisplayObject} - A new clone of the displayObject for the tile.
		 */
		_this.getDisplayObjectForTile = function(tileNumber) {
			if (!ss.isEmpty(_this.getTile(tileNumber) && !ss.isEmpty(_this.getTile(tileNumber).displayObject))) {
				return _this.getTile(tileNumber).displayObject.clone();
			} else {
				return new createjs.Container();
			}
		};

		/**
		 * Add a tile to the tileset.
		 * @param {Number} tileNumber     				  - The tile number for the tile in the level file.
		 * @param {createjs.DisplayObject} displayObject  - The display object to show this tile with.
		 * @param {Object} gameParameters 				  - An object containing any game specific parameters this tile needs.
		 */
		_this.addTile = function(tileNumber, displayObject, gameParameters) {
			// don't allow duplicate tiles, because that could result in some strange things.
			if (_this.hasTile(tileNumber)) {
				_this.removeTile(tileNumber);
			}

			// Add the tile to the list.
			_tiles.push({
				"tileNumber"     : tileNumber,
				"displayObject"  : displayObject,
				"gameParameters" : gameParameters
			});
		};

		/**
		 * Release the memory used by this instance.
		 */
		_this.release = function() {
			_tiles = undefined;
			_this = undefined;
		};

		return _this.construct();
	}

	// put this class into it's namespace.
	ss.IsometricTileset = IsometricTileset;
}());

/* global require, createjs, ss */


(function() {
	"use strict";

	IsometricGrid.prototype = new createjs.Container();
	IsometricGrid.prototype.constructor = IsometricGrid;

	/**
	 * Class representing an isometric grid.
	 * @param {Number} width      - The width of the grid (in tiles).
	 * @param {Number} height     - The height of a grid (in tiles).
	 * @param {Number} tileWidth  - The width of a tile, in pixels.
	 * @param {Number} tileHeight - The height of the top of the tile, in pixels.
	 * @param {Number} tileDepth  - The depth (height of the bottom part) of the tile, in pixels.
	 *
	 *                         ^     _
	 *                        / \     |
	 *                       /   \    |
	 *                      |\   /|  height
	 *                      | \ / |   |
	 *                 _    \  v  /  _|
	 *                |      \ | /
	 *               depth    \|/
	 *                |_       v
	 *
	 *                     |_width_|
	 */
	function IsometricGrid (width, height, tileWidth, tileHeight, tileDepth) {
		// call the parent container constructor.
		createjs.Container.call(this);

		// keep a locally scoped copy of this.
		var _this = this;

		// {Number} - The width of the grid (in tiles).
		var _width;

		// {Number} - The height of the grid (in tiles).
		var _height;

		// {Number} - The width of the tiles (in pixels).
		var _tileWidth;

		// {Number} - The height of the tiles (in pixels).
		var _tileHeight;

		// {Number} - The depth of the tiles (in pixels).
		var _tileDepth;

		// {Array} - The tiles themselves [2d array].
		var _tiles = [];

		/**
		 * Start this instance.
		 */
		_this.start = function(width, height, tileWidth, tileHeight, tileDepth) {
			if (ss.isEmpty(tileWidth)) {
				tileWidth = 128;
			}

			if (ss.isEmpty(tileHeight)) {
				tileHeight = 64;
			}

			if (ss.isEmpty(tileDepth)) {
				tileDepth = 64;
			}

			_tileWidth = tileWidth;
			_tileHeight = tileHeight;
			_tileDepth = tileDepth;

			_width = width;
			_height = height;

			var x, y;

			for(x = 0; x < _width; x++) {
				_tiles[x] = [];
			}

			for(y = 0; y < _height; y++) {
				for(x = _width - 1; x >= 0 ; x--) {
					var newTile = new ss.IsometricTile(tileWidth, tileHeight, tileDepth);
					newTile.x = x * tileWidth + ((y % 2)  * tileWidth / 2);
					newTile.y = y * tileHeight / 2;
					_tiles[x][y] = newTile;
					_this.addChild(newTile);
				}
			}
		};

		_this.getWidth = function() {
			return _width;
		};

		_this.getHeight = function() {
			return _height;
		};

		/**
		 * Set the alpha of the shapes that are displayed on top of this grid.
		 * @param {[type]} alpha [description]
		 */
		_this.setShapeAlpha = function( alpha ) {
			for(var x = 0; x < _width; x++) {
				for(var y = 0; y < _height; y++) {
					_tiles[x][y].setShapeAlpha(alpha);
				}
			}
		};

		/**
		 * Draw the shapes for the isometric tiles.
		 */
		_this.drawShapes = function() {
			for(var x = 0; x < _width; x++) {
				for(var y = 0; y < _height; y++) {
					_tiles[x][y].drawShape();
				}
			}
		};

		/**
		 * Draw debug coordinates on the shape's containers.
		 * @param  {string} font   - (optional) createjs font specification.
		 * @param  {string} colour - colour of the text.
		 */
		_this.drawDebugCoordinates = function(font, colour) {
			if (ss.isEmpty(font)) {
				font = "20px Arial";
			}

			if (ss.isEmpty(colour)) {
				colour = "#000000";
			}

			for(var x = 0; x < _width; x++) {
				for(var y = 0; y < _height; y++) {
					var container = _tiles[x][y].getContainer();
					var newText = new createjs.Text("(" + x + ", " + y + ")", font, colour);
					newText.textAlign = "center";
					newText.textBaseline = "middle";

					container.addChild(newText);
					newText.x = _tileWidth / 2;
				}
			}
		};

		/**
		 * Get the tile corodinate at a given direction from the given coordinate.
		 * @param  {Number} xIndex    - The x-index  of the tile.
		 * @param  {Number} yIndex    - The y-index of the tile.
		 * @param  {Number} direction - The direction of the tile to get.
		 * @return {Array [x,y]}      - The coordinate in the given direction from the tile.
		 */
		_this.getTileCoordinateAtDirection = function(xIndex, yIndex, direction) {
			var isOffsetRow = (yIndex % 2 !== 0); // determine whether we're in an offset row or a non-offset row.
			var returnCoord = [ 0, 0 ];
			switch(direction) {
				case ss.IsometricGridDirection.LEFT:
					returnCoord = [ xIndex - 1, yIndex ];
				break;
				case ss.IsometricGridDirection.RIGHT:
					returnCoord = [ xIndex + 1, yIndex ];
				break;
				case ss.IsometricGridDirection.UP:
					returnCoord = [ xIndex, yIndex - 2 ];
				break;
				case ss.IsometricGridDirection.DOWN:
					returnCoord = [ xIndex, yIndex + 2 ];
				break;
				case ss.IsometricGridDirection.UP_LEFT:
					if (isOffsetRow) {
						returnCoord = [ xIndex, yIndex - 1 ];
					} else {
						returnCoord = [ xIndex - 1, yIndex - 1 ];
					}
				break;
				case ss.IsometricGridDirection.UP_RIGHT:
					if (isOffsetRow) {
						returnCoord = [ xIndex + 1, yIndex - 1 ];
					} else {
						returnCoord = [ xIndex, yIndex - 1 ];
					}
				break;
				case ss.IsometricGridDirection.DOWN_LEFT:
					if (isOffsetRow) {
						returnCoord = [ xIndex, yIndex + 1 ];
					} else {
						returnCoord = [ xIndex - 1, yIndex + 1 ];
					}
				break;
				case ss.IsometricGridDirection.DOWN_RIGHT:
					if (isOffsetRow) {
						returnCoord = [ xIndex + 1, yIndex + 1 ];
					} else {
						returnCoord = [ xIndex, yIndex + 1 ];
					}
				break;
			}
			return returnCoord;
		};

		/**
		 * Get the tiles connected to a given tile.
		 * @param  {Number} xIndex                        - The x-index of the tile we're getting tiles connected to.
		 * @param  {Number} yIndex                        - The y-index of the tile we're getting teils connected to.
		 * @param  {Boolean} includeDiagonals             - true to include diagonals [UP_LEFT, UP_RIGHT, DOWN_LEFT, DOWN_RIGHT], false otherwise.
		 * @param  {Boolena} includeHorizontalAndVertical - true to include horizontal and vertical [UP, DOWN, LEFT, RIGHT], false otherwise.
		 * @return {Array}                                - A list of coordinates of the connected tiles in the form [[x,y], [x,y], [x,y], [x,y]]
		 */
		_this.getConnectedTiles = function(xIndex, yIndex, includeDiagonals, includeHorizontalAndVertical) {
			var tiles = [];

			if (includeDiagonals) {
				tiles.push(_this.getTileCoordinateAtDirection(xIndex, yIndex, ss.IsometricGridDirection.UP_LEFT));
				tiles.push(_this.getTileCoordinateAtDirection(xIndex, yIndex, ss.IsometricGridDirection.UP_RIGHT));
				tiles.push(_this.getTileCoordinateAtDirection(xIndex, yIndex, ss.IsometricGridDirection.DOWN_LEFT));
				tiles.push(_this.getTileCoordinateAtDirection(xIndex, yIndex, ss.IsometricGridDirection.DOWN_RIGHT));
			}

			if (includeHorizontalAndVertical) {
				tiles.push(_this.getTileCoordinateAtDirection(xIndex, yIndex, ss.IsometricGridDirection.LEFT));
				tiles.push(_this.getTileCoordinateAtDirection(xIndex, yIndex, ss.IsometricGridDirection.RIGHT));
				tiles.push(_this.getTileCoordinateAtDirection(xIndex, yIndex, ss.IsometricGridDirection.UP));
				tiles.push(_this.getTileCoordinateAtDirection(xIndex, yIndex, ss.IsometricGridDirection.DOWN));
			}

			return tiles;
		};

		/**
		 * Get the height of a tile.
		 * @return {Number} - The height of a tile.
		 */
		_this.getTileHeight = function() {
			return _tileHeight;
		};

		/**
		 * Get the width of a tile.
		 * @return {Number} - The width of a tile.
		 */
		_this.getTileWidth = function() {
			return _tileWidth;
		};

		/**
		 * Get the center of the tile at the given coordinates.
		 * @param  {Number} xIndex - The x component of the coordinate.
		 * @param  {Number} yIndex - The y component of the coordinate.
		 * @return {ss.Vector2}    - The center of the tile.
		 */
		_this.getCenterOfTile = function(xIndex, yIndex) {
			return new ss.Vector2((_tiles[xIndex][yIndex].x + _tileWidth / 2) + _this.x, _tiles[xIndex][yIndex].y + _this.y);
		};

		/**
		 * Get the tile at a given index.
		 * NOTE: this function doesn't do any bounds checking, so it may give errors, or undefined if you ask for a coordinate outside of the grid.
		 * @param  {Number} xIndex 		- The x-index of the tile to get.
		 * @param  {Number} yIndex 		- The y-index of the tile to get.
		 * @return {ss.IsometricTile}   - The tile at the given coordinates.
		 */
		_this.getTile = function(xIndex, yIndex) {
			if (xIndex < 0 || xIndex >= _width) {
				return undefined;
			}
			if (yIndex < 0 || yIndex >= _height) {
				return undefined;
			}
			return _tiles[xIndex][yIndex];
		};

		/**
		 * Check if the given tile is in bounds.
		 * @param {Number} tileX - The x coordinate of the tile to check.
		 * @param {Number} tileY - The y coordinate of the tile to check.
		 * @return {Boolean}     - True if the tile is in bounds, false otherwise.
		 */
		_this.isTileInBounds = function(tileX, tileY) {
			// check if we're left of the bounds.
			if (tileX < 0) {
				return false;
			}

			// check if we're above the bounds.
			if (tileY < 0) {
				return false;
			}

			// check if we're to the right of the bounds.
			if (tileX > _width - 1) {
				return false;
			}

			// check if we're below the bounds.
			if (tileY > _height - 1) {
				return false;
			}

			return true;
		};

		/**
		 * Trace a diagonal from a starting file, and extending in a given direction.
		 * @param  {Array[x,y]} startingTile 				- The starting tile.
		 * @param  {ss.IsometricGridDirection} direction 	- The direction to cast out the diagonal.
		 * @return {Array}              					- The tiles along the diagnoal, in order, starting from the starting tile, and ending at the borders of the grid.
		 */
		function _traceDiagonal (startingTile, direction) {
			// keep a list of the tiles in the diagonal.
			var newDiagonal = [];

			// start at the starting tile.
			var currentTile = startingTile;

			// while we're still in bounds.
			while(_this.isTileInBounds(currentTile[0], currentTile[1])) {
				// add the tile to the diagonal, and go to the next tile.
				newDiagonal.push(_this.getTile(currentTile[0], currentTile[1]));
				currentTile = _this.getTileCoordinateAtDirection(currentTile[0], currentTile[1], direction);
			}

			return newDiagonal;
		}

		/**
		 * Get the direction of the diagonals that will correspond to the x-axis in the square grid version of this isometric grid.
		 * @return {ss.IsometricDirection} - The direction of the x-axis in the square grid, in the isometric grid.
		 */
		function _getXDiagonalDirection () {
			return ss.IsometricGridDirection.UP_LEFT;
		}

		/**
		 * Get the list of starting tiles to trace diagonals when converting to a square grid.
		 * @return {Array[Array[x,y], Number]} - The tiles coordinates in the list, as well as how much to offset them.
		 */
		function _getDiagonalStartingTilesWithcolumnOffsets () {
			var tileGridWidth = _this.getWidth();
			var tileGridHeight = _this.getHeight();

			var bottomcolumnOffset = (tileGridHeight % 2) === 0;

			var startingTiles = [];

			// IF THE BOTTOM ROW ISN'T OFFSET ( ODD HEIGHT) :
			// start at the bottom left, move right, until the end.
			// then go back one, and go UP-RIGHT.
			// then go up, until the end.
			// y- offsets start at 0.

			// IF THE BOTTOM ROW IS OFFSET ( EVEN HEIGHT):
			// start at the bottom left, move right, until the end.
			// then go up, until the end.
			// y- offsets start at 1.

			// Trace the bottom of the grid, moving from left to right.
			for(var x = 0; x < tileGridWidth; x++) {
									 /* coordinate */              /*    offset */
				startingTiles.push([ [ x, tileGridHeight - 1 ], tileGridWidth - x - 1 ]);
			}

			var lastTile = [ tileGridWidth - 1, tileGridHeight - 1 ];

			var offset = 1;

			// if we weren't offset, go up right before going up.
			if (!bottomcolumnOffset) {
				lastTile = _this.getTileCoordinateAtDirection(lastTile[0], lastTile[1],  ss.IsometricGridDirection.UP_RIGHT);
									 /* coordinate */    /* offset */
				startingTiles.push([ lastTile, 				0 ]);
			}

			// TRACE the right side of the grid, going up one tile at a time.
			var nextTile = _this.getTileCoordinateAtDirection(lastTile[0], lastTile[1],  ss.IsometricGridDirection.UP);
			while(_this.isTileInBounds(nextTile[0], nextTile[1])) {
				startingTiles.push([ nextTile, offset ]);
				nextTile = _this.getTileCoordinateAtDirection(nextTile[0], nextTile[1],  ss.IsometricGridDirection.UP);
				offset++;
			}

			return startingTiles;
		}

		/**
		 * Get a square grid from this tile grid.
		 * @return {Array[Array]} - a version of this grid, where the tiles are re-arranged into a square grid, 45 degrees from the isometric version of the grid.
		 */
		_this.getSquareGrid = function() {
			// get the tiles to start tracing diagonals from.
			var startingTiles = _getDiagonalStartingTilesWithcolumnOffsets();

			// prep the diagonals array.
			var diagonals = [];

			// get the direction to trace along.
			var direction = _getXDiagonalDirection();

			// we'll determine the height of the grid as we trace the diagonals.
			var newGridHeight = 0;

			// The width is the number of starting tiles.
			var newGridWidth = startingTiles.length;

			// loop through the starting tiles, and trace the diagonal starting at each one.
			for(var i = 0; i < startingTiles.length; i++) {
				// trace the diagonal.
	 			diagonals.push(_traceDiagonal(startingTiles[i][0], direction));

	 			// determine what the height of the array would be along that diagonal, taking into account how much it should be offset.
			   	var length = diagonals[i].length + startingTiles[i][1];

			   	// If the height is less than the lenght of the current diagonal + the offset,
			   	// update the height.
			   	if (newGridHeight < length) {
			   		newGridHeight = length + 1;
			   	}
			}

			// create a grid to store the square version of the isogrid in.
			var newGrid = [];

			// loop through the grid.
			for(var x = 0; x < newGridWidth; x++) {
				newGrid[x] = [];
				for(var initY = 0; initY < newGridHeight; initY++) {
					// initialize the array to unefined.
					newGrid[x][initY] = undefined;
				}

				// offset the column by the correct amount.
				var columnOffset = startingTiles[x][1];
				for(var y = 0; y < diagonals[x].length; y++) {
					newGrid[x][newGridHeight - (y + columnOffset) - 1] = diagonals[x][y];
				}
			}
			return newGrid;
		};

		/**
		 * Release the memory used by this instance.
		 */
		_this.release = function() {
			for(var x = 0; x < _width; x++) {
				for(var y = 0; y < _height; y++) {
					_this.removeChild(_tiles[x][y]);
					_tiles[x][y].release();
				}
			}

			_tiles = undefined;
			_width = undefined;
			_height = undefined;
			_tileWidth = undefined;
			_tileHeight = undefined;
			_tileDepth = undefined;

			_this = undefined;
		};

		_this.start(width, height, tileWidth, tileHeight, tileDepth);
	}

	ss.IsometricGrid = IsometricGrid;

	// {Object} - An enumeration containing the possible grid directions.
	ss.IsometricGridDirection = {};
	ss.IsometricGridDirection.UP_LEFT = "UP_LEFT";
	ss.IsometricGridDirection.UP_RIGHT = "UP_RIGHT";
	ss.IsometricGridDirection.DOWN_LEFT = "DOWN_LEFT";
	ss.IsometricGridDirection.DOWN_RIGHT = "DOWN_RIGHT";
	ss.IsometricGridDirection.UP = "UP";
	ss.IsometricGridDirection.DOWN = "DOWN";
	ss.IsometricGridDirection.LEFT = "LEFT";
	ss.IsometricGridDirection.RIGHT = "RIGHT";
}());


/* global ss, console */

(function() {
	"use strict";

	TiledLoader.prototype = {};
	TiledLoader.prototype.constructor = TiledLoader;

	/**
	 * Tiled loader, used to load isometric (staggered) tiles from the Tiled editor, output as .json files.
	 * http://www.mapeditor.org/ (NOTE: this code was tested with version 0.12.3 of Tiled for Windows)
	 */
	function TiledLoader () {
		// keep a locally scoped copy of this
		var _this = this;

		_this.construct = function() {
			return _this;
		};

		/**
		 * Load a tiled file, into a tile grid.
		 * @param  {ss.IsometricTileSet} tileSet         - The tileset to load.
		 * @param  {Object(JSON)} tiledFile       		 - The json output from the tiled editor.
		 * @param  {Array[String]} tilesLayers   		 - The name(s) of the layer in the file containing the tiles.
		 * @param  {String} depthLayer      			 - The name of the layer in the file containing depth information.
		 * @return {ss.IsometricGrid}                 	 - The isometric grid that the tiles have been loaded into.
		 */
		_this.load = function(tileSet, tiledFile, tilesLayers, depthLayer) {
			var width = tiledFile.width;
			var height = tiledFile.height;
			var tileWidth = tiledFile.tilewidth;
			var tileHeight = tiledFile.properties.TileHeight;
			var tileDepth = tiledFile.properties.TileDepth;
			var depthOffsetAmount = tiledFile.properties.DepthOffsetAmount;

			if (ss.isEmpty(tileHeight)) {
				tileHeight = 64;
				console.warn("Couldn't find TileHeight property in tiled file, using default (64)");
			} else {
				tileHeight = parseInt(tileHeight);
			}

			if (ss.isEmpty(tileDepth)) {
				tileDepth = 64;
				console.warn("Couldn't find TileDepth property in tiled file, using default (64)");
			} else {
				tileDepth = parseInt(tileDepth);
			}

			if (ss.isEmpty(depthOffsetAmount)) {
				depthOffsetAmount = 16;
				console.warn("Couldn't find DepthOffsetAmount property in tiled file, using default (16)");
			} else {
				depthOffsetAmount = parseInt(depthOffsetAmount);
			}

			var i, x, y, dataIndex, tileNum, tile;
			var tileArray = _getDataArrayFromLayer(tiledFile, tilesLayers[0]);
			var depthArray;
			if (!ss.isEmpty(depthLayer)) {
			 depthArray = _getDataArrayFromLayer(tiledFile, depthLayer);
			} else {
				depthArray = [];
				for(i = 0; i < width * height; i++) {
					depthArray[i] = 0;
				}
			}

			var newGrid = new ss.IsometricGrid(width, height, tileWidth, tileHeight, tileDepth);

			// set the tiles.
			for(x = 0; x < width; x++) {
				for(y = 0; y < height; y++) {
					dataIndex = x + (y * width);
					tileNum = tileArray[dataIndex];
					tile = newGrid.getTile(x, y);
					tile.setImage(tileSet.getDisplayObjectForTile(tileNum));
					tile.setGameParameters(tileSet.getGameParametersForTile(tileNum));
					tile.setDepthOffset(depthArray[dataIndex] * -depthOffsetAmount);
				}
			}

			for(i = 1; i < tilesLayers.length; i++) {
				tileArray = _getDataArrayFromLayer(tiledFile, tilesLayers[i]);
				for(x = 0; x < width; x++) {
					for(y = 0; y < height; y++) {
						dataIndex = x + (y * width);
						tileNum = tileArray[dataIndex];
						tile = newGrid.getTile(x, y);
						var displayObject = tileSet.getDisplayObjectForTile(tileNum);
						tile.pushImage(displayObject);
					}
				}
			}

			return newGrid;
		};

		function _getDataArrayFromLayer (tiledFile, layerName) {
			var layers = tiledFile.layers;
			for(var i = 0; i < layers.length; i++) {
				if (ss.isEmpty(layers[i].name)) {
					continue;
				}
				if (layers[i].name.toLowerCase() === layerName.toLowerCase()) {
					return layers[i].data;
				}
			}
			return undefined;
		}

		/**
		 * Release the memory used by this instance.
		 */
		_this.release = function() {
			_this = undefined;
		};

		return _this.construct();
	}

	ss.TiledLoader = TiledLoader;
}());
