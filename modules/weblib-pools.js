/*! weblib-ss 0.0.1 */
/*! weblib-ss 0.0.1 */
/* global ss */
( function () {
	"use strict";

	function PoolManager () {

		var _this = this;

		var _guid; // jshint ignore:line
		var _accessor;
		var _objectMap;
		var _definitionSet; // jshint ignore:line

		var _construct = function () {

			_accessor = guid ();
			_objectMap = {};
		};

		var s4 = function () {
		    return Math.floor ( ( 1 + new Date ().getTime () ) * 0x10000 ).toString ( 16 ).substring ( 1 );
		};

		var guid = function () {
		  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4(); // jshint ignore:line
		};

		_this.instantiate = function () {		

			//console.log ( arguments );

			var classFunc = arguments [ 0 ];
			var args = [];
			var instance;

				// rebuild the arguments
			for ( var i = 1; i < arguments.length; i++ ) {
				args.push ( arguments [ i ] );
			}	

				
			if ( _objectMap [ classFunc [ _accessor ] ] instanceof Array && _objectMap [ classFunc [ _accessor ] ].length > 0 ) {
				instance = _objectMap [ classFunc [ _accessor ] ].shift ();
			} else {
				instance = new classFunc (); // jshint ignore:line
			}

			if ( instance.init instanceof Function ) {
				instance.init.apply ( instance, args );
			}
			return instance;

			
		};

			// reclaim an object to be used later
		_this.reclaim = function ( instance ) {

			var classFunc = instance.constructor;

				// attach a GUID if the class definition does not currently have one
			if ( ss.isEmpty ( classFunc [ _accessor ] ) ) {
				classFunc [ _accessor ] = guid ();
			}

			if ( _objectMap [ classFunc [ _accessor ] ] instanceof Array ) {
				_objectMap [ classFunc [ _accessor ] ].push ( instance );
			} else {
				_objectMap [ classFunc [ _accessor ] ] = [ instance ];
			}

		};

		_this.getAllByClass = function ( classFunc ) {
			return _objectMap [ classFunc [ _accessor ] ];
		};

		_this.release = function () {

			for ( var classFunc in _objectMap ) {

				var objectSet = _objectMap [ classFunc [ _accessor ] ];

				for ( var i = 0; i < objectSet.length; i++ ) {
					var object = objectSet [ i ];

					if ( !ss.isEmpty ( object.release ) ) {
						 object.release ();
					}
				}		
			}
		};

		return _construct ();
	}

	ss.PoolManager = new PoolManager ();
	
} () );