/*! weblib-ss 0.0.1 */
/* global require, ss */



( function( ) {
	"use strict";

	ss.CommandState = {};
	ss.CommandState.EXECUTED = "CommandState.EXECUTED";
	ss.CommandState.CANCEL = "CommandState.CANCEL";
	ss.CommandState.IDLE = "CommandState.IDLE";

} ( ) );

/* global require, ss */



( function( ) {
    "use strict";

    /*
    * Class AbstractCommand
    *   Provides a base for classes which act as executable commands
    */

    createjs.EventDispatcher.initialize ( AbstractCommand.prototype );

    function AbstractCommand ( id ) {

        var _this = this;

        var _id;
        var _state;
        var _async;

        var _construct = function ( id ) {
            _id = id;

            _this.init ();

            return _this;
        };

        var _executeComplete = function () {

                // make sure we can only execute each node once
            if ( _state != ss.CommandState.EXECUTED ) {
                 _state = ss.CommandState.EXECUTED;
                _this.dispatchEvent ( new createjs.Event ( "EXECUTE" ) ) ;
            }
        };

        var _cancelComplete = function () {

            if ( _state != ss.CommandState.CANCEL ) {
                _state = ss.CommandState.CANCEL;
                _this.dispatchEvent ( new createjs.Event ( "CANCEL" ) ) ;
            }
        };

        _this.async = function ( value ) {
            return ( ss.isEmpty ( value ) ) ? _async : _async = value;
        };

        _this.id = function ( value ) {
            return ( ss.isEmpty ( value ) ) ? _id : _id = value;
        };

        _this.state = function ( value ) {
            return ( ss.isEmpty ( value ) ) ? _state : _state = value;
        };

        _this.execute = function ( /* session */ ) {
           _executeComplete ();
        };

        _this.cancel = function ( /* session */ ) {
           _cancelComplete ();
        };

        _this.reset = function () {
            _state = ss.CommandState.IDLE;
        };

        _this.toString = function () {
            return "ICommand:" + _id;
        };

        _this.init = function () {
            _state = ss.CommandState.IDLE;
            _async = false;
        };

        _this.reclaim = function () {
            _this.removeAllListeners ();
        };

        _this.release = function () {
            _id = undefined;
            _state = undefined;
            _async = undefined;
            _executeComplete = undefined;
            _cancelComplete = undefined;


            for ( var property in _this ) {
                delete _this [ property ];
            }
        };

        return _construct ( id );
    }

    ss.AbstractCommand = AbstractCommand;

} ( ) );

/* global require, ss, $*/




( function() {
    "use strict";

    /*
    *
    * AjaxCommand
    *
    */

    AjaxCommand.prototype = new ss.AbstractCommand ();
    AjaxCommand.prototype.constructor = AjaxCommand;

    function AjaxCommand ( url, settings ) {
        
        ss.AbstractCommand.call ( this, "AjaxCommand" );

        var _super = {};
        var _this = this;

        var _jqueryAjax;
        var _url;
        var _context;
        var _complete;
        var _success;
        var _response;
        var _settings;

        var _construct = function ( url, settings ) {
            _url = url;
            _settings = settings;

                // check if both parameters are empty
            if ( !ss.isEmpty ( url ) && !ss.isEmpty ( settings ) ) {
                _this.init ( url, settings );
            }

            return _this;
        };

        var _ajaxComplete = function () {

            if ( !ss.isEmpty ( _complete ) ) {
                _complete.apply ( _context );
            }

                // all done, execution complete
            _super.execute ();
        };

        var _ajaxSuccess = function ( response ) {
            _response = response;

            if ( !ss.isEmpty ( _success ) ) {
               _success.apply ( _context, [ response ] );
            }
        };

        _super.init = _this.init;
        _this.init = function ( url, settings ) {

            _super.init ();

            _url = url;
            _settings = settings;

            _context = _settings.context;
            _complete = _settings.complete;
            _success = _settings.success;

                 // replace complete with our own complete callbacks, then callback original
            _settings.context = _this;
            _settings.complete = _ajaxComplete;
            _settings.success = _ajaxSuccess;
        };

        _super.execute = _this.execute;
        _this.execute = function( /*session*/ ) {

            if ( _this.state () == "CommandState.IDLE" ) {
                _this.state ( "CommandState.EXECUTING" );
                _jqueryAjax = $.ajax ( _url, _settings );
            }

        };

        _super.cancel = _this.cancel;
        _this.cancel = function( /*session*/ ) {

            if ( !ss.isEmpty ( _jqueryAjax ) ) {
                _jqueryAjax.abort ();
            }

                // let the command finish
            _super.cancel ();
        };

        _super.reclaim = _this.reclaim;
        _this.reclaim = function () {

            _super.reclaim ();

                // abort if loading
            if ( !ss.isEmpty ( _jqueryAjax ) ) {
                _jqueryAjax.abort ();
            }

            ss.PoolManager.reclaim ( AjaxCommand, _this );
        };

        _this.release = function () {

            var property;

            _super.release ();

            _jqueryAjax = undefined;
            _url = undefined;
            _context = undefined;
            _complete = undefined;
            _success = undefined;
            _response = undefined;
            _settings = undefined;

            for ( property in _this ) {
                delete _super [ property ];
            }

            for ( property in _this ) {
                delete _this [ property ];
            }
    };

        return _construct ( url, settings );
    }

    ss.AjaxCommand = AjaxCommand;
} () );

/* global require, ss */




( function() {
    "use strict";

    /*
    *
    * ApplyCommand
    *
    */

    ApplyCommand.prototype = new ss.AbstractCommand ();
    ApplyCommand.prototype.constructor = ApplyCommand;

    function ApplyCommand ( callback, params, conxtext ) {

        ss.AbstractCommand.call ( this );
        
        var _super = {};
        var _this = this;

        var _callback;
        var _params;
        var _context;

        var _construct = function ( callback, params, conxtext ) {
            
            if ( !ss.isEmpty ( callback ) ) {
                _this.init ( callback, params, conxtext );
            }

            return _this;
        };

        _super.init = _this.init;
        _this.init = function ( callback, params, conxtext ) {

            _super.init ();

            _callback = callback;
            _params = params;
            _context = conxtext;
        };

        _super.execute = _this.execute;
        _this.execute = function( /*session*/ ) {

            if ( _this.state () == "CommandState.IDLE" ) {
                _this.state ( "CommandState.EXECUTING" );
                _callback.apply ( _context, _params );
                _super.execute ();
            }

        };

        _super.reclaim = _this.reclaim;
        _this.reclaim = function () {
            _super.reclaim ();

            ss.PoolManager.reclaim ( ApplyCommand, _this );
        };

        return _construct ( callback, params, conxtext );
    }

    ss.ApplyCommand = ApplyCommand;
} ());

/* global require, ss, console, $, createjs */






( function() {
    "use strict";

    /*
    *
    * CommandQueue
    *
    */

    createjs.EventDispatcher.initialize ( CommandQueue.prototype );
    function CommandQueue ( params ) {

        var _this = this;

        var _iIterator; // IIterator;
        var _activeCommand; // ICommand;
        var _currentCommandSet; // CommandSet;
        var _asyncCommandSet;

        var _commandSetQueue;
        var _isConsuming;
        var _isPaused;
        var _session;
        var _autoClean;

        var _construct = function ( params ) {

            _iIterator = new ss.Iterator ( [] );
            _commandSetQueue = [];
            _asyncCommandSet = [];
            _isConsuming = false;
            _isPaused = false;
            _autoClean = ( params && params.autoClean === true ) ? true : false; 

            if ( params && params.autoStart === true ) {
                _this.start ();
            }

            CommandQueue.referenceSet.push ( _this );

            return _this;
        };

        var _isQueueEmpty = function () {
            return ( _iIterator.hasNext () || _commandSetQueue.length ) ? false : true;
        };

        var _getNextCommand = function () {
            if ( !_iIterator.hasNext () ) {
                var currentCommandSet = _commandSetQueue.shift ();
                _iIterator = currentCommandSet.getIterator ();
                _currentCommandSet = currentCommandSet;
            }

            return _iIterator.next ();
        };

        var _consumeExecute = function () {

        	if ( _isPaused ) {
        		return;
        	}

            if ( ss.isEmpty ( _activeCommand ) && !_isQueueEmpty () ) {
                _activeCommand = _getNextCommand ();

                    // automatically handle asynchronous commands
                if ( _activeCommand.async () ) {

                        // NEW : save a copy of async commands otherwise l can't cancel them
                    _asyncCommandSet.push ( _activeCommand );
                    _activeCommand.on ( "EXECUTE" , _onAsyncCommandExecuteListener ) ;

                        // mimic an instantaneous execute
                    _activeCommand.execute ( _session ) ;
                    _onCommandExecuteListener ( _activeCommand );
                } else {
                    _activeCommand.on ( "EXECUTE" , _onCommandExecuteListener );
                    _activeCommand.execute ( _session ) ;
                }
            }
        };

        var _consumeCancel = function () {

            if ( _activeCommand ) {
                _activeCommand.addEventListener ( "CANCEL", _onCommandCancelListener ) ;
                _activeCommand.cancel ( _session ) ;
            } else if ( ss.isEmpty ( _activeCommand ) && !_isQueueEmpty () ) {
                _activeCommand = _getNextCommand ();
                _activeCommand.addEventListener ( "CANCEL" , _onCommandCancelListener ) ;
                _activeCommand.cancel ( _session ) ;
            }
        };

        var _disposeOfCommand = function ( iCommand ) {
                // remove of all possible listeners
            iCommand.off ( "EXECUTE" , _onCommandExecuteListener );
            iCommand.off ( "CANCEL" , _onCommandCancelListener );
        };

        var _onCommandExecuteListener = function ( event ) { // jshint ignore:line
            _activeCommand = undefined;

                // dispatch an execute command
            _this.dispatchEvent ( new createjs.Event ( "EXECUTE" ) ) ;

            if ( _isQueueEmpty () ) {
                _this.dispatchEvent ( new createjs.Event ( "SEQUENCE_EXECUTED" ) );
                _this.dispatchEvent ( new createjs.Event ( "QUEUE_EMPTY" ) );
            }

                // unless the the commandQueue has been stopped continue consuming
            if ( _isConsuming ) {
                _consumeExecute ();
            }
        };

        var _onCommandCancelListener = function ( event ) { // jshint ignore:line

            _disposeOfCommand ( _activeCommand );
            _activeCommand = undefined;

                // dispatch an cancel command
            _this.dispatchEvent ( new createjs.Event ( "CANCEL" ) ) ;

            if ( _isQueueEmpty () ) {
                _this.dispatchEvent ( new createjs.Event ( "SEQUENCE_CANCELLED" ) );
                _this.dispatchEvent ( new createjs.Event ( "QUEUE_EMPTY" ) );
            } else {
                _consumeCancel ();
            }
        };

        var _onAsyncCommandExecuteListener = function ( event ) {

                // just remove the command from the async list
            _asyncCommandSet.splice ( _asyncCommandSet.indexOf ( event.target ), 1 );
        };

        _this.init = function ( params ) {
            _iIterator = new ss.Iterator ( [] );
            _commandSetQueue = [];
            _asyncCommandSet = [];
            _isConsuming = false;
            _isPaused = false;
            _autoClean = ( params && params.autoClean === true ) ? true : false; 

            if ( params && params.autoStart === true ) {
                _this.start ();
            }
        };

        _this.queue = function ( item ) {

            var commandSet;
            if ( item instanceof ss.AbstractCommand ) {
                commandSet = new ss.CommandSet ();
                commandSet.push ( item );
                _commandSetQueue.push ( commandSet );
            } else if ( item.toString () == "CommandSet" ) {
                commandSet = item;
                if ( commandSet.length () ) {
                    _commandSetQueue.push ( item ) ;
                }
            } else {
                throw "CommandQueue.queue ( ... ) argument must be either an ICommand or CommandSet : " + item;
            }

            if ( _this.debug === true ) {
                console.log ( _iIterator );
                console.log ( _commandSetQueue );
                console.log ( _activeCommand );
            }

            if ( !_iIterator.hasNext () && _commandSetQueue.length ) {
                var currentCommandSet = _commandSetQueue.shift ();
                _iIterator = currentCommandSet.getIterator () ;
                _currentCommandSet = currentCommandSet;
            }

            if ( _isConsuming ) {
                _consumeExecute ();
            }
        };

        _this.start = function ( iCommand ) {

            if ( !ss.isEmpty ( iCommand ) ) {
                _this.queue ( iCommand );
            }

            _isConsuming = true;
            _consumeExecute ();
        };

        _this.stop = function () {
            _isConsuming = false;
        };

        _this.cancel = function () {

                // first cancal all the async commands
            while( _asyncCommandSet.length > 0 ) {
                _asyncCommandSet.pop ().cancel ( _session );
            }

            _consumeCancel ();
        };

        _this.pause = function () {
        	_isPaused = true;
        };

        _this.resume = function () {

        	_isPaused = false;
        	_consumeExecute ();

        };

        _this.session = function ( value ) {
            return ( ss.isEmpty ( value ) ) ? _session : _session = value;
        };

        _this.reclaim = function () {

            var commandSet;

            _this.stop ();

            while ( _commandSetQueue.length ) {
                commandSet = _commandSetQueue.pop ();

                if ( _autoClean ) {
                    commandSet.reclaim ();
                }
            }

            while ( _asyncCommandSet.length ) {
                commandSet = _asyncCommandSet.pop ();

                if (  _autoClean ) {
                    commandSet.reclaim ();
                }
            }

            ss.PoolManager.reclaim ( CommandQueue, _this );
        };

        _this.release = function () {

            _iIterator = undefined;
            _activeCommand = undefined;
            _currentCommandSet = undefined;

            _asyncCommandSet = undefined;

            _commandSetQueue = undefined;
            _isConsuming = undefined;
            _session = undefined;

            _construct = undefined;

            _isQueueEmpty = undefined;
            _getNextCommand = undefined;
            _consumeExecute = undefined;
            _consumeCancel = undefined;
            _disposeOfCommand = undefined;
            _onCommandExecuteListener = undefined;
            _onCommandCancelListener = undefined;
            _onAsyncCommandExecuteListener = undefined;

            for ( var property in _this ) {
                delete _this [ property ];
            }
            
        };

        return _construct ( params );
    }


    /*
    *
    * CommandQueue.init ( element )
    *
    */


    CommandQueue.init = function( element ) {

        function evalDataString ( dataString ) {

            var fixedDataString = dataString;
            var isJSONRegExp = /\s*{.*?}\s*/i;
            if ( isJSONRegExp.test ( dataString ) ) {
                // fix if there are no parenthesis, add them
                var regExpFix = /\({.*?}\)/i;
                if ( !regExpFix.test ( dataString ) ) {
                    fixedDataString = "(" + dataString + ")";
                }
            }

            return eval ( fixedDataString ); // jshint ignore:line
        }

        function queueCommandSet ( htmlCommandNode, commandSet ) {
            var target, event, data, propery;
            var nodeName = htmlCommandNode.nodeName.toLowerCase ();

            var iCommand;
            switch( nodeName ) {
                case "apply" :
                    var callback = eval ( $( htmlCommandNode ).attr ( "callback" ) ); // jshint ignore:line
                    var params = eval ( $( htmlCommandNode ).attr ( "params" ) ); // jshint ignore:line
                    iCommand = commandSet.apply ( callback, params );
                    break;
                case "ajax" :
                    var url = $( htmlCommandNode ).attr ( "url" );
                    var settings = evalDataString ( $( htmlCommandNode ).attr ( "settings" ) );
                    iCommand = commandSet.ajax ( url, settings );
                    break;
                case "css" :
                    var selector = $( htmlCommandNode ).attr ( "selector" );
                    var propeties = evalDataString ( $( htmlCommandNode ).attr ( "properties" ) );
                    var parent = eval ( $( htmlCommandNode ).attr ( "parent" ) ); // jshint ignore:line
                    iCommand = commandSet.css ( selector, propeties, parent );
                    break;
                case "lock" :
                    var delay = $( htmlCommandNode ).attr ( "delay" );
                    iCommand = commandSet.lock ( delay );
                    break;
                case "dispatchevent" :
                    target = evalDataString ( $( htmlCommandNode ).attr ( "target" ) );
                    event = $( htmlCommandNode ).attr ( "event" );
                    data = evalDataString ( $( htmlCommandNode ).attr ( "data" ) );
                    iCommand = commandSet.dispatchEvent ( target, event, data );
                    break;
                case "listener" :
                    target = evalDataString ( $( htmlCommandNode ).attr ( "target" ) );
                    event = $( htmlCommandNode ).attr ( "event" );
                    data = evalDataString ( $( htmlCommandNode ).attr ( "data" ) );
                    iCommand = commandSet.listener ( target, event, data );
                    break;
                case "assign" :
                    target = evalDataString ( $( htmlCommandNode ).attr ( "target" ) );
                    propery = $( htmlCommandNode ).attr ( "property" );
                    // var value = evalDataString ( $( htmlCommandNode ).attr ( "value" ) );
                    iCommand = commandSet.assign ( target, event, data );
                    break;
                case "debug" :
                    target = evalDataString ( $( htmlCommandNode ).attr ( "target" ) );
                    event = $( htmlCommandNode ).attr ( "event" );
                    data = evalDataString ( $( htmlCommandNode ).attr ( "data" ) );
                    iCommand = commandSet.listener ( target, event, data );
                    break;
            /*
        }

        this.assign = function ( target, property, value ) {
            return this.apply ( this._assign, [ target, property, value ] );
        }


        this.debug
        */

                default :
                    // alert ( "nothing found" );
            }
        }

        $ ( "commandQueue", element ).each ( function( index, value ) {

            var parentHTMLNode = $ ( value ).parent ();
            var autoStart;
            try {
                autoStart = ( $( value ).attr ( "autostart" ).toLowerCase () == "false" ) ? false : true;
            } catch ( error ) {
                autoStart = true;
            }

            var commandQueue = new CommandQueue ( null, autoStart );
            parentHTMLNode.commandQueue = commandQueue;

            $( "commandSet", this ).each ( function( index, value ) {

                var commandSet = new ss.CommandSet ();

                $( "*", value ).each ( function( index, value ) {
                    queueCommandSet ( value, commandSet );
                } );

                commandQueue.queue ( commandSet );
            } );
        } );
    };

    CommandQueue.referenceSet = [];

    ss.CommandQueue = CommandQueue;
} ());

/* global require, ss, console */






/*
* Class CommandSet
*   Contains a set of commands and provides convenience methods for adding new commands.
*/

( function() {
    "use strict";

    function CommandSet ( params ) {

        var _this = this;

        var _commandSet = [];

        _this.length = function() {
            return _commandSet.length;
        };

        _this.getIterator = function() {
            return new ss.Iterator ( _commandSet );
        };

        _this.unshift = function( iCommand ) {
            _commandSet.unshift ( iCommand );
        };

        _this.push = function( iCommand ) {
            _commandSet.push ( iCommand );
        };

        _this.apply = function( callBack, params, context ) {
            var applyCommand = new ss.ApplyCommand ( callBack, params, context );
            _commandSet.push ( applyCommand );
            return applyCommand;
        };

        _this.css = function( selector, cssProperties ) {
            var cssCommand = new ss.CSSCommand ( selector, cssProperties );
            _commandSet.push ( cssCommand );
            return cssCommand;
        };

        _this.ajax = function( url, settings ) {
            var ajaxCommand = new ss.AjaxCommand ( url, settings );
            _commandSet.push ( ajaxCommand );
            return ajaxCommand;
        };

        _this.tweenTo = function( target, time, tweenProperties ) {
            var tweenToCommand = new ss.TweenToCommand ( target, time, tweenProperties );
            _commandSet.push ( tweenToCommand );
            return tweenToCommand;
        };

        _this.easeTo = function( target, time, tweenProperties ) {
            var easeToCommand = new ss.EaseToCommand ( target, time, tweenProperties );
            _commandSet.push ( easeToCommand );
            return easeToCommand;
        };

            /* public function tweenFrom ( target : Object, time : Number, tweenProperties : Object ) : ICommand {
                var tweenCommand : TweenCommand = new TweenCommand ( target, time, tweenProperties, false );
                _commandSet.push ( tweenCommand );
                return tweenCommand;
            } */

        _this.lock = function( delay ) {
            var lockCommand = new ss.LockCommand ( delay );
            _commandSet.push ( lockCommand );
            return lockCommand;
        };

        _this.dispatchEvent = function ( target, event ) {
            return _this.apply ( target.dispatchEvent, [ event ], target );
        };

        _this.listener = function( target, event, listener, context ) {
            var listenerCommand = new ss.ListenerCommand ( target, event, listener, context );
            _commandSet.push ( listenerCommand );
            return listenerCommand;
        };

        /* _this.assign = function ( target, property, value ) {
            return _this.apply ( _assign, [ target, property, value ] );
        }

        _this._assign = function ( target, property, value ) {
            target [ property ] = value;
        } */

        _this.thread = function( commandSet ) {
            var threadCommand = new ss.ThreadCommand ( commandSet );
            _commandSet.push ( threadCommand );
            return threadCommand;
        };

        _this.debug = function( message ) {
            _this.apply ( console.log, [ message ] );
        };

        _this.circuit = function( circuitStr, commandMap ) {
            var circuitCommand = new ss.CircuitCommand ( circuitStr, commandMap );
            _commandSet.push ( circuitCommand );
            return circuitCommand;
        };

        _this.queue = function( debug ) {

            if ( debug === true ) {
                console.log ( _commandSet );
            }

            var commandQueue = ss.PoolManager.instantiate ( ss.CommandQueue, { "autoClean" : true, "autoStart" : true } );
            commandQueue.queue ( _this );
            return commandQueue;
        };

        _this.release = function() {
            for( var i = 0; i < _commandSet.length; i++ ) {
                _commandSet [ i ].cancel ();
            }

            _commandSet = null;
        };

        _this.toString = function() {
            return "CommandSet";
        };
    }

    ss.CommandSet = CommandSet;

} () );

/* global require, ss, $ */




(function() {
    "use strict";

    /*
    *
    * CSSCommand
    *
    */

    CSSCommand.prototype = new ss.AbstractCommand ();
    CSSCommand.prototype.constructor = CSSCommand;

    function CSSCommand ( selector, cssProperties, parent ) {

         ss.AbstractCommand.call ( this, "CSSCommand" );

        var _super = {};
        var _this = this;

        var _selector;
        var _cssProperties;
        var _parent;

        var _construct = function ( selector, cssProperties, parent ) {
            _selector = selector;
            _cssProperties = cssProperties;
            _parent = parent;

            return _this;
        };

        _super.execute = _this.execute;
        _this.execute = function( /*session*/ ) {

            if ( _this.state () == "CommandState.IDLE" ) {

                    // mark the command as "EXECUTING"
                _this.state ( "CommandState.EXECUTING" );

                    // param can be a selector ( String ) or jQuery object
                try {
                    $ ( _selector, _parent ).css ( _cssProperties );
                } catch ( e ) {
                    _selector.css ( _cssProperties );
                }
                    // pretty much a synchronous command
                _super.execute ();

            }
        };

        return _construct ( selector, cssProperties, parent );
    }

    ss.CSSCommand = CSSCommand;
} ());

/* global require, ss, console, createjs */




( function() {
    "use strict";

    // NOTE: Requires TweenJS

    /*
    *
    * EaseToCommand ( for CreateJS object )
    *
    */
    function EaseToCommand ( target, time, tweenObject, completeCallback ) {

        var _super = {};
        var _this = ss.AbstractCommand.call ( this, "EaseCommand" );

        var _tween;
        var _target;
        var _time;
        var _tweenObject;
        var _completeCallback;
        // var _onCompleteParams = tweenObject.onCompleteParams;

        function _construct ( target, time, tweenObject, completeCallback ) {
            _target = target;
            _time = time;
            _tweenObject = tweenObject;
            _completeCallback = completeCallback;

            return _this;
        }

        function _handleComplete () {

          // add on complete callback
                if ( !ss.isEmpty ( _completeCallback ) ) {
                     _completeCallback ();
                }

                // all done
            _super.execute ();
        }

        _super.execute = _this.execute;
        _this.execute = function( /*session*/ ) {

            if ( _this.state () == "CommandState.IDLE" ) {
                _this.state ( "CommandState.EXECUTING" );

                _tween = createjs.Tween.get ( _target ).to ( _tweenObject, _time ).call ( _handleComplete );

                console.log ( _tween );
                console.log ( _target );
                console.log ( _tweenObject );
                console.log ( _time );

            }
        };
        _super.cancel = _this.cancel;
        _this.cancel = function( /*session*/ ) {
            if ( !ss.isEmpty ( _tween ) ) {
                _tween.removeTweens ( _tweenObject );
            }
            _this._cancel ();
        };

        return _construct ( target, time, tweenObject, completeCallback );
    }

    ss.EaseToCommand = EaseToCommand;
} ());

/* global require, ss */




/*
*
* ListenerCommand
*
*/

( function( ) {
    "use strict";

    ListenerCommand.prototype = new ss.AbstractCommand ();
    ListenerCommand.prototype.constructor = ListenerCommand;

    function ListenerCommand ( target, event, listener, context ) {

        ss.AbstractCommand.call ( this, "ListenerCommand" );

        var _super = {};
        var _this = this;

        var _target;
        var _event;
        var _listener;
        var _context;

        var _construct = function ( target, event, listener, context ) {
            
            if ( !ss.isEmpty ( target ) ) {
                _this.init ( target, event, listener, context );
            }

            return _this;
        };

        var _onEventCaptured = function ( target, data ) {
            if ( _listener ) {
                _listener.apply ( _this._context, [ target, data ] );
            }
            _super.execute ();
        };

        _super.init = _this.init;
        _this.init = function ( target, event, listener, context ) {

            _super.init ();

            _target = target;
            _event = event;
            _listener = listener;
            _context = context;
        };

        _super.execute = _this.execute;
        _this.execute = function( /*session*/ ) {
            if ( _this.state () == "CommandState.IDLE" ) {
                _this.state ( "CommandState.EXECUTING" );
                _target.on ( _event, _onEventCaptured );
            }
        };

        _super.reclaim = _this.reclaim;
        _this.reclaim = function () {

            _super.reclaim ();

            _target.off ( _event, _onEventCaptured );

            ss.PoolManager.reclaim ( ListenerCommand, _this );

        };

        _super.release = _this.release;
        _this.release = function( /*session*/ ) {
            _target.off ( _event, _onEventCaptured );
            _super.release ();
        };

        return _construct ( target, event, listener, context );
    }

    ss.ListenerCommand = ListenerCommand;

} () );

/* global require, ss */




/*
*
* LockCommand
*
*/

( function() {
    "use strict";

    LockCommand.prototype = new ss.AbstractCommand ();
    LockCommand.prototype.constructor = LockCommand;

    function LockCommand ( delay ) {

        ss.AbstractCommand.call ( this, "LockCommand" );

        var _super = {};
        var _this = this;

        var _delay;
        var _timeOut;

        var _construct = function ( delay ) {
            _delay = delay;

            if ( arguments.length ) {
                _this.init ( delay );
            }

            return _this;
        };

        var _timeOutComplete = function () {
            _super.execute ();
        };

        _super.init = _this.init;
        _this.init = function ( delay ) {

            _super.init ();

            _delay = delay;
        };

        _this.unlock = function() {
            _super.execute ();
        };

        _super.execute = _this.execute;
        _this.execute = function ( /*session*/ ) {

            if ( _this.state () == "CommandState.IDLE" ) {
                _this.state ( "CommandState.EXECUTING" );
                
                if ( _delay instanceof Function ) {
                    _delay ( _this );
                } else if ( _delay ) {
                    _timeOut = setTimeout ( function() {
                        _timeOutComplete ();
                    }, _delay );
                }

            }
        };

        _super.cancel = _this.cancel;
        _this.cancel = function ( /*session*/ ) {
            if ( _timeOut ) {
                clearTimeout ( _timeOut );
            }
            _super.cancel ();
        };

        _super.reclaim = _this.reclaim;
        _this.reclaim = function () {
            
             _super.reclaim ();

            if ( _timeOut ) {
                clearTimeout ( _timeOut );
            }

            ss.PoolManager.instantiate ( LockCommand, _this );
        };

        return _construct ( delay );
    }

        // assign to the namespace
    ss.LockCommand = LockCommand;

} () );

/* global require, ss */






(function() {
    "use strict";

    function ThreadCommand ( item ) {

        ss.AbstractCommand.call ( this, "ThreadCommand" );

        var _super = {};
        var _this = this; 
        var _commandQueue;
        var _threadCommandQueue;

        var _construct = function ( item ) {
            _commandQueue = ( item instanceof ss.CommandSet ) ? new ss.CommandQueue ( item, false ) : item;
        };

        _super.execute = _this.execute;
        _this.execute = function( /*session*/ ) {
            var commandSet = new ss.CommandSet ();
            commandSet.listener ( _commandQueue, "SEQUENCE_EXECUTED" );
            commandSet.apply ( _super.execute );
            _threadCommandQueue = commandSet.queue ();

                // start the commandQueue
            _commandQueue.start ();
        };

        _super.cancel = _this.cancel;
        _this.cancel = function( /*session*/ ) {
            _threadCommandQueue.cancel ();
        };

        return _construct ( item );
    }

    ss.ThreadCommand = ThreadCommand;
} ());

/* global require, ss, console, TweenMax */




( function()  {
    "use strict";

    /*
    *
    * TweenToCommand
    *
    */

    TweenToCommand.prototype = new ss.AbstractCommand ();
    TweenToCommand.prototype.constructor = TweenToCommand;

    function TweenToCommand ( target, time, tweenObject ) {

        ss.AbstractCommand.call ( this, "TweenCommand" );

        var _super = {};
        var _this = this;

        var _tweenCommand;
        var _target;
        var _time;
        var _tweenObject;
        var _onComplete;
        var _onCompleteParams;

        var _construct = function ( target, time, tweenObject ) {
           
            if ( !ss.isEmpty ( target ) ) {
                _this.init ( target, time, tweenObject );
            }

            return _this;
        };

            // replace onComplete function
        var _onCompleteCallBack = function ( /*instance*/ ) {
                // launch a onComplete callback if one exists
            if ( !ss.isEmpty ( _onComplete ) ) {
                _onComplete.apply ( _this, _onCompleteParams );
            }

            if ( _this.debug === true ) {
                console.log ( "executed" );
            }

            _super.execute ();
        };

        _super.init = _this.init;
        _this.init = function ( target, time, tweenObject ) {

            _super.init ();

            _target = target;
            _time = time;
            _tweenObject = tweenObject;
            _onComplete = tweenObject.onComplete;
            _onCompleteParams = tweenObject.onCompleteParams;

            _tweenObject.onComplete = _onCompleteCallBack;
            _tweenObject.onCompleteParams = [ _this ];

        };

        _this.debug = function ( val ) {
            _this.debug = val;
        };

        _super.execute = _this.execute;
        _this.execute = function( /* session */ ) {

            _tweenCommand = TweenMax.to ( _target, _time, _tweenObject );
        };

        _super.cancel = _this.cancel;
        _this.cancel = function( /* session */ ) {

            if ( !ss.isUndefined ( _tweenCommand ) ) {
                _tweenCommand.kill ();
            }
            _super.cancel ();
        };

        _super.reclaim = _this.reclaim;
        _this.reclaim = function () {
            _super.reclaim ();

            if ( !ss.isUndefined ( _tweenCommand ) ) {
                _tweenCommand.kill ();
            }

            ss.PoolManager.instantiate ( TweenToCommand, _this );
        };

        return _construct ( target, time, tweenObject );
    }

    ss.TweenToCommand = TweenToCommand;

} () );

/* global require, ss */




( function( ) {
	"use strict";

	function Dictionary () {
    	this.hashes = [];
	}

	Dictionary.prototype = {

	    "constructor" : Dictionary,

	    "put" : function( key, value ) {
	        this.hashes.push ( {
	            "key"   : key,
	            "value" : value
	        } );
	    },

	    "get" : function( key ) {
	        for( var i = 0; i < this.hashes.length; i++ ) {
	            if ( this.hashes [ i ].key == key ) {
	                return this.hashes [ i ].value;
	            }
	        }
	    }
	};

	AbstractCircuitNode.prototype = new ss.AbstractCommand ();
	AbstractCircuitNode.prototype.constructor = AbstractCircuitNode;

	AbstractCircuitNode.OR = "CircuitNode.OR";
	AbstractCircuitNode.AND = "CircuitNode.AND";
	AbstractCircuitNode.NOT = "CircuitNode.NOT";
	AbstractCircuitNode.XOR = "CircuitNode.XOR";
	AbstractCircuitNode.PUSH = "CircuitNode.PUSH";
	AbstractCircuitNode.POP = "CircuitNode.POP";

		/**
		 * An Abstract class to subclass all
		 * @param {Array} children : {ns.AbstractCommand|ns.AbstractCircuitNode} 		The children nodes passed in. Rarely used...
		 * @return - {ns.AbstractCircuitNode} 											The sprite for that character.
		 */

	function AbstractCircuitNode ( children ) {

			// basic variables for OOP
		var _super = {};
		var _this = this;

			// mix-in AbstractCommand
		ss.AbstractCommand.call ( _this );

			// mix-in TreeNode functionality
		ss.TreeNode.call ( _this );

			// these both keep track of modifiers and how their applied to Command values
		var _modifierDict;
		var _prependModifiers;

			/**
			 * Do l really need to tell you what this does?!?!?
			 * @param {Array} children : {ns.AbstractCommand|ns.AbstractCircuitNode} 		The children nodes passed in. Rarely used...
			 * @return - {ns.AbstractCircuitNode} 											The sprite for that character.
			 */
		var _construct = function ( children ) {
			_modifierDict = new Dictionary ();
			_prependModifiers = [];

				// add children the hierarchy
			_this.init ( children );

				// return reference
			return _this;
		};

			/**
			 * Use to signal a child noce has change to buble up the hierarchy
			 */
		var _onChangeListener = function () {
			_this.dispatchEvent ( new createjs.Event ( "CHANGE" ) );
		};

		_this.init = function ( children ) {
				// add children the hierarchy
			if ( !ss.isEmpty ( children ) ) {
				for( var i = 0; i < children.length; i++ ) {
					_this.addChild ( children [ i ] );
				}
			}
		};

			/**
			 * Allows the string circuit to have !(NOT) operator
			 * @param {String} token : The value of the current token
			 */
		_this.prependModifier = function ( token ) {
			if ( ss.isEmpty ( _prependModifiers ) ) {
				_prependModifiers = [];
			}
				// some quick validation
			if ( token instanceof String ) {
				switch( String ( token ).toUpperCase () ) {
					case "NOT" :
					case "!" :
						_prependModifiers.push ( AbstractCircuitNode.NOT );
						break;
					default :
						throw new Error ( "AbstractCircuitNode::prependModifier ( ... ) - Unknown Modifier added " + token );
				}
			}
		};

			/**
			 * Cleans the circuit command so we don't get mulitple change dispatches
			 */
		_super.clear = _this.clear;
		_this.clear = function () {

			var iterator = _this.children ();
			while( iterator.hasNext () ) {

				var node = iterator.next ();

					// if this is a circuit node
				node.off ( "CHANGE", _onChangeListener );

					// if this is a regular command
				node.off ( "EXECUTE", _onChangeListener );
				node.off ( "REVERSE", _onChangeListener );
				node.off ( "CANCEL", _onChangeListener );
			}

			_modifierDict = new Dictionary ();
			_prependModifiers = [];

			_super.clear ();
		};

			/**
			 * add the node as a child
			 * @param node : {ns.AbstractCommand|ns.AbstractCircuitNode} Adds an command to another circuit
			 */
		_super.addChild = _this.addChild;
		_this.addChild = function ( node ) {

				// validate if this is an AbstractCircuitNode or a ns.AbstractCommand
			if ( node instanceof AbstractCircuitNode || node instanceof ss.AbstractCommand ) {

					// attach the modifier using a ns.Dictionary
				if ( _prependModifiers ) {
					_modifierDict.put ( node, _prependModifiers );
					_prependModifiers = null;
				} else {

						// if this is one of the Circuit Commands
					node.on ( "CHANGE", _onChangeListener );

						// if the node is just a Command
					node.on ( "EXECUTE", _onChangeListener );
					node.on ( "REVERSE", _onChangeListener );
					node.on ( "CANCEL", _onChangeListener );
				}
					// everything is good time to add it as a tree node
				_super.addChild ( node );

			} else {
				throw "Parameter must be an CircuitNode or Command : " + node;
			}
		};

			/**
			 * return a value that can be using in a logical comparison
			 * @param {Boolean} node : if the a child has been executed
			 */
		_this.value = function() {
			throw "AbstractCircuitNode::value () must be overriden in subclasses";
		};

			/**
			 * TBI implemented
			 * @param {ns.AbstractCommand|ns.AbstractCircuitNode} node : Adds an command to another circuit
			 */
		_this.removeChild = function( /*node*/ ) {
			throw "please remember to implement AbstractCircuitNode::removeChild";
		};

			/**
			 * extract a value base on what type of node is pass in.
			 * @param {ns.AbstractCommand|ns.AbstractCircuitNode} node :  Adds an command to another circuit
			 */
		_this.processValue = function( node ) {
			var retValue;
			if ( node instanceof AbstractCircuitNode ) {
				retValue = node.value ();
			} else if ( node instanceof ss.AbstractCommand ) {
				switch( node.state () ) {
					case ss.CommandState.EXECUTED :
					case ss.CommandState.REVERSED :
					case ss.CommandState.CANCELLED :
						retValue = true;
						break;
					default :
						retValue = false;
				}
			} else {
				throw new Error ( "AbstractCircuitNode::getValue () - parameter must be an ICircuitNode or ICommand" );
			}

				// modifier check and flip it if we have a modifer
			var modifier = _modifierDict.get ( node );
			if ( modifier && modifier.indexOf ( AbstractCircuitNode.NOT ) > -1 ) {
				return !retValue;
			}

			return retValue;
		};

		_this.relcaim = function () {
			_modifierDict = new Dictionary ();
			_prependModifiers = [];
		};

		return _construct ( children );
	}

		// assign to the namespace
	ss.AbstractCircuitNode = AbstractCircuitNode;

} () );

/* global require, ss, console */



( function() {
	"use strict";

	CircuitCommand.prototype = new ss.AbstractCircuitNode ();
	CircuitCommand.prototype.constructor = CircuitCommand;

		/**
		 * Do l really need to tell you what this does?!?!?
		 * @param {ns.AbstractCommand|ns.AbstractCircuitNode} children :  		The children nodes passed in. Rarely used...
		 * @return - {ns.CircuitCommand} 										The sprite for that character.
		 */
	function CircuitCommand ( circuitStr, commandMap ) {

		var _super = {};
		var _this = ss.AbstractCircuitNode.call ( this );
		var _iCircuitNode;
		var _commandMap;

			/**
			 * Do l really need to tell you what this does?!?!?
			 * @param {String}  circuitStr : 			A string representation of the circuit
			 * @param {Object|Array} commandMap :  		References of the
			 * @return - {ns.CircuitCommand} 			The sprite for that character.
			 */
		var _construct = function ( circuitStr, commandMap ) {

			if ( !ss.isEmpty ( circuitStr ) && !ss.isEmpty ( commandMap ) ) {
				_this.init ( circuitStr, commandMap );
			}

			return _this;
		};

			/**
			 * This support OOO. That's right Order Of Operations. So recurse that ish.
			 * @param {Array} tokenSet :  		Whitespace seperated tokens
			 * @return - {CircuitCommand}
			 */
		var _recurseCircuitHeirachy = function( tokenSet ) {

			var token;
			var params = [];

				// parse each token
			while( tokenSet.length ) {

				token = tokenSet.shift ();

				switch( token ) {
					case "(" :
						params.push ( _recurseCircuitHeirachy ( tokenSet ) );
						break;
					case ")" :
						return params.shift ();
					case "AND" :
						params.unshift ( new ss.ANDNode () );
						break;
					case "OR" :
						params.unshift ( new ss.ORNode () );
						break;
					case "XOR" :
						params.unshift ( new ss.XORNode () );
						break;
					case "!" :
					case "NOT" :
						params.push ( "NOT" );
						continue;
					default :
						if ( _commandMap [ token ] instanceof ss.AbstractCommand ) {
							params.push ( _commandMap [ token ] );
						} else {
							throw "No ICommand was attached to ID : " + token;
						}
				}

				_buildCircuit ( params );

			}

			return params.shift ();
		};

			/**
			 * Builds a circuit node and add the children
			 * @param {Array} params :  		Whitespace seperated tokens
			 */
		var _buildCircuit = function( params ) {

			if ( params [ 0 ] instanceof ss.AbstractCircuitNode ) {

				var iCircuitNode = params.shift ();
				while( params.length ) {

					var node = params.shift ();
						// extract all modifiers
					while( _isModifier ( node ) ) {
						iCircuitNode.prependModifier ( node );
						node = params.shift ();
					}

					iCircuitNode.addChild ( node );
				}

				params.unshift ( iCircuitNode );
			}
		};

			/**
			 * Is it a modifier? Or is it not?!?! Ahhh, just one of life's many unanswered questions
			 * @param {String} token :  		Whitespace seperated tokens
			 * @return - {CircuitCommand}
			 */
		var _isModifier = function( token ) {

			switch( token ) {
				case "NOT" :
				case "!" :
					return true;
			}

			return false;
		};

		var _onCircuitNodeListener = function ( event ) {

			event.target.off ( "CHANGE", _onCircuitNodeListener );

			if ( _iCircuitNode.value () ) {
				_super.execute ();
				_this.clear ();
			}
		};

		_this.init = function ( circuitStr, commandMap ) {

				// the commands can also be add via an array and assign by their ID
			if ( commandMap instanceof Array ) {
				_commandMap = {};

					// add each command via it's id (). If none was set then sucks... Your code will blow up
				for( var i = 0; i < commandMap.length; i++ ) {
					var iCommand = commandMap [ i ];
					_commandMap [ iCommand.id () ] = iCommand;
				}
			} else {

					// just directly assign the map
				_commandMap = commandMap;
			}

				// seperate the circuitStr into tokens
			var adaptedCircuit = circuitStr.replace ( "!", "NOT" );
			var tokenSet = adaptedCircuit.split ( /\s+/ );

				// parse the circuit
			_iCircuitNode = _recurseCircuitHeirachy ( tokenSet );
		};

		_super.execute = _this.execute;
		_this.execute = function ( /* session */ ) {
			if ( _iCircuitNode.value () ) {
				_super.execute ();
				_this.clear ();
			} else {
				_iCircuitNode.on ( "CHANGE", _onCircuitNodeListener );
			}
		};

		_this.value = function () {

		};

		_super.reclaim = _this.reclaim;
		_this.reclaim = function () {
			_super.reclaim ();


			ss.PoolManager.reclaim ( CircuitCommand, _this );
		};

		_this.release = function () {

			_super = undefined;
			_this = undefined;
			_iCircuitNode = undefined;
			_commandMap = undefined;

			_construct = undefined;
			_recurseCircuitHeirachy = undefined;
			_buildCircuit = undefined;
			_isModifier = undefined;
			_onCircuitNodeListener = undefined;

			for ( var property in _this ) {
				delete _this [ property ];
			}
		};

		return _construct ( circuitStr, commandMap );
	}

	ss.CircuitCommand = CircuitCommand;

} () );

/* global require, ss */



( function() {

	"use strict";

	ORNode.prototype = new ss.AbstractCircuitNode ();
	ORNode.prototype.constructor = ORNode;

		/**
		 * Do l really need to tell you what this does?!?!?
		 * @param {ns.AbstractCommand|ns.AbstractCircuitNode} children :  		The children nodes passed in. Rarely used...
		 * @return - {ns.ORNode} 												A node use to do AND logical comparisons
		 */
	function ORNode ( children ) {

		var _this = ss.AbstractCircuitNode.call ( this, children );

			/**
			 * Do l really need to tell you what this does?!?!?
			 * @param {ns.AbstractCommand|ns.AbstractCircuitNode} children :  		The children nodes passed in. Rarely used...
			 * @return - {ns.ORNode} 												A node use to do OR logical comparisons
			 */
		function _construct ( /*children*/ ) {
			return _this;
		}

			/**
			 * Return the logical OR comparison of all children
			 */
		_this.value = function() {

				// grab all the children
			var iterator = _this.children ();

				// Simple validation to check if we have any children. Why wouldn't we >:(
			if ( iterator.hasNext () ) {

					// get the initial value
				var result = _this.processValue ( iterator.next () );

					// keep comparing each child against itself
				while( iterator.hasNext () ) {
					result |= _this.processValue ( iterator.next () );
				}

					// were all done
				return result;
			}

				// no children. WTF?!?!?!
			return false;
		};

		return _construct ( children );
	}

		// assign to the
	ss.ORNode = ORNode;
} () );

/* global require, ss */




( function ( ) {
	"use strict";

	ANDNode.prototype = new ss.AbstractCircuitNode ();
	ANDNode.prototype.constructor = ANDNode;

		/**
		 * Do l really need to tell you what this does?!?!?
		 * @param {ns.AbstractCommand|ns.AbstractCircuitNode} children :  		The children nodes passed in. Rarely used...
		 * @return - {ns.ANDNode} 												A node use to do AND logical comparisons
		 */
	function ANDNode ( children ) {

		var _this = ss.AbstractCircuitNode.call ( this, children );

			/**
			 * Do l really need to tell you what this does?!?!?
			 * @param {ns.AbstractCommand|ns.AbstractCircuitNode} children :  		The children nodes passed in. Rarely used...
			 * @return - {ns.ANDNode} 												A node use to do AND logical comparisons
			 */
		function _construct ( /*children*/ ) {
			return _this;
		}

			/**
			 * Return the logical AND comparison of all children
			 */
		_this.value = function() {

				// grab all the children
			var iterator = _this.children ();

				// Simple validation to check if we have any children. Why wouldm't we >:(
			if ( iterator.hasNext () ) {

					// get the initial value
				var result = _this.processValue ( iterator.next () );

					// keep comparing each child against itself
				while( iterator.hasNext () ) {
					result &= _this.processValue ( iterator.next () );
				}

					// were all done
				return result;
			}

				// no children. WTF?!?!?!
			return false;
		};

		return _construct ( children );
	}

		// assign to
	ss.ANDNode = ANDNode;
} ( ss ) );

/* global require, ss */



( function() {
	"use strict";

	XORNode.prototype = new ss.AbstractCircuitNode ();
	XORNode.prototype.constructor = XORNode;

		/**
		 * Do l really need to tell you what this does?!?!?
		 * @param {ns.AbstractCommand|ns.AbstractCircuitNode} children :  		The children nodes passed in. Rarely used...
		 * @return - {ns.XORNode} 												A node use to do XOR logical comparisons
		 */
	function XORNode ( children ) {

		var _this = ss.AbstractCircuitNode.call ( this, children );

			/**
			 * Do l really need to tell you what this does?!?!?
			 * @param {ns.AbstractCommand|ns.AbstractCircuitNode} children :  		The children nodes passed in. Rarely used...
			 * @return - {ns.XORNode} 												A node use to do XOR logical comparisons
			 */
		function _construct ( /*children*/ ) {
			return _this;
		}

			/**
			 * Return the logical XOR comparison of all children
			 */
		_this.value = function() {

				// grab all the children
			var iterator = _this.children ();

				// Simple validation to check if we have any children. Why wouldm't we >:(
			if ( iterator.hasNext () ) {

					// get the initial value
				var result = _this.processValue ( iterator.next () );

					// keep comparing each child against itself
				while( iterator.hasNext () ) {
					result ^= _this.processValue ( iterator.next () );
				}

					// were all done
				return result;
			}

				// no children. WTF?!?!?!
			return false;
		};

		return _construct ( children );
	}

		// assign to the name space
	ss.XORNode = XORNode;

} () );

/* globals require, ss */





( function () {
	"use strict";

	CommandTree.prototype = new ss.TreeNode ();
	CommandTree.prototype.constructor = CommandTree;

	function CommandTree () {

		ss.TreeNode.call ( this );
		var _this = this; 
		var _commandQueue;

		var _construct = function () {
			_commandQueue = new ss.CommandQueue ();

			CommandTree.referenceSet.push ( _this );
		};

		_this.init = function () {

		};

		_this.queue = function ( command ) {
			_commandQueue.queue ( command );
		};

		_this.start = function ( command ) {

			if ( !ss.isEmpty ( command ) ) {
				_commandQueue.queue ( command );
			}

			_commandQueue.start ();
  
			var treeNodeIterator = new ss.TreeNodeIterator ( _this );

			while ( treeNodeIterator.canTraverse () ) {
				var commandTree = treeNodeIterator.traverse ();
				commandTree.start ();
			}
		};

		_this.cancel = function () {

			_commandQueue.cancel ();

			var treeNodeIterator = new ss.TreeNodeIterator ( _this );

			while ( treeNodeIterator.canTraverse () ) {
				var commandTree = treeNodeIterator.traverse ();
				commandTree.cancel ();
			}
		};

		_this.stop = function () {
			var treeNodeIterator = new ss.TreeNodeIterator ( _this );

			while ( treeNodeIterator.canTraverse () ) {
				var commandTree = treeNodeIterator.traverse ();
				commandTree.stop ();
			}
		};

		_this.reclaim = function () {

			_commandQueue.cancel ();

			var treeNodeIterator = new ss.TreeNodeIterator ( _this );

			while ( treeNodeIterator.canTraverse () ) {
				var commandTree = treeNodeIterator.traverse ();
				commandTree.reclaim ();
			}

			ss.PoolManager.reclaim ( CommandTree, _this );
		};


		return _construct ();
	}

	CommandTree.referenceSet = [];

	ss.CommandTree = CommandTree;

} () );