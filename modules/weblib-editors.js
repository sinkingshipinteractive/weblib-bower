/*! weblib-ss 0.0.1 */
/* global $, ss.isEmpty, require, Interpolation, ss */






( function() {
	'use strict';

	// Regex to test if a string is a hex color.

	var colorRegex, EditorHelpers;
	colorRegex = /^#(?:[0-9a-f]{3}){1,2}$/i;

	ss.EditorHelpers = {};

	/**
	* Checks if a string is a hex color.
	**/
	ss.EditorHelpers.isHtmlColour = function(color) {
	    if (colorRegex.test(color)) {
			return true;
		}
		return false;
	};

	/**
	* Create a basic input box for properties of an object that don't have a custom input type.
	* @param {Div} $div -> The div to put the editor into.
	* @param {Object} currentComponent -> The component whose property we're exposing.
	* @param {String} currentProperty -> The property we're exposing.
	**/
	ss.EditorHelpers.createInputEditor = function($div, currentComponent, currentProperty) {
		var $valueName, $input;

		$valueName = $('<div>' + currentProperty + ':</div>');
		$input = $('<input type="text" name="' + currentProperty + '" value="' + currentComponent[currentProperty] + '" />');
		$input.data ( 'dataObject', currentComponent );

		$div.append($valueName);
		$valueName.append($input);

		return $input;
	};

	/**
	 * Create a select editor (list of options.)
	 * @param  {<div>} $div              The div to put the editor into.
	 * @param  {Object} currentComponent The component being edited.
	 * @param  {string} currentProperty  The property being edited.
	 * @param  {string} options          The list of all possible options.
	 * @param  {string} current          The currently selected option.
	 * @return {<select>}                The select form that was created.
	 */
	ss.EditorHelpers.createSelectEditor = function($div, currentComponent, currentProperty, options, current) {
		var $valueName = $('<div>' + currentProperty + ':</div>');

		var $sel = $('<select> </select>');
		$sel.data ( 'dataObject', currentComponent );
		$sel.data ( 'dataProperty', currentProperty );

		$valueName.append($sel);
		var opt;
		for(opt in options) {
			if (typeof opt === 'string') {
				$sel.append('<option value="' + options[opt] + '"> ' + options[opt] + ' </option>');
			}
		}

		$sel.val(current);

		$div.append($valueName);

		return $sel;

	};

	/**
	* Create an editor input box for numbers.
	* Validates that the input is numeric, and adds the ability to horizontally slide the number.
	* @param {div} $div -> The div to put the editor into.
	* @param {Object} currentComponent -> The component whose property we're exposing.
	* @param {String} currentProperty -> The property we're exposing.
	**/
	ss.EditorHelpers.addNumberEditor = function($div, currentComponent, currentProperty) {
		var $input, $this, tObject, $numberEditorDiv;

		$numberEditorDiv = $('<div></div>');

		$input = ss.EditorHelpers.createInputEditor($numberEditorDiv, currentComponent, currentProperty);

		$input.change ( function() {
			$this = $ (this);
			tObject = $this.data ('dataObject');
			tObject [ $this.attr ('name') ] = eval($this.val ()); // jshint ignore:line
		} );

		$input.numeric();

		// TODO: Make Numbers slideable.
		/*$input.css('cursor', 'ew-resize');

		$input.mousedown( function () {

		} );*/

		$div.append($numberEditorDiv);

		return $numberEditorDiv;
	};

	/**
	* Adds an editor for string properties.
	* @param {Div} $div -> The div to put the editor into.
	* @param {Object} currentComponent -> The component whose property we're exposing.
	* @param {String} currentProperty -> The property we're exposing.
	**/
	ss.EditorHelpers.addStringEditor = function($div, currentComponent, currentProperty) {
		var $input, $this, tObject;

		$input = ss.EditorHelpers.createInputEditor ($div, currentComponent, currentProperty);

		$input.change ( function() {
			$this = $ ( this );
			tObject = $this.data ('dataObject');
			tObject [ $this.attr ('name') ] = eval('"' + $this.val () + '"'); // jshint ignore:line
		} );
	};

	/**
	* Adds an editor for color properties.
	* @param {Div} $div -> The div to put the editor into.
	* @param {Object} currentComponent -> The component whose property we're exposing.
	* @param {String} currentProperty -> The property we're exposing.
	**/
	ss.EditorHelpers.addColorEditor = function($div, currentComponent, currentProperty) {
		var $valueName, $input, $this, tObject;

		$valueName = $('<div>' + currentProperty + ':</div>');
		$input = $('<input type="color" name="' + currentProperty + '" value="' + currentComponent[currentProperty] + '" />');
		$input.data ( 'dataObject', currentComponent );

		$input.change ( function() {
			$this = $ ( this );
			tObject = $this.data ('dataObject');
			tObject [ $this.attr ('name') ] = $this.val ();
		} );

		$div.append($valueName);
		$valueName.append($input);

		return $input;
	};

	/**
	* Create an editor for a boolean field.
	* @param {Div} $div -> The div to put the editor into.
	**/
	ss.EditorHelpers.addBooleanEditor = function($div, component, property) {
		var $valueName, $input, $this, tObject;
		$valueName = $('<div>' + property + ':</div>');
		$input = $('<input type="checkbox" name="' + property + '" checked="' + component[property] + '" />');
		$input.data ( 'dataObject', component );

		$input.change ( function() {
			$this = $ ( this );
			tObject = $this.data ('dataObject');
			tObject [ $this.attr ('name') ] = $this.prop('checked');
		} );

		$div.append($valueName);
		$valueName.append($input);
	};

	/*
	* Get the name of a given interpolation function.
	* @param property: the function itself.
	*/
	ss.EditorHelpers.getInterpolationFunction = function(property) {
		var prop;
		for(prop in ss.Interpolation) {
			if (property === ss.Interpolation[prop]) {
				return prop;
			}
		}
	};

	// cache for getting the list of interpolation functions.
	var interpolationFunctions = [];

	/*
	* Get a list of the interpolation functions.
	* @return {Array} A list of the interpolation functions.
	*/
	ss.EditorHelpers.getListOfInterpolationFunctions = function() {
		if (interpolationFunctions.length === 0) {
			var prop;
			interpolationFunctions = [];
			for(prop in Interpolation) {
				if (Interpolation[prop].length === 1) {
					interpolationFunctions.push(prop);
				}
			}
		}
		return interpolationFunctions;
	};

	/**
	 * Create an editor for an interpolation function.
	 * @param {JQuery Div} $div      The div to put the editor into.
	 * @param {object} component the component the editor is being created for.
	 * @param {string} property  The property that the editor is being created for.
	 * @param {interp} interp    The current interpolation function.
	 */
	ss.EditorHelpers.addInterpolationFunctionEditor = function($div, component, property, interp) {
		var options = ss.EditorHelpers.getListOfInterpolationFunctions();
		var $input = ss.EditorHelpers.createSelectEditor($div, component, property, options, interp);

		$input.change ( function() {
			var $this = $ (this);
			var tObject = $this.data ('dataObject');
			var tProp = $this.data ('dataProperty');
			tObject [ tProp ] = Interpolation[$this[0].value];
		} );

	};

	/**
	* Create a custom editor for a given property.
	* @param {ObjecT} $div ->
	**/
	ss.EditorHelpers.createEditorForProperty = function($div, component, property) {
		var data;

		data = component[property];

		switch(typeof data) {
			case 'boolean':
				// Add Editor for boolean properties.
				ss.EditorHelpers.addBooleanEditor($div, component, property);
			break;
			case 'number':
				// Add editor for numeric properties.
				ss.EditorHelpers.addNumberEditor($div, component, property);
			break;
			case 'object':
				// if we're looking at an object, see if it has a custom editor.
				if (typeof data.customEditor === 'function') {
					// if we have a custom editor, display it.
					data.customEditor($div, property, data);
				} else {
					if (data.exposedVariables === undefined) {
						// If we don't have a component, hide it.
						data.exposedVariables = Object.keys(data);
						data.name = property;
						ss.EditorHelpers.createStandardEditorForComponent($div, data);
					} else {
						// If we have a component, display it.
						ss.EditorHelpers.createStandardEditorForComponent($div, data);
					}
				}
			break;
			case 'string':
				if (ss.EditorHelpers.isHtmlColour(data)) {
					// check if the string represents a color, if so, add a colour editor.
					ss.EditorHelpers.addColorEditor($div, component, property);
				} else {
					// if it doesn't represent a color, use a regular computer editor.
					ss.EditorHelpers.addStringEditor($div, component, property);
				}
			break;
			case 'function':
				// if we found a function that was exposed, see if it's an interpolation function,
				// if so show the interpolation editor, otherwise, use a string editor.
				var interp = ss.EditorHelpers.getInterpolationFunction(component[property]);
				if (!ss.isEmpty(interp)) {
					ss.EditorHelpers.addInterpolationFunctionEditor($div, component, property, interp);
				} else {
					ss.EditorHelpers.addStringEditor($div, component, property);
				}
				break;

			default:
				// if we don't know how to edit something, put in a standard editor.
				ss.EditorHelpers.addStringEditor($div, component, property);
			break;
		}
	};

	/*
	* Create a standard editor for the component.
	* @param $container -> The container element to put the component into.
	* @param currentComponent -> The component to create an editor for.
	*/
	ss.EditorHelpers.createStandardEditorForComponent = function($container, currentComponent) {
		var properties, propIndex, currentProperty;

		$container.append($('<div class="componentName">' + currentComponent.name + '</div>'));

		properties = currentComponent.exposedVariables;
		for(propIndex = 0; propIndex < properties.length; propIndex++) {
			// create an editor for each propery of the object.
			currentProperty = properties[propIndex];
			ss.EditorHelpers.createEditorForProperty($container, currentComponent, currentProperty);
		}
	};

	/*
	* Create a standard editor for the component.
	* @param $container -> The container element to put the component into.
	* @param obj -> The component to create an editor for.
	*/
	ss.EditorHelpers.createStandardEditorForObject = function($container, obj) {
		var properties, propIndex, currentProperty;

		$container.append($('<div class="componentName">' + obj.name + '</div>'));

		properties = Object.keys(obj);
		for(propIndex = 0; propIndex < properties.length; propIndex++) {
			// create an editor for each propery of the object.
			currentProperty = properties[propIndex];
			ss.EditorHelpers.createEditorForProperty($container, obj, currentProperty);
		}
	};

	/*
	* Create a standard editor.
	* @param $container -> the container element to put the editor into.
	* @param obj -> the object whose editor you're creating.
	*/
	ss.EditorHelpers.createStandardEditor = function($container, obj) {
		 if (!ss.isEmpty(obj.exposedVariables)) {
			ss.EditorHelpers.createStandardEditorForComponent($container, obj);
		} else {
			ss.EditorHelpers.createStandardEditorForObject($container, obj);
		}
	};

	/*
	* Create an editor.
	* @param $container -> the container element to put the editor into.
	* @param obj -> the object whose editor you're creating.
	*/
	ss.EditorHelpers.createEditor = function($container, obj) {
		if (!ss.isEmpty(obj.createCustomEditor)) {
			obj.createCustomEditor($container);
		} else {
			ss.EditorHelpers.createStandardEditor($container, obj);
		}
	};

} ());

/* global require, console, ss */





( function() {
	"use strict";

	/**
	 * Class of static helper functions to help with serialization and deserialization of objects.
	 */
	ss.SerializationHelpers = ss.SerializationHelpers || {};

	// ///// BEGIN - list of constants for names of known types that can be automatically deserialized //////
	ss.SerializationHelpers.TYPE_VECTOR2 = "Vector2";
	// ///// END - list of constants for names of known types that can be automatically deserialized //////

	/*
	* Helper used to serialize an object to JSON.
	* @param objectToSerialize {string}: The object to serialize. Will use JSON.stringify unless the object to serialize has a serialize method, which will be used instead.
	* NOTE:
	* 	 Objects that use this serialization system need:
	* 	 	- a "name" property, which matches the name of the class of the object.
	* 	  	- a "exposedVariables" property, which matches the variables that will be set on the object when deserializing.
	* NOTE:
	* 		By default, when deserializing, the object's constructor will be called with no parameters, and the exposed variables will be set individually, if a different
	* 	 	behaviour is desired, a deserialize method is required.
	* @return {string}:
	*         a serialized representation of the object, in the following form:
	*         {
	*         	"name": "NAME_OF OBJECT",
	*           "objectJSON": "JSON_REPRESENTATION_OF_OBJECT"
	*         }
	*/
	ss.SerializationHelpers.serialize = function(objectToSerialize) {

		var objectJSON;

		if (typeof objectToSerialize.serialize === "function") {
			objectJSON = objectToSerialize.serialize();
		}

		objectJSON = JSON.stringify(objectToSerialize);

		return JSON.stringify({
			"name"       : objectToSerialize.name,
			"objectJSON" : objectJSON
		});

	};


	/**
	 * This method is used to detect known object types to deserialize.
	 * @param  {Object} jsonObject The object whose type to try to determing.
	 * @return {bool/string} false if the type is unkown, a string representing the type if the type is known.
	 */
	ss.SerializationHelpers.getCustomType = function(jsonObject) {
		if (typeof jsonObject === "object") {
			// we're looking at an object.

			// is it something we recognize?
			if (ss.SerializationHelpers.isVector2(jsonObject) ) {
				// it's a vector2!
				return ss.SerializationHelpers.TYPE_VECTOR2;
			}

			// hmm.. that's not good, it's an object, but I don't know what it it.
			console.warn("warning: Attempting to deserialize an unkown object: " + JSON.stringify(jsonObject));
			return false;
		}

		return false;
	};

	/**
	 * Attempts to check if an object parsed from JSON is meant to be a vector2.
	 * checks for exactly 2 properties, named x & y.
	 * @param  {Object}  jsonObject: An object parsed from json data, that may represent a Vector2.
	 * @return {Boolean}: true if the object seems to represent a Vector2, false if it doesn't.
	 */
	ss.SerializationHelpers.isVector2  = function(jsonObject) {
		var numProperties = Object.keys(jsonObject).length;
		if (numProperties === 2) {
			// I thought I saw a Vector2.
			if (!ss.isEmpty(jsonObject.x) && !ss.isEmpty(jsonObject.y)) {
				// I DID!, I DID SAW A VECTOR2!
				return true;
			}
		}
	};

	/*
	* Custom deserializer for Vector2.
	* @param {Object} jsonObject: an object with x and y properties that will be converted to Vector2.
	* @return {Vector2}: a Vector2 build from the object representation.
	*/
	ss.SerializationHelpers.deserializeVector2 = function(jsonObject) {
		return new ss.Vector2(jsonObject.x, jsonObject.y);
	};

	/*
	* Deserialize a known custom type.
	* @param jsonObject {Object}: An object, parsed from json data, to be converted into the proper custom type.
	* @param type{string}: The type of object to deserialize.
	* @return a deserialized version of the json object, or undefined if the type is unknown.
	*/
	ss.SerializationHelpers.deserializeCustomType = function(jsonObject, type) {

		switch(type) {
			case ss.SerializationHelpers.TYPE_VECTOR2:
				return ss.SerializationHelpers.deserializeVector2(jsonObject);
		}
		return undefined;
	};

	/*
	* Perform basic deserialization of an object, this will call the constructor on the class
	* then set each of the exposed variables to the values in the JSON object.
	* NOTE:
	* 	- All primitive types will be automatically deserialized.
	* 	- Reference to the following custom classes will deserialize automatically:
	* 		- Vector2
	* 	- Any other classes will need to use a custom deserialization method to manually deserialize them.
	* @serializedObject {string}: A serialized representation of the object, in the following form:
	* 		{
	*        	"name": "NAME_OF OBJECT",
	*         	"objectJSON": "JSON_REPRESENTATION_OF_OBJECT"
	*        }
	* @return {Object}: the deserialized object.
	*/
	ss.SerializationHelpers.basicDeserialize = function(serializedObject) {
		var objDefinition = JSON.parse(serializedObject);
		var newObj = new window[objDefinition.name]();

		var objData = JSON.parse(objDefinition.objectJSON);
		var exposedVar, i;

		if (!ss.isEmpty(newObj.exposedVariables)) {
			for(i = 0; i < newObj.exposedVariables.length; i++) {
				exposedVar = newObj.exposedVariables[i];

				// check for custom data types:
				var customType = ss.SerializationHelpers.getCustomType(objData[exposedVar]);
				if (customType) {
					var deserializedCustomObj = ss.SerializationHelpers.deserializeCustomType(objData[exposedVar], customType);
					newObj[exposedVar] = deserializedCustomObj;
				} else {
					newObj[exposedVar] = objData[exposedVar];
				}
			}
		}

		return newObj;
	};


	/*
	* Deserialize an object. If the object has a deserialize method, this will be called, otherwise,
	* basicDeserialize will be used by default.
	* NOTE:
	* 	- All primitive types will be automatically deserialized.
	* 	- Reference to the following custom classes will deserialize automatically:
	* 		- Vector2
	* 	- Any other classes will need to use a custom deserialization method to manually deserialize them.
	* @serializedObject {string}: A serialized representation of the object, in the following form:
	* 		{
	*        	"name": "NAME_OF OBJECT",
	*         	"objectJSON": "JSON_REPRESENTATION_OF_OBJECT"
	*        }
	* @return {Object}: the deserialized object.
	*/
	ss.SerializationHelpers.deserialize = function(serializedObject) {
		var objDefinition = JSON.parse(serializedObject);
		var newObj = new window[objDefinition.name]();

		if (typeof newObj.deserialize === "function") {
			newObj = newObj.deserialize(serializedObject);
		} else {
			newObj = ss.SerializationHelpers.basicDeserialize(serializedObject);
		}
		return newObj;
	};

} ());
