/*! weblib-ss 0.0.1 */
/* global require, createjs, ss */




( function() {
	"use strict";

	/**
	 * Bitmap font class, represents a bitmap font.
	 * @param {JSON string} dataFile              JSON string containing the data about the font (character offsets, kerning, etc.)
	 * @param {JSON string} spriteSheetDefinition JSON string containing the spritesheet definition for the font.
	 */
	ss.BitmapFont = function(dataFile, spriteSheetDefinition) {

		this.fontDataFile = dataFile;

		this.spriteSheetDefinition = spriteSheetDefinition;
		this.spriteSheet = new createjs.SpriteSheet(spriteSheetDefinition);

	};

	ss.BitmapFont.prototype.constructor = ss.BitmapFont;

	/**
	 * Get the line height for the font.
	 * @return {Number} The line height for the font.
	 */
	ss.BitmapFont.prototype.getLineHeight = function() {
		return this.fontDataFile.common.lineHeight;
	};

	ss.BitmapFont.prototype.getCharacterHeight = function(character) {
		return this.getCharDataByCharacter(character).height;
	};

	/**
	 * Get a sprite for the given character.
	 * @param  {character} character 	The character whose sprite to get.
	 * @return {createjs.Sprite}        The sprite for that character.
	 */
	ss.BitmapFont.prototype.getSpriteForCharacter = function(character) {
		if (ss.isEmpty(this.spriteSheetDefinition.animations[character])) {
			throw("character " + character + " not found in font.");
		}
		return new createjs.Sprite(this.spriteSheet, character.toString());
	};

	/**
	 * Get the identifier in the font for a given character.
	 * @param  {character} character 	The character whose id to get.
	 * @return {Number}           		The id of the given character in the data file.
	 */
	ss.BitmapFont.prototype.getCharIdByCharacter = function(character) {
		for(var i = 0; i < this.fontDataFile.chars.length; i++) {
			if (this.fontDataFile.chars[i].letter == character) {
				return this.fontDataFile.chars[i].id;
			}
		}
		return -1;
	};

	/**
	 * Get the data about a given character.
	 * @param  {character} character  The character to get.
	 * @return {Object}           	  The character's data.
	 */
	ss.BitmapFont.prototype.getCharDataByCharacter = function(character) {
		for(var i = 0; i < this.fontDataFile.chars.length; i++) {
			if (this.fontDataFile.chars[i].letter == character) {
				return this.fontDataFile.chars[i];
			}
		}
		return {};
	};

	/**
	 * Get the y offset for a given character.
	 * @param  {character} character   	The character whose y offset to get.
	 * @return {Number}           		The y offset for the character.
	 */
	ss.BitmapFont.prototype.getYOffsetForCharacter = function(character) {
		return this.getCharDataByCharacter(character).yoffset;
	};

	/**
	 * Get the x offset of a given character.
	 * @param  {character} character 	The character whose x offset to get.
	 * @return {Number}					The x offset for the character.
	 */
	ss.BitmapFont.prototype.getXOffsetForCharacter = function(character) {
		return this.getCharDataByCharacter(character).xoffset;
	};

	/**
	 * Get the kerning for a given pair of letters.
	 * @param  {character} currentLetter   	The current letter.
	 * @param  {character} nextLetter      	The next letter.
	 * @return {Number}               		The Kerning for the given pair of letters.
	 */
	ss.BitmapFont.prototype.getKerning = function(currentLetter, nextLetter) {
		var firstLetterId = this.getCharIdByCharacter(currentLetter);
		var secondLetterId = this.getCharIdByCharacter(nextLetter);

		for(var i = 0; i < this.fontDataFile.kerning.length; i++) {
			var kern = this.fontDataFile.kerning[i];
			if (kern.first == firstLetterId && kern.second == secondLetterId) {
				return kern.amount;
			}
		}

		return 0;
	};

	/**
	 * Get the amount to move, excluding kerning for the current letter.
	 * @param  {character} currentLetter 	The letter whose movement amount to get.
	 * @return {Number}               		The amount to move.
	 */
	ss.BitmapFont.prototype.getAmountToMove = function(currentLetter) {
		return this.getCharDataByCharacter(currentLetter).xadvance;
	};

	/**
	 * The amount to move from one letter to the next, including kerning.
	 * @param  {character} currentLetter 	The first character.
	 * @param  {character} nextLetter    	The next letter.
	 * @return {Number}               		The amount to move, including kerning.
	 */
	ss.BitmapFont.prototype.getAmountToMoveWithKerning = function(currentLetter, nextLetter) {
		var amtToMove = this.getCharDataByCharacter(currentLetter).xadvance;
		if (!ss.isEmpty(nextLetter)) {
			var theKerning = this.getKerning(currentLetter, nextLetter);
			amtToMove += theKerning;

		}
		return amtToMove;
	};

} () );

/* global require, createjs, ss */




( function( ) {
	"use strict";

	/**
	 * Constant for center text alignment.
	 * @type {String}
	 */
	ss.TEXT_ALIGNMENT_CENTER = "center";

	/**
	 * Constant for left text alignment.
	 * @type {String}
	 */
	ss.TEXT_ALIGNMENT_LEFT = "left";

	/**
	 * Constant for right text alignment.
	 * @type {String}
	 */
	ss.TEXT_ALIGNMENT_RIGHT = "right";

	/**
	 * Bitmap Text class, used to render blocks of text using a bitmap font.
	 * @param {ss.BitmapFont} bitmapFont 	The font to use.
	 * @param {string} text       		 	The text to display.
	 * @param {string} alignment  			One of ss.TEXT_ALIGNMENT_CENTER, ss.TEXT_ALIGNMENT_LEFT, ss.TEXT_ALIGNMENT_RIGHT to represent the text alignment.
	 */
	ss.BitmapText = function(bitmapFont, text, alignment) {
		// call the container constructor.
		createjs.Container.call(this);

		if (ss.isEmpty(alignment)) {
			// if the text alignment wasn't set, set it to left.
			alignment = ss.TEXT_ALIGNMENT_LEFT;
		}

		// set the font and alignment.
		this.font = bitmapFont;
		this.alignment = alignment;

		this.letters = [];

		// change the text.
		this.changeText(text);
	};

	// set the prototype to container, and connect the constructor.
	ss.BitmapText.prototype = new createjs.Container();
	ss.BitmapText.prototype.constructor = ss.BitmapText;

	/**
	 * Clear the existing text.
	 */
	ss.BitmapText.prototype.clearText = function() {
		this.removeAllChildren();
	};

	/**
	 * Get the width of a line of text.
	 * @param  {string} line 	The line whose width to get.
	 * @return {Number}      	The width of the line, in pixels.
	 */
	ss.BitmapText.prototype.getWidthOfLine = function(line) {
		// track the width in a variable.
		var currentWidth = 0;

		// run through each character.
		for(var i = 0; i < line.length; i++) {

			// get the current character.
			var currentCharacter = line[i];

			// convert special characters.
			currentCharacter = this.convertCharacter(currentCharacter);

			if (i < line.length - 1) {
				// if there's a character after this one, add the amount to move and the kerning.
				currentWidth += this.font.getAmountToMoveWithKerning(currentCharacter, line[i + 1] );
			} else {
				// if there isn't a character after this one, add just the amount ot move.
				currentWidth += this.font.getAmountToMove(currentCharacter);
			}
		}

		// return the total.
		return currentWidth;
	};

	/**
	 * Convert special characters to their representation in the SpriteSheet.
	 * @param  {character} 	currentCharacter 	The current character.
	 * @return {string}                  		The converted character.
	 */
	ss.BitmapText.prototype.convertCharacter = function(currentCharacter) {
		if (currentCharacter == "1") {
			currentCharacter = "one";
		} else if (currentCharacter == "2") {
			currentCharacter = "two";
		} else if (currentCharacter == "3") {
			currentCharacter = "three";
		} else if (currentCharacter == "4") {
			currentCharacter = "four";
		} else if (currentCharacter == "5") {
			currentCharacter = "five";
		} else if (currentCharacter == "6") {
			currentCharacter = "six";
		} else if (currentCharacter == "7") {
			currentCharacter = "seven";
		} else if (currentCharacter == "8") {
			currentCharacter = "eight";
		} else if (currentCharacter == "9") {
			currentCharacter = "nine";
		} else if (currentCharacter == "0") {
			currentCharacter = "zero";
		}

		return currentCharacter;
	};

	/**
	 * Get the starting x offset for a line, given the max line width, and the text alignment.
	 * @param  {Number} currentLineWidth 	The width of the current line.
	 * @param  {Number} maxLineWidth     	The width of the widest line.
	 * @param  {String} alignment        	The text alignment for the block.
	 * @return {Number}                  	The starting x offset for the line.
	 */
	ss.BitmapText.prototype.getStartingXOffset = function(currentLineWidth, maxLineWidth, alignment) {
		switch(alignment) {
			case ss.TEXT_ALIGNMENT_LEFT:
				// if we're left aligned, the offset should be zero.
				return 0;
			case ss.TEXT_ALIGNMENT_RIGHT:
				// if we're right aligned, the offset should be the max width subtract the current width.
				return maxLineWidth - currentLineWidth;
			case ss.TEXT_ALIGNMENT_CENTER:
				// if we're center aligned, the offset should be half of the max width subtract the current width.
				return (maxLineWidth - currentLineWidth) / 2;
		}
	};

	ss.BitmapText.prototype.getLetters = function () {
		return this.letters;
	};

	/**
	 * Change the text displayed on this instance.
	 * @param  {string} text The new text to display.
	 */
	ss.BitmapText.prototype.changeText = function(text) {
		// first clear the old text, if there is one.
		this.clearText();

		// make sure the 1st parameter was a string.
		this.text = text.toString();

		// split up the lines of the text.
		var lines = this.text.split("\n");
		this.letters = [];

		// put the cursor at the top of the text.
		var cursorY = 0;

		// determine how wide the widest line of the text is.
		var i;
		var maxLineWidth = -1;

		for(i = 0; i < lines.length; i++) {
			maxLineWidth = Math.max(this.getWidthOfLine(lines[i]), maxLineWidth);
		}

		// loop through each of the lines.
		for(i = 0; i < lines.length; i++) {

			// determine the width of the current line.
			var currentLineWidth = this.getWidthOfLine(lines[i]);

			// set the starting x offset based on the width and alignment for this line.
			var cursorX = this.getStartingXOffset(currentLineWidth, maxLineWidth, this.alignment);

			// get a reference to the current line.
			var currentLine = lines[i];

			for(var j = 0; j < currentLine.length; j++) {

				// prepare the current character for inclusion in the line.
				var currentCharacter = currentLine[j];
				currentCharacter = this.convertCharacter(currentCharacter);

				if (currentCharacter != " ") {
					// if the currnet character isn't a space,
					// get the sprite for it.
					var nextLetter = this.font.getSpriteForCharacter(currentCharacter);

					// set the x and y offset from the line of text for this character image.
					nextLetter.x = this.font.getXOffsetForCharacter(currentCharacter) + cursorX;
					nextLetter.y = this.font.getYOffsetForCharacter(currentCharacter) + cursorY;

					this.letters.push ({ letter: currentCharacter, image:nextLetter, x:nextLetter.x, y:nextLetter.y});

					// add the letter to this container.
					this.addChild(nextLetter);
				}

				if (i < currentLine.length - 1) {
					// if we're not the last character in the string, include Kerning before
					// moving the cursor to the next position.
					cursorX += this.font.getAmountToMoveWithKerning(currentCharacter, currentLine[j + 1] );
				} else {
					// if we're the last character, move the cursor, without Kerning.
					cursorX += this.font.getAmountToMove(currentCharacter);
				}
			}

			// move the cursor to the next line.
			cursorY += this.font.getLineHeight();
		}

		// set the width of the container to the width of the widest line.
		this.width = maxLineWidth;

		// set the height of the container to the cursor position.
		this.height = cursorY;

		switch(this.alignment) {
			case ss.TEXT_ALIGNMENT_LEFT:
				this.regX = 0;
			break;
			case ss.TEXT_ALIGNMENT_RIGHT:
				this.regX = maxLineWidth;
			break;
			case ss.TEXT_ALIGNMENT_CENTER:
				this.regX = (maxLineWidth) / 2;
			break;
		}
	};

} () );

/* global require, createjs, ss */





( function( ) {
	"use strict";

	/**
	 * A line of text drawn along a bezier curve.
	 * @param {ss.BitmapFont} bitmapFont  		The font to use to display this text.
	 * @param {string} text        				The text to display.
	 * @param {Array[ss.Vector2]} curvePoints 	The control points along the bezier curve.
	 */
	ss.BezierBitmapText = function(bitmapFont, text, curvePoints) {

		createjs.Container.call(this);

		this.bezier = new ss.Bezier(curvePoints);
		this.font = bitmapFont;
		this.text = text;

		// figure out the conversion factor between
		this.pixelToPercentRatio = 1.0 / this.bezier.getApproximateLength();
		this.displayTextAtPosition(0);
	};

	ss.BezierBitmapText.prototype = new createjs.Container();
	ss.BezierBitmapText.prototype.constructor = ss.BezierBitmapText;

	/**
	 * Clear the text from this container.
	 */
	ss.BezierBitmapText.prototype.clearText = function() {
		this.removeAllChildren();
	};

	/**
	 * Display text at a given position along the curve.
	 * NOTE: if part of the text goes past the end of the curve,
	 * it will be displayed quite strangely.
	 * @param  {Number} t How far along the curve the start of the text will display, in the 0-1 range.
	 */
	ss.BezierBitmapText.prototype.displayTextAtPosition = function(t) {

		// first clear the text.
		this.clearText();

		// start the cursor at zero.
		var cursorX = 0;

		// loop through the characters in the text.
		for(var i = 0; i < this.text.length; i++) {
			// get the current character.
			var currentCharacter = this.text[i];

			// find your position along the curve.
			var currentPositionAlongCurve = (cursorX * this.pixelToPercentRatio) + t;

			// if the current character is not a space.
			if (currentCharacter != " ") {
				var nextLetter = this.font.getSpriteForCharacter(currentCharacter);

				var tangent = this.bezier.getApproximateTangent(currentPositionAlongCurve).clone();
				var normal = new ss.Vector2(tangent.y * -1, tangent.x );
				var pointOnCurve = this.bezier.evaluate(currentPositionAlongCurve).clone();

				var xOffset = tangent.mult(this.font.getXOffsetForCharacter(currentCharacter));
				var yOffset = normal.mult(this.font.getYOffsetForCharacter(currentCharacter));

				var characterPosition = pointOnCurve.add(xOffset).add(yOffset);
				nextLetter.x = characterPosition.x;
				nextLetter.y = characterPosition.y;

				nextLetter.rotation = tangent.angleDeg();

				this.addChild(nextLetter);
			}

			if (i < this.text.length - 1) {
					// if we're not the last character in the string, include Kerning before
					// moving the cursor to the next position.
					cursorX += this.font.getAmountToMoveWithKerning(currentCharacter, this.text[i + 1] );
				} else {
					// if we're the last character, move the cursor, without Kerning.
					cursorX += this.font.getAmountToMove(currentCharacter);
				}
		}
	};

} () );
