/*! weblib-ss 0.0.1 */
/* global require, createjs, console, ss */




( function() {
	"use strict";

	/*
	* Class CreateJSParticleEmitter extends createjs.Container, AbstractParticleEmitter
	*	Particle emitter that can be used with the CreateJS display hierarchy
	*/

	console.log("cjsPE, " + ss + ", " + createjs);


	/*
	* Create a new CreateJSParticleEmitter
	*/
	ss.CreateJSParticleEmitter = function() {

		// Call Container base class constructor
		createjs.Container.call(this);

		// Call AbstractParticleEmitter constructor
		ss.AbstractParticleEmitter.call(this);

		var _this = this;
		var _super = {};

		// createjs.Container - The container that particles should be drawn to
		var _particleContainer;

		var _previousPositionX, _previousPositionY;

		/*
		* Set this particle emitter up with a set of instantiated objects
		* @param maxParticles:Integer - Maximum particles that can exist at once
		* @param minLife:Number - Minimum lifetime of a particle
		* @param maxLife:Number - Maximum lifetime of a particle
		* @param spawnShape:BaseSpawnShape - Determines location and other starting properties of particles
		* @param spawnTimer:BaseSpawnTimer - Handles the timing of when particles are spawned
		* @param updaterList:Array[BaseParticleUpdater] - List of particles updaters defining particle updates to be made
		* @param particleContainer:createjs.Container [Optional] - An optional container to draw the particles to.
		* 		If not provided, particles will be drawn to this emitter object itself.
		*/
		_super.setupFromObjects = _this.setupFromObjects;
		_this.setupFromObjects = function(maxParticles, minLife, maxLife, particleFactory, spawnTimer, spawnShape, updaterList, particleContainer) {
			// Set the particle container if one was provided
			_particleContainer = particleContainer ? particleContainer : _this;
			_previousPositionX = _this.x;
			_previousPositionY = _this.y;
			_super.setupFromObjects(maxParticles, minLife, maxLife, particleFactory, spawnTimer, spawnShape, updaterList);
		
		};

		/*
		* Set up this emitter using a single generic object containing configuration data
		* @param dataObj:Object - Object containing configuration data for the emitter
		* @param particleContainer:createjs.Container [Optional] - An optional container to draw the particles to.
		* 		If not provided, particles will be drawn to this emitter object itself.
		*/
		_super.setupFromJson = _this.setupFromJson;
		_this.setupFromJson = function(dataObj, particleContainer) {
			// Set the particle container if one was provided
			_particleContainer = particleContainer ? particleContainer : _particleContainer;

			_super.setupFromJson(dataObj);
		};

		/*
		* Spawn a set of new particles
		* @param numToSpawn:[Number] - The number of new particles to spawn
		*/
		_this._spawnParticles = function(numToSpawn) {
			var newParticle;
			var particlePos;
			var prevParticlePos;

			// Spawn the requested number of particles
			for(var i = 0; i < numToSpawn; i++) {

				// TODO: Get the particle from a particle pool
				newParticle = _this._particleFactory.createParticle();

				_particleContainer.addChild(newParticle);
				_this._spawnShape.setSpawnPosition(newParticle);

				// If requested, fully transform the particle according to this emitter
				if (_this._inheritTransform) {
					prevParticlePos = _this.localToGlobal(_previousPositionX, _previousPositionY);
					prevParticlePos = _particleContainer.globalToLocal(prevParticlePos.x, prevParticlePos.y);
					particlePos = _this.localToGlobal(newParticle.x, newParticle.y);
					particlePos = _particleContainer.globalToLocal(particlePos.x, particlePos.y);
					newParticle.x = ss.MathUtils.lerp1D(particlePos.x, prevParticlePos.x, i / numToSpawn);
					newParticle.y = ss.MathUtils.lerp1D(particlePos.y, prevParticlePos.y, i / numToSpawn);
				// Otherwise just offset particle to account for difference between emitter location and origin of particle container
				}else {
					newParticle.x += ss.MathUtils.lerp1D(_this._particleOrigin.x, _previousPositionX, i / numToSpawn);
					newParticle.y += ss.MathUtils.lerp1D(_this._particleOrigin.y, _previousPositionY, i / numToSpawn);
				}

				_this._particles.push(newParticle);

				// Set a lifetime for this particle
				newParticle.maxLife = _this._minLifetime + Math.random() * (_this._maxLifetime - _this._minLifetime);
			}

			if (_this._inheritTransform) {
				_previousPositionX = _this.x;
				_previousPositionY = _this.y;
			} else {
				_previousPositionX = this._particleOrigin.x;
				_previousPositionY = this._particleOrigin.y;
			}

		};

		/*
		* Helper function that enables per-frame updating of this emitter
		*/
		_this._enableUpdates = function() {
			createjs.Ticker.addEventListener("tick", _updateTick);
		};

		/*
		* Helper function that disables per-frame updating of this emitter
		*/
		_this._disableUpdates = function() {
			createjs.Ticker.removeEventListener("tick", _updateTick);
		};

		/*
		* Remove the particle at the provided index
		* @param index:int - The index of the particle to be removed
		*/
		_this._removeParticleAt = function(index) {
			var remParticle = _this._particles[index];

			_particleContainer.removeChild(remParticle);
			_this._particles.splice(index, 1);

			// TODO: Return the particle object to the available pool

		};

		/*
		* Helper function that updates the origin point
		*/
		_this._updateParticleOrigin = function() {

			// Position the spawn shape according to the emitter's position
			_this._particleOrigin = _this.localToGlobal(0, 0);
			_this._particleOrigin = _particleContainer.globalToLocal(_this._particleOrigin.x, _this._particleOrigin.y);

			_previousPositionX = this._particleOrigin.x;
			_previousPositionY = this._particleOrigin.y;
		};

		/*
		* Handle an update tick
		* @tickEvent:[createjs.Event] - Event containing data for the tick
		*/
		function _updateTick (tickEvent) {
			_this._handleUpdate(tickEvent.delta / 1000.0);
		}

	};

	// Inherit from Container
	ss.CreateJSParticleEmitter.prototype = new createjs.Container();
	ss.CreateJSParticleEmitter.prototype.constructor = ss.CreateJSParticleEmitter;

} ());

/* global require, ss, createjs */




( function() {
	"use strict";

	/*
	* Class BitmapParticle extends createjs.Bitmap, AbstractParticle
	*	Manages the data and display of an individual particle
	*/

	/*
	* Create a new BitmapParticle
	* @param image:Image|HTMLCanvasElement|String - The source for the bitmap
	* @param regX:[Number] (Optional) - Registration point in the X
	* @param regY:[Number] (Optional) - Registration point in the Y
	*/
	ss.BitmapParticle = function(image, regX, regY) {
		// Extend createjs.Bitmap
		createjs.Bitmap.call(this, image);
		this.mouseEnabled = false;

		// Inherit additional properties from AbstractParticle
		ss.AbstractParticle.call(this);

		// Set registration point for this particle
		this.regX = regX !== undefined ? regX : 0;
		this.regY = regY !== undefined ? regY : 0;

		// Disable input on this particle
		this.mouseEnabled = false;
	};

	// Extend createjs.Bitmap
	ss.BitmapParticle.prototype = new createjs.Bitmap();
	ss.BitmapParticle.prototype.constructor = ss.BitmapParticle;

} ());

/* global require, ss, createjs */




( function() {
	"use strict";

	/*
	* Class ContainerParticle extends createjs.Container, AbstractParticle
	*	Manages the data and display of an individual particle
	*/

	/*
	* Create a new ContainerParticle
	* @param sourceContainer:createjs.Container - The object to clone for each particle
	* @param regX:[Number] (Optional) - Registration point in the X
	* @param regY:[Number] (Optional) - Registration point in the Y
	*/
	ss.ContainerParticle = function(sourceContainer, regX, regY) {
		// Extend createjs.Container
		createjs.Container.call(this);
		this.mouseEnabled = false;

		this.addChild(sourceContainer.clone(true));

		// Inherit additional properties from AbstractParticle
		ss.AbstractParticle.call(this);

		// Set registration point for this particle
		this.regX = regX !== undefined ? regX : 0;
		this.regY = regY !== undefined ? regY : 0;

		// Disable input on this particle
		this.mouseEnabled = false;
	};

	// Extend createjs.Container
	ss.ContainerParticle.prototype = new createjs.Container();
	ss.ContainerParticle.prototype.constructor = ss.ContainerParticle;

} ());

/* global require, ss, createjs */



( function() {
	"use strict";

	/*
	* Class SpriteParticle extends createjs.Sprite, AbstractParticle
	*	Manages the data and display of an individual sprite-based particle
	*/

	/*
	* Create a new SpriteParticle
	* @param spriteSheet:[SpriteSheet] - The spritesheet containing the images for thsi particle
	* @param animationName:[String] - Name of the animation to use to display this sprite
	*/
	ss.SpriteParticle = function(spriteSheet, animationName, regX, regY) {
		// Extend createjs.Sprite
		createjs.Sprite.call(this, spriteSheet, animationName);

		// Inherit additional properties from AbstractParticle
		ss.AbstractParticle.call(this);

		// Set registration point for this particle
		this.regX = regX !== undefined ? regX : 0;
		this.regY = regY !== undefined ? regY : 0;

		// Disable input on this particle
		this.mouseEnabled = false;
	};

	// Extend createjs.Bitmap
	ss.SpriteParticle.prototype = new createjs.Sprite();
	ss.SpriteParticle.prototype.constructor = ss.SpriteParticle;

} ());

/* global require, ss, console */





( function() {
	"use strict";

	/*
	* Class BitmapParticleFactory
	*	Handles the instantiation of new bitmap particles
	*/

	/*
	* Create a new BitmapParticleFactory
	* @param image:Image - The image to use for each particle
	*/
	ss.BitmapParticleFactory = function(image) {
		var _image = image;

		if (ss.isEmpty(_image)) {
			console.log("!! WARNING !! No valid image provided for bitmap particles!");
		}

		/*
		* Create a new particle
		* @return:AbstractParticle - The newly created particle
		*/
		this.createParticle = function() {
			return new ss.BitmapParticle(_image, _image.width / 2, _image.height / 2);
		};
	};

} () );

/* global require, ss, console */




( function() {
	"use strict";

	/*
	* Class ContainerParticleFactory
	*	Handles the instantiation of new bitmap particles
	*/

	/*
	* Create a new ContainerParticleFactory
	* @param sourceContainer:Container - The Container to clone for each particle (must not be empty)
	*/
	ss.ContainerParticleFactory = function(sourceContainer) {
		var _sourceContainer = sourceContainer;
		var _scBounds = _sourceContainer.getBounds() || _sourceContainer.nominalBounds || new ss.Rectangle(0, 0, 0, 0);

		if (_sourceContainer === null || _sourceContainer.getNumChildren() === 0) {
			console.log("!! WARNING !! No valid source provided for container particles!");
		}

		/*
		* Create a new particle
		* @return:AbstractParticle - The newly created particle
		*/
		this.createParticle = function() {
			return new ss.ContainerParticle(_sourceContainer, _scBounds.width / 2, _scBounds.height / 2);
		};
	};

} ());

/* global require, ss, console, createjs */





( function() {
	"use strict";

	/*
	* Class SpriteParticleFactory
	*	Handles the instantiation of new sprite-based particles
	*/

	/*
	* Create a new SpriteParticleFactory
	* @param spriteSheet:[String] - Spritesheet with images to use for the created particles
	* @param animationName:[String] - Name of the animation to use for created particles
	*/
	ss.SpriteParticleFactory = function(spriteSheet, animationName) {
		// [String] - Spritesheet with images to use for the created particles
		var _spriteSheet;
		// [String] - Name of the animation to use for created particles
		var _animName;

		// [createjs.Sprite] - Sprite used to obtain properties of the particles
		var _sprite;

		// [Number] - X coordinate of the particle registration point
		var _spriteRegX;
		// [Number] - Y coordinate of the particle registration point
		var _spriteRegY;

		/*
		* Initialize this SpriteParticleFactory
		* @param spriteSheet:[String] - Spritesheet with images to use for the created particles
		* @param animationName:[String] - Name of the animation to use for created particles
		*/
		function _construct (spriteSheet, animationName) {
			// Throw warning if no spritesheet was provided
			if (ss.isEmpty(spriteSheet)) {
				console.log("!! WARNING !! No valid spritesheet provided for SpriteParticleFactory!");
			}

			// Throw warning if no animation name was provided
			if (ss.isUndefined(animationName)) {
				console.log("!! WARNING !! No frame or animation name provided for SpriteParticleFactory");
			}

			_spriteSheet = spriteSheet;
			_animName = animationName;

			_sprite = new createjs.Sprite(spriteSheet, animationName);
			_spriteRegX = _sprite.getBounds().width / 2;
			_spriteRegY = _sprite.getBounds().height / 2;
		}

		/*
		* Create a new particle
		* @return:AbstractParticle - The newly created particle
		*/
		this.createParticle = function() {
			return new ss.SpriteParticle(_spriteSheet, _animName, _spriteRegX, _spriteRegY);
		};

		return _construct(spriteSheet, animationName);
	};

} ());
