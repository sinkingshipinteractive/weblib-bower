/*! weblib-ss 0.0.1 */
/* global require, ss */



( function() {
	"use strict";

	function Interval (low, high, data) {
		this.low = low;
		this.high = high;
		this.data = data;
	}

	Interval.prototype.constructor = Interval;

	ss.Interval = Interval;
} () );

/* global require, ss */



( function() {
	"use strict";

	function IntervalTreeNode (interval) {
		this.interval = interval;
		this.left = undefined;
		this.right = undefined;
		this.max = 0;
		this.min = Number.MAX_VALUE;
	}

	IntervalTreeNode.prototype.constructor = IntervalTreeNode;

	ss.IntervalTreeNode = IntervalTreeNode;

} () );

/* global require, ss */






( function() {
	"use strict";

	function IntervalTree () {
		this.root = undefined;
	}

	IntervalTree.prototype.constructor = IntervalTree;

	IntervalTree.prototype.insertList = function(intervalList) {
		var sorted = this.sortIntervalArray(intervalList);
		this.insertOrderedList(sorted);
	};

	IntervalTree.prototype.insertOrderedList = function(orderedIntervalList) {

		if (orderedIntervalList.length <= 2) {
			for(var i = 0; i < orderedIntervalList.length; i++) {
				this.insert(orderedIntervalList[i]);
			}
			return;
		}
		var middleIndex = Math.floor(orderedIntervalList.length / 2);
		var middle = orderedIntervalList[middleIndex];
		this.insert(middle);

		var left = orderedIntervalList.slice(0, middleIndex);
		var right = orderedIntervalList.slice(middleIndex + 1);

		this.insertOrderedList(left);
		this.insertOrderedList(right);
	};

	IntervalTree.prototype.insert = function(newInterval) {
		if (ss.isEmpty(this.root)) {
			this.root = this.insertRecursive(this.root, newInterval);
		} else {
			this.insertRecursive(this.root, newInterval);
		}
	};

	IntervalTree.prototype.insertRecursive = function(currentRoot, newInterval) {
		if (ss.isEmpty(currentRoot)) {
			var leaf = new ss.IntervalTreeNode(newInterval);
			leaf.max = newInterval.high;
			leaf.min = newInterval.low;
			return leaf;
		}

		var rootLow = currentRoot.interval.low;

		if (newInterval.low < rootLow) {
			currentRoot.left = this.insertRecursive(currentRoot.left, newInterval);
		} else {
			currentRoot.right = this.insertRecursive(currentRoot.right, newInterval);
		}

		if (currentRoot.max < newInterval.high) {
			currentRoot.max = newInterval.high;
		}

		if (currentRoot.min > newInterval.low) {
			currentRoot.min = newInterval.low;
		}

		return currentRoot;
	};

	IntervalTree.prototype.doIntervalsOverlap = function(i1, i2) {
		return i1.low <= i2.high && i2.low <= i1.high;
	};

	IntervalTree.prototype.compareIntervals = function(i1, i2) {
		if (i1.low < i2.low) {
			return -1;
		}
		if (i1.low > i2.low) {
			return 1;
		}
		return 0;
	};

	IntervalTree.prototype.sortIntervalArray = function(inputArray) {
		return inputArray.sort(this.compareIntervals);
	};

	IntervalTree.prototype.overlapSearchRecursive = function(currentRoot, interval) {
		if (ss.isEmpty(currentRoot)) {
			return [];
		}

		var returnList = [];

		if (this.doIntervalsOverlap(currentRoot.interval, interval)) {
			returnList.push(currentRoot.interval);
		}

		if (!ss.isEmpty(currentRoot.left) && currentRoot.left.max >= interval.low) {
			returnList = returnList.concat(this.overlapSearchRecursive(currentRoot.left, interval));
		}

		if (!ss.isEmpty(currentRoot.right) && currentRoot.right.min <= interval.high) {
			returnList = returnList.concat(this.overlapSearchRecursive(currentRoot.right, interval));
		}
		return returnList;
	};

	IntervalTree.prototype.firstOverlapSerachRecursive = function(currentRoot, interval) {
		if (ss.isEmpty(currentRoot)) {
			return undefined;
		}

		if (this.doIntervalsOverlap(currentRoot.interval, interval)) {
			return currentRoot.interval;
		}

		if (!ss.isEmpty(currentRoot.left) && currentRoot.left.max >= interval.low) {
			return this.firstOverlapSerachRecursive(currentRoot.left, interval);
		}

		return this.firstOverlapSerachRecursive(currentRoot.right, interval);
	};

	IntervalTree.prototype.getOverlapWithInterval = function(interval) {
		return this.overlapSearchRecursive(this.root, interval);
	};

	IntervalTree.prototype.getOverlapWithPosition = function(number) {
		return this.overlapSearchRecursive(this.root, new ss.Interval(number, number));
	};

	IntervalTree.prototype.getFirstOverlapWithInterval = function(interval) {
		return this.firstOverlapSerachRecursive(this.root, interval);
	};

	IntervalTree.prototype.getFirstOverlapWithPosition = function(number) {
		return this.firstOverlapSerachRecursive(this.root, new ss.Interval(number, number));
	};

	IntervalTree.prototype.inOrder = function() {
		return this.inOrderRecursive(this.root);
	};

	IntervalTree.prototype.inOrderRecursive = function(root) {

		if (ss.isEmpty(root)) {
			return [];
		}

		var returnList = [];

		returnList = returnList.concat(this.inOrderRecursive(root.left));
		returnList.push(root);
		returnList = returnList.concat(this.inOrderRecursive(root.right));

		return returnList;
	};

	ss.IntervalTree = IntervalTree;

} () );

/* global ss, require */



( function() {
	"use strict";

	/*
	* Utility functions for working with collection.
	*/
	ss.CollectionUtils = ss.CollectionUtils || new function() { // jshint ignore:line
		var _this = this;

		/*
		* Get a random object in an array.
		* @param arr: [Array] the array to get a random element from.
		*/
		_this.getRandomObjectInArray = function(arr) {
			return arr[Math.floor(Math.random() * arr.length)];
		};
	}; // jshint ignore:line

} () );

/* global require, ss */




/*
*
* Class Iterator
*   Allows the traversal of an array of values
*/

( function() {
    "use strict";

    function Iterator ( array, index ) {

        var _this = this;
        var _index;
        var _array;

        var _construct = function( array, index ) {
          
            if ( !ss.isEmpty ( array ) ) {
                _this.init ( array, index );
            }
            

            return _this;
        };

            /*
            * private members
            */
        var _shuffleOrder = function() {
            return ( Math.round ( Math.random () ) - 0.5 );
        };

        _this.init = function ( array, index ) {

            _array = array.slice ();
            _index = ( ss.isEmpty ( index ) ) ? -1 : index;
        };

            /*
            * public members
            */
        _this.length = function() {
            return _array.length;
        };

        _this.shuffle = function() {
            _array = _array.sort ( _shuffleOrder );

            return _this;
        };

        _this.reverse = function() {
            _array = _array.reverse ();
            if ( _index > 0 ) {
                _index = ( _array.length - 1 ) - _index;
            }

             return _this;
        };

        _this.reset = function() {
            _index = -1;

             return _this;
        };

        _this.clone = function() {
            return new Iterator ( _array.slice ( 0 ) ) ;
        };

        _this.hasNext = function() {
            return ( ( _index + 1 ) < _array.length );
        };

        _this.next = function() {
            return _array [ ++_index ];
        };

        _this.peek = function( offset ) {

            if ( ss.isEmpty ( offset ) ) {
                 offset = 0;
            }

                // allow
            return _array [ Math.max ( 0, Math.min ( _index + offset, ( _array.length - 1 ) ) ) ];
        };

        _this.hasPrevious = function() {
            return ( _index > 0 );
        };

        _this.previous = function() {
            return _array [ --_index ];
        };

        _this.toArray = function() {
            return _array.slice ( 0 );
        };

        _this.release = function() {
            _index = undefined;
            _array = undefined;
            _this = undefined;
            _construct = undefined;
            _shuffleOrder = undefined;

                // clear out the public members
            for( var name in _this ) {
                delete _this [ name ];
            }
        };

        _this.reclaim = function () {

        };

        return _construct ( array, index );
    }

    ss.Iterator = Iterator;
    window.Iterator = Iterator; // jshint ignore:line

} ( ss ) );

/* global require, ss */



(function() {
	"use strict";

	ShuffledRandomizer.prototype = {};
	ShuffledRandomizer.prototype.constructor = ShuffledRandomizer;

	/**
	 * Randomizes a group of objects by shuffling them.
	 * @param {Array} optionsList - The list of options to randomize.
	 */
	function ShuffledRandomizer (optionsList) {
		// {ss.ShuffleRandomizer} Keep a locally scoped copy of this.
		var _this = this;

		// The list of options.
		var _optionsList = optionsList.slice();

		// the current remaining shuffled list.
		var _currentList = [];

		/**
		 * Shuffle the list of objects and stick them on the shuffled list.
		 */
		this.shuffle = function() {
			var toShuffle = _optionsList.slice();

			_currentList = [];

			while(toShuffle.length > 0) {
				var randomIndex = Math.floor( Math.random() * toShuffle.length);

				_currentList.push(toShuffle[randomIndex]);
				toShuffle.splice(randomIndex, 1);
			}
		};

		/**
		 * Get the next item in the list.
		 * @return {Object} The next item in the list.
		 */
		this.getNextItem = function() {
			if (_currentList.length < 1) {
				_this.shuffle();
			}
			return _currentList.pop();
		};

		/**
		 * Get the next item in the list. After the last item, the list doens't get repopulated, like in the getNextItem function.
		 * @return {Object} The next item in the list.
		 */
		this.getNextItemNotLoop = function() {
			if (_currentList.length >= 1) {
				return _currentList.pop();
			}else {
				return undefined;
			}
		};

		/**
		 * Returns the ammount of items still in the _currentList
		 * @return {Number} Ammount of items
		 */
		this.getAmmountRemainingItems = function () {
			return _currentList.length;
		};

		/**
		 * Release this instance.
		 */
		this.release = function() {
			_currentList = undefined;
			_optionsList = undefined;
		};
	}

	ss.ShuffledRandomizer = ShuffledRandomizer;
}());

/* global require, ss */




( function() {
	"use strict";

	function TreeNode ( children, data ) {

		var _this = ss.AbstractEventDispatcher ( this );

		var _parent;
		var _data;
		var _children;

		function _construct ( children, data ) {
			_data = data;
			_children = ss.isEmpty ( children ) ? [] : children;

			_this.showChildren = _children;

			return _this;
		}

	    _this.parent = function( val ) {
	        return ( ss.isEmpty ( val ) ) ? _parent : _parent = val;
	    };

	    _this.children = function( array ) {
	    	if ( array !== true ) {
	    		return new ss.Iterator ( _children );
	    	}
	        return _children.slice ();
	    };

	    _this.siblings = function() {
	        return ss.TreeNode.getSiblings ( _this );
	    };

	    _this.isRoot = function() {
	        return ss.isUndefined ( _parent );
	    };

	    _this.hasChildren = function() {
	        return ( _children.length > 0 );
	    };

	    _this.hasSiblings = function() {
	        if ( _parent ) {
				return _parent.numChildren () > 1;
	        }

			return false;
	    };

	    _this.numChildren = function() {
	        return _children.length;
	    };

	    _this.numSiblings = function() {
	        if ( _parent ) {
				return _parent.numChildren ();
			}
			return 0;
	    };

	    _this.depth = function() {
	        var depthCount = 0;
			for( var i = 0; i < _children.length; i++ ) {
	            var node = _children [ i ];
				depthCount += ( node instanceof ss.TreeNode ) ? node.depth () : 1;
			}

			return depthCount;
	    };

		_this.contains = function( obj ) {
			for( var i = 0; i < _children; i++ ) {
	            var node = _children [ i ];
				if ( node instanceof ss.TreeNode ) {
					if ( node.contains ( obj ) ) {
						return true;
					}
				} else if ( node == obj ) {
					return true;
				}
			}

			return false;
		};

	    _this.clear = function() {
	        while( _children.length ) {
	            var item = _children.shift ();

	            if ( item instanceof ss.TreeNode ) {
	                item.clear ();
	            }
	        }
	    };

	    _this.addChildren = function( itemSet ) {
	        _children = _children.concat ( itemSet );
	    };

	    _this.addChild = function( child ) {

	    		// assign this as a parent only if
	    	if ( child instanceof TreeNode ) {
	    		child.parent ( _this );
	    	}

	            // add to the list
	        _children.push ( child );
	    };

	    _this.removeChild = function( item ) {

	        for( var i = 0; i < _children.length (); i++ ) {
	            if ( item == _children [ i ] ) {
	                _children.splice ( i, 1 );
	                return;
	            }
	        }

	        throw item + " does not exist as an child";
	    };

	    return _construct ( children, data );
	}

	/* TreeNode.setParent = function ( parent, child ) {

		child.parent  = parent;
	} */

	TreeNode.getSiblings = function( treeNode ) {

		var parent = treeNode.parent ();

		if ( ss.isEmpty ( parent ) ) {
			return new ss.Iterator ( [] );
		}

			// grab children and remove the current treenode
		var siblingSet = parent.children ( true );
		siblingSet = siblingSet.splice ( siblingSet.indexOf ( treeNode ), 1 );

		return new ss.Iterator ( siblingSet );
	};

		// assign to namespace
	ss.TreeNode = TreeNode;

} () );

/* global require, ss */


( function( ) {
	"use strict";

	    // internal function definitions
	function CachedTraversal ( parentNode ) {
		this.parentNode = parentNode;
		this.iIterator = parentNode.children ();
	}

	function TreeNodeIterator ( treeNode ) {

		var _this = this;
		var _cachedTraversalSet; // : Array;

		var _rootNode;
		var _recursionPreventionSet;

		function _construct ( treeNode) {

			_recursionPreventionSet = [];

			_rootNode = treeNode;

					// reset the whole tree
			_this.reset ( _this );
		}

	        // private functions

		/*function _parentNode ( ) {
			return _cachedTraversalSet [ ( _cachedTraversalSet.length - 1 ) ].parentNode;
		}*/

		function _iIterator ( ) {
			return _cachedTraversalSet [ ( _cachedTraversalSet.length - 1 ) ].iIterator;
		}

		function _getAllIndexes ( arr, val ) {
		    var indexes = [], i = -1;
		    while((i = arr.indexOf(val, i + 1)) != -1) {
		        indexes.push(i);
		    }
		    return indexes;
		}

			// public functions
		_this.reset = function( ) {

			_recursionPreventionSet = [];

			var cachedTraversal = new CachedTraversal ( _rootNode ) ;
			_cachedTraversalSet = [ cachedTraversal ] ;
		};

		_this.canTraverse = function() {
			for( var i = 0; i < _cachedTraversalSet.length; i++ ) {
				var cachedTraversal = _cachedTraversalSet [ i ];
				if ( cachedTraversal.iIterator.hasNext ( ) ) {
					return true;
				}
			}

				// found nothing can traverse
			return false;
		};

		_this.traverse = function() {

			var treeNode; // : TreeNode;
			// var iIterator; // : IIterator;

			if ( _iIterator ().hasNext () ) {
				treeNode = _iIterator ().next ();

	        	if ( treeNode.hasChildren () && _getAllIndexes ( _recursionPreventionSet, treeNode ).length < 2 ) { // _recursionPreventionSet.indexOf ( treeNode ) == -1 ) {
					var cachedTraversal = new CachedTraversal ( treeNode ) ;
					_cachedTraversalSet.push ( cachedTraversal ) ;
					_recursionPreventionSet.push ( treeNode );
				}
			} else {

				while( _cachedTraversalSet.length && !_iIterator ().hasNext () ) {
					_cachedTraversalSet.pop () ;
				}

				treeNode = _iIterator ().next () ;
			}

			return treeNode;
		};

	    _this.find = function( property , value ) {
	        var treeNodeIterator = new ss.TreeNodeIterator ( _rootNode ) ;

	        while( treeNodeIterator.canTraverse () ) {

	            var treeNode = treeNodeIterator.traverse () ;

	            if ( treeNode.data [ property ] == value ) {
	                return treeNode;
	            }
	        }

	        return null;
	    };

	    this.traverseAll = function() {

	    	_recursionPreventionSet = [];
	        var traversalSet = [];

	        while( _this.canTraverse () ) {
	            traversalSet.push ( _this.traverse () );
	        }

	        return new ss.Iterator ( traversalSet );
	    };

	    return _construct ( treeNode );
	}

	ss.TreeNodeIterator = TreeNodeIterator;

} () );

/* global require, ss, $ */



( function( ) {
	"use strict";

	function TreeNodeParser () {

	    var _instance = ss.AbstractEventDispatcher ( this );
	    var _rootNode;

	    function _recurseChildrenJSON ( json ) {
			return new ss.TreeNode ( _extractChildrenJSON ( json ), _extractRawDataJSON ( json ) );
		}

	    function _extractRawDataJSON ( json ) {

			var cloneObject = {};

			for( var property in json ) {
				if ( String ( property ).toLowerCase () != "children" ) {
					cloneObject [ property ] = json [ property ];
				}
			}

			return cloneObject;
		}

		function _extractChildrenJSON ( json ) {

			var resultSet = [];

	        if ( typeof ( json.children ) !== String ( undefined ) ) {
	            for( var i = 0; i < json.children.length; i++ ) {
	                var item = json.children [ i ];
	                var childNode = _recurseChildrenJSON ( item );
	                resultSet.push ( childNode );
	            }
	        }

			return resultSet;
		}

		function _recurseChildrenXML ( $xml ) {
			return new ss.TreeNode ( _extractChildrenXML ( $xml ), _extractRawDataXML ( $xml ) );
		}

		function _extractChildrenXML ( /*$xml*/ ) {
			return [];
		}

		function _extractRawDataXML ( $xml ) {

			// check if the node has children
			if ( $xml.children ( ":not(children)" ).length ) {

				var rawData = {};

				$ ( $xml.children ( ":not(children)" ) ).each ( function( index, value ) { // jshint ignore:line

						// save the node name for ease
					var nodeName = $ ( this ).get ( 0 ).nodeName;

						// if an entry exist make it an array otherwise assign
					if ( ss.isUndefined ( rawData [ nodeName ] ) ) {
						rawData [ nodeName ] = _extractRawDataXML ( $ ( this ) );
					} else {
						try {
								// assign another reference
							rawData [ nodeName ].push ( _extractRawDataXML ( $ ( this ) ) );
						} catch ( e ) {
								// concatenate the values
							rawData [ nodeName ] = [ rawData [ nodeName ], _extractRawDataXML ( $ ( this ) ) ];
						}
					}

				} );

				return rawData;
			}

				// just a text node return the value
			return $xml.text ();
		}

	    _instance.parseJSON = function( jsonParsed ) {
				// recurse through the object and build the tree structure
			_rootNode = _recurseChildrenJSON ( jsonParsed );
	        return _rootNode;
		};

		_instance.parseXML = function( $xml ) {
				// recurse through the object and build the tree structure

			_rootNode = _recurseChildrenXML ( $xml );

	        return _rootNode;
		};

	    return _instance;
	}

	ss.TreeNodeParser = TreeNodeParser;
} () );

/* global require, ss */




(function () {
	"use strict";

	ObjectSet.prototype = {};
	ObjectSet.prototype.constructor = ObjectSet;

	/**
	 * Represents the set data type
	 */
	function ObjectSet() {

		// keep a locally scoped copy of this.
		var _this = this;

		/**
		 * The object itself that holds the set.
		 */
		var _setObj;

		/**
		 * The number of elements in the set.
		 * @type {Number} - The number of elements in the set.
		 */
		var _numElements = 0;

		/**
		 * Construct this instance.
		 */
		var _construct = function () {
			_setObj = {};

			return _this;
		};

		/**
		 * Add an element to the set.
		 * @param {String} value - The value to save in the set.
		 */
		_this.add = function(value) {
			if(ss.isEmpty(_setObj[value])) {
				_setObj[value] = true;
				_numElements++;
			}
		};

		/**
		 * Remove an element from the set.
		 * @param  {String} value - The value to save in the set.
		 */
		_this.remove = function (value) {
			if(!ss.isEmpty(_setObj[value])) {
				delete _setObj[value];
				_numElements--;
			}
		};

		/**
		 * Whether or not the set contains a given value.
		 * @param  {String}  value - The value to check.
		 * @return {Boolean}       - True if the value is in the set, false otherwise.
		 */
		_this.has = function(value) {
			return !ss.isEmpty(_setObj[value]);
		};

		/**
		 * The number of elements in the set.
		 * @return {Number} - The number of items in the set.
		 */
		_this.size = function() {
			return _numElements;
		};

		/**
		 * Clear the items in the set.
		 */
		_this.clear = function () {
			_numElements = 0;
			_setObj = {};
		};

		/**
		 * Get a list of the items in the set.
		 * @return {Array[String]} - The items in the set.
		 */
		_this.keys = function() {
			return Object.keys(_setObj);
		};

		/**
		 * Release the memory used by this instance.
		 */
		_this.release = function () {
			_numElements = undefined;
			_setObj = undefined;
			_this = undefined;
		};

		return _construct();
	}

	ss.ObjectSet = ObjectSet;

}());

