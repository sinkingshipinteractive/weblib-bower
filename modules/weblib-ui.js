/*! weblib-ss 0.0.1 */
/* global require, ss */



(function() {
	"use strict";

	/*
	* Abstract class for game objects that display percents.
	*/
	function AbstractPercentDisplay (sublassReference) {

		var _this = sublassReference;

		function _construct () {
			return _this;
		}

		_this.displayPercent = function(percent) { // jshint ignore:line
		};

		return _construct();
	}

	ss.AbstractPercentDisplay = AbstractPercentDisplay;
} ());

/* global require, createjs, ss */





( function() {
	"use strict";

	function ImageProgressIndicator (bgImage, foregroundImage, offset) {

		var _this = ss.AbstractPercentDisplay(this);

		// the fill of the bar
		var _foregroundImage;

		// the background image of the loading bar
		var _backgroundImage;

		// this offsets the foreground image on its x axis
		var xOffset;

		function _construct (bgImage, foregroundImage, offset) {
			_this.initialize();

			if (ss.isUndefined(offset)) {
				xOffset = 0;
			}else {
				xOffset = offset;
			}

			_backgroundImage = new ss.CreateJSAssetManager.getBitmap(bgImage);
			_this.addChild(_backgroundImage);
			_backgroundImage.regX = _backgroundImage.getBounds().width / 2;
			_backgroundImage.regY = _backgroundImage.getBounds().height / 2;

			_foregroundImage = new ss.CreateJSAssetManager.getBitmap(foregroundImage);
			_this.addChild(_foregroundImage);
			_foregroundImage.regX = xOffset;
			_foregroundImage.regY = _foregroundImage.getBounds().height / 2;
			_foregroundImage.x = -((_backgroundImage.getBounds().width / 2) - xOffset);

			_this.displayPercent(0.01);

			return _this;
		}

		_this.setXOffset = function(offset) {
			xOffset = offset;
		};

		_this.displayPercent = function(percent) {
			percent = ss.MathUtils.clamp(percent, 0, 1);
			_foregroundImage.scaleX = percent;
			// _foregroundImage.x = _foregroundImage.getBounds().width * (1 - percent);
		};

		_this.release = function() {
			_this.removeChild(_backgroundImage);
			_backgroundImage = undefined;

			_this.removeChild(_foregroundImage);
			_foregroundImage = undefined;

			_this = undefined;
		};

		return _construct(bgImage, foregroundImage, offset);
	}

	ImageProgressIndicator.prototype = new createjs.Container();

	ss.ImageProgressIndicator = ImageProgressIndicator;

} ());

/* global require, createjs, ss */





( function() {
	"use strict";

	function TextProgressIndicator (colour) {

		var _this = ss.AbstractPercentDisplay(this);

		var _textDisplay;

		function _construct (colour, font) {

			if (ss.isEmpty(font)) {
				font = "60px refrigerator-deluxe";
			}

			if (ss.isEmpty(colour)) {
				colour = "#FFFFFF";
			}
			_this.initialize();

			_textDisplay = new createjs.Text("", font, colour);
			_textDisplay.textAlign = "center";
			_this.addChild(_textDisplay);

			_this.displayPercent(0);

			return _this;
		}

		_this.displayPercent = function(percent) {
			percent = ss.MathUtils.clamp(percent, 0, 1);

			_textDisplay.text = (percent * 100).toFixed(0) + "%";
		};

		return _construct(colour);
	}

	TextProgressIndicator.prototype = new createjs.Container();

	ss.TextProgressIndicator = TextProgressIndicator;

} ());

/* global require, createjs, ss */


( function (){
    "use strict";

    CreateJSSwipeDetection.prototype.constructor = CreateJSSwipeDetection;

    /**
     * 'static' singleton Class SwipeDetection
     *
     * 
     */
    function CreateJSSwipeDetection() {

        var _this =  this;
        var _monitoredObjects = [];

        _this.SWIPE_BEGIN = "SWIPE_BEGIN";
        _this.SWIPE_COMPLETE = "SWIPE_COMPLETE";
        _this.SWIPE_PARTIAL = "SWIPE_PARTIAL";

        var _inSwipe = false;


        var _construct = function(){

            if(typeof CreateJSSwipeDetection.instance == "object") {
                return CreateJSSwipeDetection.instance;
            } 

            CreateJSSwipeDetection.instance = _this;
            return _this;
        };

        /**
         * Monitor the indicated DisplayObject for mouse events and report any swipes
         * ----------------------------------------------------------------------------
         * @param  {createjs.DisplayObject} -- the object to monitor
         * @return {void}
         */
        _this.monitor = function(displayObj){
            displayObj.addEventListener("mousedown",_onBegin);
            displayObj.addEventListener("pressmove",_onMove);
            displayObj.addEventListener("pressup",_onRelease);
            _monitoredObjects[displayObj] = {};
        };

        /**
         * Stop reporting swipes on the indicated DisplayObject.
         * -----------------------------------------------------------
         * @param  {createjs.DisplayObject}
         * @return {void}
         */
        _this.stopMonitoring = function(displayObj){
            displayObj.removeEventListener("mousedown",_onBegin);
            displayObj.removeEventListener("pressmove",_onMove);
            displayObj.removeEventListener("pressup",_onRelease);
            delete _monitoredObjects[displayObj];
        };

        var _stopMonitoringAll = function(){
            for(var i=0; i<_monitoredObjects.length; i++){
                _this.stopMonitoring(_monitoredObjects[i]);
            }
        };

        /**
         * User has touched a monitored object. Record position and report.
         * ---------------------------------------------------------------------- 
         * @param  {MouseEvent} evt
         * @return {void}
         */
        var _onBegin = function(evt){  
            console.log("beginning press for "+evt.currentTarget);
            _monitoredObjects[evt.currentTarget].startPos = new ss.Vector2(evt.stageX,evt.stageY);
            _announcePress(evt.currentTarget);
        };

        /**
         * Swipe in-progress. Measure swipe vector so far and report.
         * -----------------------------------------------------------------
         * @param  {MouseEvent} evt 
         * @return {void}
         */
        var _onMove = function(evt){
            _inSwipe = true;
            var newPos = new ss.Vector2(evt.stageX,evt.stageY);
            if(_monitoredObjects[evt.currentTarget].lastPos === undefined){
                _monitoredObjects[evt.currentTarget].lastPos = _monitoredObjects[evt.currentTarget].startPos;
            }
            _monitoredObjects[evt.currentTarget].partialSwipeV = newPos.subtract(_monitoredObjects[evt.currentTarget].lastPos);
            _monitoredObjects[evt.currentTarget].lastPos = newPos;

            _announceMove(evt.currentTarget);
        };

        /**
         * Swipe complete. Measure swipe vector and report.
         * ------------------------------------------------------
         * @param  {MouseEvent} evt 
         * @return {void}     
         */
        var _onRelease = function(evt){
            _inSwipe = false;
            var finalPosition = new ss.Vector2(evt.stageX,evt.stageY);
            _monitoredObjects[evt.currentTarget].fullSwipeV = finalPosition.subtract(_monitoredObjects[evt.currentTarget].startPos);
            _announceSwipe(evt.currentTarget);
        };

        /**
         * Dispatch an event announcing that a swipe has begun on the target object
         * (.currentTarget is the object being monitored)
         * ---------------------------------------------------------------------------------
         * @param  {createjs.DisplayObject} displayObj 
         * @return {void}
         */
        var _announcePress = function(displayObj){
            var swipeBeginEvent = new createjs.Event(_this.SWIPE_BEGIN,true);
            displayObj.dispatchEvent(swipeBeginEvent);
        };

        /**
         * Dispatch an event containing .swipeVector representing a swipe in progress
         * (and .currentTarget is the object being monitored)
         * -------------------------------------------------------------------------------------
         * @param  {createjs.DisplayObject} displayObj 
         * @return {void}
         */
        var _announceMove = function(displayObj){
            var swipeMoveEvent = new createjs.Event(_this.SWIPE_PARTIAL,true);
            swipeMoveEvent.swipeVector = _monitoredObjects[displayObj].partialSwipeV;
            displayObj.dispatchEvent(swipeMoveEvent);
        };

        /**
         * Dispatch an event containing .swipeVector representing a completed swipe
         * (and .currentTarget is the object being monitored)
         * -------------------------------------------------------------------------------------
         * @param  {createjs.DisplayObject} displayObj 
         * @return {void}
         */
        var _announceSwipe = function(displayObj){
            var swipeCompleteEvent = new createjs.Event(_this.SWIPE_COMPLETE,true);
            swipeCompleteEvent.swipeVector = _monitoredObjects[displayObj].fullSwipeV;
            displayObj.dispatchEvent(swipeCompleteEvent);
        };

        /**
         * Stop monitoring on all objects and reset to default values.
         * ---------------------------------------------------------------
         * @return {void}
         */
        _this.release = function(){
            _stopMonitoringAll();
            _inSwipe = false;
        };

        return _construct();
    }

    ss.CreateJSSwipeDetection =  new CreateJSSwipeDetection();

}());

/* global createjs, ss */

(function() {
	"use strict";

	Scale9.prototype = new createjs.Container();
	Scale9.prototype.constructor = Scale9;

	/**
	 * [IsometricGrid description]
	 * @param {Json} image    The image to use the 9-scaling with
	 * @param {[type]} x      Left edge of the scale 9 grid rectangle
	 * @param {[type]} y      The top edge of the scale 9 grid rectangle
	 * @param {[type]} width  Width of the scale 9 rectangle
	 * @param {[type]} height Height of the scale 9 grid rectangle
	 */
	function Scale9 (x, y, width, height) {
		// call the parent container constructor.
		createjs.Container.call(this);

		// keep a locally scoped copy of this.
		var _this = this;

		var _width = width;

		var _height = height;

		var _rectX = x;

		var _rectY = y;

		var _leftTopCorner;
		var _rightTopCorner;
		var _leftBottomCorner;
		var _rightBottomCorner;
		var _topMiddle;
		var _leftVertical;
		var _rightVertical;
		var _bottomMiddle;
		var _background;

		_this.start = function(x, y, width, height) {

			_width = width;

			_height = height;

			_rectX = x;

			_rectY = y;

			draw();
		};

		_this.setRect = function(newX, newY, newWidth, newHeight) {
			_rectX = newX;
			_rectY = newY;
			_width = newWidth;
			_height = newHeight;
		};

		_this.getX = function() {
			return _rectX;
		};

		_this.getY = function() {
			return _rectY;
		};

		_this.getRect = function() {};

		_this.resize = function(newWidth, newHeight) {};

		_this.scale = function(newScaleX, newScaleY) {};

		_this.release = function() {

			_this.removeChild(_leftTopCorner);
			_leftTopCorner = undefined;

			_this.removeChild(_rightTopCorner);
			_rightTopCorner = undefined;

			_this.removeChild(_leftBottomCorner);
			_leftBottomCorner = undefined;

			_this.removeChild(_rightBottomCorner);
			_rightBottomCorner = undefined;

			_this.removeChild(_topMiddle);
			_topMiddle = undefined;

			_this.removeChild(_leftVertical);
			_leftVertical = undefined;

			_this.removeChild(_rightVertical);
			_rightVertical = undefined;

			_this.removeChild(_bottomMiddle);
			_bottomMiddle = undefined;

			_this.removeChild(_background);
			_background = undefined;

			_this = undefined;
		};

		_this.setLeftTopCorner = function(newImgae) {
			_leftTopCorner = newImgae;
		};

		_this.setRightTopCorner = function(newImgae) {
			_rightTopCorner = newImgae;
		};

		_this.setLeftBottomCorner = function(newImgae) {
			_leftBottomCorner = newImgae;
		};

		_this.setRightBottomCorner = function(newImgae) {
			_rightBottomCorner = newImgae;
		};

		_this.setTopMiddle = function(newImgae) {
			_topMiddle = newImgae;
		};

		function draw () {

			var horizontalDistance;
			var verticalDistance;

			// top section
			_leftTopCorner = ss.CreateJSAssetManager.getBitmap("topLeft");
			_this.addChild(_leftTopCorner);
			_leftTopCorner.x = _rectX;
			_leftTopCorner.y = _rectY;

			_rightTopCorner = ss.CreateJSAssetManager.getBitmap("topRight");

			horizontalDistance = _width - _leftTopCorner.getBounds().width - _rightTopCorner.getBounds().width;

			var temp = ss.CreateJSAssetManager.getBitmap("topMiddle");
			_topMiddle = new createjs.Shape();
			_topMiddle.graphics.beginBitmapFill(temp.image).drawRect((_rectX + _leftTopCorner.getBounds().width),
					_rectY, horizontalDistance, temp.getBounds().height);
			_this.addChild(_topMiddle);

			_this.addChild(_rightTopCorner);
			_rightTopCorner.x = _rectX + _leftTopCorner.getBounds().width + horizontalDistance;
			_rightTopCorner.y = _rectY;

			// vertical section
			_leftBottomCorner = ss.CreateJSAssetManager.getBitmap("bottomLeft");

			verticalDistance = _height - _leftTopCorner.getBounds().height - _leftBottomCorner.getBounds().height;

			var leftVert = ss.CreateJSAssetManager.getBitmap("leftMiddle");
			_leftVertical = new createjs.Shape();

			// _this.addChild(_leftVertical);
			_leftVertical.graphics.beginBitmapFill(leftVert.image).drawRect(_rectX,
				_rectY + _leftTopCorner.getBounds().height, leftVert.getBounds().width, verticalDistance);
			/*_this.addChild(leftVert);
			leftVert.x = _rectX;
			leftVert.y = _rectY + _leftTopCorner.getBounds().height;*/

			var rightVertImage = ss.CreateJSAssetManager.getBitmap("rightMiddle");
			_rightVertical = new createjs.Shape();
			_rightVertical.graphics.beginBitmapFill(rightVertImage.image).drawRect(_rectX + _leftTopCorner.getBounds().width + horizontalDistance, _rectY +
				 _leftTopCorner.getBounds().height, rightVertImage.getBounds().width, verticalDistance);
			_this.addChild(_rightVertical);

			// bottom section
			_this.addChild(_leftBottomCorner);
			_leftBottomCorner.x = _rectX;
			_leftBottomCorner.y = _rectY + verticalDistance + _leftTopCorner.getBounds().height;

			var btmMiddleImage =  ss.CreateJSAssetManager.getBitmap("bottomMiddle");
			_bottomMiddle = new createjs.Shape();
			_bottomMiddle.graphics.beginBitmapFill (btmMiddleImage.image, "repeat", new createjs.Matrix2D().identity()).drawRect((_rectX + _leftBottomCorner.getBounds().width),
					_rectY + verticalDistance +	_leftBottomCorner.getBounds().height, horizontalDistance, btmMiddleImage.getBounds().height);
			_this.addChild(_bottomMiddle);

			_rightBottomCorner = ss.CreateJSAssetManager.getBitmap("bottomRight");
			_this.addChild(_rightBottomCorner);
			_rightBottomCorner.x = _rectX + _leftTopCorner.getBounds().width + horizontalDistance;
			_rightBottomCorner.y = _rectY + verticalDistance + _leftTopCorner.getBounds().height;

			// background image
			var backgroundImage = ss.CreateJSAssetManager.getBitmap("middle");
			_background = new createjs.Shape();
			_background.graphics.beginBitmapFill(backgroundImage.image).drawRect(_rectX + _leftTopCorner.getBounds().width, _rectY +
				 _leftTopCorner.getBounds().height, horizontalDistance, verticalDistance);
			_this.addChild(_background);

		}

		return _this.start(x, y, width, height);
	}

	ss.Scale9 = Scale9;

}());

/* global require, createjs, ss */



(function() {
	"use strict";

	SimpleTextButton.prototype = new createjs.Container();
	SimpleTextButton.prototype.constructor = SimpleTextButton;

	/**
	 *
	 */
	function SimpleTextButton () {
		// call the parent container constructor.
		createjs.Container.call(this);

		// keep a locally scoped copy of this.
		var _this = this;

		var _shape;
		var _text;

		var _bgColour;
		var _selectedBackgroundColour;
		var _strokeWidth;
		var _strokeColour;
		var _width;
		var _height;

		var _clickListener;
		var _downListener;
		var _upListener;


		/**
		 * Start this instance.
		 */
		_this.start = function(text, width, height, backgroundColour, textColour, strokeColour, selectedBackgroundColour, font, strokeWidth) {
			if (ss.isEmpty(width)) {
				width = 200;
				_width = width;
			}

			if (ss.isEmpty(height)) {
				height = 50;
				_height = height;
			}

			if (ss.isEmpty(text)) {
				text = "";
			}

			if (ss.isEmpty(backgroundColour)) {
				backgroundColour = "#FFFFFF";
				_bgColour = backgroundColour;
			}

			if (ss.isEmpty(strokeColour)) {
				strokeColour = "#000000";
				_strokeColour = strokeColour;
			}

			if (ss.isEmpty(selectedBackgroundColour)) {
				selectedBackgroundColour = "#AAAAAA";
				_selectedBackgroundColour = selectedBackgroundColour;
			}

			if (ss.isEmpty(font)) {
				font = "30px Arial";
			}

			if (ss.isEmpty(strokeWidth)) {
				strokeWidth = 2;
				_strokeWidth = strokeWidth;
			}

			if (ss.isEmpty(textColour)) {
				textColour = "#000000";
			}

			_shape = new createjs.Shape();
			_shape.graphics.setStrokeStyle(strokeWidth);
			_shape.graphics.beginStroke(strokeColour);
			_shape.graphics.beginFill(backgroundColour);
			_shape.graphics.drawRoundRect(0, 0, width, height, 5);
			_this.addChild(_shape);

			_text = new createjs.Text(text, font, textColour);
			_text.textAlign = "center";
			_text.textBaseline = "middle";
			_text.x = width / 2;
			_text.y = height / 2;
			_this.addChild(_text);

			_this.mouseEnabled = true;
			_this.hitArea = _shape;
			_this.cursor = "pointer";

			_addEventListeners();
		};

		this.setText = function(newText) {
			_text.text = newText;
		};

		function _onClick () {
			_this.dispatchEvent(ss.SimpleTextButton.CLICKED);
		}

		function _mouseDown () {
			_shape.graphics.clear();
			_shape.graphics.setStrokeStyle(_strokeWidth);
			_shape.graphics.beginStroke(_strokeColour);
			_shape.graphics.beginFill(_selectedBackgroundColour);
			_shape.graphics.drawRoundRect(0, 0, _width, _height, 5);
		}

		function _mouseUp () {
			_shape.graphics.clear();
			_shape.graphics.setStrokeStyle(_strokeWidth);
			_shape.graphics.beginStroke(_strokeColour);
			_shape.graphics.beginFill(_bgColour);
			_shape.graphics.drawRoundRect(0, 0, _width, _height, 5);
		}

		function _addEventListeners () {
			_clickListener = _this.on("click", _onClick);
			_downListener = _this.on("mousedown", _mouseDown);
			_upListener = _this.on("pressup", _mouseUp);
		}

		function _removeEventListeners () {
			_this.off("click", _clickListener);
			_this.off("mousedown", _downListener);
			_this.off("pressup", _upListener);
		}

		/**
		 * Release the memory used by this instance.
		 */
		_this.release = function() {
			_removeEventListeners();

			_this.removeChild(_shape);
			_shape = undefined;

			_this.removeChild(_text);
			_text = undefined;

			_bgColour = undefined;
			_selectedBackgroundColour = undefined;
			_strokeWidth = undefined;
			_strokeColour = undefined;
			_width = undefined;
			_height = undefined;
			_clickListener = undefined;
			_downListener = undefined;
			_upListener = undefined;

			_this = undefined;
		};
	}

	ss.SimpleTextButton = SimpleTextButton;
	ss.SimpleTextButton.CLICKED = "SIMPLE_TEXT_BUTTON_CLICKED";

}());

