/*! weblib-ss 0.0.1 */
/* global require, ss */

/*
* class AbstractParticle
*	Defines common properties for all particles
* NOTE: We expect all particles to be a subclass of DisplayObject
*/



(function() {
	"use strict";

	/*
	* Create a new AbstractParticle
	*/
	function AbstractParticle () {

		// Number - Max lifetime of this particle (in seconds)
		this.maxLife = 0;

		// Number - Time elapsed since this particle was spawned (in seconds)
		this.curLife = 0;

		// Number - Value between 0 and 1 indicating the fraction of max lifetime this particle has existed for
		this.normalizedLife = 0;

		// Number - Velocity in X direction
		this.velX = 0;
		// Number - Velocity in Y direction
		this.velY = 0;
	}

	// Add this to the Sinking Ship namespace
	ss.AbstractParticle = AbstractParticle;

} ());

/* global require, ss*/



(function() {
	"use strict";
	/*
	* Class AbstractParticleEmitter
	*	Provides a base class for all particle emitters
	*/

	/*
	* Create a new AbstractParticleEmitter
	*/
	function AbstractParticleEmitter () { // jshint ignore:line
		var _this = this;

		// BaseSpawnShape - Object that determines the placement of newly spawned particles
		_this._spawnShape = null;

		// BaseSpawnTimer - Object that handles the timing of particle spawning
		_this._spawnTimer = null;

		// BaseParticleFactory - Object that instantiates new particle instances
		_this._particleFactory = null;

		// Array[BaseParticleUpdater] - List of updates to be performed on particles
		_this._particleUpdaters = [];

		// Array[BaseParticle] - List of particles currently in existance
		// NOTE: It would likely be more efficient to use a linked list to prevent frequent array splices on this list
		_this._particles = [];

		// [Boolean] - Wheter new particles should inherit the transform of this emitter container
		_this._inheritTransform = false;

		// Integer - Maximum number of particles that can exist at the same time
		_this._maxParticles = 0;

		// Number - Minimum lifetime for any particle
		_this._minLifetime = 0;
		// Number - Maximum lifetime for any particle
		_this._maxLifetime = 0;

		// Point - Origin to position particles relative to
		_this._particleOrigin = null;

		// Boolean - Whether the emitter is currently set to emit particles
		_this._isEmitting = false;

		// Boolean - Whether the emitter is currently paused (no emission or updates)
		// We pause the emitter to start with to ensure no updates are made before the emitter is set up
		_this._isPaused = true;

		// [Boolean] - Whether this emitter should destroy itself when all existing particles have finished
		_this._destroyWhenDone = false;

		/*
		* Initialize this particle emitter
		*/
		function _construct () {
			return _this;
		}

		/*
		* Set this particle emitter up with a set of instantiated objects
		* @param maxParticles:Integer - Maximum particles that can exist at once
		* @param minLife:Number - Minimum lifetime of a particle
		* @param maxLife:Number - Maximum lifetime of a particle
		* @param spawnShape:BaseSpawnShape - Determines location and other starting properties of particles
		* @param spawnTimer:BaseSpawnTimer - Handles the timing of when particles are spawned
		* @param updaterList:Array[BaseParticleUpdater] - List of particles updaters defining particle updates to be made
		*/
		_this.setupFromObjects = function(maxParticles, minLife, maxLife, particleFactory, spawnTimer, spawnShape, updaterList) {
			_this._maxParticles = maxParticles;
			_this._minLifetime = minLife;
			_this._maxLifetime = maxLife;

			_this._particleFactory = particleFactory;
			_this._spawnTimer = spawnTimer;
			_this._spawnShape = spawnShape;

			// Copy all updaters into a new array of updaters
			_this._particleUpdaters = updaterList.concat();

			// Start running the update loop for this emitter
			_this.resume();
		};

		/*
		* Set up this emitter using a single generic object containing configuration data
		* @param dataObj:Object - Object containing configuration data for the emitter
		*/
		_this.setupFromJson = function(/*dataObj*/) {

			throw "AbstractParticleEmitter.setupFromJson: This method is not implemented yet!";

			// TODO: Parse JSON data and create all required objects

			// Start running the update loop for this emitter
			// _this.resume();
		};

		/**
		 * Set whether or not you're emitting particles.
		 * @param {Boolean} isEmitting Whether or not to emit particles.
		 */
		_this.setIsEmitting = function(isEmitting) {
			if (isEmitting && !_this._isEmitting) {
				_this.startEmit();
			} else if (!isEmitting && _this._isEmitting) {
				_this.stopEmit();
			}
		};

		/*
		* Start emitting particles
		*/
		_this.startEmit = function(destroyWhenDone) {
			// Start the emitter if not currently running
			if (!_this._isEmitting) {
				_this._isEmitting = true;
			}

			// Reset the spawning timer to the initial state
			_this._spawnTimer.resetSpawning();

			if (ss.isDefined(destroyWhenDone)) {
				_this._destroyWhenDone = destroyWhenDone;
			}
		};

		/*
		* Stop emitting particles (existing particles will continue updating)
		* @param destroyWhenDone:[Boolean] (Optional) - Whether the emitter should destroy itself when all existing particles are gone
		*												Defaults to false
		*/
		_this.stopEmit = function(destroyWhenDone) {
			// Stop the emitter if currently running
			if (_this._isEmitting) {
				_this._isEmitting = false;
			}

			// If provided, set flag for destroying when existing particles are done
			if (ss.isDefined(destroyWhenDone)) {
				_this._destroyWhenDone = destroyWhenDone;
			}
		};

		/*
		* Check if this emitter is currently emitting
		* @return:[Boolean] - True if this emitter is set to emit, false otherwise
		*/
		_this.isEmitting = function() {
			return _this._isEmitting;
		};

		/*
		* Pause all updates for this emitter
		*	This will suspend particle emissions and particle updates
		*/
		_this.pause = function() {
			// Pause the emitter if not already paused
			if (!_this._isPaused) {
				_this._disableUpdates();
				_this._isPaused = true;
			}
		};

		/*
		* Resume all updates for this emitter
		*	This will resume particle emissions (if active) and particle updates
		*/
		_this.resume = function() {
			// Resume the emitter if currently paused
			if (_this._isPaused) {
				_this._enableUpdates();
				_this._isPaused = false;
			}
		};

		/*
		* Check if this emitter is currently paused or not
		* @return:[Boolean] - True if the emitter is currently paused, false otherwise
		*/
		_this.isPaused = function() {
			return _this._isPaused;
		};

		/*
		* Immediately destroy this emitter and any particles associated with it
		*/
		_this.destroy = function() {
			// Disable all updates for this emitter
			_this.pause();
			_this.removeAllParticles();
			// Remove all particles from the display container
			// for(var i = 0; i < _this._particles.length; i++){
			// 	_particleContainer.removeChild(_this._particles[i]);
			// }

			// Clear the particle array
			_this._particles = [];

			// TODO: Clear the particle pool
		};

		/*
		* Set whether spawned particles should be positioned according to the full transform of this emitter.
		* @param shouldInherit:[Boolean] - Whether spawned particles should be positioned according to the full transform of this emitter.
		*	If true, spawned particles will use the global location, rotation, and scale of this emitter. (more expensive)
		*	If false, spawned particles will be positioned relative to this emitter, but will ignore its rotation and scaling. (less expensive)
		*/
		_this.setParticleInheritTransform = function(shouldInherit) {
			_this._inheritTransform = shouldInherit;
		};

		/**
		 * Removes all particles from the emitter.
		 */
		_this.removeAllParticles = function() {
			var i;

			for(i = _this._particles.length - 1; i >= 0; i--) {
				_this._removeParticleAt(i);
			}
		};

		/*
		* Handles an update tick for the particle emitter
		* @param delta:[Number] - Elapsed time in seconds
		*/
		_this._handleUpdate = function(delta) {

			var numToSpawn = 0;
			var curParticle;
			// var delta = tickEvent.delta / 1000.0;
			var spawnStartIndex;
			var i;

			if (_this._spawnTimer.isComplete () ) {
				_this.stopEmit();
			}

			// Perform common updates for all particles (lifetime and velocity)
			for(i = _this._particles.length - 1; i >= 0; i--) {
				curParticle = _this._particles[i];

				// Update position of particle based on its current velocity
				curParticle.x += delta * curParticle.velX;
				curParticle.y += delta * curParticle.velY;

				// Update particle lifetime
				curParticle.curLife += delta;
				curParticle.normalizedLife = curParticle.curLife / curParticle.maxLife;

				// Check if this particle should be removed from the list
				if (curParticle.normalizedLife >= 1.0) {
					_this._removeParticleAt(i);
				}
			}

			// Perform additional updates on any exiting particles
			for(i = 0; i < _this._particleUpdaters.length; i++) {
				_this._particleUpdaters[i].updateParticles(_this._particles, delta);
			}

			// Check for newly emitted particles if currently emitting
			if (_this._isEmitting) {
				numToSpawn = _this._spawnTimer.getSpawnNumber(delta);

				// Constrain number of spawned particles to the maximum
				numToSpawn = Math.min(numToSpawn, _this._maxParticles - _this._particles.length);

				// If new particles should be spawned, spawn them and set them up
				if (numToSpawn > 0) {

					// Update the origin point for positioning particles
					// This ensures that particles will be placed properly even when adding them to another container object
					_this._updateParticleOrigin();

					spawnStartIndex = _this._particles.length;

					// Spawn the new particles
					_this._spawnParticles(numToSpawn);

					// Have updaters perform any required initialization on the new particles
					for(i = 0; i < _this._particleUpdaters.length; i++) {
						_this._particleUpdaters[i].initParticles(_this._particles, spawnStartIndex, _this._particles.length - 1);
					}
				}
			// if not emitting, check if all particles have completed
			} else {
				if (_this._destroyWhenDone && _this._particles.length <= 0) {
					_this.destroy();
				}
			}
		};

		return _construct();
	}

	ss.AbstractParticleEmitter = AbstractParticleEmitter;

} ());

/* global require, ss */


( function() {
	"use strict";

	/*
	* Class AbstractSpawnTimer
	*	Base class for those that handle the timing of particle spawning
	*/
	ss.AbstractSpawnTimer = function() {
		/*
		* Reset this spawning to its initial conditions
		*/
		this.resetSpawning = function() {
			// OVERRIDE IN SUBCLASS
		};

		/*
		* Get the number of particles that should be spawned due to the elapsed time
		* @param delta:Number - Time elapsed since last update (in seconds)
		* @return:[Number] - The number of new particles to be spawned
		*/
		this.getSpawnNumber = function(delta) { // jshint ignore:line

			// OVERRIDE IN SUBCLASS

			return 0;
		};


		/*
		* Return true if the timer is done emitting particales
		* @return:[Bool] - If timer is not emitting particales, false otherwise.
		*/
		this.isComplete = function() {
			return false;
		};
	};

} ());

/* global require, ss */




(function() {
	"use strict";
	/*
	* Class BurstSpawnTimer
	*	Spawns particles in a single (or close to single) burst
	*/

	/*
	* Create a new BurstSpawnTimer
	* @param numParticles:[Number] - Number of total particles in the burst
	* @param maxPerFrame:[Number] (Optional) - Maximum particles to be spawned each frame.
	*		This can be used to prevent slowdown from creating all particles in a single frame
	*/
	ss.BurstSpawnTimer = function(numParticles, maxPerFrame) {

		// Call base class constructor
		ss.AbstractSpawnTimer.call(this);

		this.name = "BurstSpawnTimer";
		this.exposedVariables = [ "totalParticles", "maxPerFrame" ];

		// [Number] - Total number of particles to be spawned in this burst
		this.totalParticles = numParticles;

		// [Number] - Number of remaining particles to be spawned
		var _remainingParticles = numParticles;

		// [Number] - Maximum particles to be spawned each frame
		this.maxPerFrame = ss.isUndefined(maxPerFrame) ? Number.MAX_VALUE : maxPerFrame;

		/*
		* Reset this spawning to its initial conditions
		*/
		this.resetSpawning = function() {
			_remainingParticles = this.totalParticles;
		};

		/*
		* Get the number of particles that should be spawned due to the elapsed time
		* @param delta:Number - Time elapsed since last update (in seconds)
		* @return:Number - The number of new particles to be spawned
		*/
		this.getSpawnNumber = function(delta) { // jshint ignore:line

			// If all particles have been spawned already, don't spawn any more
			if (_remainingParticles <= 0) {
				return 0;
			}

			var numParticles = Math.min(this.maxPerFrame, _remainingParticles);
			_remainingParticles -= numParticles;

			return numParticles;
		};

		/*
		* Return true if the timer is done emitting particales
		* @return:[Bool] - If timer is not emitting particales, false otherwise.
		*/
		this.isComplete = function() {
			return _remainingParticles <= 0;
		};

	};

} ());

/* global require, ss */



( function() {
	"use strict";
	/*
	* Class UniformSpawnTimer
	*	Timer for spawning particles at a uniform rate over time
	*/

	/*
	* Create a new UniformSpawnTimer
	* @param rate:Number - Rate to spawn particles (particles / second)
	*/
	ss.UniformSpawnTimer = function(rate)  {

		// Call base class constructor
		ss.AbstractSpawnTimer.call(this);

		var _this = this;
		this.name = "UniformSpawnTimer";
		this.exposedVariables = [ "spawnRate" ];

		// Number - How many particles to spawn per second
		this.spawnRate = 0;

		// Number - Interval between particles spawns (in seconds)
		var _spawnInterval = 0;

		// Number - Counts the time since the previous particle was spawned
		var _nextSpawnCounter = 0;

		var _oldSpawnRate = this.spawnRate;

		/*
		* Initialize this spawn timer
		*/
		function _construct (rate) {
			_this.setSpawnRate(rate);

			return _this;
		}

		/*
		* Set the rate to spawn particles at
		* @param rate:Number - Rate to spawn particles (particles / second)
		*/
		_this.setSpawnRate = function(rate) {

			// Throw error if spawn rate is invalid
			if (rate <= 0) {
				throw("ERROR: UniformSpawnTimer.setSpawnRate: Invalid spawn rate: " + rate);
			}

			this.spawnRate = rate;
			_spawnInterval = 1 / this.spawnRate;
			_oldSpawnRate = this.spawnRate;
		};

		/*
		* Get the number of particles that should be spawned due to the elapsed time
		* @param delta:Number - Time elapsed since last update (in seconds)
		* @return:Number - The number of new particles to be spawned
		*/
		_this.getSpawnNumber = function(delta) {
			if (_oldSpawnRate != this.spawnRate) {
				_this.setSpawnRate(this.spawnRate);
			}
			var numSpawn = 0;

			_nextSpawnCounter += delta;

			// If enough time has elapsed to spawn new particles, calculate how many should be spawned
			if (_nextSpawnCounter >= _spawnInterval) {
				numSpawn = Math.floor(_nextSpawnCounter / _spawnInterval);
				_nextSpawnCounter -= numSpawn * _spawnInterval;
			}

			return numSpawn;
		};

		return _construct(rate);

	};

}());

/* global require, ss */


( function() {
	"use strict";

	/*
	* Class AbstractSpawnShape
	*	Provides an interface for determining position of newly spawned shapes
	*/

	ss.AbstractSpawnShape = function() {
		// Number - Origin point of this spawn shape on the X axis
		this.originX = 0;
		// Number - Origin point of this spawn shape on the Y axis
		this.originY = 0;

		/*
		* Set the spawn position of a particle
		*/
		this.setSpawnPosition = function(particle) { // jshint ignore:line
			// OVERRIDE IN SUBCLASS
		};
	};

} ());

/* global require, ss */






( function() {
	"use strict";

	/*
	* Class LineSpawnShape extends AbstractSpawnShape
	*	Spawns particles evenly spaced along a line
	*/

	/*
	* Create a new LineSpawnShape
	* @param x1:[Number] - X coordinate of the first line point
	* @param y1:[Number] - Y coordinate of the first line point
	* @param x2:[Number] - X coordinate of the last line point
	* @param y2:[Number] - Y coordinate of the last line point
	* @param minSpeed:[Number] [Optional] - Minimum speed to initialize particles with (in pixels/second).  Velocity will be directed perpendicular to the line.
	* @param maxSpeed:[Number] [Optional] - Maximum speed to initialize particles with (in pixels/second).  Velocity will be directed perpendicular to the line.
	* @param isBidirectional:[Boolean] - Whether particles should spawn on both sides of the line
	*/
	ss.LineSpawnShape = function(x1, y1, x2, y2, minSpeed, maxSpeed, isBiDirectional) {

		// Call base constructor
		ss.AbstractSpawnShape.call(this);
		this.name = "LineSpawnShape";
		this.exposedVariables = [ "minSpeed", "maxSpeed", "x1", "y1", "x2", "y2" ];

		// [Number] - Minimum speed to spawn particles with
		this.minSpeed = ss.isUndefined(minSpeed) ? 0 : minSpeed;
		// [Number] - Maximum speed to spawn particles with
		this.maxSpeed = ss.isUndefined(maxSpeed) ? 0 : maxSpeed;

		// [Number] - X coordinate of the first line point
		this.x1 = x1;
		// [Number] - Y coordinate of the first line point
		this.y1 = y1;
		// [Number] - X coordinate of the last line point
		this.x2 = x2;
		// [Number] - Y coordinate of the last line point
		this.y2 = y2;

		// [Boolean] - Whether particles should spawn on both sides of the line
		var _isBiDirectional = ss.isDefined(isBiDirectional) ? isBiDirectional : false;

		/*
		* Set the spawn position of a particle
		* @param particle:AbstractParticle - Particle whose position should be set
		*/
		this.setSpawnPosition = function(particle) {
			var randNum = Math.random();
			var randSpeed = this.minSpeed + Math.random() * (this.maxSpeed - this.minSpeed);
			var newVel;
			var mag;

			// Set particle position
			particle.x = this.x1 + randNum * (this.x2 - this.x1);
			particle.y = this.y1 + randNum * (this.y2 - this.y1);

			// Calculate velocity to be perpendicular to the line
			newVel = new ss.Vector2((this.y2 - this.y1), -(this.x2 - this.x1));
			mag = newVel.magnitude();
			newVel.x *= (randSpeed / mag);
			newVel.y *= (randSpeed / mag);

			// If bi-directional, choose the side of the line the velocity should be on
			if (_isBiDirectional) {
				if (Math.floor(Math.random() * 2) == 1) {
					newVel.x *= -1;
					newVel.y *= -1;
				}
			}

			particle.velX = newVel.x;
			particle.velY = newVel.y;
		};

		/*
		* Update the line along which particles should be spawned
		* @param x1:[Number] - X coordinate of the first line point
		* @param y1:[Number] - Y coordinate of the first line point
		* @param x2:[Number] - X coordinate of the last line point
		* @param y2:[Number] - Y coordinate of the last line point
		*/
		this.updateSpawnLine = function(x1, y1, x2, y2) {
			this.x1 = x1;
			this.y1 = y1;
			this.x2 = x2;
			this.y2 = y2;
		};

	};

	// Extend AbstractSpawnShape
	ss.LineSpawnShape.prototype = new ss.AbstractSpawnShape();
	ss.LineSpawnShape.prototype.constructor = ss.LineSpawnShape;
} ());

/* global require, ss*/






(function() {
	"use strict";

	/*
	* Class RadialSpawnShape extends AbstractSpawnShape
	*	Spawns particles within a circle of a given radius, setting the direction of their velocity away from the center
	*/

	/*
	* Create a new RadialSpawnShape
	* @param center:Vector2 - Centerpoint for the radial spawn
	* @param radius:Number [Optional] - Maximum distance from the center that particles may spawn
	* @param minSpeed:Number [Optional] - Minimum speed to initialize particles with (in pixels/second)
	* @param maxSpeed:Number [Optional] - Maximum speed to initialize particles with (in pixels/second)
	* @param minAngle:Number [Optional] - Minimum angle around a circle that particles can spawn (in degrees)
	* @param maxAngle:Number [Optional] - Maximum angle around a circle that particles can spawn (in degrees)
	*/
	ss.RadialSpawnShape = function(center, radius, minSpeed, maxSpeed, minAngle, maxAngle) {
		// Call base constructor
		ss.AbstractSpawnShape.call(this);
		this.name = "RadialSpawnShape";
		this.exposedVariables = [ "radius", "minSpeed", "maxSpeed", "minAngleRad", "maxAngleRad", "center" ];

		// Vector2 - Centerpoint of the spawning
		this.center = center;
		// Number - Distance from  the center that particles can spawn
		this.radius = ss.isUndefined(radius) ? 0 : radius;

		// [Number] - Minimum speed to spawn particles with
		this.minSpeed = ss.isUndefined(minSpeed) ? 0 : minSpeed;
		// [Number] - Maximum speed to spawn particles with
		this.maxSpeed = ss.isUndefined(maxSpeed) ? 0 : maxSpeed;

		// Number - Minimum angle around a circle that particles can spawn (in radians)
		this.minAngleRad = ss.isUndefined(minAngle) ? 0 : minAngle * ss.MathConst.DEG_TO_RAD;
		// Number - Maximum angle around a circle that particles can spawn (in radians)
		this.maxAngleRad = ss.isUndefined(maxAngle) ? ss.MathConst.TWO_PI : maxAngle * ss.MathConst.DEG_TO_RAD;

		/*
		* Set the spawn position of a particle
		* @param particle:AbstractParticle - Particle whose position should be set
		*/
		this.setSpawnPosition = function(particle) {
			var randOffset = Math.random() * this.radius;
			var randAngleRad = this.minAngleRad + Math.random() * (this.maxAngleRad - this.minAngleRad);
			var randSpeed = this.minSpeed + Math.random() * (this.maxSpeed - this.minSpeed);

			// Set particle position
			particle.x = this.center.x + Math.cos(randAngleRad) * randOffset;
			particle.y = this.center.y + Math.sin(randAngleRad) * randOffset;

			// Set velocity to be a normalized vector directed outwards from the center
			particle.velX = randSpeed * Math.cos(randAngleRad);
			particle.velY = randSpeed * Math.sin(randAngleRad);
		};

	};

	// Extend AbstractSpawnShape
	ss.RadialSpawnShape.prototype = new ss.AbstractSpawnShape();
	ss.RadialSpawnShape.prototype.constructor = ss.RadialSpawnShape;
} ());

/* global require, ss */





( function() {
	"use strict";

	/*
	* Class BoxSpawnShape extends AbstractSpawnShape
	*	Spawns particles randomly scattered within the createjs rectangle area
	*/

	/*
	* Create a new BoxSpawnShape
	* @param rect:[Rectangle] - The create js rectangle which the particles spawns in
	* @param minSpeed:[Number] [Optional] - Minimum speed to initialize particles with (in pixels/second).  Velocity will be directed perpendicular to the line.
	* @param maxSpeed:[Number] [Optional] - Maximum speed to initialize particles with (in pixels/second).  Velocity will be directed perpendicular to the line.
	* @param minRotate:[Number] - The initial rotation of the particle rotation
	* @param maxRotate:[Number] - The initial rotation of the particle rotation
	*/
	ss.BoxSpawnShape = function(rect, minSpeed, maxSpeed, minRotate, maxRotate) {

		// Call base constructor
		ss.AbstractSpawnShape.call(this);
		this.name = "BoxSpawnShape";
		this.exposedVariables = [ "minSpeed", "maxSpeed", "x", "y", "w", "h" ];

		// [Number] - Minimum speed to spawn particles with
		this.minSpeed = ss.isUndefined(minSpeed) ? 0 : minSpeed;
		// [Number] - Maximum speed to spawn particles with
		this.maxSpeed = ss.isUndefined(maxSpeed) ? 0 : maxSpeed;

		this.minRotate = minRotate === undefined ? 0 : minRotate;
		this.maxRotate = maxRotate === undefined ? 0 : maxRotate;

		// [Number] - X coordinate of the top left corner
		this.x = rect.x;
		// [Number] - Y coordinate of the top left corner
		this.y = rect.y;
		// [Number] - width of the spawn area
		this.w = rect.width;
		// [Number] - height of the spawn area
		this.h = rect.height;

		/*
		* Set the spawn position of a particle
		* @param particle:AbstractParticle - Particle whose position should be set
		*/
		this.setSpawnPosition = function(particle) {

			var randAngle = Math.random() * ss.MathConst.TWO_PI;
			var randSpeed = this.minSpeed + Math.random() * (this.maxSpeed - this.minSpeed);
			var newVel;

			// Set particle position
			particle.x = this.x + Math.random() * (this.w);
			particle.y = this.y + Math.random() * (this.h);

			// Set the partivle initial rotation
			particle.rotation = this.minRotate + (Math.random() * (this.maxRotate - this.minRotate));

			// Calculate velocity in a random direction from spawn point
			newVel = new ss.Vector2( randSpeed * Math.cos(randAngle), randSpeed * Math.sin(randAngle));

			particle.velX = newVel.x;
			particle.velY = newVel.y;
		};

		/**
		 * Set the spawn rect to different values.
		 * @param  {Rect} rect the new spawn rect.
		 */
		this.resetSpawnRect = function(rect) {
			this.x = rect.x;
			this.y = rect.y;
			this.w = rect.width;
			this.h = rect.height;
		};
	};

	// Extend AbstractSpawnShape
	ss.BoxSpawnShape.prototype = new ss.AbstractSpawnShape();
	ss.BoxSpawnShape.prototype.constructor = ss.BoxSpawnShape;

} ());

/* global require, ss */







( function() {
	"use strict";
	/*
	* Class BoxSpawnShape extends AbstractSpawnShape
	*	Spawns particles randomly scattered within the createjs rectangle area
	*/

	/*
	* Create a new BoxSpawnShape
	* @param rect:[Rectangle] - The create js rectangle which the particles spawns in
	* @param minSpeed:[Number] [Optional] - Minimum speed to initialize particles with (in pixels/second).  Velocity will be directed perpendicular to the line.
	* @param maxSpeed:[Number] [Optional] - Maximum speed to initialize particles with (in pixels/second).  Velocity will be directed perpendicular to the line.
	* @param minRotate:[Number] - The initial rotation of the particle rotation
	* @param maxRotate:[Number] - The initial rotation of the particle rotation
	*/
	ss.BoxSpawnShapeWithDistribution = function(rect, minSpeed, maxSpeed, minRotate, maxRotate, xDistribution, yDistribution, flipX, flipY) {
		// Call base constructor
		ss.AbstractSpawnShape.call(this);
		this.name = "BoxSpawnShapeWithDistribution";
		this.exposedVariables = [ "minSpeed", "maxSpeed", "x", "y", "w", "h", "xDistribution", "yDistribution" ];

		// [Number] - Minimum speed to spawn particles with
		this.minSpeed = ss.isUndefined(minSpeed) ? 0 : minSpeed;
		// [Number] - Maximum speed to spawn particles with
		this.maxSpeed = ss.isUndefined(maxSpeed) ? 0 : maxSpeed;

		this.minRotate = minRotate === undefined ? 0 : minRotate;
		this.maxRotate = maxRotate === undefined ? 0 : maxRotate;

		this.xDistribution = xDistribution;
		this.yDistribution = yDistribution;

		this.flipX = flipX;
		this.flipY = flipY;

		// [Number] - X coordinate of the top left corner
		this.x = rect.x;
		// [Number] - Y coordinate of the top left corner
		this.y = rect.y;
		// [Number] - width of the spawn area
		this.w = rect.width;
		// [Number] - height of the spawn area
		this.h = rect.height;

		/*
		* Set the spawn position of a particle
		* @param particle:AbstractParticle - Particle whose position should be set
		*/
		this.setSpawnPosition = function(particle) {

			var randAngle = Math.random() * ss.MathConst.TWO_PI;
			var randSpeed = this.minSpeed + Math.random() * (this.maxSpeed - this.minSpeed);
			var newVel;
			// Set particle position
			particle.x = this.x + ss.MathUtils.getRandomNumberWithDistribution(this.xDistribution, flipX) * (this.w);
			particle.y = this.y + ss.MathUtils.getRandomNumberWithDistribution(this.yDistribution, flipY) * (this.h);

			// Set the partivle initial rotation
			particle.rotation = this.minRotate + (Math.random() * (this.maxRotate - this.minRotate));

			// Calculate velocity in a random direction from spawn point
			newVel = new ss.Vector2( randSpeed * Math.cos(randAngle), randSpeed * Math.sin(randAngle));

			particle.velX = newVel.x;
			particle.velY = newVel.y;
		};

		/**
		 * Set the spawn rect to different values.
		 * @param  {Rect} rect the new spawn rect.
		 */
		this.resetSpawnRect = function(rect) {
			this.x = rect.x;
			this.y = rect.y;
			this.w = rect.width;
			this.h = rect.height;
		};
	};

	// Extend AbstractSpawnShape
	ss.BoxSpawnShapeWithDistribution.prototype = new ss.AbstractSpawnShape();
	ss.BoxSpawnShapeWithDistribution.prototype.constructor = ss.BoxSpawnShapeWithDistribution;

} ());

/* global require, ss */




( function() {
	"use strict";
	/*
	* class BaseParticleUpdater
	*	Base class for classes that perform updates on a list of particles
	*/
	ss.BaseParticleUpdater = function() {
		var _this = this;

		/*
		* Initializes a set of particles for use by this updater
		* @param particles:Array[AbstractParticle] - List of particles to be updated
		* @param startIndex:int - The index to start updating at
		* @param endIndex:int - The index to end updating at
		*/
		_this.initParticles = function(particles, startIndex, endIndex) { // jshint ignore:line
			// OVERRIDE IN SUBCLASS to perform initialization
		};

		/*
		* Perform updates on all particles in a list
		* @param particles:Array[AbstractParticle] - List of particles to be updated
		* @param delta:Number - Time elapsed since last update (in seconds)
		*/
		_this.updateParticles = function(particles, delta) { // jshint ignore:line
			// OVERRIDE IN SUBCLASS to perform updates
		};

		/*
		* Perform updates on all particles in a list
		* NOTE: We sanitize the start and end indices in this function, then call an internal function
		*		To perform the actual updates.
		* @param particles:Array[AbstractParticle] - List of particles to be updated
		* @param delta:Number - Time elapsed since last update (in seconds)
		* @param startIndex:int [Optional] - The index to start updating at (will start at 0 by default)
		* @param endIndex:int [Optional] - The index to end updating at (will go to end by default)
		*/
		// _this.updateParticles = function(particles, delta, startIndex, endIndex){
		// 	var start = startIndex !== undefined ? startIndex : 0;
		// 	var end = endIndex !== undefined ? endIndex : particles.length - 1;

		// 	_this._updateParticlesRanged(particles, delta, start, end);
		// }

		/*
		* [PROTECTED]
		* Perform updates on all particles in a list
		* @param particles:Array[AbstractParticle] - List of particles to be updated
		* @param delta:Number - Time elapsed since last update (in seconds)
		* @param startIndex:int - The index to start updating at
		* @param endIndex:int - The index to end updating at
		*/
		// _this._updateParticlesRanged = function(particles, delta, startIndex, endIndex){

		// 	//OVERRIDE IN SUBCLASS to perform updates

		// 	//NOTE: The start and end indices are sanitized in
		// }

	};

}());

/* global require, ss */




(function() {
	"use strict";

	/*
	* Class DragUpdater extends BaseParticleUpdater
	*	Applies drag to slow down a particle while preserving its direction
	*	The drag is dependent on the current
	*/

	/*
	* Create a new DragUpdater
	* @param drag [Number] - The magnitude of the drag coefficient to apply.  Higher number results in more drag.
	*/
	ss.DragUpdater = function(drag) {

		// Call base class constructor
		ss.BaseParticleUpdater.call(this);

		var _this = this;
		this.name = "DragUpdater";
		this.exposedVariables = [ "dragCoeficcient" ];

		// [Number] - The drag coefficient to apply.  This number should always be negative so that it works
		// in the opposite direction of the particle's movement.
		this.dragCoeficcient = -drag;

		/*
		* [PROTECTED OVERRIDE]
		* Perform updates on all particles in a list
		* @param particles:Array[AbstractParticle] - List of particles to be updated
		* @param delta:Number - Time elapsed since last update (in seconds)
		* @param startIndex:int - The index to start updating at
		* @param endIndex:int - The index to end updating at
		*/
		_this.updateParticles = function(particles, delta) {
			var curParticle;
			var startSpeed;
			var endSpeed;

			var velX;

			for(var i = 0; i < particles.length; i++) {
				curParticle = particles[i];
				velX = curParticle.velX;

				startSpeed = Math.sqrt(curParticle.velX * curParticle.velX + curParticle.velY * curParticle.velY);
				endSpeed = startSpeed / (1 - this.dragCoeficcient * delta * startSpeed);
				curParticle.velX = curParticle.velX * (endSpeed / startSpeed);
				curParticle.velY = curParticle.velY * (endSpeed / startSpeed);
			}
		};
	};

	ss.DragUpdater.prototype = new ss.BaseParticleUpdater();
	ss.DragUpdater.prototype.constructor = ss.DragUpdater;

} ());

/* global require, ss */



(function() {
	"use strict";

	/*
	* Class ForceUpdater extends BaseParticleUpdater
	*	Applies a constant force to all particles, modifying their velocity
	*/

	/*
	* Create a new ForceUpdater
	* @param accelX:Number - Acceleration in the X axis
	* @param accelY:Number - Acceleration in the Y axis
	*/
	ss.ForceUpdater = function(accelX, accelY) {

		// Call base class constructor
		ss.BaseParticleUpdater.call(this);

		var _this = this;
		this.name = "ForceUpdater";
		this.exposedVariables = [ "accelX", "accelY" ];

		// [Number] - Acceleration along the X axis (pixels/s/s)
		this.accelX = accelX;
		// [Number] - Acceleration along the Y axis (pixels/s/s)
		this.accelY = accelY;

		/*
		* [PROTECTED OVERRIDE]
		* Perform updates on all particles in a list
		* @param particles:Array[AbstractParticle] - List of particles to be updated
		* @param delta:Number - Time elapsed since last update (in seconds)
		*/
		_this.updateParticles = function(particles, delta) {

			for(var i = 0; i < particles.length; i++) {
				particles[i].velX += this.accelX * delta;
				particles[i].velY += this.accelY * delta;
			}
		};
	};

	ss.ForceUpdater.prototype = new ss.BaseParticleUpdater();
	ss.ForceUpdater.prototype.constructor = ss.ForceUpdater;

}());

/* global require, ss, createjs */




( function() {
	"use strict";

	// !! WARNING !!: The performance of this updater is currently terrible!  Please don't actually use this until performance updates have been made!

	/*
	* Class InterpolateColourUpdater extends BaseParticleUpdater
	*	Updates particles by interpolating one property from a fixed start value to a fixed end value
	*/

	/*
	* Create a new InterpolateColourUpdater
	* @param redStart:[Number] - Starting red multiplier (between 0 and 1)
	* @param greenStart:[Number] - Starting green multiplier (between 0 and 1)
	* @param blueStart:[Number] - Starting blue multiplier (between 0 and 1)
	* @param alphaStart:[Number] - Starting alpha multiplier (between 0 and 1)
	* @param redEnd:[Number] - Ending red multiplier (between 0 and 1)
	* @param greenEnd:[Number] - Ending green multiplier (between 0 and 1)
	* @param blueEnd:[Number] - Ending blue multiplier (between 0 and 1)
	* @param alphaEnd:[Number] - Ending alpha multiplier (between 0 and 1)
	* @param interpFunc:Function - Function to perform the interpolation
	*		This function should take a value between 0 and 1 and return a value between 0 and 1
	*/
	ss.InterpolateColourUpdater = function(redStart, greenStart, blueStart, alphaStart, redEnd, greenEnd, blueEnd, alphaEnd, interpFunc) {

		// Call base class constructor
		ss.BaseParticleUpdater.call(this);

		var _this = this;

		this.name = "InterpolateColourUpdater";
		this.exposedVariables = [ "r1", "g1", "b1", "a1", "r2", "g2", "b2", "a2", "func" ];

		// [Number] - Starting values for each colour component
		this.r1 = redStart;
		this.g1 = greenStart;
		this.b1 = blueStart;
		this.a1 = alphaStart;

		// [Number] - Ending values for each colour component
		this.r2 = redEnd;
		this.g2 = greenEnd;
		this.b2 = blueEnd;
		this.a2 = alphaEnd;

		// [Function] - The interpolation function to call to update the particle property.
		this.func = interpFunc;

		/*
		* Initializes a set of particles for use by this updater
		* @param particles:Array[AbstractParticle] - List of particles to be initialized.
		* @param startIndex:int - The index to start intializing at (will start at 0 by default)
		* @param endIndex:int - The index to end intializing at (will go to end by default)
		*/
		_this.initParticles = function(particles, startIndex, endIndex) {
			for(var i = startIndex; i <= endIndex; i++) {
				particles[i].filters = [ new createjs.ColorFilter(this.r1, this.g1, this.b1, this.a1, 0, 0, 0, 0) ];
				particles[i].cache(0, 0, particles[i].getBounds().width, particles[i].getBounds().height);
			}
		};

		/*
		* [PROTECTED OVERRIDE]
		* Perform updates on all particles in a list
		* @param particles:Array[AbstractParticle] - List of particles to be updated
		* @param delta:Number - Time elapsed since last update (in seconds)
		*/
		_this.updateParticles = function(particles, delta) { // jshint ignore:line
			var interpValue;
			var colFilter;
			var rangeR = (this.r2 - this.r1);
			var rangeG = (this.g2 - this.g1);
			var rangeB = (this.b2 - this.b1);
			var rangeA = (this.a2 - this.a1);
			for(var i = 0; i < particles.length; i++) {
				interpValue = this.func(particles[i].normalizedLife);
				colFilter = particles[i].filters[0];

				colFilter.redMultiplier = this.r1 + interpValue * rangeR;
				colFilter.greenMultiplier = this.g1 + interpValue * rangeG;
				colFilter.blueMultiplier = this.b1 + interpValue * rangeB;
				colFilter.alphaMultiplier = this.a1 + interpValue * rangeA;

				particles[i].updateCache();
			}
		};

	};

	ss.InterpolateColourUpdater.prototype = new ss.BaseParticleUpdater();
	ss.InterpolateColourUpdater.prototype.constructor = ss.InterpolateColourUpdater;

} ());

/* global require, ss */





(function() {
	"use strict";

	/*
	* Class InterpolatePropertyUpdater extends BaseParticleUpdater
	*	Updates particles by interpolating one property from a fixed start value to a fixed end value
	*/

	/*
	* Create a new InterpolatePropertyUpdater
	* @param propertyName:String - Name of the property to update on the particle
	* @param start:Number - Starting value
	* @param end:Number - Ending value
	* @param interpFunc:Function - Function to perform the interpolation
	*		This function should take a value between 0 and 1 and return a value between 0 and 1
	* @param subProperty:String (Optional) - An optional subproperty name to use in order to access structured properties (vectors, etc.)
	*/
	ss.InterpolatePropertyUpdater = function(propertyName, start, end, interpFunc, subProperty) {

		// Call base class constructor
		ss.BaseParticleUpdater.call(this);

		var _this = this;
		this.name = "InterpolatePropertyUpdater";
		this.exposedVariables = [ "propName", "start", "end", "interpolationFunction" ];

		// [String] The name of the property to modify
		this.propName = propertyName;
		this.subPropName = ss.isEmpty(subProperty) ? undefined : subProperty;

		// [Number] - Starting value of the property
		this.start = start;
		// [Number] - Ending  value of the property
		this.end = end;

		// [Function] - The interpolation function to call to update the particle property
		this.interpolationFunction = interpFunc;

		/*
		* [PUBLIC OVERRIDE]
		* Initializes a set of particles for use by this updater
		* @param particles:Array[AbstractParticle] - List of particles to be updated
		* @param startIndex:int - The index to start updating at
		* @param endIndex:int - The index to end updating at
		*/
		_this.initParticles = function(particles, startIndex, endIndex) {
			var i;

			// No sub-property defined
			if (this.subPropName === undefined) {
				for(i = startIndex; i <= endIndex; i++) {
					particles[i][this.propName] = this.start;
				}
			// Have a subproperty, so use it for initialization
			}else {
				for(i = startIndex; i <= endIndex; i++) {
					particles[i][this.propName][this.subPropName] = this.start;
				}
			}
		};

		/*
		* [PUBLIC OVERRIDE]
		* Perform updates on all particles in a list
		* @param particles:Array[AbstractParticle] - List of particles to be updated
		* @param delta:Number - Time elapsed since last update (in seconds)
		*/
		_this.updateParticles = function(particles, delta) { // jshint ignore:line
			var range = this.end - this.start;
			var i;

			// No sub-property defined
			if (this.subPropName === undefined) {
				for(i = 0; i < particles.length; i++) {
					// TESTING
					// console.log("Update - Start: " + this.start);
					// console.log("Update - Norm Life: " + particles[i].normalizedLife);
					// console.log("Update - Interpolated: " + this.interpolationFunction(particles[i].normalizedLife));
					// console.log("Update - Range: " + range);

					particles[i][this.propName] = this.start + this.interpolationFunction(particles[i].normalizedLife) * range;
				}
			// Have a subproperty, so use it for updates
			}else {
				for(i = 0; i < particles.length; i++) {
					particles[i][this.propName][this.subPropName] = this.start + this.interpolationFunction(particles[i].normalizedLife) * range;
				}
			}
		};

	};

} ());

/* global require, ss */





(function() {
	"use strict";

	/*
	* Class ObjectSeekUpdater extends BaseParticleUpdater
	*	Accelerates particles towards a particular object
	*/

	/*
	* Create a new ObjectSeekUpdater
	* @param obj [DisplayObject] - Display object to move towards
	* @param accel [Number] - The magnitude of the acceleration in the direction of the object
	*/
	ss.ObjectSeekUpdater = function(target, accel) {

		// Call base class constructor
		ss.BaseParticleUpdater.call(this);

		var _this = this;
		this.name = "ObjectSeekUpdater";
		this.exposedVariables = [ "accel" ];

		// [DisplayObject] - Display object to move towards
		var _target = target;

		// [Number] - The magnitude of the acceleration in the direction of the object
		this.accel = accel;

		/*
		* [PROTECTED OVERRIDE]
		* Perform updates on all particles in a list
		* @param particles:Array[AbstractParticle] - List of particles to be updated
		* @param delta:Number - Time elapsed since last update (in seconds)
		*/
		_this.updateParticles = function(particles, delta) {

			var dirVector = new ss.Vector2();
			var mag;
			var curParticle;

			for(var i = 0; i < particles.length; i++) {
				curParticle = particles[i];

				dirVector.x = _target.x - curParticle.x;
				dirVector.y = _target.y - curParticle.y;

				mag = dirVector.magnitude();

				dirVector.x *= this.accel / mag;
				dirVector.y *= this.accel / mag;

				curParticle.velX += dirVector.x * delta;
				curParticle.velY += dirVector.y * delta;
			}
		};
	};

	ss.ObjectSeekUpdater.prototype = new ss.BaseParticleUpdater();
	ss.ObjectSeekUpdater.prototype.constructor = ss.ObjectSeekUpdater;

} ());

/* global require, ss */





(function() {
	"use strict";

	/*
	* Class PropertyRateChangeUpdater extends BaseParticleUpdater
	*	Modifies a property on a particle with a constant rate of change
	*/

	/*
	* Create a new PropertyRateChangeUpdater
	* @param propertyName:[String] - Name of the property to be updated
	* @param changeRate:[Number] - Rate of change for the property (units / second)
	* @param subProperty:[String] (Optional) - An optional subproperty name to use in order to access structured properties (vectors, etc.)
	*/
	ss.PropertyRateChangeUpdater = function(propertyName, changeRate, subProperty) {
		// Call base class constructor
		ss.BaseParticleUpdater.call(this);

		var _this = this;
		this.name = "PropertyRateChangeUpdater";
		this.exposedVariables = [ "propName", "rate" ];

		// [String] - Name of the property to modify
		this.propName = propertyName;

		// [String] - An optional sub-property name
		this.subPropName = ss.isEmpty(subProperty) ? undefined : subProperty;

		// [Number] - Rate of change to apply
		this.rate = changeRate;

		/*
		* [PROTECTED OVERRIDE]
		* Perform updates on all particles in a list
		* @param particles:Array[AbstractParticle] - List of particles to be updated
		* @param delta:Number - Time elapsed since last update (in seconds)
		*/
		_this.updateParticles = function(particles, delta) {
			var i;

			if (this.subPropName === undefined) {
				for(i = 0; i < particles.length; i++) {
					particles[i][this.propName] += this.rate * delta;
				}
			} else {
				for(i = 0; i < particles.length; i++) {
					particles[i][this.propName][this.subPropName] += this.rate * delta;
				}
			}
		};
	};

	ss.PropertyRateChangeUpdater.prototype = new ss.BaseParticleUpdater();
	ss.PropertyRateChangeUpdater.prototype.constructor = ss.PropertyRateChangeUpdater;

} ());

/* global require, ss */






( function() {
	"use strict";

	/*
	* class BaseParticleUpdater
	*	Base class for classes that perform updates on a list of particles
	*/
	ss.RandomInitialRotationUpdater = function(minRotation, maxRotation) {
		ss.BaseParticleUpdater.call(this);
		var _this = this;
		this.name = "RandomInitialRotationUpdater";

		/*
		* Initializes a set of particles for use by this updater
		* @param particles:Array[AbstractParticle] - List of particles to be updated
		* @param startIndex:int - The index to start updating at
		* @param endIndex:int - The index to end updating at
		*/
		_this.initParticles = function(particles, startIndex, endIndex) {
			// OVERRIDE IN SUBCLASS to perform initialization
			var i;
			for(i = startIndex; i <= endIndex; i++) {
				particles[i].rotation = ss.MathUtils.randomRange(minRotation, maxRotation);
			}
		};

	};

	ss.RandomInitialRotationUpdater.prototype = new ss.BaseParticleUpdater();
	ss.RandomInitialRotationUpdater.prototype.constructor = ss.RandomInitialRotationUpdater;

} ());

/* global require, ss */





(function() {
	"use strict";

	/*
	* class RandomPropertyInitializer
	*	Initializes a property of the particles to a randomized range of values
	* @param property:[String] - Name of the property to be initialized
	* @param minValue:[Number] - Minimum value for the property
	* @param maxValue:[Number] - Maximum value for the property
	* @param subProperty:[String] (Optional) - An optional subproperty name to use in order to access structured properties (vectors, etc.)
	*/
	ss.RandomPropertyInitializer = function(property, minValue, maxValue, subProperty) {

		ss.BaseParticleUpdater.call(this);
		var _this = this;
		this.name = "RandomPropertyInitializer";
		this.exposedVariables = [ "property", "minValue", "maxValue" ];

		// [String] - Name of the property to be initialized
		this.property = property;
		// [String] - An optional sub-property name
		this.subPropName = ss.isEmpty(subProperty) ? undefined : subProperty;

		// [Number] - Minimum value for the property
		this.minValue = minValue;
		// [Number] - Maximum value for the property
		this.maxValue = maxValue;

		/*
		* Initializes a set of particles for use by this updater
		* @param particles:Array[AbstractParticle] - List of particles to be updated
		* @param startIndex:int - The index to start updating at
		* @param endIndex:int - The index to end updating at
		*/
		_this.initParticles = function(particles, startIndex, endIndex) {
			var i;
			var _range = this.maxValue - this.minValue;

			// No sub property, so set the property directly
			if (this.subPropName === undefined) {
				for(i = startIndex; i <= endIndex; i++) {
					particles[i][this.property] = this.minValue + Math.random() * _range;
				}
			// Sub property provided, so set the property's sub-property
			}else {
				for(i = startIndex; i <= endIndex; i++) {
					particles[i][this.property][this.subPropName] = this.minValue + Math.random() * _range;
				}
			}
		};
	};

	ss.RandomPropertyInitializer.prototype = new ss.BaseParticleUpdater();
	ss.RandomPropertyInitializer.prototype.constructor = ss.RandomPropertyInitializer;

} ());

/* global require, ss*/





(function() {
	"use strict";

	/*
	* class RandomScaleInitializer
	*	Initializes the x and y scale of a particle to the same randomized value
	* @param minScale:[Number] - Minimum scale of the particle
	* @param maxScale:[Number] - Maximum scale of the particle
	*/
	ss.RandomScaleInitializer = function(minScale, maxScale) {

		ss.BaseParticleUpdater.call(this);
		var _this = this;
		this.name = "RandomScaleInitializer";
		this.exposedVariables = [ "minScale", "maxScale" ];

		// [Number] - Minimum value for the property
		this.minScale = minScale;
		// [Number] - Maximum value for the property
		this.maxScale = maxScale;

		/*
		* Initializes a set of particles for use by this updater
		* @param particles:Array[AbstractParticle] - List of particles to be updated
		* @param startIndex:int - The index to start updating at
		* @param endIndex:int - The index to end updating at
		*/
		_this.initParticles = function(particles, startIndex, endIndex) {
			var randScale;
			var _range = this.maxScale - this.minScale;

			for(var i = startIndex; i <= endIndex; i++) {
				randScale = this.minScale + Math.random() * _range;
				particles[i].scaleX = randScale;
				particles[i].scaleY = randScale;
			}
		};
	};

	ss.RandomScaleInitializer.prototype = new ss.BaseParticleUpdater();
	ss.RandomScaleInitializer.prototype.constructor = ss.RandomScaleInitializer;

} ());

/* global require, ss */





(function() {
	"use strict";

	/*
	* Class RandomRotationUpdater extends BaseParticleUpdater
	*	Selects a random rate of rotation and updates the particles with that rate of rotation
	*/

	/*
	* Create a new PropertyRateChangeUpdater
	* @param minRotRate:[Number] - Minimum rate of rotation (in degress / second)
	* @param maxRotRate:[Number] (Optional) - Maximum rate of rotation (in degress / second)
	*/
	ss.RandomRotationUpdater = function(minRotRate, maxRotRate) {
		// Call base class constructor
		ss.BaseParticleUpdater.call(this);

		var _this = this;
		this.name = "RandomRotationUpdater";
		this.exposedVariables = [ "minRotRate", "maxRotRate" ];

		// [Number] - Minimum rate of rotation
		this.minRotRate = minRotRate;
		// [Number] - Maximum rate of rotation
		this.maxRotRate = ss.isDefined(maxRotRate) ? maxRotRate : minRotRate;

		/*
		* Initializes a set of particles for use by this updater
		* @param particles:Array[AbstractParticle] - List of particles to be updated
		* @param startIndex:int - The index to start updating at
		* @param endIndex:int - The index to end updating at
		*/
		_this.initParticles = function(particles, startIndex, endIndex) {
			var i;
			var _range = this.maxRotRate - this.minRotRate;
			for(i = startIndex; i <= endIndex; i++) {
				particles[i].rotationVel = this.minRotRate + Math.random() * _range;
			}
		};

		/*
		* [PROTECTED OVERRIDE]
		* Perform updates on all particles in a list
		* @param particles:Array[AbstractParticle] - List of particles to be updated
		* @param delta:Number - Time elapsed since last update (in seconds)
		*/
		_this.updateParticles = function(particles, delta) {
			for(var i = 0; i < particles.length; i++) {
				particles[i].rotation += particles[i].rotationVel * delta;
			}
		};
	};

	ss.RandomRotationUpdater.prototype = new ss.BaseParticleUpdater();
	ss.RandomRotationUpdater.prototype.constructor = ss.RandomRotationUpdater;

} ());

/* global require, ss */




( function() {
	"use strict";

	/*
	* Class CycleInterpolatePropertyUpdater extends BaseParticleUpdater
	*/

	/*
	* Create a new CycleInterpolatePropertyUpdater
	* @param propertyName:String - Name of the property to update on the particle
	* @param start:Number - Starting value
	* @param end:Number - Ending value
	* @param interpFunc:Function - Function to perform the interpolation
	*		This function should take a value between 0 and 1 and return a value between 0 and 1
	* @param cycleTime:Number - Time that one full cycle of updating should take (in seconds)
	* @param startOffset:Number (Optional) - How far into the cycle the updater should start (in seconds) - Defaults to 0
	* @param subProperty:String (Optional) - An optional subproperty name to use in order to access structured properties (vectors, etc.) - Defaults to no sub-property
	*/
	ss.CycleInterpolatePropertyUpdater = function(propertyName, start, end, interpFunc, cycleTime, startOffset, subProperty) {

		// Call base class constructor
		ss.BaseParticleUpdater.call(this);

		var _this = this;

		// Set this up for use with the visual editor
		_this.name = "CycleInterpolatePropertyUpdater";
		_this.exposedVariables = [ "propName", "start", "end", "interpolationFunction", "cycleTime", "startOffset", "subProperty" ];

		// [String] The name of the property to modify
		_this.propName = propertyName;

		// A subproperty name to use in order to access structured properties
		_this.subPropName = ss.isDefined(subProperty) ? subProperty : undefined;

		// [Number] - Starting value of the property
		_this.start = start;
		// [Number] - Ending  value of the property
		_this.end = end;

		// [Function] - The interpolation function to call to update the particle property
		_this.interpolationFunction = interpFunc;

		// [Number] - Time that one full cycle of updating should take (in seconds)
		_this.semiCycle = cycleTime / 2.0;
		// [Number] - How far into the cycle the updater should start (in seconds)
		_this.startOffset = ss.isDefined(startOffset) ? startOffset : 0;
		// [Number] - Normalized start offset between 0 and 1
		_this.normalizedStartOffset = startOffset / _this.semiCycle;

		// [Number] - The full range of values covered
		_this.range = _this.end - _this.start;

		/*
		* [PUBLIC OVERRIDE]
		* Initializes a set of particles for use by this updater
		* @param particles:Array[AbstractParticle] - List of particles to be updated
		* @param startIndex:int - The index to start updating at
		* @param endIndex:int - The index to end updating at
		*/
		_this.initParticles = function(particles, startIndex, endIndex) {
			var i;

			// No sub-property defined
			if (_this.subPropName === undefined) {
				for(i = startIndex; i <= endIndex; i++) {
					particles[i][_this.propName] = _this.start + _this.interpolationFunction(_this.normalizedStartOffset) * _this.range;
				}
			// Have a subproperty, so use it for initialization
			}else {
				for(i = startIndex; i <= endIndex; i++) {
					particles[i][_this.propName][_this.subPropName] = _this.start + _this.interpolationFunction(_this.normalizedStartOffset) * _this.range;
				}
			}
		};

		/*
		* [PUBLIC OVERRIDE]
		* Perform updates on all particles in a list
		* @param particles:Array[AbstractParticle] - List of particles to be updated
		* @param delta:Number - Time elapsed since last update (in seconds)
		*/
		_this.updateParticles = function(particles, delta) { // jshint ignore:line
			var i;
			var curCycleTime;

			// No sub-property defined
			if (this.subPropName === undefined) {
				for(i = 0; i < particles.length; i++) {
					// Calculate the current time within the cycle
					curCycleTime = (_this.startOffset + particles[i].curLife) % (_this.semiCycle * 2);
					if (curCycleTime >= _this.semiCycle) {
						curCycleTime = _this.semiCycle - (curCycleTime - _this.semiCycle);
					}

					// Update property based on current cycle time
					particles[i][_this.propName] = _this.start + _this.interpolationFunction(curCycleTime / _this.semiCycle) * _this.range;
				}
			// Have a subproperty, so use it for updates
			}else {
				for(i = 0; i < particles.length; i++) {
					// Calculate the current time within the cycle
					curCycleTime = (_this.startOffset + particles[i].curLife) % (_this.semiCycle * 2);
					if (curCycleTime >= _this.semiCycle) {
						curCycleTime = _this.semiCycle - (curCycleTime - _this.semiCycle);
					}

					// Update property based on current cycle time
					particles[i][_this.propName][_this.subPropName] = _this.start + _this.interpolationFunction(curCycleTime / _this.semiCycle) * _this.range;
				}
			}
		};

	};

	// Extend base classes
	ss.CycleInterpolatePropertyUpdater.prototype = new ss.BaseParticleUpdater();
	ss.CycleInterpolatePropertyUpdater.prototype.constructor = ss.CycleInterpolatePropertyUpdater;

} ());
