/*! weblib-ss 0.0.1 */
/* global require, createjs, ss */




( function() {
	"use strict";

	// Request to go to another specific scene
	SceneEvent.GOTO_SCENE = "gotoscene";
	// Scene is loaded and ready to run
	SceneEvent.LOADED = "loaded";
	// Scene execution has completed
	SceneEvent.COMPLETE = "complete";

	SceneEvent.prototype = new createjs.Event();
	SceneEvent.prototype.constructor = SceneEvent;

	/*
	* Class SceneEvent extends BaseEvent
	*	An event related to the scene system
	* @param type:[String] - The type of scene event
	* @param eventData:[Object] (Optional) - Additional data related to this event
	*/
	function SceneEvent (type, eventData) {
		createjs.Event.call(this, type);
		var _this = this;

		// [Object] - Object holding addition data for this event
		// _this.sceneData = isEmpty(sceneData) ? {} : sceneData;

		// [Object] - Additional data related to this event
		_this.eventData = ss.isDefined(eventData) ? eventData : {};

	}

	ss.SceneEvent = SceneEvent;

}());

/* global require, ss */



(function() {
	"use strict";

	/*
	* Class SceneTransitionEventData
	*	Encapsulates all data required for an event to transition to another scene
	*/

	/*
	* Create a new SceneTransitionEventData object
	* @param sceneName:[String] - Name of the scene type to transition to
	* @param sceneData:[Object] (Optional) - Data object to be passed to the newly created scene
	* @param transition:[AbstractSceneTransition] (Optional) - Specific transition to use.
	*/
	function SceneTransitionEventData (sceneName, sceneData, transition) { // jshint ignore:line

		var _this = this;

		// [String] - Name of the scene type to transition to
		_this.sceneName = sceneName;
		// [Object] - Data object to be passed to the newly created scene
		_this.sceneData = ss.isDefined(sceneData) ? sceneData : {};
		// [AbstractSceneTransition] - Specific transition to use
		_this.transition = ss.isDefined(transition) ? transition : null;
	}

	ss.SceneTransitionEventData = SceneTransitionEventData;
} ());

/* global createjs, require, ss */




(function() {
	"use strict";

	AbstractScene.prototype = new createjs.Container();
	AbstractScene.prototype.constructor = AbstractScene;

	/*
	* Class AbstractScene extends AbstractEventDispatcher
	*	Defines the interface that all scenes are expected to implement
	*/
	function AbstractScene (width, height) {

		createjs.Container.call(this);
		var _this =  this; // AbstractEventDispatcher(this);

		// [Number] - Width of this scene (in pixels)
		_this.sceneWidth = width;
		// [Number] - Height of this scene (in pixels)
		_this.sceneHeight = height;

		// [Boolean] - Whether this scene is currently paused
		_this._paused = false;

		function _construct () {
			return _this;
		}

		/*
		* Initialize this scene with data specific to it
		*/
		_this.initWithData = function(dataObj) { // jshint ignore:line

		};

		/*
		* Check if this scene is loaded and ready to be started
		* @return:[Boolean] - True if this scene is loaded, false otherwise
		*/
		_this.isLoaded = function() {
			return true;
		};

		/*
		* Have this scene load any additional content it requires
		*/
		_this.loadScene = function() {
			// OVERRIDE in subclasses to start loading additional content

			// Dispatch a loaded event right away by default
			this.dispatchEvent(new ss.SceneEvent(ss.SceneEvent.LOADED), null, null);
		};

		/*
		* Have the scene perform any necessary starting operations
		*/
		_this.startScene = function() {
		};

		/*
		* Update this scene
		* @param delta:Number - Time elapsed since last update (in seconds)
		*/
		_this.updateScene = function(delta) { // jshint ignore:line

		};

		/*
		* Pause this scene
		*/
		_this.pauseScene = function() {
			_this._paused = true;
		};

		/*
		* Resume running this scene
		*/
		_this.resumeScene = function() {
			_this._paused = false;
		};

		/*
		* Check if this scene is currently paused
		* @return:[Boolean] - True if the scene is paused, false otherwise
		*/
		_this.isPaused = function() {
			return _this._paused;
		};

		/*
		* Have this scene clean up any resources
		*/
		_this.destroyScene = function() {

		};

		return _construct();
	}

	ss.AbstractScene = AbstractScene;

} ());

/* global require, ss */




( function() {
	"use strict";
	/*
	* Abstract factory for creating scenes.
	*/
	function AbstractSceneFactory ( reference ) { // jshint ignore:line
 		// create a locally scoped copy of this.
		var _this = reference || this;

		/*
		* Create a new factory.
		*/
		function _construct () {
			return _this;
		}

		/*
		* Create a scene given its name
		* @param sceneName:String - Name of the scene to create
		* @param sceneData:Object - Data to use to set up the scene
		* @return:AbstractScene - The object to handle the actual scene
		*/
		_this.createSceneByName = function ( sceneName, sceneData ) { // jshint ignore:line
			return null;
		};

		return _construct ();
	}

	ss.AbstractSceneFactory = AbstractSceneFactory;

}());

/* global require, ss*/


( function() {
	/*
	* Class AbstractSceneManager
	*	Defines the interface for all scene managers
	*/

	/*
	* Create a new AbstractSceneManager
	* @param sceneContainer:[Container] - Container to draw scenes to
	* @param sceneFactory:[AbstractSceneFactory] - Object to use to generate new scenes
	* @param width:[Integer] - Width of the entire scene area (in pixels)
	* @param height:[Integer] - Height of the entire scene area (in pixels)
	*/
	function AbstractSceneManager (sceneContainer, sceneFactory, width, height) { // jshint ignore:line
		"use strict";

		var _this = this;

		// [Integer] - Width of the entire scene area (in pixels)
		_this._sceneWidth = width;
		// [Integer] - Height of the entire scene area (in pixels)
		_this._sceneHeight = height;

		/*
		* Start transitioning to a scene of a particular name
		* @param sceneName:String - Name of the scene to go to
		* @param dataObj:Object - Object containing setup data for the scene
		*/
		_this.gotoScene = function(sceneName, dataObj) { // jshint ignore:line
			// OVERRIDE in sub-classes
		};

		/*
		* Update this scene manager and any current scene
		* @param delta:Number - Time elapsed since last update (in seconds)
		*/
		_this.update = function(delta) { // jshint ignore:line
			// OVERRIDE in sub-classes
		};

		/*
		* Get the width of scenes being managed here
		* @return:[Number] - The width of scenes being managed here (in pixels)
		*/
		_this.getSceneWidth = function() {
			return _this._sceneWidth;
		};

		/*
		* Get the height of scenes being managed here
		* @return:[Number] - The height of scenes being managed here (in pixels)
		*/
		_this.getSceneHeight = function() {
			return _this._sceneHeight;
		};

		/*
		* Get the currently active scene
		* @return:[AbstractScene] - The currently active scene, or null if there isn't one
		*/
		_this.getCurrentScene = function() {
			// OVERRIDE in sub-classes
			return null;
		};

		/*
		* Pause the scene manager
		*/
		_this.pause = function() {
			// OVERRIDE in sub-classes
		};

		/*
		* Resume the scene manager
		*/
		_this.resume = function() {
			// OVERRIDE in sub-classes
		};

		_this.getCurrentData = function () {
			// OVERRIDE in sub-classes
		};

	}

	ss.AbstractSceneManager = AbstractSceneManager;

}());

/* global require, ss, console */







(function() {
	"use strict";

	/*
	* Class BasicSceneManager extends AbstractSceneManager
	*	Manages the current scene being shown and transitions between scenes
	*/

	// Extend AbstractSceneManager
	BasicSceneManager.prototype = new ss.AbstractSceneManager();
	BasicSceneManager.prototype.constructor = BasicSceneManager;

	function BasicSceneManager (sceneContainer, sceneFactory, width, height) {
		// Call base class constructor
		ss.AbstractSceneManager.call(this, sceneContainer, sceneFactory, width, height);

		var _this = this;

		// Container to draw all scene content to
		var _sceneContainer;

		// Factory to create scene objects with
		var _sceneFactory;

		// Integer - Width of the entire scene area in pixels
		var _sceneWidth;
		// Integer - Height of the entire scene area in pixels
		var _sceneHeight;

		// AbstractScene - Current game scene being displayed
		var _curScene;

		var _curData;

		/*
		* Create a SceneManager
		* @param sceneContainer:Container - Container to draw all scene content to
		* @param sceneFactory:AbstractSceneFactory - Object which will create the actual scene objects
		* @param width:Integer - Width of the entire scene area
		* @param height:integer - Height of the entire scene area
		*/
		function _construct (sceneContainer, sceneFactory, width, height) {
			_sceneContainer = sceneContainer;
			_sceneFactory = sceneFactory;

			_sceneWidth = width;
			_sceneHeight = height;

			_curScene = null;
		}

		/*
		* Start transitioning to a scene of a particular name
		* @param sceneName:String - Name of the scene to go to
		* @param dataObj:Object - Object containing setup data for the scene
		*/
		_this.gotoScene = function(sceneName, dataObj) {
			var newScene;

			_clearCurrentScene();

			_curData = dataObj;

			// Create the new scene and set it as the current one
			newScene = _sceneFactory.createSceneByName(sceneName, dataObj);
			_setCurrentScene(newScene);

			newScene.startScene();
		};

		_this.getCurrentData = function () {
			return _curData;
		};


		/*
		* Update this scene manager and any current scene
		* @param delta:Number - Time elapsed since last update (in seconds)
		*/
		_this.update = function(delta) {
			if (!ss.isEmpty(_curScene)) {
				_curScene.updateScene(delta);
			}
		};

		/*
		* Get the width of scenes being managed here
		* @return:[Number] - The width of scenes being managed here (in pixels)
		*/
		_this.getSceneWidth = function() {
			return _sceneWidth;
		};

		/*
		* Get the height of scenes being managed here
		* @return:[Number] - The height of scenes being managed here (in pixels)
		*/
		_this.getSceneHeight = function() {
			return _sceneHeight;
		};

		/*
		* Get the currently active scene
		* @return:[AbstractScene] - The currently active scene
		*/
		_this.getCurrentScene = function() {
			return _curScene;
		};

		/*
		* Helper function that sets the current scene
		* @param scene:AbstractScene - Scene to set at the current one
		*/
		function _setCurrentScene (scene) {
			// Clear any current scene
			_clearCurrentScene();

			_curScene = scene;
			_sceneContainer.addChild(_curScene);
			_addSceneListeners(_curScene);
		}

		/*
		* Helper function that cleans up any current scene
		*/
		function _clearCurrentScene () {
			if (!ss.isEmpty(_curScene)) {
				_removeSceneListeners(_curScene);
				_sceneContainer.removeChild(_curScene);
				_curScene.destroyScene();
				_curScene = null;
			}

		}



		/*
		* Add required listeners to a scene
		* @param scene:AbstractScene - The scene to add listeners to
		*/
		function _addSceneListeners (scene) {
			scene.addEventListener(ss.SceneEvent.GOTO_SCENE, _handleGotoScene);
		}

		/*
		* Remove required listeners from a scene
		* @param scene:AbstractScene - The scene to remove listeners from
		*/
		function _removeSceneListeners (scene) {
			scene.removeEventListener(ss.SceneEvent.GOTO_SCENE, _handleGotoScene);
		}

		/*
		* Handle a request to go to a new scene
		* @param sceneEvent:SceneEvent - Object containing data related to the event
		*/
		function _handleGotoScene (sceneEvent) {
			var targetSceneName = sceneEvent.eventData.sceneName;

			// Throw an error if no scene name was defined
			if (ss.isUndefined(targetSceneName)) {
				console.log("!! WARNING !! Received GOTO_SCENE event but no scene name was provided in the event data!");
				throw "!! ERROR !! Received GOTO_SCENE event but no scene name was provided in the event data!";
			}

			_this.gotoScene(targetSceneName, sceneEvent.eventData.sceneData);
		}

		return _construct(sceneContainer, sceneFactory, width, height);

	}

	ss.BasicSceneManager = BasicSceneManager;

}());

/* global require, createjs, ss */


(function() {
	"use strict";

	/*
	* Class AbstractSceneTransition extends EventDispatcher
	*	Defines the interface for a scene transition
	*/

	/*
	* Create a new AbstractSceneTransition
	*/
	function AbstractSceneTransition () {

		// Call base class constructor
		createjs.EventDispatcher.call(this);

		var _this = this;

		// [Boolean] - Whether this transition is currently paused
		_this._paused = false;

		/*
		* Start executing a transition from one scene to another
		* @param curScene:AbstractScene - Current scene to be transitioned out
		* @param nextScene:AbstractScene - Scene to transition to
		*/
		_this.startTransition = function(curScene, nextScene, sceneContainer) { // jshint ignore:line
			// OVERRIDE in sub-classes

			// Dispatch a complete event immediately
			_this.dispatchEvent(new createjs.Event(AbstractSceneTransition.COMPLETE));
		};

		/*
		* Pause this transition
		*/
		_this.pauseTransition = function() {
			_this._paused = true;
		};


		/*
		* Resume this transition
		*/
		_this.resumeTransition = function() {
			_this._paused = false;
		};

		/*
		* Update this transition
		* @param delta:Number - Time elapsed since last update (in seconds)
		*/
		_this.update = function(delta) { // jshint ignore:line
			// OVERRIDE in sub-classes
		};

		/*
		* Destroy this transition and clean up any references
		*/
		_this.destroy = function() {
			_this.removeAllEventListeners();
		};
	}
	// Event type fired on completion of a scene transition
	AbstractSceneTransition.COMPLETE = "complete";

	// Extend EventDispatcher
	AbstractSceneTransition.prototype = new createjs.EventDispatcher();
	AbstractSceneTransition.prototype.constructor = AbstractSceneTransition;

	ss.AbstractSceneTransition = AbstractSceneTransition;
}());

/* global require, createjs, ss */





( function() {
	"use strict";

	/*
	* Class FadeColourTransition extends AbstractSceneTransition
	*	Fades the current scene to a solid colour, then fades the next scene in from that colour
	*/

	FadeColourTransition.prototype = new ss.AbstractSceneTransition();
	FadeColourTransition.prototype.constructor = FadeColourTransition;

	/*
	* Create a new FadeColourTransition
	* @param fadeTime:[Number] - How long the fade out and fade in should each last (in seconds)
	* @param fadeColour:[String] - String representing the colour to fade out to / in from
	*/
	function FadeColourTransition (fadeTime, fadeColour) {

		// Call base class constructor
		ss.AbstractSceneTransition.call(this);

		var _this = this;
		var _super = {};

		// [Number] = How long the fade out and fade in should each last (in seconds)
		var _fadeTime = ss.isUndefined(fadeTime) ? 0 : fadeTime;

		// [String] - String representing the colour to fade out to / in from
		var _fadeColour = ss.isUndefined(fadeColour) ? "#000000" : fadeColour;

		// [AbstractScene] - The scene to transition out of
		var _curScene;
		// [AbstractScene] - The scene to transition into
		var _nextScene;

		// [Container] - Container to add/remove scenes to/from
		var _sceneContainer;

		// [DisplayObject] - The overlay to use for fading
		var _overlay;

		// [Tween] - Tween managing the fade operation
		var _fadeTween;

		/*
		* Start executing a transition from one scene to another
		* @param curScene:AbstractScene - Current scene to be transitioned out
		* @param nextScene:AbstractScene - Scene to transition to
		*/
		_this.startTransition = function(curScene, nextScene, sceneContainer) {
			_curScene = curScene;
			_nextScene = nextScene;
			_sceneContainer = sceneContainer;

			// Create the overlay
			_overlay = new createjs.Shape();
			_overlay.graphics.beginFill(_fadeColour);
			_overlay.graphics.drawRect(0, 0, _nextScene.sceneWidth, _nextScene.sceneHeight);
			_overlay.graphics.endFill();
			_overlay.cache(0, 0, _nextScene.sceneWidth, _nextScene.sceneHeight);

			// Start fading in right away if there is no current scene
			if (_curScene === null) {
				_startFadeIn();
			// Otherwise, start fading the current scene out first
			} else {
				_startFadeOut();
			}
		};

		/*
		* OVERRIDE
		* Pause this transition
		*/
		_super.pauseTransition = _this.pauseTransition;
		_this.pauseTransition = function() {
			// Do nothing if already paused
			if (_this._paused) {
				return;
			}

			_super.pauseTransition();

			// Pause the fade tween
			if (!ss.isEmpty(_fadeTween)) {
				_fadeTween.setPaused(true);
			}
		};


		/*
		* Resume this transition
		*/
		_super.resumeTransition = _this.resumeTransition;
		_this.resumeTransition = function() {
			// Do nothing if not paused
			if (!_this._paused) {
				return;
			}

			_super.resumeTransition();

			// Resume the fade tween
			if (!ss.isEmpty(_fadeTween)) {
				_fadeTween.setPaused(false);
			}
		};

		/*
		* Destroy this transition
		*/
		_super.destroy = _this.destroy;
		_this.destroy = function() {
			// Remove references to the scene container
			if (!ss.isEmpty(_sceneContainer)) {
				_sceneContainer.removeChild(_overlay);
			}
			_sceneContainer = undefined;

			// Clean up the overlay
			if (!ss.isEmpty(_overlay )) {
				createjs.Tween.removeTweens(_overlay);
			}
			_overlay = undefined;
			_fadeTween = undefined;

			_curScene = undefined;
			_nextScene = undefined;

			_super.destroy();
		};

		/*
		* Start fading the current scene out
		*/
		function _startFadeOut () {
			// Fade the overlay in on top of the current scene
			_overlay.alpha = 0;
			_sceneContainer.addChild(_overlay);
			_fadeTween = createjs.Tween.get(_overlay);
			_fadeTween.to({ alpha : 1 }, _fadeTime * 1000);
			_fadeTween.call(_handleFadeOutComplete);
		}

		/*
		* Handle completion of the current scene fading out
		*/
		function _handleFadeOutComplete (tweenEvent) { // jshint ignore:line
			createjs.Tween.removeTweens(_overlay);

			// Remove the current scene and and overlay
			_sceneContainer.removeChild(_curScene);
			_sceneContainer.removeChild(_overlay);

			// Start fading in the next scene
			_startFadeIn();
		}

		/*
		* Start fading in the next scene
		*/
		function _startFadeIn () {
			_sceneContainer.addChild(_nextScene);

			// Fade the overlay out
			_overlay.alpha = 1;
			_sceneContainer.addChild(_overlay);
			_fadeTween = createjs.Tween.get(_overlay);
			_fadeTween.to({ alpha : 0 }, _fadeTime * 1000);
			_fadeTween.call(_handleFadeInComplete);
		}

		/*
		* Handle completion of the next scene fading in
		*/
		function _handleFadeInComplete (tweenEvent) { // jshint ignore:line
			createjs.Tween.removeTweens(_overlay);
			_sceneContainer.removeChild(_overlay);

			// Inform others of the completion of the transition
			_this.dispatchEvent(new createjs.Event(ss.AbstractSceneTransition.COMPLETE));
		}
	}

	ss.FadeColourTransition = FadeColourTransition;
} ());

/* global require, createjs, ss*/





( function() {
	"use strict";

	/*
	* Class InstantTransition extends AbstractSceneTransition
	*	Transitions to the next scene immediately with no effects
	*/

	// Extend EventDispatcher
	InstantTransition.prototype = new ss.AbstractSceneTransition();
	InstantTransition.prototype.constructor = InstantTransition;

	/*
	* Create a new InstantTransition
	*/
	function InstantTransition () {

		// Call base class constructor
		ss.AbstractSceneTransition.call(this);

		var _this = this;

		/*
		* OVERRIDE
		* Start executing a transition from one scene to another
		* @param curScene:AbstractScene - Current scene to be transitioned out
		*		NOTE: If curScene is null, the next scene will be transitioned in right away
		* @param nextScene:AbstractScene - Scene to transition to
		*/
		_this.startTransition = function(curScene, nextScene, sceneContainer) {

			// If there is a current scene, remove it
			if (curScene !== null) {
				sceneContainer.removeChild(curScene);
			}

			// Add the next scene and dispatch a complete event
			sceneContainer.addChild(nextScene);
			_this.dispatchEvent(new createjs.Event(ss.AbstractSceneTransition.COMPLETE));
		};

		/*
		* Update this transition
		* @param delta:Number - Time elapsed since last update (in seconds)
		*/
		_this.update = function(delta) { // jshint ignore:line
			// OVERRIDE in sub-classes
		};

		// _super.destroy = _this.destroy;
		// _this.destroy = function(){

		// 	_super.destroy();
		// }
	}

	ss.InstantTransition = InstantTransition;
}());

/* global require, createjs, console, ss */







(function() {
	"use strict";

	/*
	* Class PanTransition extends AbstractSceneTransition
	*	Transitions to the next scene by panning the current one out and simultaneously panning the next one in
	*/

	// Extend EventDispatcher
	PanTransition.prototype = new ss.AbstractSceneTransition();
	PanTransition.prototype.constructor = PanTransition;

	// Possible directions for panning
	PanTransition.LEFT = "left";
	PanTransition.RIGHT = "right";
	PanTransition.UP = "up";
	PanTransition.DOWN = "down";

	/*
	* Create a new PanTransition
	* @param panTime:[Number] - Duration of the pan effect (in seconds)
	* @param panDirection:[String] (Optional) - An enumerated value indicating the direction to pan (Defaults to LEFT)
	* @param interpolationFunc:[Function] (Optional) - An interpolation function for the panning (Defaults to linear)
	* @param panSound:[String] (Optional) - Name of a sound effect to play
	*/
	function PanTransition (panTime, panDirection, interpolationFunc, panSound) {

		// Call base class constructor
		ss.AbstractSceneTransition.call(this);

		var _this = this;
		var _super = {};

		// [Number] - Duration of the pan effect (in seconds)
		var _panTime = panTime;

		// [String] - Enumerated value for the direction of panning
		var _panDirection = ss.isEmpty(panDirection) ? PanTransition.LEFT : panDirection;

		// [Function] - An interpolation function for the panning
		var _interpFunc = ss.isEmpty(interpolationFunc) ? createjs.Ease.linear : interpolationFunc;

		// [String] - Name of a sound to play when performing the pan effect
		var _panSoundName = ss.isEmpty(panSound) ? null : panSound;

		// [AbstractScene] - The currently displayed scene
		var _curScene = null;
		// [AbstractScene] - The scene to be displayed next
		var _nextScene = null;

		// [Container] - The container which scenes are displayed in
		var _sceneContainer = null;

		// [Tween] - Tween moving the current scene
		var _curSceneTween = null;
		// [Tween] - Tween moving the next scene
		var _nextSceneTween = null;

		// [Vector2] - Ve
		// var _panVelocity;

		/*
		* OVERRIDE
		* Start executing a transition from one scene to another
		* @param curScene:AbstractScene - Current scene to be transitioned out
		*		NOTE: If curScene is null, the next scene will be transitioned in right away
		* @param nextScene:AbstractScene - Scene to transition to
		*/
		_this.startTransition = function(curScene, nextScene, sceneContainer) {
			var curTarget;

			_curScene = curScene;
			_nextScene = nextScene;
			_sceneContainer = sceneContainer;

			// Position the next scene and current scene destination based on direction
			switch(_panDirection){
				case PanTransition.LEFT:
					_nextScene.x = _nextScene.sceneWidth;
					_nextScene.y = 0;
					curTarget = new ss.Vector2(-nextScene.sceneWidth, 0);
					break;
				case PanTransition.RIGHT:
					_nextScene.x = -_nextScene.sceneWidth;
					_nextScene.y = 0;
					curTarget = new ss.Vector2(nextScene.sceneWidth, 0);
					break;
				case PanTransition.UP:
					_nextScene.x = 0;
					_nextScene.y = _nextScene.sceneHeight;
					curTarget = new ss.Vector2(0, -_nextScene.sceneHeight);
					break;
				case PanTransition.DOWN:
					_nextScene.x = 0;
					_nextScene.y = -nextScene.sceneHeight;
					curTarget = new ss.Vector2(0, _nextScene.sceneHeight);
					break;
				default:
					console.log("!! WARNING !! - Unsupported pan direction: " + _panDirection);
					break;
			}

			// If there is a current scene, create and start a tween for it
			if (_curScene !== null) {
				_curSceneTween = createjs.Tween.get(_curScene);
				_curSceneTween.to({ x : curTarget.x, y : curTarget.y }, _panTime * 1000, _interpFunc);
			}

			// Create and start a tween for the next scene
			_sceneContainer.addChild(_nextScene);
			_nextSceneTween = createjs.Tween.get(_nextScene);
			_nextSceneTween.to({ x : 0, y : 0 }, _panTime * 1000, _interpFunc);
			_nextSceneTween.call(_handlePanComplete);

			// Play a sound effect for the panning
			if (_panSoundName !== null) {
				ss.SoundManager.playSound(_panSoundName, ss.SoundPriority.MULTI_CHANNEL_ONLY);
			}
		};

		/*
		* OVERRIDE
		* Pause this transition
		*/
		_super.pauseTransition = _this.pauseTransition;
		_this.pauseTransition = function() {
			// Do nothing if already paused
			if (_this._paused) {
				return;
			}

			_super.pauseTransition();

			if (!ss.isEmpty(_curSceneTween)) {
				_curSceneTween.setPaused(true);
			}

			if (!ss.isEmpty(_nextSceneTween)) {
				_nextSceneTween.setPaused(true);
			}

		};


		/*
		* Resume this transition
		*/
		_super.resumeTransition = _this.resumeTransition;
		_this.resumeTransition = function() {
			// Do nothing if not paused
			if (!_this._paused) {
				return;
			}

			_super.resumeTransition();

			if (_curSceneTween !== null) {
				_curSceneTween.setPaused(false);
			}

			if (_nextSceneTween !== null) {
				_nextSceneTween.setPaused(false);
			}
		};

		/*
		* Update this transition
		* @param delta:Number - Time elapsed since last update (in seconds)
		*/
		_this.update = function(delta) { // jshint ignore:line
			// OVERRIDE in sub-classes
		};

		/*
		* Destroy this transition
		*/
		_super.destroy = _this.destroy;
		_this.destroy = function() {
			if (!ss.isEmpty(_curScene)) {
				createjs.Tween.removeTweens(_curScene);
			}
			_curScene = undefined;
			_curSceneTween = undefined;

			if (!ss.isEmpty(_nextScene)) {
				createjs.Tween.removeTweens(_nextScene);
			}
			_nextScene = undefined;
			_nextSceneTween = undefined;

			_sceneContainer = undefined;
			_super.destroy();
		};

		/*
		* Handle completion of the panning operation
		*/
		function _handlePanComplete (tweenEvent) { // jshint ignore:line
			// Clean the current scene and any tweens on it
			if (_curScene !== null) {
				createjs.Tween.removeTweens(_curScene);
				_sceneContainer.removeChild(_curScene);
				_curSceneTween = null;
			}
			createjs.Tween.removeTweens(_nextScene);
			_nextSceneTween = null;

			// Dispatch a complete event
			_this.dispatchEvent(new createjs.Event(ss.AbstractSceneTransition.COMPLETE));
		}
	}

	ss.PanTransition = PanTransition;
} ());

/* global require, ss */






( function() {
	"use strict";

	/*
	* Class TransitionSceneManager extends AbstractSceneManager
	*	Manages the current scene being shown and allows the specification of transitions between scenes
	*/

	// Extend AbstractSceneManager
	TransitionSceneManager.prototype = new ss.AbstractSceneManager();
	TransitionSceneManager.prototype.constructor = TransitionSceneManager;

	/*
	* Create a new TransitionSceneManager
	* @param sceneContainer:[Container] - Container to draw scenes to
	* @param sceneFactory:[AbstractSceneFactory] - Object to use to generate new scenes
	* @param width:[Integer] - Width of the entire scene area (in pixels)
	* @param height:[Integer] - Height of the entire scene area (in pixels)
	*/
	function TransitionSceneManager (sceneContainer, sceneFactory, width, height) {
		// Call base class constructor
		ss.AbstractSceneManager.call(this, sceneContainer, sceneFactory, width, height);

		var _this = this;

		// Container to draw all scene content to
		var _sceneContainer;

		// Factory to create scene objects with
		var _sceneFactory;

		// Integer - Width of the entire scene area in pixels
		var _sceneWidth;
		// Integer - Height of the entire scene area in pixels
		var _sceneHeight;

		// [AbstractScene] - Current game scene being displayed
		var _curScene;
		// [AbstractScene] - The scene to transition to
		var _nextScene;

		var _curData;

		// [AbstractSceneTransition] - The transition to use as the default
		var _defaultTransition;

		// [AbstractSceneTransition] - The currently active transition (if any)
		var _curTransition;

		/*
		* Create a TransitionSceneManager
		* @param sceneContainer:Container - Container to draw all scene content to
		* @param sceneFactory:AbstractSceneFactory - Object which will create the actual scene objects
		* @param width:Integer - Width of the entire scene area
		* @param height:integer - Height of the entire scene area
		*/
		function _construct (sceneContainer, sceneFactory, width, height) {
			_sceneContainer = sceneContainer;
			_sceneFactory = sceneFactory;

			_sceneWidth = width;
			_sceneHeight = height;

			_curScene = null;
			_nextScene = null;

			_defaultTransition = null;
			_curTransition = null;
		}

		/*
		* Set the transition that should be used if no other transition is specified
		* @param transition:[AbstractSceneTransition] - The transition to use as the default
		*/
		_this.setDefaultTransition = function(transition) {
			_defaultTransition = transition;
		};

		/*
		* Start transitioning to a scene of a particular name
		* @param sceneName:String - Name of the scene to go to
		* @param dataObj:Object - Object containing setup data for the scene
		* @param transition:[AbstractSceneTransition] (Optional) - The transition to use between scenes instead of any set default
		* @return:[AbstractScene] - The actual scene object that will be transitioned to
		*/
		_this.gotoScene = function(sceneName, dataObj, transition) {
			// true
			// console.log("Scene Manager: gotoScene " + sceneName);

			_curTransition = ss.isEmpty(transition) ? _defaultTransition : transition;

			_curData = dataObj;

			// Remove listeners for scene events from the current scene
			if (_curScene !== null) {
				_removeSceneListeners(_curScene);
			}

			// Create the new scene
			_nextScene = _sceneFactory.createSceneByName(sceneName, dataObj);

			// If this scene is already loaded, start running the transition
			if (_nextScene.isLoaded()) {
				// true
				// console.log("Scene loaded");

				_startTransition(_curTransition);
			// If this scene isn't loaded yet, wait for it to finish loading
			}else {
				// TODO: Have the option to display a loading indicator here?

				// true
				// console.log("Scene not loaded = loading scene");

				_nextScene.addEventListener(ss.SceneEvent.LOADED, _handleNextSceneLoaded);
				_nextScene.loadScene();
			}

			return _nextScene;
		};

		_this.getCurrentData = function () {
			return _curData;
		};

		/*
		* Update this scene manager and any current scene
		* @param delta:Number - Time elapsed since last update (in seconds)
		*/
		_this.update = function(delta) {

			// true
			// console.log("Scene Manager: updating manager ");

			// If a transition is currently executing, update it
			if (_curTransition !== null) {
				_curTransition.update(delta);
				return;
			}

			// If there is a current scene, update it
			if (_curScene !== null) {
				// true
				// console.log("Scene Manager: updating current scene");
				_curScene.updateScene(delta);
			}
		};

		/*
		* Get the currently active scene
		* @return:[AbstractScene] - The currently active scene
		*/
		_this.getCurrentScene = function() {
			return _curScene;
		};

		/*
		* Pause the scene manager
		*/
		_this.pause = function() {
			if (_curScene !== null) {
				_curScene.pauseScene();
			}

			// Pause any active transition
			if (!ss.isEmpty(_curTransition)) {
				_curTransition.pauseTransition();
			}
		};

		/*
		* Resume the scene manager
		*/
		_this.resume = function() {
			if (_curScene !== null) {
				_curScene.resumeScene();
			}

			// Resume any active transition
			if (!ss.isEmpty(_curTransition)) {
				_curTransition.resumeTransition();
			}
		};

		/*
		* Handle the next scene finishing loading
		* @param sceneEvent:SceneEvent - Event object
		*/
		function _handleNextSceneLoaded (sceneEvent) { // jshint ignore:line
			// true
			// console.log("Scene manager: Handle scene loaded");

			_nextScene.removeEventListener(ss.SceneEvent.LOADED, _handleNextSceneLoaded);

			// Start the actual transition
			_startTransition(_curTransition);
		}

		/*
		* Start executing the transition that was set up
		*/
		function _startTransition (transition) {
			_curTransition = transition;

			// If there was no transition provided and no default transition, change scenes immediately
			if (_curTransition === null) {
				_setCurrentScene(_nextScene);
				_nextScene.startScene();
			// If a transition was provided, start executing it
			}else {
				_curTransition.addEventListener(ss.AbstractSceneTransition.COMPLETE, _handleTransitionComplete);
				_curTransition.startTransition(_curScene, _nextScene, _sceneContainer);
			}

		}

		/*
		* Handle completion of a scene transition
		*/
		function _handleTransitionComplete (e) { // jshint ignore:line
			// true
			// console.log("Scene Manager: Transition complete - Starting scene");

			_curTransition.removeEventListener(ss.AbstractSceneTransition.COMPLETE, _handleTransitionComplete);
			_curTransition.destroy();
			_curTransition = null;

			// Destroy and replace the current scene
			if (_curScene !== null) {
				_curScene.destroyScene();
			}
			_curScene = _nextScene;

			// Listen for events and start running the scene
			_addSceneListeners(_curScene);
			_curScene.startScene();
		}

		/*
		* Helper function that sets the current scene immediately
		* @param scene:AbstractScene - Scene to set at the current one
		*/
		function _setCurrentScene (scene) {
			// Clear any current scene
			_clearCurrentScene();

			_curScene = scene;
			_sceneContainer.addChild(_curScene);
			_addSceneListeners(_curScene);
		}

		/*
		* Helper function that cleans up any current scene
		*/
		function _clearCurrentScene () {
			if (_curScene !== null) {
				_removeSceneListeners(_curScene);
				_sceneContainer.removeChild(_curScene);
				_curScene.destroyScene();
				_curScene = null;
			}

		}

		/*
		* Add required listeners to a scene
		* @param scene:AbstractScene - The scene to add listeners to
		*/
		function _addSceneListeners (scene) {
			scene.addEventListener(ss.SceneEvent.GOTO_SCENE, _handleGotoScene);
		}

		/*
		* Remove required listeners from a scene
		* @param scene:AbstractScene - The scene to remove listeners from
		*/
		function _removeSceneListeners (scene) {
			scene.removeEventListener(ss.SceneEvent.GOTO_SCENE, _handleGotoScene);
		}

		/*
		* Handle a request to go to a new scene
		* @param sceneEvent:SceneEvent - Object containing data related to the event
		*/
		function _handleGotoScene (sceneEvent) {
			var targetSceneName = sceneEvent.eventData.sceneName;
			var sceneData = sceneEvent.eventData.sceneData;
			var transition = sceneEvent.eventData.transition;

			_this.gotoScene(targetSceneName, sceneData, transition);
		}

		return _construct(sceneContainer, sceneFactory, width, height);

	}

	ss.TransitionSceneManager = TransitionSceneManager;

} ());
